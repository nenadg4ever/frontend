import { WALLET_TRANSACTION, ACCEPT_TO_DELIVER, PICKEDUP_ORDER, DELIVER_ORDER, REFUND_ORDER } from "./actionTypes";
import { GET_SINGLE_DELIVERY_ORDER } from "../singleorder/actionTypes";
import { WALLET_TRANSACTION_URL, ACCEPT_TO_DELIVER_URL, PICKEDUP_ORDER_URL, DELIVER_ORDER_URL, DELIVER_ORDER_BY_STORE_URL, CREATE_REFUND_URL } from "../../../configs";
import Axios from "axios";

//added by mojo START
export const walletTransaction = (token, user_id, order_id, type, add_amount_description, payment_method, paypal, iban, receivername) => dispatch => {
    Axios.post(WALLET_TRANSACTION_URL, {
        token: token,
        user_id: user_id,
        order_id: order_id,
        type: type,
        add_amount_description: add_amount_description, 
        payment_method, 
        paypal, 
        iban, 
        receivername
    })
        .then(response => {
            const result = response.data;
            dispatch({ type: WALLET_TRANSACTION, payload: result });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: result });
        })
        .catch(function(error) {
            console.log(error);
        });
};
//added by mojo END

export const acceptToDeliverOrder = (token, delivery_guy_id, order_id) => dispatch => {
    Axios.post(ACCEPT_TO_DELIVER_URL, {
        token: token,
        delivery_guy_id: delivery_guy_id,
        order_id: order_id
    })
        .then(response => {
            const accepted_order = response.data;
            dispatch({ type: ACCEPT_TO_DELIVER, payload: accepted_order });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: accepted_order });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const pickupOrder = (token, order_id) => dispatch => {
    Axios.post(PICKEDUP_ORDER_URL, {
        token: token,
        order_id: order_id
    })
        .then(response => {
            const pickup_order = response.data;
            dispatch({ type: PICKEDUP_ORDER, payload: pickup_order });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: pickup_order });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const deliverOrder = (token, order_id, delivery_pin) => dispatch => {
    Axios.post(DELIVER_ORDER_URL, {
        token: token,
        order_id: order_id,
        delivery_pin: delivery_pin
    })
        .then(response => {
            const pickup_order = response.data;
            dispatch({ type: DELIVER_ORDER, payload: pickup_order });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: pickup_order });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const deliverOrderByStore = (order_id, unique_order_id) => dispatch => {
    Axios.post(DELIVER_ORDER_BY_STORE_URL, {
        order_id: order_id,
        unique_order_id: unique_order_id
    })
        .then(response => {
            const pickup_order = response.data;
            dispatch({ type: DELIVER_ORDER, payload: pickup_order });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: pickup_order });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const createRefund = (order_id, unique_order_id, refunds) => dispatch => {
    Axios.post(CREATE_REFUND_URL, {
        order_id: order_id,
        unique_order_id: unique_order_id,
        refunds: refunds
    })
        .then(response => {
            const refund_order = response.data;
            dispatch({ type: REFUND_ORDER, payload: refund_order });
            dispatch({ type: GET_SINGLE_DELIVERY_ORDER, payload: refund_order });
        })
        .catch(function(error) {
            console.log(error);
        });
};
