import { PLACE_ORDER, CART_DISCOUNT } from "./actionTypes";

const initialState = {
    checkout: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case PLACE_ORDER:
            return { ...state, checkout: action.payload };
        case CART_DISCOUNT:
            return {...state, cartDiscount: action.payload};
        default:
            return state;
    }
}
