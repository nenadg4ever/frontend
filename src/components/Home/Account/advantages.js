import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';
import { css } from './style.css';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { faMeteor } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

class PaymentMethod extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }


        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Bankkonto"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    customBg={"#1D82D2"}
                />

                <div className="pb-200" style={{background:"#F6F6F6", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto", marginTop:"-1px"}}>

                    {/* search friends */}
                    <div className="rounding">
                        <div style={{overflow:"auto", width:"100%", padding:"0 15px 20px", position:"relative"}}>
                           
                        </div>
                    </div>

                    {/* all apps */}
                    <div style={{position:"relative", background:"#fff", margin:"15px 15px", marginTop:"-100px", borderRadius:"10px", padding:"10px 5px"}}>

                        <div className="text-center" style={{fontWeight:600, fontFamily:"open sans"}}>Bankkarte digitalisieren</div>
                        <div className="text-center mb-20" style={{fontSize:"12px", color:"#999"}}>Erleben Sie schnelle und einfache Zahlungen</div>

                        <ul className="homeApps30">
                            <li onClick={() => window.location = "/transfer"}>
                                <FontAwesomeIcon icon={faShieldAlt} style={{color:"#0F8CE9"}} />
                                <div>Sicher</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = "https://lieferservice123.com"}>
                                <FontAwesomeIcon icon={faMeteor} style={{color:"#0F8CE9"}} />
                                <div>Schnell</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = ""}>
                                <FontAwesomeIcon icon={faCheckCircle} style={{color:"#0F8CE9"}} />
                                <div>Bequem</div>
                                <Ink duration="500" />
                            </li>
                        </ul>

                        <Link to="/home/account/addBankAcc"><div style={{margin:"0 10px"}}><Button style={{position:"relative", width:"100%"}}><FontAwesomeIcon icon={faPlusCircle} className="mr-1" /> Jetzt hinzufügen<Ink duration="300" /></Button></div></Link>

                        <div className="text-center mt-10" style={{fontSize:"12px"}}>Hilfe beim Digitalisieren von Bankkarten ></div>

                    </div>


                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,

});

export default connect(
    mapStateToProps,
    {}
)(PaymentMethod);
