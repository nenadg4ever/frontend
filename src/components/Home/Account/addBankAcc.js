import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { css } from './style.css';

class AddBankAcc extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Bankkonto hinzufügen"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div style={{background:"#fff", minHeight:"800px", margin: "auto", overflow:"auto"}}>

                        <div style={{padding:"10px 20px 5px"}}>

                            <div className="pt-10 mb-10" style={{fontSize:"16px", fontWeight:600, fontFamily:"open sans"}}>Kartentyp auswählen</div>
                            <label className="cardtypeCheckbox mr-5">
                                <div>Bankkonto</div>
                                <input type="radio" name="card_type" />
                                <span className="radiobtn"></span>
                                <Ink duration="300" />
                            </label>
                            <label className="cardtypeCheckbox">
                                <div>Kreditkarte</div>
                                <input type="radio" name="card_type" />
                                <span className="radiobtn"></span>
                                <Ink duration="300" />
                            </label>

                            <div className="pt-10 mb-10" style={{fontSize:"16px", fontWeight:600, fontFamily:"open sans"}}>Karteninformation</div>
                            <div>
                                {/* <span style={{position:"absolute", left:"30px", marginTop:"7px"}}>Land</span>
                                <input type="text" name="country" className="form-control mt-10" style={{paddingLeft:"60px"}} /> */}
                                <select name="country" id="country" className="form-control" style={{padding:"8px 5px"}}>
                                    <option value="AT">Österreich</option>
                                    <option value="BE">Belgien</option>
                                    <option value="CY">Zypern</option>
                                    <option value="EE">Estland</option>
                                    <option value="FI">Finnland</option>
                                    <option value="FR">Frankreich</option>
                                    <option value="DE">Deutschland</option>
                                    <option value="GR">Griechenland</option>
                                    <option value="IE">Irland</option>
                                    <option value="IT">Italien</option>
                                    <option value="LV">Lettland</option>
                                    <option value="LT">Litauen</option>
                                    <option value="LU">Luxemburg</option>
                                    <option value="MT">Malta</option>
                                    <option value="NL">Niederlande</option>
                                    <option value="PT">Portugal</option>
                                    <option value="SK">Slowakei</option>
                                    <option value="SI">Slowenien</option>
                                    <option value="ES">Spanien</option>
                                </select>
                            </div>
                            <div>
                                <input placeholder="Kontoinhaber" type="text" name="full_name" className="form-control mt-10" style={{padding:"8px 10px"}} />
                            </div>
                            <div>
                                <input placeholder="IBAN" type="text" name="iban" className="form-control mt-10" style={{padding:"8px 10px"}} />
                            </div>
                        </div>

                        <div className="m-20"><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Hinzufügen<Ink duration="300" /></Button></div>

                        <div className="text-center" style={{padding:"20px", fontSize:"11px", fontWeight:300, fontFamily:"open sans"}}><FontAwesomeIcon icon={faShieldAlt} /> Mit dem Absenden bestätige ich, dass ich die Gebühren von Paydizer, den Servicevertrag und dem <span style={{color:"#5469D4"}}>Stripe Connected Account-Vertrag</span> gelesen und verstanden habe.</div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    {}
)(AddBankAcc);
