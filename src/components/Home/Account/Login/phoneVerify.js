import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";
import { loginUser, registerUser, sendOtp, verifyOtp } from "../../../../services/user/actions";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';


class PhoneVerify extends Component {

    constructor() {
        super();
       
        this.innerRef = React.createRef();

    }

    state = {
        loading: false,
        email: "",
        phone: localStorage.getItem("fromTel"),
        password: "",
        otp: "",
        accessToken: "",
        provider: "",
        error: false,
        email_phone_already_used: false,
        invalid_otp: false,
        showResendOtp: false,
        countdownStart: false,
        countDownSeconds: 15,
        email_pass_error: false
    };

    componentDidMount() {

        const { user } = this.props;
        if (user.success) {
            return (
                <Redirect to={"/"} />
            );
        }
        
        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }

    
    componentWillReceiveProps(nextProps) {
        const { user } = this.props;
        if (user !== nextProps.user) {
            this.setState({ loading: false });
        }
        if (nextProps.user.success) {
            if (nextProps.user.data.default_address !== null) {
                const userSetAddress = {
                    lat: nextProps.user.data.default_address.latitude,
                    lng: nextProps.user.data.default_address.longitude,
                    address: nextProps.user.data.default_address.address,
                    house: nextProps.user.data.default_address.house,
                    tag: nextProps.user.data.default_address.tag
                };
                localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
            }
            // this.context.router.history.goBack();
            return (
                <Redirect to={"/"} />
            );
        }
        // if (nextProps.user.email_phone_already_used) {
        //     this.setState({ email_phone_already_used: true });
        // }
        // if (nextProps.user.otp) {
        //     this.setState({ email_phone_already_used: false, error: false });
        //     //otp sent, hide reg form and show otp form
        //     document.getElementById("loginForm").classList.add("hidden");
        //     document.getElementById("socialLoginDiv").classList.add("hidden");
        //     document.getElementById("phoneFormAfterSocialLogin").classList.add("hidden");
        //     document.getElementById("otpForm").classList.remove("hidden");

        //     //start countdown
        //     this.setState({ countdownStart: true });
        //     this.handleCountDown();
        //     this.validator.hideMessages();
        // }

        // if (nextProps.user.valid_otp) {
        //     this.setState({ invalid_otp: false, error: false, loading: true });
        //     // register user
        //     if (this.state.social_login) {
        //         this.props.loginUser(
        //             this.state.name,
        //             this.state.email,
        //             null,
        //             this.state.accessToken,
        //             this.state.phone,
        //             this.state.provider,
        //             JSON.parse(localStorage.getItem("userSetAddress"))
        //         );
        //     } else {
        //         this.props.registerUser(
        //             this.state.name,
        //             this.state.email,
        //             this.state.phone,
        //             this.state.password,
        //             JSON.parse(localStorage.getItem("userSetAddress"))
        //         );
        //     }

        //     console.log("VALID OTP, REG USER NOW");
        //     this.setState({ loading: false });
        // }

        // if (nextProps.user.valid_otp === false) {
        //     console.log("Invalid OTP");
        //     this.setState({ invalid_otp: true });
        // }

        if (!nextProps.user) {
            this.setState({ error: true });
        }

        //old user, proceed to login after social login
        // if (nextProps.user.proceed_login) {
        //     console.log("From Social : user already exists");
        //     this.props.loginUser(
        //         this.state.name,
        //         this.state.email,
        //         null,
        //         this.state.accessToken,
        //         null,
        //         this.state.provider,
        //         JSON.parse(localStorage.getItem("userSetAddress"))
        //     );
        // }

        // if (nextProps.user.enter_phone_after_social_login) {
        //     this.validator.hideMessages();
        //     document.getElementById("loginForm").classList.add("hidden");
        //     document.getElementById("socialLoginDiv").classList.add("hidden");
        //     document.getElementById("phoneFormAfterSocialLogin").classList.remove("hidden");
        //     // populate name & email
        //     console.log("ask to fill the phone number and send otp process...");
        // }

        if (nextProps.user.data === "DONOTMATCH") {
            //email and pass donot match
            this.setState({ error: false, email_pass_error: true });
        }
    }

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    handleRegister = event => {
        event.preventDefault();
        this.setState({ loading: true });
        // if (this.validator.fieldValid("phone")) {
            // if (localStorage.getItem("enSOV") === "true") {
            //     //sending email and phone, first verify if not exists, then send OTP from the server
            //     this.props.sendOtp(this.state.email, this.state.phone, null);
            // } else {
                this.props.loginUser(
                    this.state.name,
                    this.state.email,
                    null, // password
                    this.state.accessToken,
                    this.state.phone,
                    this.state.sms_code,
                    this.state.provider,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            // }
        // } else {
        //     this.setState({ loading: false });
        //     console.log("Validation Failed");
        //     this.validator.showMessages();
        // }
    };

    handleTelChange = event => {
        this.setState({ sms_code: event.target.value });
    };

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user } = this.props;
        if (user.success) {
            return (
                <Redirect to={"/"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={""}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="bg-white text-center" style={{maxWidth:"468px", margin: "auto", padding:"18px"}}>

                    <div style={{fontSize:"30px"}}>Code eingeben</div>
                    <div className="pt-10 ml-20" style={{fontWeight:300, fontFamily:"open sans"}}>Code wurde gesendet an</div>
                    <div><b>{localStorage.getItem("phoneCountryCode")} {localStorage.getItem("fromTel")}</b></div>

                    <div className="mt-20 pb-10">
                        <form onSubmit={this.handleRegister} id="phoneFormAfterSocialLogin">
                            <input type="hidden" name="phone" value={localStorage.getItem("fromTel")} />

                            <input maxLength="4" type="tel" name="sms_code" style={{border:"2px solid #ccc", borderRadius:"3px", padding:"0 10px", fontSize:"40px", width:"100%", fontWeight:600, fontFamily:"open sans"}} className="text-center" ref={this.innerRef} autocomplete="off" onChange={this.handleTelChange} />

                            <div className="mt-10"><Button type="submit" style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Weiter<Ink duration="300" /></Button></div>

                        </form>
                    </div>

                    <div className="text-center" style={{padding:"10px"}}>SMS erneut senden</div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    { loginUser, registerUser, sendOtp, verifyOtp }
)(PhoneVerify);
