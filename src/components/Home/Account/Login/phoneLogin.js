import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";
import { sendSMSCode } from "../../../../services/user/actions";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

class PhoneLogin extends Component {



    constructor() {
        super();

        if(localStorage.getItem("fromTel")){
            var fromTel = localStorage.getItem("fromTel");
        } else {
            var fromTel = "";
        }

        this.state = {
            fromTel: fromTel
        };
       
        this.innerRef = React.createRef();

    }



    componentDidMount() {

        const { user } = this.props;
        if (user.success) {
            return (
                <Redirect to={"/"} />
            );
        }
        
        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    verifyPhone = () => {

        console.log("this.state.fromTel", this.state.fromTel);
        console.log("this.state.fromTel.length", this.state.fromTel.length);

        if(this.state.fromTel){
            this.props.sendSMSCode(this.state.fromTel);

            if(this.state.fromTel == localStorage.getItem("fromTel")){
            } else {
                localStorage.setItem("fromTel", this.state.fromTel);
            }

            setTimeout(() => {
                window.location = "/home/account/login/phoneVerify"

                // const { history } = this.props;
                // this.props.history.push("/home/account/login/phoneVerify");
            }, 1500)
        }
    }

    handleTelChange = event => {
        this.setState({ fromTel: event.target.value });
    };

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user } = this.props;
        if (user.success) {
            return (
                <Redirect to={"/"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={""}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="bg-white text-center" style={{maxWidth:"468px", margin: "auto", padding:"18px"}}>

                    <div style={{fontSize:"30px"}}>Login per Telefon</div>

                    {/* enter phone number */}
                    <div className="mt-20" style={{width:"100%"}}>
                        <div style={{ position:"relative"}} className="pb-10">

                            <input type="tel" name="phone" defaultValue={localStorage.getItem("fromTel")?localStorage.getItem("fromTel"):""} style={{border:"1px solid #448AFF", borderRadius:"3px", fontSize:"16px", width:"100%", padding:"12px 10px", paddingLeft:"70px"}} ref={this.innerRef} autoComplete="off" placeholder="Telefonnummer eingeben" onChange={this.handleTelChange} />
                            
                            <span style={{fontSize:"16px", padding:"10px", position:"absolute", top:"3px", left:"5px"}}>+43 <FontAwesomeIcon icon={faCaretDown} /></span> 
                        </div>

                        <div className="mt-10"><Button onClick={() => this.verifyPhone()} style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Weiter<Ink duration="300" /></Button></div>
                    </div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    { sendSMSCode }
)(PhoneLogin);
