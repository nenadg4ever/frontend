import React, { Component, useReducer } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';

class PhoneNumber extends Component {


    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user } = this.props;

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Telefonnummer"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div style={{padding:"15px 15px 10px 18px", position:"fixed", top:"0", right:"0", zIndex:9, fontSize:"20px"}}>
                    <FontAwesomeIcon icon={faQuestionCircle} />
                    <Ink duration="500" />
                </div>

                <div className="mt-50" style={{margin: "auto", overflow:"auto"}}>

                    <div className="text-center mt-50">
                        <div className="pt-10" style={{fontSize:"16px", fontWeight:500, fontFamily:"open sans"}}>Aktuelle Handynummer</div>
                        {/* <div className="mt-10 mb-30" style={{padding:"0 20px"}}>Diese Telefonnummer wird für den Login verwendet.</div> */}
                        <div className="pt-10" style={{fontSize:"26px", fontWeight:600, fontFamily:"open sans"}}>{user.data.phone}</div>
                    </div>

                    <Link to="">
                        <div className="m-20 mt-50"><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Telefonnummer ändern<Ink duration="300" /></Button></div>
                    </Link>

                    <div className="text-center" style={{width:"100%", position:"absolute", bottom:0, padding:"20px", fontSize:"12px"}}><FontAwesomeIcon icon={faShieldAlt} /> Wir bemühen uns, Ihre Kontosicherheit zu schützen.</div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(PhoneNumber);
