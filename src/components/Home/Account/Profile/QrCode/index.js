import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import QRCode from 'qrcode.react';
import BackWithSearch from '../../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';

class Qrcode extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Qr Code"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    customBg={"#333"}
                />

                <div onClick={() => this.toogleMenu()} style={{padding:"15px 15px 10px 18px", position:"fixed", top:"0", right:"0", zIndex:9, color:"#fff", fontSize:"20px"}}>
                    <FontAwesomeIcon icon={faEllipsisH} />
                    <Ink duration="500" />
                </div>

                <div id="hamburgerMenu" style={{display:"none", fontSize:"12px", fontWeight:600, fontFamily:"open sans", zIndex:9, background:"#fff", position:"absolute", top:"50px", right:"5px", borderRadius:"3px", boxShadow:"rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px"}}>

                    <div style={{position:"absolute", marginTop:"-10px", right:"15px"}}><FontAwesomeIcon icon={faCaretUp} style={{color:"#fff", fontSize:"16px"}} /></div>

                    <div style={{padding:"10px 18px", position:"relative", borderBottom:"1px solid #eee"}}>
                        Speichern
                        <Ink duration="500" />
                    </div>
                    <div style={{padding:"10px 18px", position:"relative"}}>
                        Zum Desktop hinzufügen
                        <Ink duration="500" />
                    </div>

                </div>

                <div style={{background: "#333", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto", color:"#fff"}}>

                    {/* add bank card */}
                    {/* <div style={{textAlign:"center"}}>
                        <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Um diese Funktion nutzen zu können, müssen Sie eine Bankkarte oder eine andere Zahlungsmethode hinzufügen.</div>
                        <Button>Hinzufügen</Button>
                    </div> */}

                    <div style={{background:"#fff", margin:"0 10px", padding:"40px 15px 60px", borderRadius:"3px"}} className="mb-20">

                        <div style={{overflow:"auto", width:"100%", padding:"10px 18px 20px", position:"relative"}}>
                            <div style={{float:"left", width:"50px", background:"#eee", display:"inline-block", padding:"8px 5px 5px 12px", borderRadius:"3px", position:"absolute"}}>
                                <FontAwesomeIcon icon={faUser} style={{color:"#bbb", fontSize:"30px"}} />
                            </div>

                            <div style={{float:"right", width:"calc(100% - 65px)", color:"#666"}}>
                                <div style={{float:"left"}}>
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>{user.data.phone}</div>
                                    <div style={{fontSize:"14px", fontWeight:400, fontFamily:"open sans"}}>{user.data.name ? user.data.name:"Kein Username"}</div>
                                </div>
                            </div>
                        </div>

                        <div style={{textAlign:"center"}}><QRCode value={user.data.phone} size="100" /></div>
                        <div className="text-center mt-10" style={{fontSize:"12px", color:"#666"}}>QR Code scannen und mich als Freund speichern</div>
                    </div>


                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Qrcode);
