import React, { Component, useReducer } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

class Address extends Component {

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user } = this.props;

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Meine Adresse"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="mt-50" style={{margin: "auto", overflow:"auto"}}>

                    <div className="text-center mt-50">
                        <FontAwesomeIcon icon={faMapMarkerAlt} style={{width:"60px", height:"60px"}} />
                        <div className="pt-10" style={{fontSize:"16px", fontWeight:500, fontFamily:"open sans"}}>Keine Adresse gefunden</div>
                    </div>

                    <div className="text-center" style={{borderTop:"1px solid #eee", width:"100%", position:"absolute", bottom:0, padding:"20px", fontSize:"12px"}}>
                        <Link to="">
                            <Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Adresse hinzufügen<Ink duration="300" /></Button>
                        </Link>
                    </div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Address);
