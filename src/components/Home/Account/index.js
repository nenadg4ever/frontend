import React, { Component } from "react";

import Footer from "../Footer";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { getSingleLanguageData } from "../../../services/translations/actions";
import DelayLink from "../../helpers/delayLink";
import Ink from "react-ink";
import { Link } from "react-router-dom";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUniversity } from '@fortawesome/free-solid-svg-icons';
import { faList } from '@fortawesome/free-solid-svg-icons';
import { faEuroSign } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faCog } from '@fortawesome/free-solid-svg-icons';


class Account extends Component {

    componentDidMount() {
        const { user } = this.props;
      

        if (localStorage.getItem("state") !== null) {
            const languages = JSON.parse(localStorage.getItem("state")).languages;
            if (languages && languages.length > 0) {
                if (localStorage.getItem("multiLanguageSelection") === "true") {
                    if (localStorage.getItem("userPreferedLanguage")) {
                        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                        // console.log("Called 1");
                    } else {
                        if (languages.length) {
                            const id = languages.filter(lang => lang.is_default === 1)[0].id;
                            this.props.getSingleLanguageData(id);
                        }
                    }
                } else {
                    // console.log("Called 2");
                    if (languages.length) {
                        const id = languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            }
        }
    }

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }
        const languages = JSON.parse(localStorage.getItem("state")).languages;
        console.log(languages);


        return (
            <React.Fragment>

                    {/* set lang */}
                    {/* {localStorage.getItem("multiLanguageSelection") === "true" && (
                        languages && languages.length > 0 && (
                            <div className="mt-4 d-flex align-items-center justify-content-center">
                                <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                                <select
                                    onChange={this.handleOnChange}
                                    defaultValue={
                                        localStorage.getItem("userPreferedLanguage")
                                            ? localStorage.getItem("userPreferedLanguage")
                                            : languages.filter(lang => lang.is_default === 1)[0].id
                                    }
                                    className="form-control language-select"
                                >
                                    {languages.map(language => (
                                        <option value={language.id} key={language.id}>
                                            {language.language_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        )
                    )} */}

                    <div style={{maxWidth:"468px", margin:"auto", background:"#F6F6F6", minHeight:"800px"}}>

                        <div className="mb-10" style={{background:"#1D82D2", color:"#A3E0FD", fontSize:"18px"}}>
                            <div style={{float:"left", color:"#fff", padding:"10px 18px 0", fontWeight:600, fontFamily:"open sans"}}>Konto</div>
                            <Link to="/home/account/settings"><div style={{float:"right", color:"#fff", padding:"10px 18px 10px", fontWeight:600, fontFamily:"open sans", position:"relative"}} ><FontAwesomeIcon icon={faCog} /><Ink duration="300" /></div></Link>

                            <Link to="/home/account/profile">
                                <div style={{overflow:"auto", width:"100%", padding:"10px 18px 20px", position:"relative"}}>

                                    <div style={{float:"left", width:"50px", background:"#eee", display:"inline-block", padding:"5px 10px", borderRadius:"10px", border:"2px solid #A3E0FD", position:"absolute"}}>
                                        <FontAwesomeIcon icon={faUser} style={{color:"#bbb", fontSize:"30px"}} />
                                        <div style={{background:"rgba(0, 0, 0, 0.5)", position:"absolute", bottom:0, left:0, color:"#fff", display:"inline-block", borderBottomLeftRadius:"10px", borderBottomRightRadius:"10px", fontSize:"10px", textAlign:"center", width:"47px"}}>Avatar</div>
                                    </div>

                                    <div style={{float:"right", width:"calc(100% - 65px)"}}>
                                        <div style={{float:"left"}}>
                                            <div style={{fontWeight:600, fontFamily:"open sans"}}>{user.data.name ? user.data.name:"Kein Username"}</div>
                                            <div style={{fontSize:"14px", fontWeight:400, fontFamily:"open sans"}}>{user.data.phone}</div>
                                        </div>
                                        <div className="flex-auto text-right mt-15" style={{float:"right"}}>
                                            <i className="si si-arrow-right" style={{color:"#fff"}} />
                                        </div>
                                    </div>

                                    <Ink duration="500" />

                                </div>
                            </Link>
                        </div>

                        <div style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"5px", margin:"0 10px"}}>

                            <div className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <DelayLink to={"/home/account"} delay={200}>
                                    <div className="display-flex py-2">
                                        <div className="mr-10 border-0">
                                            <FontAwesomeIcon icon={faList} style={{color:"#EF961D", fontSize:"20px"}} />
                                        </div>
                                        <div className="flex-auto border-0">Transaktionen</div>
                                        <div className="flex-auto text-right">
                                            <i className="si si-arrow-right" />
                                        </div>
                                    </div>
                                </DelayLink>
                                <Ink duration="500" />
                            </div>

                            <Link to="/home/wallet">
                                <div className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                    <div className="display-flex py-2">
                                        <div className="mr-10 border-0">
                                            <FontAwesomeIcon icon={faEuroSign} style={{color:"#48A1DD", minWidth:"20px", fontSize:"20px"}} />
                                        </div>
                                        <div className="flex-auto border-0">Kontostand</div>
                                        <div className="flex-auto text-right">
                                            0.00 <i className="si si-arrow-right" />
                                        </div>
                                    </div>
                                    <Ink duration="500" />
                                </div>
                            </Link>

                            <Link to={"/home/account/advantages"}>
                                <div className="category-list-item" style={{padding:"10px", position:"relative"}}>
                                        <div className="display-flex py-2">
                                            <div className="mr-10 border-0">
                                                <FontAwesomeIcon icon={faUniversity} style={{color:"#EF961D", fontSize:"20px"}} />
                                            </div>
                                            <div className="flex-auto border-0">Bankkonten</div>
                                            <div className="flex-auto text-right">
                                                <i className="si si-arrow-right" />
                                            </div>
                                        </div>
                                    <Ink duration="500" />
                                </div>
                            </Link>

                        </div>

                        {/* customer center */}
                        <div className="mt-20" style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"5px", margin:"0 10px"}}>

                            <div className="category-list-item" style={{padding:"10px", position:"relative"}}>
                                <DelayLink to={"/home/account"} delay={200}>
                                    <div className="display-flex py-2">
                                        <div className="mr-10 border-0">
                                            <FontAwesomeIcon icon={faQuestionCircle} style={{color:"#48A1DD", fontSize:"20px"}} />
                                        </div>
                                        <div className="flex-auto border-0">Kundenservice</div>
                                        <div className="flex-auto text-right">
                                            <i className="si si-arrow-right" />
                                        </div>
                                    </div>
                                </DelayLink>
                                <Ink duration="500" />
                            </div>

                        </div>

                    </div>

                    <Footer active_account={true} />

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getSingleLanguageData }
)(Account);
