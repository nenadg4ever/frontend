import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

class Deposit extends Component {

    constructor() {
        super();
       
        this.innerRef = React.createRef();
    }

    componentDidMount() {
        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Aufladen"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="mb-200" style={{background:"#F6F6F6", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto"}}>

                    <div style={{background:"#fff"}}>
                        <div className="pt-10 ml-20" style={{fontWeight:300, fontFamily:"open sans"}}>Aufladebetrag</div>
                        <div style={{ position:"relative"}} className="pb-10">
                            <input type="tel" name="amount" style={{borderTopLeftRadius:"5px", borderTopRightRadius:"5px", margin:"5px 10px 0", padding:"0 10px", paddingLeft:"35px", border:"none", fontSize:"40px", width:"calc(100% - 20px)", fontWeight:600, fontFamily:"open sans"}} ref={this.innerRef} autocomplete="off" />
                            <span style={{fontSize:"30px", padding:"10px", position:"absolute", top:"5px", left:"10px", fontWeight:600, fontFamily:"open sans", color:"#000"}}>€</span> 
                        </div>
                    </div>

                    <div style={{margin:"20px 20px"}}><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Weiter<Ink duration="300" /></Button></div>
                    <div className="text-center" style={{padding:"10px", color:"blue", fontSize:"12px"}}>Tipp: Bankkarte digitalisieren und Aufladungen überspringen.</div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    {}
)(Deposit);
