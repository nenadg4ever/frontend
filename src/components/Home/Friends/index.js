import React, { Component } from "react";

import Footer from "../Footer";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { getSingleLanguageData } from "../../../services/translations/actions";
import Ink from "react-ink";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';

class Friends extends Component {

    componentDidMount() {
        const { user } = this.props;
      
        if (localStorage.getItem("state") !== null) {
            const languages = JSON.parse(localStorage.getItem("state")).languages;
            if (languages && languages.length > 0) {
                if (localStorage.getItem("multiLanguageSelection") === "true") {
                    if (localStorage.getItem("userPreferedLanguage")) {
                        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                        // console.log("Called 1");
                    } else {
                        if (languages.length) {
                            const id = languages.filter(lang => lang.is_default === 1)[0].id;
                            this.props.getSingleLanguageData(id);
                        }
                    }
                } else {
                    // console.log("Called 2");
                    if (languages.length) {
                        const id = languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            }
        }
    }

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    toogleUserPages () {
        var x = document.getElementById("contacts");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    //import contacts from android phone
    importFriends = async () => {
        try {
            // check if its supported
            const supported = ('contacts' in navigator && 'ContactsManager' in window);
            if (!supported) {
                alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 77 oder höher.");
            } else {
                // properties to get from contact.
                const props = [
                    "name",
                    "tel",
                    // "email",
                    // "addresses",
                    // "icons"
                ];
                // multiple means you can select multiple contact at a time.
                const friends = await navigator.contacts.select(props, { multiple: true });

                if(friends.length > 0){
                    // this.setState({friends})
                    // this.hideModalRider2();
                    // this.showModalRider3();
                    // this.helpFriends2(fromName, fromTel, riderAddress);
                } 
            }        
        } catch (error) {
            console.error(error);   
            alert("error: "+error)
        }
    }  
    importFriends2 = async () => {
        var div = document.getElementById('riderStatus');
        div.innerHTML = '<span style="color:green">Wird geladen...</span>';

        let friends = JSON.stringify(this.state.friends);
        // var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_riderFriends.php?riderName='+fromName+'&riderTel='+btoa(fromTel)+'&riderAddress='+riderAddress+'&friends='+friends;
        // axios
        // .get(url)
        // .then(response => {
        //     setTimeout(function () {
        //         div.innerHTML = response.data.text;
        //     }, 100);
        // })
        // .catch(function(error) {
        //     console.log(error);
        // });
    }  

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }
        const languages = JSON.parse(localStorage.getItem("state")).languages;
        console.log(languages);


        return (
            <React.Fragment>

                    {/* set lang */}
                    {/* {localStorage.getItem("multiLanguageSelection") === "true" && (
                        languages && languages.length > 0 && (
                            <div className="mt-4 d-flex align-items-center justify-content-center">
                                <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                                <select
                                    onChange={this.handleOnChange}
                                    defaultValue={
                                        localStorage.getItem("userPreferedLanguage")
                                            ? localStorage.getItem("userPreferedLanguage")
                                            : languages.filter(lang => lang.is_default === 1)[0].id
                                    }
                                    className="form-control language-select"
                                >
                                    {languages.map(language => (
                                        <option value={language.id} key={language.id}>
                                            {language.language_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        )
                    )} */}

                    <div style={{maxWidth:"468px", margin:"auto", background:"#F6F6F6", minHeight:"800px"}}>

                        <div className="mb-10" style={{background:"#1D82D2", color:"#A3E0FD", fontSize:"18px"}}>
                            <div style={{float:"left", color:"#fff", padding:"10px 18px 0", fontWeight:600, fontFamily:"open sans"}}>Freunde</div>

                            {/* add icon */}
                            <div onClick={() => this.toogleUserPages()} style={{float:"right", color:"#fff", padding:"10px 18px 10px", fontWeight:600, fontFamily:"open sans", position:"relative"}} ><FontAwesomeIcon icon={faPlusCircle} /><Ink duration="300" /></div>

                            <div id="contacts" style={{display:"none", fontSize:"12px", fontWeight:600, fontFamily:"open sans", zIndex:1, background:"rgba(0, 0, 0, 0.9)", color:"#fff", position:"absolute", top:"40px", right:"10px", borderRadius:"10px"}}>

                                <div style={{padding:"10px 18px", position:"relative"}}>
                                    <FontAwesomeIcon icon={faUserPlus} className="mr-5" />
                                    Kontakt hinzufügen
                                    <Ink duration="500" />
                                </div>
                                <div style={{padding:"10px 18px", position:"relative"}}>
                                    <FontAwesomeIcon icon={faQrcode} className="mr-10" />  
                                    Scan QR Code
                                    <Ink duration="500" />
                                </div>
                                <div style={{padding:"10px 18px", position:"relative"}}>
                                    <FontAwesomeIcon icon={faQrcode} className="mr-10" />
                                    Mein QR Code
                                    <Ink duration="500" />
                                </div>

                            </div>

                            {/* search friends */}
                            <div style={{overflow:"auto", width:"100%", padding:"10px 18px 20px", position:"relative"}}>
                                <input placeholder="Freunde suchen" type="text" style={{width:"100%", borderRadius:"5px", padding:"5px 10px", paddingLeft:"40px", border:"1px solid #eee", fontSize:"14px"}} />
                                <span style={{position:"absolute", top:15, left:30, color:"#999", fontSize:"16px"}}><FontAwesomeIcon icon={faSearch} /></span>
                            </div>
                        </div>

                        <div style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"5px", margin:"0 10px"}}>
                            {/* import contacts (android only) */}
                            <div onClick={() => this.importFriends()} className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faAddressBook} style={{color:"#EF961D", fontSize:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        Kontakte importieren
                                        <div style={{fontSize:"14px", fontWeight:"normal"}}>Vom Handy importieren</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>
                        </div>

                    </div>

                    <Footer active_friends={true} />

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getSingleLanguageData }
)(Friends);
