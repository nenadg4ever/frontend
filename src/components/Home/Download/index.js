import React, { Component } from "react";

import { Redirect } from "react-router";
import { connect } from "react-redux";
import { getSingleLanguageData } from "../../../services/translations/actions";
import { getSettings } from "../../../services/settings/actions";

import Ink from "react-ink";
import axios from "axios";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faApple } from '@fortawesome/free-brands-svg-icons';
import { faAndroid } from '@fortawesome/free-brands-svg-icons';
import { faChrome } from '@fortawesome/free-brands-svg-icons';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown, Accordion, Card } from 'react-bootstrap';

declare var INSTALL_APP_POPUP;

class Download extends Component {

    
    constructor(props) {
        super(props);
        
        this.state = { 
            installModal: false,
        };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { user } = this.props;

        if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        }
      
        if (localStorage.getItem("state") !== null) {
            const languages = JSON.parse(localStorage.getItem("state")).languages;
            if (languages && languages.length > 0) {
                if (localStorage.getItem("multiLanguageSelection") === "true") {
                    if (localStorage.getItem("userPreferedLanguage")) {
                        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                        // console.log("Called 1");
                    } else {
                        if (languages.length) {
                            const id = languages.filter(lang => lang.is_default === 1)[0].id;
                            this.props.getSingleLanguageData(id);
                        }
                    }
                } else {
                    // console.log("Called 2");
                    if (languages.length) {
                        const id = languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            }
        }
    }

    //install app
    showModalApps () {
        this.setState({
           appsModal: true
        });
    }
    hideModalApps = () => {
        this.setState({
            appsModal: false
        });
    }
    showModalInstall () {
        this.setState({
           installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }

    //android
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    // ios
    tooglePwaPrompt () {
        var x = document.getElementById("showPwaprompt");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        // if (localStorage.getItem("storeColor") === null) {
        //     return <Redirect to={"/"} />;
        // }
        const { user } = this.props;

        // if (!user.success) {
        //     return (
        //         //redirect to login page if not loggedin
        //         <Redirect to={"/login"} />
        //     );
        // }
        
        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        return (
            <React.Fragment>

                <div style={{padding:"10px 20px", margin:"auto", background:"#1D82D2", minHeight:"800px"}}>

                    <div style={{maxWidth:"468px", margin:"auto"}}>

                        <div className="mt-20 mb-50 text-center" style={{color:"#fff", fontSize:"20px"}}>
                            <div style={{fontSize:"30px", fontWeight:"bold"}}>Paydizer</div>
                            <i style={{textTransform:"uppercase", fontWeight:"bold"}}>macht das Leben einfach</i>
                        </div>
                        
                        <div style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"5px"}}>

                            <div onClick={() => window.location = "https://paydizer.com/d/paydizer.apk"} className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faAndroid} style={{color:"#82C91E", fontSize:"40px", width:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        Android (APK)
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>Chrome v72 oder höher</div>
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>Apps aus externen Quellen zulassen</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>

                            <div onClick={() => this.next("", pwaSupported)} className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faAndroid} style={{color:"#82C91E", fontSize:"40px", width:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        Android (PWA)
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>Chrome v70 oder höher</div>
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>Google Play Konto erforderlich</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>

                            <div onClick={() => this.tooglePwaPrompt()} className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faApple} style={{color:"#00AAEE", fontSize:"40px", width:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        iPhone (Safari)
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>iOS 11.3 oder höher</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>



                            <div onClick={() => window.location = "https://paydizer.com"} className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faChrome} style={{color:"#4C6EF5", fontSize:"40px", width:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        Web-version
                                        <div style={{fontSize:"13px", fontWeight:"normal"}}>Google Chrome empfohlen</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>

                            

                        </div>

                    </div>

                </div>

                {/* android pwa */}
                <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>Installieren</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            Klicke auf hinzufügen um Paydizer auf deinen Bildschirm zu installieren.
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                            Hinzufügen
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* iphone */}
                <div id="showPwaprompt" style={{display:"none"}}>
                    <PWAPrompt debug={1} 
                        copyTitle={"Installieren"} 
                        copyBody={"Diese App zum Startbildschirm hinzufügen"}
                        copyShareButtonLabel={"1) Klicken Sie auf 'Teilen'"}
                        copyAddHomeButtonLabel={"2) Zum Home-Bildschirm"}
                        copyClosePrompt={"Schließen"} 
                    />
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData }
)(Download);
