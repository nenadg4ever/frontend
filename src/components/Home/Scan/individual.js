import React, { Component } from "react";
import { Redirect } from "react-router";

import Jello from "react-reveal/Jello";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import QrReader from 'react-qr-reader'
import Ink from "react-ink";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

class Individual extends Component {

 
    constructor() {
        super();
       
        this.innerRef = React.createRef();
    }

    componentDidMount() {
        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }

     
    handleScan = data => {
        if (data) {
            this.setState({
                result: data
            })
        }
    }
    handleError = err => {
        console.error(err)
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Transfer"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div style={{position: "relative", margin: "auto", overflow:"auto"}}>

                    <div className="text-center mt-10" style={{padding:"10px"}}>
                        <div style={{width:"50px", background:"#eee", display:"inline-block", padding:"5px 10px", borderRadius:"10px", border:"2px solid #ccc"}}>
                            <FontAwesomeIcon icon={faUser} style={{color:"#bbb", fontSize:"30px"}} />
                        </div>

                        <div className="mt-10" style={{fontWeight:600, fontFamily:"open sans"}}>Transfer an <span style={{border:"1px solid red", color:"red", borderRadius:"3px", padding:"0 2px", fontSize:"10px"}}>Privat</span> MOHAMED (nicht verifiziert)</div>
                    </div>

                    <div className="mt-30 ml-20" style={{fontWeight:300, fontFamily:"open sans"}}>Überweisungsbetrag</div>
                    <div style={{ position:"relative"}}>
                        <input type="tel" name="amount" style={{borderTopLeftRadius:"5px", borderTopRightRadius:"5px", margin:"5px 10px 0", padding:"0 10px", paddingLeft:"35px", border:"none", fontSize:"40px", width:"calc(100% - 20px)", fontWeight:600, fontFamily:"open sans"}} ref={this.innerRef} autocomplete="off" />
                        <span style={{fontSize:"30px", padding:"10px", position:"absolute", top:"5px", left:"10px", fontWeight:600, fontFamily:"open sans", color:"#000"}}>€</span> 
                    </div>

                    <div style={{borderBottomLeftRadius:"5px", borderBottomRightRadius:"5px", background:"#fff", padding:0, margin:"0 10px", borderTop:"1px solid #eee", fontSize:"12px", fontFamily:"open sans", fontWeight:300}}><input type="text" name="remarks" style={{width:"100%", padding:"5px 10px", border:"none", fontSize:"12px"}}  autocomplete="off" placeholder="Kommentar hinzufügen (max. 20 Zeichen)" /></div>

                    <div style={{margin:"20px 20px"}}><Button style={{fontSize:"18px", height:"50px", width:"100%"}}>Überweisung bestätigen</Button></div>

                   <div className="text-center" style={{padding:"20px", bottom:0, color:"#999", fontSize:"12px"}}>Das Geld wird sofort auf das Konto des Empfängers eingezahlt und kann nicht zurückgefordert werden.</div>
                    
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Individual);
