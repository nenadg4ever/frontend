import React, { Component } from "react";
import { Redirect } from "react-router";

import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';

class Vendor extends Component {

 
    constructor() {
        super();
       
        this.innerRef = React.createRef();
    }

    componentDidMount() {
        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }

     
    handleScan = data => {
        if (data) {
            this.setState({
                result: data
            })
        }
    }
    handleError = err => {
        console.error(err)
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Zurück"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div style={{background:"#eee", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto"}}>

                    <div className="mt-20" style={{padding:"10px"}}>
                        <img src={'https://lieferservice123.com/assets/img/restaurants/1588881376xwPvvJuJAm.png'} style={{width: "40px", height: "40px", padding: "1px", borderRadius: "30px", border: "1px solid #ccc", marginRight:"5px"}} /> <span style={{fontWeight:600, fontFamily:"open sans"}}>Billa</span>
                    </div>

                    <div style={{ position:"relative"}}>
                        <input type="tel" name="amount" style={{borderTopLeftRadius:"5px", borderTopRightRadius:"5px", margin:"20px 10px 0", padding:"5px 10px", paddingLeft:"40px", border:"none", fontSize:"40px", width:"calc(100% - 20px)", fontWeight:600, fontFamily:"open sans"}} ref={this.innerRef} />
                        <span style={{fontSize:"30px", padding:"10px", position:"absolute", top:"25px", left:"15px", fontWeight:600, fontFamily:"open sans", color:"#000"}}>€</span> 
                    </div>

                    <div style={{borderBottomLeftRadius:"5px", borderBottomRightRadius:"5px", background:"#fff", margin:"0 10px 0", padding:"10px", borderTop:"1px solid #eee", fontSize:"12px", fontFamily:"open sans", fontWeight:300}}>Mindestbestrag ist 1,50 €</div>

                    <div className="mt-30 text-center" style={{color:"blue"}}>Kommentar hinzufügen</div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Vendor);
