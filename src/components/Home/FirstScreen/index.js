import React, { Component } from "react";
import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";
import Footer from "../Footer";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { faCamera } from '@fortawesome/free-solid-svg-icons';
import { faUniversity } from '@fortawesome/free-solid-svg-icons';
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons';
import { faBiking } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faAd } from '@fortawesome/free-solid-svg-icons';
import { faPeopleCarry } from '@fortawesome/free-solid-svg-icons';
import { faBtc } from '@fortawesome/free-brands-svg-icons';
import { faHotel } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-regular-svg-icons';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faHandHoldingHeart } from '@fortawesome/free-solid-svg-icons';
import { faTaxi } from '@fortawesome/free-solid-svg-icons';
import { faGrinHearts } from '@fortawesome/free-regular-svg-icons';
import { faWallet } from '@fortawesome/free-solid-svg-icons';
import { faBuromobelexperte } from '@fortawesome/free-brands-svg-icons';
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons';
import { faThLarge } from '@fortawesome/free-solid-svg-icons';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { faBarcode } from '@fortawesome/free-solid-svg-icons';
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faExchangeAlt } from '@fortawesome/free-solid-svg-icons';
import { faEuroSign } from '@fortawesome/free-solid-svg-icons';
import { faAndroid } from '@fortawesome/free-brands-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

//cookie
import CookieConsent from "react-cookie-consent";


class FirstScreen extends Component {

    // constructor(props) {
    //     super(props);
        
    //     this.state = { 
    //     };
    // }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {

        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

    }

    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    
    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user } = this.props;
        if (!user.success) {
            return (
                <Redirect to={"/home/account/login"} />
            );
        }


        //required chrome version for alert
        if(osName == "Windows"){
            var versionRequired = 70;
        } else if(osName == "Android"){
            var versionRequired = 31;
        } else if(osName == "Ubuntu"){
            var versionRequired = 72;
        } 

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = <span onClick={this.getMyLocation} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenSetupLocation")}<Ink duration="500" /></span>
        } else if(isMobileSafari){
            var install = <span onClick={() => { this.showPrompt(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
        } else {
            var install = <span onClick={() => { this.showModalBerechtigungen(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
        }
        
        if(localStorage.getItem("userPreferedLanguage") == 1 && localStorage.getItem("country") == "AT"){
            var langCss = "locale-flag-de-at";
        } else if(localStorage.getItem("userPreferedLanguage") == 1 && localStorage.getItem("country") == "DE"){
            var langCss = "locale-flag-de";
        } else if(localStorage.getItem("userPreferedLanguage") == 2){
            var langCss = "locale-flag-en";
        }
        

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <CookieConsent
                    location="bottom"
                    buttonText="Okay"
                    cookieName="myAwesomeCookieName2"
                    style={{ background: "#2B373B" }}
                    buttonStyle={{ color: "#4e503b", fontSize: "13px" }}
                    expires={150}
                >
                Wir verwenden Cookies zur Verbesserung unserer Website.
                </CookieConsent> */}

                <div style={{maxWidth:"468px", margin:"auto", background:"#F6F6F6"}} className="pb-300">

                    <ul className="homeHeader">
                        <Link to="/home/scan"><li>
                            <img src="https://lieferservice123.com/assets/img/home/scan-white.png" />
                            <div>Scan</div>
                            <Ink duration="500" />
                        </li></Link>

                        <Link to="/home/pay/activate"><li>
                            {/* <img src="https://lieferservice123.com/assets/img/home/qr-code-white3.png" style={{border:"2px solid #fff", padding:"5px", borderRadius:"5px"}} /> */}
                            <FontAwesomeIcon icon={faQrcode} style={{border:"2px solid #fff", padding:"0 5px", borderRadius:"5px", width:"30px", height:"30px"}}  />
                            <div>Pay</div>
                            <Ink duration="500" />
                        </li></Link>

                        <Link to="/home/request"><li>
                            <FontAwesomeIcon icon={faEuroSign} style={{border:"2px solid #fff", padding:"5px 0", borderRadius:"5px", width:"30px", height:"30px", borderBottomLeftRadius:"60%", borderBottomRightRadius:"60%"}}  />
                            <div>Anfordern</div>
                            <Ink duration="500" />
                        </li></Link>

                        <Link to="/home/wallet"><li>
                            {/* <FontAwesomeIcon icon={faWallet} /> */}
                            {/* <img src="https://lieferservice123.com/assets/img/home/wallet-white3.png" style={{height:"31px"}} /> */}
                            <img src="https://lieferservice123.com/assets/img/home/wallet-white3.png" style={{border:"0.4px #fff solid", borderLeft:"1px solid #fff", borderRadius:"5px", width:"30px", height:"30px"}} />

                            <div>Guthaben</div>
                            {/* <div>Wallet</div>
                            <div>Kontostand</div> */}
                            <Ink duration="500" />
                        </li></Link>
                    </ul>

                    {/* all apps */}
                    <ul className="homeApps">
                        <li onClick={() => window.location = "/transfer"}>
                            <FontAwesomeIcon icon={faExchangeAlt} style={{background:"#0F8CE9", color:"#fff", padding:"0 5px", borderRadius:"5px", width:"30px", height:"30px"}} />
                            <div>Transfer</div>
                            <Ink duration="500" />
                        </li>

                        <li onClick={() => window.location = "https://lieferservice123.com"}>
                            <FontAwesomeIcon icon={faUtensils} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} />
                            <div>Restaurants</div>
                            <Ink duration="500" />
                        </li>

                        <li onClick={() => window.location = ""}>
                            <FontAwesomeIcon icon={faStore} style={{color:"#8360c3"}} />
                            <div>Shops</div>
                            <Ink duration="500" />
                        </li>

                        {/* <li>
                            <FontAwesomeIcon icon={faAd} style={{color:"#48A1DD"}} />
                            <div>Kleinanzeigen</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faMobileAlt} style={{color:"#EF961D"}} />
                            <div>Guthaben</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faPeopleCarry} style={{color:"#0AB4F3"}} />
                            <div>Dienstleistungen</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faBtc} style={{color:"#F4B400"}} />
                            <div>Kryptomarkt</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faHotel} style={{color:"#FF385C"}} />
                            <div>Unterkünfte</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faTaxi} style={{color:"#E0AD02"}} />
                            <div>Taxi</div>
                            <Ink duration="500" />
                        </li>

                        <li>
                            <FontAwesomeIcon icon={faHandHoldingHeart} style={{color:"#F3016A"}} />
                            <div>Dating</div>
                            <Ink duration="500" />
                        </li> */}

                        <Link to="/apps">
                            <li>
                                <FontAwesomeIcon icon={faGripHorizontal} style={{color:"#aaa"}} />
                                <div>Mehr</div>
                                <Ink duration="500" />
                            </li>
                        </Link>

                    </ul>

                    <div></div>

                    {/* notification bar */}
                    <NavLink to="/home/download">
                        <div style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"10px", margin:"10px 15px"}}>
                            <div className="category-list-item" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-15 border-0">
                                        <FontAwesomeIcon icon={faAndroid} style={{color:"#82C91E", fontSize:"40px"}} />
                                    </div>
                                    <div className="flex-auto border-0">
                                        Download Paydizer
                                        <div style={{fontSize:"14px", fontWeight:"normal"}}>Zum Bildschirm hinzufügen</div>
                                    </div>
                                    <div className="flex-auto text-right mt-10">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                                <Ink duration="500" />
                            </div>
                        </div>
                    </NavLink>

                    {/* notification if not bank account added */}
                    <div style={{background:"#fff", padding:"18px", margin:"10px 15px", borderRadius:"10px"}}>

                        <div style={{fontWeight:"bold", fontSize:"16px", textAlign:"center", letterSpacing:"-1px", color:"#333", textShadow: "1px 1px #eee"}}>Kontaktloses Bezahlen mit QR Code</div>

                        <p style={{textAlign:"center", fontWeight:300, fontFamily:"open sans", fontSize:"12px"}}>Erleben Sie schnelle, einfache und sichere Online-Zahlungen.</p>

                        <p className="homeNotification">
                            <i><img src="https://lieferservice123.com/assets/img/home/wallet.png" /></i>
                            <span>
                                <b>Ein Konto für alle Transaktionen</b><br/>Bezahle bei Restaurants, Supermärkte, Tankstellen, Online Shops, etc. 
                            </span>
                        </p>

                        <p className="homeNotification">
                            <i><img src="https://lieferservice123.com/assets/img/home/scan.png" /></i>
                            <span>
                                <b>Bezahlen mit dem Handy</b><br/>App öffnen und QR Code vom Händler scannen.
                            </span>
                        </p>

                        <Button style={{width:"100%", position:"relative"}}>START<Ink duration="300" /></Button>
                    </div>

                    <Footer active_home={true} />


                </div>
                
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    settings: state.settings.settings,
    user: state.user.user,
    language: state.languages.language,
});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData }
)(FirstScreen);
