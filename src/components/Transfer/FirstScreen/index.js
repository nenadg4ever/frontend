import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

import { css } from './style.css';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList } from '@fortawesome/free-solid-svg-icons';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { faUniversity } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { faGift } from '@fortawesome/free-solid-svg-icons';
import { faReceipt } from '@fortawesome/free-solid-svg-icons';
import { faAlignLeft } from '@fortawesome/free-solid-svg-icons';

class FirstScreen extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Transfer"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    blueBg={true}
                />

                <div onClick={() => this.toogleMenu()} style={{padding:"15px 20px 20px 18px", position:"fixed", top:"0", right:"0", zIndex:9, color:"#fff"}}>
                    <FontAwesomeIcon icon={faEllipsisH} />
                    <Ink duration="500" />
                </div>

                {/* hamburger enu */}
                <div id="hamburgerMenu" style={{display:"none", fontSize:"12px", fontWeight:600, fontFamily:"open sans", zIndex:9, background:"#fff", position:"absolute", top:"50px", right:"5px", borderRadius:"3px", boxShadow:"rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px"}}>

                    <div style={{position:"absolute", marginTop:"-10px", right:"15px"}}><FontAwesomeIcon icon={faCaretUp} style={{color:"#fff", fontSize:"16px"}} /></div>

                    <div style={{padding:"10px 18px", position:"relative", borderBottom:"1px solid #eee"}}>
                        {/* QR Code in Galerie speichern */}
                        Hilfe
                        <Ink duration="500" />
                    </div>
                    <div style={{padding:"10px 18px", position:"relative"}}>
                        Sicherheitstipps
                        <Ink duration="500" />
                    </div>

                </div>

                <div className="mb-200" style={{position: "relative", margin: "auto", overflow:"auto", marginTop:"-1px"}}>

                    {/* search friends */}
                    <div className="rounding">
                        <div style={{overflow:"auto", width:"100%", padding:"0 15px 20px", position:"relative"}}>
                            <input placeholder="An Mobilnr. überweisen" type="text" style={{width:"100%", borderRadius:"10px", padding:"10px 15px", border:"1px solid #eee", fontSize:"14px"}} />
                            <span style={{position:"absolute", top:7, right:30, color:"#EF961D", fontSize:"22px"}}><FontAwesomeIcon icon={faAddressBook} /></span>
                        </div>
                    </div>

                    {/* all apps */}
                    <div style={{position:"relative", background:"#fff", margin:"15px 15px", marginTop:"-45px", borderRadius:"10px", padding:"10px 5px"}}>

                        <ul className="homeApps">
                            <li onClick={() => window.location = "/transfer"}>
                                <FontAwesomeIcon icon={faUserCircle} style={{color:"#0F8CE9"}} />
                                <div>An Freunde</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = "https://lieferservice123.com"}>
                                <FontAwesomeIcon icon={faUniversity} style={{color:localStorage.getItem("storeColor")}} />
                                <div>An Bankkonto</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = ""}>
                                <FontAwesomeIcon icon={faAlignLeft} style={{background:"#0F8CE9", color:"#fff", padding:"5px", borderRadius:"3px"}} />
                                <div>Transaktionen</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faGift} style={{color:"#FF385C"}} />
                                <div>Geschenkkarten</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faCalendarAlt} style={{color:"#0F8CE9"}} />
                                <div>Daueraufträge</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faMicrophone} style={{background:"#0F8CE9", color:"#fff", padding:"5px", borderRadius:"100%", width:"30px", height:"30px"}} />
                                <div>Sprachassistent</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faReceipt} style={{color:"#FF385C"}} />
                                <div>Go Dutch</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faGripHorizontal} style={{color:"#aaa"}} />
                                <div>Mehr</div>
                                <Ink duration="500" />
                            </li>

                        </ul>

                    </div>

                    {/* show people that you have last transacted with and favorites */}
                    <div style={{position:"relative", background:"#fff", margin:"15px 15px", borderRadius:"10px", padding:"10px 5px", overflow:"auto"}}>
                        <div style={{borderBottom:"1px solid #eee", overflow:"auto"}}>
                            <span style={{padding:"5px", float:"left", borderBottom:"2px solid #0F8CE9", color:"#0F8CE9"}} className="pl-10 pr-10 mr-20">Kürzlich</span>
                            <span style={{padding:"5px", float:"left"}}>Favoriten</span>
                            <span style={{padding:"5px", float:"right"}}>Alle ></span>
                        </div>

                        <div style={{color:"#999", fontWeight:"300", fontFamily:"open sans", textAlign:"center", padding:"15px 10px"}} className="mt-20">
                            Keine Zahlungs-Transaktionen gefunden
                        </div>
                    </div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(FirstScreen);
