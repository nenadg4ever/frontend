import React, { Component } from "react";

import Footer from "./Footer";
import Hero from "./Hero";
import Meta from "../helpers/meta";
import StoreAchievements from "./StoreAchievements";
import { connect } from "react-redux";
import { getSettings } from "../../services/settings/actions";
import { getSingleLanguageData } from "../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';

class Desktop extends Component {
    state = {
        showGdpr: false
    };
    componentDidMount() {
        if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        }

        if (!localStorage.getItem("gdprAccepted")) {
            localStorage.setItem("gdprAccepted", "false");
            if (localStorage.getItem("showGdpr") === "true") {
                this.setState({ showGdpr: true });
            }
        }

        if (localStorage.getItem("showGdpr") === "true" && localStorage.getItem("gdprAccepted") === "false") {
            this.setState({ showGdpr: true });
        }
    }
    handleGdprClick = () => {
        localStorage.setItem("gdprAccepted", "true");
        this.setState({ showGdpr: false });
    };

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.languages !== nextProps.languages) {
            if (localStorage.getItem("multiLanguageSelection") === "true") {
                if (localStorage.getItem("userPreferedLanguage")) {
                    this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                    // console.log("Called 1");
                } else {
                    if (nextProps.languages.length) {
                        const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            } else {
                // console.log("Called 2");
                if (nextProps.languages.length) {
                    const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                    this.props.getSingleLanguageData(id);
                }
            }
        }
    }

    render() {

        return (
            <React.Fragment>
                {this.state.showGdpr && (
                    <div className="fixed-gdpr">
                        <span
                            dangerouslySetInnerHTML={{
                                __html: localStorage.getItem("gdprMessage")
                            }}
                        ></span>
                        <span>
                            <button
                                className="btn btn-sm ml-2"
                                style={{ backgroundColor: localStorage.getItem("storeColor") }}
                                onClick={this.handleGdprClick}
                            >
                                {localStorage.getItem("gdprConfirmButton")}
                            </button>
                        </span>
                    </div>
                )}
                <Meta
                    seotitle={"Download Paydizer"}
                    seodescription={"Verfügbar für alle Plattformen; Senden und Empfangen von Geld in Sekundenschnelle."}
                    ogtype="website"
                    ogtitle={"Paydizer"}
                    ogdescription={"Senden und Empfangen von Geld in Sekundenschnelle."}
                    ogurl={window.location.href}
                    twittertitle={"Paydizer"}
                    twitterdescription={"Senden und Empfangen von Geld in Sekundenschnelle."}
                />

                <Hero />

                {/* <StoreAchievements /> */}

                {/* <Footer /> */}

                {/* {localStorage.getItem("multiLanguageSelection") === "true" &&
                    (this.props.languages && this.props.languages.length > 0 && (
                        <div className="mt-4 d-flex align-items-center justify-content-center">
                            <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                            <select
                                onChange={this.handleOnChange}
                                defaultValue={
                                    localStorage.getItem("userPreferedLanguage")
                                        ? localStorage.getItem("userPreferedLanguage")
                                        : this.props.languages.filter(lang => lang.is_default === 1)[0].id
                                }
                                className="form-control language-select"
                            >
                                {this.props.languages.map(language => (
                                    <option value={language.id} key={language.id}>
                                        {language.language_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                    ))} */}

                    <div style={{textAlign:"center", borderTop:"1px solid #eee", fontSize:"12px"}} className="p-20">

                        {/* <a href="#" className="m-2">Hilfe Center</a>
                        <a href="#" className="m-2">Über <FontAwesomeIcon icon={faCaretUp} /></a>
                        <a href="#" className="m-2">Deutsch <FontAwesomeIcon icon={faCaretUp} /></a> */}
                        <a href="https://paydizer.com/store" target="_blank" className="m-2">Partner</a>
                        <a href="https://gitlab.com/paydizer" target="_blank" className="m-2">Developer</a>

                        <span className="ml-10 mr-10">Copyright © 2020 Lieferservice123 e. U. All Rights Reserved.</span>

                        <a href="/pages/privacy" className="m-2">Datenschutz</a><span style={{padding:"2px"}}>|</span>
                        {/* <a href="/pages/cookies" className="m-2">Cookies</a><span style={{padding:"2px"}}>|</span> */}
                        <a href="/pages/agb" className="m-2">AGB</a><span style={{padding:"2px"}}>|</span>
                        <a href="/pages/impressum" className="m-2">Impressum</a>

                        <a
                            href={"https://twitter.com/paydizer"}
                            className="btn btn-sm btn-rounded btn-alt-secondary ml-10"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <i className="fa fa-fw fa-twitter" />
                        </a>

                    </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    settings: state.settings.settings,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData }
)(Desktop);
