import * as firebase from "firebase/app";
import messaging from "../../../init-fcm";
import { saveNotificationToken } from "../../../services/notification/actions";

import React, { Component } from "react";
import axios from "axios";

import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import Nav from "../../Takeaway/Nav";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';

//supermarket
import { SUPERMARKET_ORDER_ALL_URL } from "../../../configs/index";

import ShopperMenu from "./shopperMenu.js";

import { updateDeliveryUserInfo } from "../../../services/Delivery/user/actions";



declare var INSTALL_APP_POPUP;


class Batches extends Component {

    constructor() {
        super();
        
        this.state = { 
            supermarketOrders: [],//batches
            no_orders: false,
        };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { settings } = this.props;

        //set country if not set
        if (!localStorage.getItem("country")) {
            this.getCountry();
        }
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        const { delivery_user } = this.props;
        //update delivery guy info
        if(delivery_user.success){
            this.props.updateDeliveryUserInfo(delivery_user.data.id, delivery_user.data.auth_token);
        }

        if(localStorage.getItem("country") == "AT"){
            this.supermarketOrders();
            this.refreshSetInterval = setInterval(() => {
                this.__refreshOrderStatus();
            }, 30 * 1000);
        }
    }

    //benachrichtigungen
    showModalNotification () {
        this.setState({
            NotificationModal: true
        });
    }
    hideModalNotification = () => {
        this.setState({
            NotificationModal: false
        });
    }

    __refreshOrderStatus = () => {
        this.supermarketOrders();
        if (this.refs.refreshButton) {
            var div = this.refs.refreshButton;
            div.innerHTML = 'Laden...';
        }
        
        setTimeout(() => {
            if (this.refs.refreshButton) {
                var div = this.refs.refreshButton;
                div.innerHTML = 'Datenaktualisierung in 30 Sekunden';
            }
        }, 2 * 1000);
    };

    //lang and country
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("prefix", "43");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("prefix", "49");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("prefix", "41");
                } 
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    //all batches
    supermarketOrders = () => {
        
        axios
        .post(SUPERMARKET_ORDER_ALL_URL, {
            country: localStorage.getItem("country"),
            limit: 50,
            plz: (localStorage.getItem("userSetAddress")?this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "plz"):"")
        })
        .then(response => {

            // console.log("response", response);
            
            if (response.data.length) {
                if (JSON.stringify(this.state.supermarketOrders) !== JSON.stringify(response.data)) {
                    this.setState({
                        supermarketOrders: response.data,
                        no_orders: false
                    });
                }

            } else {
                this.setState({
                    no_orders: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 

    //view batch
    getAddress = (address, extract, long=true) => {
        // var address = address || '';
        if(address){
            if(extract == "str+nr"){
                var res = address;
                var split = res.split(",");
                if(long) var res = split.slice(0, split.length - 1).join(",");
                else var res = split[0];
                return res;
            } else if(extract == "str"){
                var res = address;
                var split = res.split(",");
                var res = split[0];
                return res;
            }  else if(extract == "plz"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only plz without city
                if(res){
                    var split = res.split(" ");
                    var res = split[1];
                    return res;
                }
            } else if(extract == "city"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only city without plz
                if(res){
                    var split = res.split(" ");
                    var res = split[2];
                    return res;
                }
            } else {
                var res = address.split(", ");
                return res[0];
            }
        }
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    //benachrichtigungen
    notificationPrompt = () => {
        const { delivery_user } = this.props;

        if (delivery_user.success) {//we need just to check if user has tel in table
            if (firebase.messaging.isSupported()) {
                let handler = this.props.saveNotificationToken;
                messaging
                    .requestPermission()
                    .then(async () => {
                        const push_token = await messaging.getToken();
                        handler(push_token, delivery_user.data.id, delivery_user.data.auth_token);
                    })
                    .catch(function(err) {
                        console.log("Unable to get permission to notify.", err);
                    });
                navigator.serviceWorker.addEventListener("message", message => console.log(message));
            }
        }
    }

    // hamburger menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    //calc how many units
    makeTotalUnits = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            sum = sum + parseFloat(row.quantity)
        });
        return sum;
    }

    // new total price after refund and replacements
    newTotal = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            if(row.found > 0 && row.found_replacement == null){
                // console.log("NORMAL ("+row.found+' * '+row.price+')', row.id+'. '+(row.found * row.price));
                sum = sum + (row.found * row.price)
            } 
            else if(row.found == 0 && row.found_replacement > 0){
                // console.log("REPLACEMENT ", row.replacement_id+'. '+(row.found_replacement * row.replacement_price));
                sum = sum + (row.found_replacement * row.replacement_price)
            } 
        });
        return sum.toFixed(2);
    }

    getSettings = (data, keyName)=>{
        var filteredData = data.filter(x=>x.key == keyName);
        var focusData = undefined;
        if(filteredData.length > 0){
            focusData = filteredData[0].value;
        }
        return focusData;
    }

    
    render() {
        const { delivery_user } = this.props;
        const { supermarketOrders } = this.state;

        if (!delivery_user.success) {
            return (
                window.location = "/delivery/login"
            );
        }

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <Nav redirectUrl={"shopper"} logo={true} active_nearme={true} disable_back_button={true} loggedin={delivery_user.success} /> */}

                {/* header */}
                <div style={{width:"100%", padding: "10px 0", float:"left"}}><div style={{maxWidth: "468px", margin: "auto"}}>

                    {/* hamburger icon */}
                    <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                    <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>Aufträge</span>
                </div></div>

                {/* batches */}
                <div style={{float:"left", width:"100%", background:"#F7F7F7"}}>
                    <div className="mb-200" style={{maxWidth: "468px", margin: "auto"}}>
                        {delivery_user.data.passport_verified ? (
                            delivery_user.data.agreement_verified ? (
                                supermarketOrders.length ? (
                                    localStorage.getItem("userSetAddress") ? (
                                        <div style={{padding: "18px 0", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>
                                            
                                            {/* <div style={{margin: "15px 0", fontSize:"24px", fontFamily:"open sans", fontWeight:400, textAlign:"center"}}>Aufträge in der Nähe</div> */}

                                            {/* <div style={{marginBottom: "30px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Sie können Einkäufe übernehmen oder die Besorgung von Lebensmitteln.</div> */}

                                            <div ref="refreshButton" style={{margin:"0 10px 10px", fontFamily:"open sans", fontWeight:300}}>Datenaktualisierung in 30 Sekunden</div>

                                            {supermarketOrders.map((row, index) => {

                                                return (
                                        
                                                    <div  style={{}}>

                                                        <div key={row.id} className="supermarketOrders" 
                                                            //onClick={this.showModalInvoiceSupermarket.bind(this, row.id)}
                                                            onClick={() => { window.location = '/viewBatch/'+row.id }}
                                                        >

                                                            {/* <div style={{float:"left", width:"25%"}}>
                                                                {this.elapsedTime(this.toDate(row.added))}
                                                            </div> */}

                                                            <div style={{width:"100%", color:"rgb(252, 128, 25)", fontWeight:"bold", fontSize:"24px"}}>{row.currencyFormat} {( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) + parseFloat(row.subtotal * row.delivery_tip/100) ).toFixed(2)}</div>

                                                            <div style={{overflow:"auto"}}>
                                                                <div style={{marginTop:"10px", float:"left", marginRight:"20px"}}>
                                                                    <FontAwesomeIcon icon={faCar} style={{marginRight:"5px"}} />{this.getDistance(row.fromLat, row.fromLng, row.shopLat, row.shopLng).toFixed(2)} km
                                                                </div>
                                                            
                                                                <div style={{marginTop:"10px", float:"left"}}><FontAwesomeIcon icon={faShoppingBag} style={{marginRight:"5px"}} />{row.items.length} Produkte \ {this.makeTotalUnits(row.items)} Einheiten</div>

                                                            </div>

                                                            <div style={{marginTop:"10px"}}><b>{row.shopName}</b> &middot; {this.getAddress(row.shopAddress, "str+nr")}</div>

                                                            <Ink duration="500" />

                                                        </div>
                                                        
                                                    </div>
                                                )
                                                
                                            })}

                                            {/* <div style={{margin:"auto", textDecoration:"underline", padding:"4px 8px", background:"#E7EEF8", color:"#4f6587", overflow:"auto", textAlign:"right"}}><a href={'/'}>Weitere Ergebnisse</a></div> */}

                                        </div> 
                                    ): (
                                        <div style={{width:"100%", float:"left", padding:"18px", borderTop:"1px solid #ccc"}}>Bitte den <a href={'/search-location?redirect=shopperPwa'} style={{color:"blue"}}>Standort festlegen</a>.</div>
                                    )
                                ) : (
                                    this.state.no_orders ? <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Keine Aufträge in deiner Umgebung gefunden.</h2> : <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Laden...</h2>
                                )
                            ) : (
                                <div style={{width:"100%", float:"left", padding:"18px", borderTop:"1px solid #ccc"}}>Möchte Sie geringfügig oder selbstständig arbeiten. Bitte <a href={'/shopperPwa?act=dashboard'} style={{color:"blue"}}>Arbeitsmodell auswählen</a>.</div>
                            )
                        ) : (
                            <div style={{width:"100%", float:"left", padding:"18px", borderTop:"1px solid #ccc"}}>Bitte <a href={'/shopperPwa?act=dashboard'} style={{color:"blue"}}>Ausweis hochladen</a> um Aufträge zu sehen.</div>
                        )}
                    </div>
                </div>

                {/* shopper menu */}
                <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                    <Modal.Header closeButton style={{background:"#fff"}}>
                        <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                            <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                            <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                        <ShopperMenu />
                    
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { getSettings, saveNotificationToken, getSingleLanguageData, updateDeliveryUserInfo }
)(Batches);