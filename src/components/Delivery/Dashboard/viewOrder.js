import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";

import { getSettings } from "../../../services/settings/actions";
import { connect } from "react-redux";

import { SUPERMARKET_ORDER_URL } from "../../../configs/index";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';

class ViewOrder extends Component {

    constructor(props) {
        super(props);

        this.state = { 
            row: [],
        };
    }

    componentDidMount() {
        this.supermarketOrders();
    }

    supermarketOrders = () => {
        const { id } = this.props;
        axios
        .post(SUPERMARKET_ORDER_URL, {
            id: id
        })
        .then (newArray => {
            // console.log("row", newArray.data);
            this.setState({
                row: newArray.data
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
 
    showTime = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return  d.getHours() + ':' + d.getMinutes();
    }
    showDateFull = (unix_timestamp) => {
        var days = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'];
        const monthNames = ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
        var d = new Date(unix_timestamp * 1000);
        return  days[d.getDay()]+', '+d.getDate()+ '. ' + (monthNames[d.getMonth()]) + ' ' + d.getHours() + ':' + d.getMinutes();
    }   

    //calc how many units
    makeTotalUnits = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            sum = sum + parseFloat(row.quantity)
        });
        return sum;
    }
    // new total price after refund and replacements
    newTotal = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            if(row.found > 0 && row.found_replacement == null){
                // console.log("NORMAL ("+row.found+' * '+row.price+')', row.id+'. '+(row.found * row.price));
                // console.log("("+row.found+" * "+row.price+") + ");
                sum = sum + (row.found * row.price)
            } 
            else if(row.found == 0 && row.found_replacement > 0){
                // console.log("REPLACEMENT ", row.replacement_id+'. '+(row.found_replacement * row.replacement_price));
                // console.log("("+row.found_replacement+" * "+row.replacement_price+") + ");
                sum = sum + (row.found_replacement * row.replacement_price)
            } 
        });
        return sum.toFixed(2);
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    render() {

        const { row } = this.state;

        return (
   
            <React.Fragment>

                {row.length == null && (
                    
                    <React.Fragment>

                        <div style={{textAlign:"center", fontWeight:"bold"}}>Autragsübersicht #{row.id}</div>

                        <div style={{color:"#888"}}>

                            <div style={{textAlign:"center"}}>
                                {this.showDateFull(row.markDeliveredDate)}<br/>

                                <div style={{color:"#000", fontSize:"35px", fontWeight:"bold"}}>
                                    {row.subtotal == this.newTotal(row.items) ? (
                                        <div>€{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) + parseFloat(row.subtotal * row.delivery_tip/100) ).toFixed(2)}</div>
                                    ) : (
                                        <div>€{( parseFloat(row.delivery_charge) + parseFloat(this.newTotal(row.items) * row.shopper_reward/100) + parseFloat(this.newTotal(row.items) * row.delivery_tip/100) ).toFixed(2)}</div>
                                    )}
                                </div>
                            </div>
                            <br/>
                            <FontAwesomeIcon icon={faCheck} style={{color:"#BCBBBC", float: "left", height: "30px", marginRight: "15px"}} />
                            <div style={{fontFamily:"open sans", fontWeight:300, fontSize:"10px"}}>{this.showTime(row.acceptedDate)}</div>
                            <div style={{fontFamily:"open sans", fontWeight:500, marginBottom:"10px"}}><b>Auftrag akzeptiert</b></div>
                            <br/>
                            <FontAwesomeIcon icon={faStore} style={{color:"#87C67A", float: "left", height: "40px", marginRight: "15px"}} />
                            <div style={{fontFamily:"open sans", fontWeight:300, fontSize:"10px"}}>Start: {this.showTime(row.startShoppingDate)}</div>
                            <div style={{fontFamily:"open sans", fontWeight:500}}><b>{row.shopName}</b></div>
                            <div style={{fontSize:"12px", marginBottom:"10px"}}>€{row.subtotal} · {row.items.length} Produkte ({this.makeTotalUnits(row.items)} Einheiten)</div>
                            <br/>
                            <FontAwesomeIcon icon={faHome} style={{color:"#87C67A", float: "left", height: "40px", marginRight: "15px", fontSize:"18px"}} />
                            <div style={{fontFamily:"open sans", fontWeight:300, fontSize:"10px"}}>Zugestellt: {this.showTime(row.markDeliveredDate)}</div>
                            <div style={{fontFamily:"open sans", fontWeight:500}}><b>Kunde</b></div>
                            <div style={{fontSize:"12px", marginBottom:"10px"}}>Entfernung: {this.getDistance(row.fromLat, row.fromLng, row.shopLat, row.shopLng).toFixed(2)} km</div>

                            <br/>
                            <br/>

                            <div style={{fontFamily:"open sans", fontWeight:600, color:"#000"}}>

                                {row.subtotal == this.newTotal(row.items) ? (

                                    <div>
                                        <div>Lieferservice Zahlung<span style={{float:"right"}}>€{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) ).toFixed(2)}</span></div>

                                        <br/>

                                        {row.delivery_tip_old == null ? (
                                            <div>
                                                Kunden Trinkgeld
                                                <span style={{float:"right"}}>€{(row.subtotal * (row.delivery_tip/100)).toFixed(2)}</span>
                                            </div>
                                        ) : (
                                            <div>
                                                Kunden Trinkgeld
                                                <span style={{float:"right"}}>€{(row.subtotal * (row.delivery_tip/100)).toFixed(2)}</span>
                                                <span style={{float:"right", color:"#999", marginRight:"5px"}}><s>€{(row.subtotal * (row.delivery_tip_old/100)).toFixed(2)}</s></span>

                                                <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Der Kunde hat das Trinkgeld nach der Lieferung geändert.</div>
                                            </div>
                                        )}

                                        <br/>
                                        <div style={{fontSize:"20px"}}>Gesamteinnahmen<span style={{float:"right"}}>€{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) + parseFloat(row.subtotal * row.delivery_tip/100) ).toFixed(2)}</span></div>

                                        <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Kunden haben 3 Tage Zeit, um ein Trinkgeld für die Bestellung zu geben.</div>
                                    </div>

                                ) : (

                                    <div>
                                        <div>
                                            Lieferservice Zahlung
                                            <span style={{float:"right"}}>€{( parseFloat(row.delivery_charge) + parseFloat(this.newTotal(row.items) * row.shopper_reward/100) ).toFixed(2)}</span>
                                            <span style={{float:"right", color:"#999", marginRight:"5px"}}><s>€{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) ).toFixed(2)}</s></span>
                                            <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Bestellanpassungen beeinflussten die Lieferservice Zahlung.</div>
                                        </div>

                                        <br/>

                                        {row.delivery_tip_old == null ? (
                                            <div>
                                                Kunden Trinkgeld
                                                <span style={{float:"right"}}>€{(this.newTotal(row.items) * (row.delivery_tip/100)).toFixed(2)}</span>
                                                <span style={{float:"right", color:"#999", marginRight:"5px"}}><s>€{(row.subtotal * (row.delivery_tip/100)).toFixed(2)}</s></span>

                                                <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Bestellanpassungen beeinflussten das Trinkgeld.</div>
                                            </div>
                                        ) : (
                                            <div>
                                                Kunden Trinkgeld
                                                <span style={{float:"right"}}>€{(this.newTotal(row.items) * (row.delivery_tip/100)).toFixed(2)}</span>
                                                <span style={{float:"right", color:"#999", marginRight:"5px"}}><s>€{(row.subtotal * (row.delivery_tip_old/100)).toFixed(2)}</s></span>

                                                <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Bestellanpassungen beeinflussten das Trinkgeld.</div>
                                                <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Der Kunde hat das Trinkgeld nach der Lieferung geändert.</div>
                                            </div>
                                        )}

                                        <br/>
                                        <div style={{fontSize:"20px"}}>Gesamteinnahmen<span style={{float:"right"}}>€{( parseFloat(row.delivery_charge) + parseFloat(this.newTotal(row.items) * row.shopper_reward/100) + parseFloat(this.newTotal(row.items) * row.delivery_tip/100) ).toFixed(2)}</span></div>
                                        
                                        <div style={{marginTop:"5px", fontSize:"12px", color:"#666", fontWeight:300, fontFamily:"open sans"}}>Kunden haben 3 Tage Zeit, um ein Trinkgeld für die Bestellung zu geben.</div>
                                    </div>

                                )}
                                
                            </div>

                        </div>

                    </React.Fragment>

                )}

            </React.Fragment>
   
        )

    }
}


const mapStateToProps = state => ({
    settings: state.settings.settings,
});
export default connect(
    mapStateToProps,
    { getSettings }
)(ViewOrder);
