import * as firebase from "firebase/app";
import messaging from "../../../init-fcm";
import { saveNotificationToken } from "../../../services/notification/actions";

import React, { Component } from "react";
import axios from "axios";

import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import Nav from "../../Takeaway/Nav";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

import Location from "../../Takeaway/Location/";
import ShopperMenu from "./shopperMenu.js";

import { updateDeliveryUserInfo } from "../../../services/Delivery/user/actions";

// copied from Location/PopularPlaces START
import Flip from "react-reveal/Flip";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import { getPopularLocations } from "../../../services/popularLocations/actions";
import Geocode from "react-geocode";
import PopularPlaces from "../../Takeaway/Location/PopularPlaces";

declare var INSTALL_APP_POPUP;


class PaymentCard extends Component {

    constructor() {
        super();
        
        this.state = { 
            cards: [],
            riderAddress: localStorage.getItem("userSetAddress") ? JSON.parse(localStorage.getItem("userSetAddress")).address : "",
        };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { settings } = this.props;
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        const { delivery_user } = this.props;
        //update delivery guy info
        if(delivery_user.success){
            this.props.updateDeliveryUserInfo(delivery_user.data.id, delivery_user.data.auth_token);
        }

        //list all cards
        this.listAllCards();


        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }
    }


    //show pin
    showModalShowPin () {
        this.setState({
            ShowPinModal: true
        });
    }
    hideModalShowPin = () => {
        this.setState({
            ShowPinModal: false
        });
    }

    //card
    showModalPaymentCard () {
        this.setState({
            PaymentCardModal: true
        });
    }
    hideModalPaymentCard = () => {
        this.setState({
            PaymentCardModal: false
        });
    }
    showModalCardShipping () {
        this.setState({
            CardShippingModal: true
        });
    }
    hideModalCardShipping = () => {
        this.setState({
            CardShippingModal: false
        });
    }
    showModalShippingConfirm () {
        this.setState({
            ShippingConfirmModal: true
        });
    }
    hideModalShippingConfirm = () => {
        this.setState({
            ShippingConfirmModal: false
        });
    }
    showModalCardDone () {
        this.setState({
            CardDoneModal: true
        });
    }
    hideModalCardDone = () => {
        this.setState({
            CardDoneModal: false
        });
    }
    showModalCardActivation () {
        this.setState({
            CardActivationModal: true
        });
    }
    hideModalCardActivation = () => {
        this.setState({
            CardActivationModal: false
        });
    }
    showModalCardActivationSuccess () {
        this.setState({
            CardActivationSuccessModal: true
        });
    }
    hideModalCardActivationSuccess = () => {
        this.setState({
            CardActivationSuccessModal: false
        });
    }
    showModalStripeAgreement () {
        this.setState({
            StripeAgreementModal: true
        });
    }
    hideModalStripeAgreement = () => {
        this.setState({
            StripeAgreementModal: false
        });
    }

    //time
    showTimeCard = (time) => {
        var unix_timestamp = new Date(time).getTime() / 1000;
        var d = new Date(unix_timestamp * 1000);
        return  d.getMonth()+'/' + d.getFullYear().toString().substr(-2);
    }

    // shopper menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    //card
    onDeliveryNameChanged = (e) => {
        this.setState({
            fromName: e.currentTarget.value
        });
    } 
    soldoAddUser = () =>{
        const { delivery_user } = this.props;
        
        var div = document.getElementById('sendPrepaid');
        div.innerHTML = 'Laden...';

        var url = '//'+window.location.hostname+'/php/soldo/soldo.php?fromName='+this.state.fromName+'&fromAddress='+this.state.riderAddress+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=addUser';
        axios
        .get(url)
        .then(response => {
            console.log("addUser", response);

            if(response.data.includes("error")){
                alert(response.data);
            } else {
                this.hideModalShippingConfirm(); 
                this.showModalCardDone();
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 
    soldoActivateCard = () => {
        const { delivery_user } = this.props;

        axios
        .get('//'+window.location.hostname+'/php/soldo/soldo.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&cvc='+this.state.cvc+'&act=activateCard')
        .then(response => {
            console.log("soldoActivateCard response", response);
            this.hideModalCardActivation();
            this.showModalCardActivationSuccess();
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    onCvcChanged = (e) => {
        this.setState({
            cvc: e.currentTarget.value
        });
    }  
    listAllCards = () => {
        const { delivery_user } = this.props;

        var url = '//'+window.location.hostname+'/php/shopperPwa_listCards.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id;
        axios
        .get(url)
        .then(response => {
            var cardsRes = this.strstr(response.data, "{");

            // console.log("response", cardsRes);
            
            if (response.data.length && JSON.parse(cardsRes).id) {
                if (JSON.stringify(this.state.cards) !== JSON.stringify(cardsRes)) {
                    this.setState({
                        cards: cardsRes,
                        no_cards: false
                    });
                }

            } else {
                this.setState({
                    no_cards: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }


    handleGeoLocationClick2 = (results, address) => {// copied from Location/PopularPlaces START
        //check if hausnummer in address
        if(address){
            var num = address.split(" ").pop();
            var first = num.charAt(0);
            var last = num.charAt(num.length-1);

            if(first.match(/^-{0,1}\d+$/) && isNaN(last)){
                console.log(num + " is a number");
                document.getElementById('errorNum2').style.display = "none";
            } else if(isNaN(num)) {//if no number at end of string, return false
                console.log(num + " is not a number");
                this.searchInput.focus(); //sets focus to element
                var val = this.searchInput.value; //store the value of the element
                this.searchInput.value = ''; //clear the value of the element
                this.searchInput.value = address+' '; //set that value back. 
    
                document.getElementById('errorNum2').style.display = "block";
                return false;
            } else {
                console.log(num + " is a number");
                document.getElementById('errorNum2').style.display = "none";
            }
        }

        //get this result and store in a localstorage
        // localStorage.setItem("geoLocation", JSON.stringify(results[0]));

        //for mysql
        console.log("fromLat", results[0].geometry.location.lat());
        console.log("fromLng", results[0].geometry.location.lng());

        const userSetAddress = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng(),
            address: results[0].formatted_address
        };
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));

        this.setState({
            riderAddress: results[0].formatted_address,
            fromLat: results[0].geometry.location.lat(),
            fromLng: results[0].geometry.location.lng()
        });
    }

    strstr = (haystack, needle, bool) => {
        // Finds first occurrence of a string within another
        //
        // version: 1103.1210
        // discuss at: http://phpjs.org/functions/strstr    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   bugfixed by: Onno Marsman
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // *     example 1: strstr(‘Kevin van Zonneveld’, ‘van’);
        // *     returns 1: ‘van Zonneveld’    // *     example 2: strstr(‘Kevin van Zonneveld’, ‘van’, true);
        // *     returns 2: ‘Kevin ‘
        // *     example 3: strstr(‘name@example.com’, ‘@’);
        // *     returns 3: ‘@example.com’
        // *     example 4: strstr(‘name@example.com’, ‘@’, true);    // *     returns 4: ‘name’
        var pos = 0;
    
        haystack += "";
        pos = haystack.indexOf(needle); if (pos == -1) {
            return false;
        } else {
            if (bool) {
                return haystack.substr(0, pos);
            } else {
                return haystack.slice(pos);
            }
        }
    }

    
    //current batches
    getAddress = (address, extract, long=true) => {
        // var address = address || '';
        if(address){
            if(extract == "str+nr"){
                var res = address;
                var split = res.split(",");
                if(long) var res = split.slice(0, split.length - 1).join(",");
                else var res = split[0];
                return res;
            } else if(extract == "str"){
                var res = address;
                var split = res.split(",");
                var res = split[0];
                return res;
            }  else if(extract == "plz"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only plz without city
                if(res){
                    var split = res.split(" ");
                    var res = split[1];
                    return res;
                }
            } else if(extract == "city"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only city without plz
                if(res){
                    var split = res.split(" ");
                    var res = split[2];
                    return res;
                }
            } else {
                var res = address.split(", ");
                return res[0];
            }
        }
    }

    render() {
        const { user, delivery_user } = this.props;
        const { cards } = this.state;

        if (!delivery_user.success) {
            return (
                //redirect to account page
                window.location = "/delivery/login"
            );
        }

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <Nav redirectUrl={"shopper"} logo={true} active_nearme={true} disable_back_button={true} loggedin={delivery_user.success} /> */}

                {/* header */}
                <div style={{width:"100%", padding: "10px 0", float:"left"}}><div style={{maxWidth: "468px", margin: "auto"}}>

                    {/* hamburger icon */}
                    <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                    <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>Zahlungskarte</span>
                </div></div>

                {/* my payment card */}
                <div style={{float:"left", width:"100%", background:"#F7F7F7"}}><div className="bg-white mb-200" style={{maxWidth: "468px", margin: "auto"}}>

                    <div style={{background:localStorage.getItem("storeColor"), color:"#fff", textAlign:"center", padding:"18px"}}>

                        <div><img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{width:"120px", border:"5px solid #fff", borderRadius:"5px"}} /></div>

                        <div style={{fontSize:"22px", marginTop:"15px"}}>Zahlungskarte abholen</div>

                        <div>Damit Sie an der Kassa zahlen können, brauchen Sie eine Prepaidkarte.</div>

                        <div style={{marginTop:"30px"}}><Button variant="secondary" style={{width:"100%"}} onClick={() => alert("Abholorte derzeit nicht verfügbar.")}>Abholorte anzeigen</Button></div>

                    </div>

                    <div style={{padding:"15px 18px 10px", borderBottom:"1px solid #E8E7E5"}}>Ihre Zahlungskarte</div>

                    {(cards.length && JSON.parse(cards).id) ? (
                        <div key={JSON.parse(cards).id} style={{padding:"18px", borderBottom:"1px solid #E8E7E5", overflow:"auto", background:"#fff"}}>
                            <div style={{width:"35px", float:"left"}}><i className="si si-credit-card" style={{fontSize:"30px"}} /></div> 
                            <div style={{width:"calc(100% - 45px)", float:"right"}}>
                                {JSON.parse(cards).masked_pan}<br/>
                                Gültig bis: {this.showTimeCard(JSON.parse(cards).expiration_date)}<br/>
                                PIN Code: **** <span onClick={() => this.showModalShowPin()} style={{cursor:"pointer", color:localStorage.getItem("storeColor")}}><FontAwesomeIcon icon={faKey} /> Pin anzeigen</span>
                            </div>
                            <Modal show={this.state.ShowPinModal} onHide={this.hideModalShowPin} size="sm">
                                <Modal.Header closeButton>
                                    {/* <Modal.Title>Dein PIN Code lautet</Modal.Title> */}
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    <div style={{fontSize:"20px", fontWeight:300, fontFamily:"open sans"}}>Dein PIN Code lautet</div>
                                    <div style={{fontSize:"30px", fontWeight:"bold"}}>{JSON.parse(cards).pin}</div>
                                </Modal.Body>
                                {/* <Modal.Footer>
                                    <Button variant="secondary" onClick={this.hideShowPin}> 
                                        Schließen
                                    </Button> 
                                </Modal.Footer> */}
                            </Modal>
                        </div>
                    ) : (
                        this.state.no_cards ? (
                            <div style={{padding:"18px", borderBottom:"1px solid #E8E7E5", overflow:"auto", background:"#fff"}}>
                                <span style={{width:"35px", float:"left"}}><i className="si si-credit-card" style={{fontSize:"30px"}} /></span> 
                                <span style={{width:"calc(100% - 45px)", float:"right"}}>Keine Karte registriert<br/>XXXX</span>
                            </div>
                        ) : (
                            <div style={{padding:"18px", borderBottom:"1px solid #E8E7E5", overflow:"auto", background:"#fff"}}>
                                <span style={{width:"35px", float:"left"}}><i className="si si-credit-card" style={{fontSize:"30px"}} /></span> 
                                <span style={{width:"calc(100% - 45px)", float:"right"}}>Laden...<br/>XXXX</span>
                            </div>
                        ) 
                    )}

                    <div style={{cursor:"pointer", padding:"18px", borderBottom:"1px solid #E8E7E5", color:localStorage.getItem("storeColor"), fontWeight:"bold", textAlign:"center", background:"#fff"}} onClick={() => this.showModalCardActivation()}>Aktivieren Sie eine Karte</div>

                    <div style={{cursor:"pointer", padding:"18px", borderBottom:"1px solid #E8E7E5", color:localStorage.getItem("storeColor"), fontWeight:"bold", textAlign:"center", background:"#fff"}} onClick={() => this.showModalPaymentCard()}>Bestellen Sie eine neue Karte</div>

                    {/* Card activation */}
                    <Modal show={this.state.CardActivationModal} onHide={this.hideModalCardActivation} size="sm">
                        <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                            <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Zahlungskarte<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Auf der Rückseite Ihrer Karte finden Sie den 3-stelligen Code (CVC).</div></Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 25px"}}>

                            <input 
                            style={{border:"1px solid rgba(0,0,0,0.15)", boxShadow:"0 2px 3px rgba(0,0,0,0.06)", height:"3.5rem", padding:"8px 14px", margin:"5px 0", width:"100%"}} 
                            placeholder={"3-stelligen Code eingeben"}
                            onChange={this.onCvcChanged}
                            />

                            {this.state.cvc ? (
                                <a onClick={() => this.soldoActivateCard()} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Aktivieren<Ink duration="500" /></a>
                            ) : (
                                <a onClick={() => alert("Bitte den 3-stelligen CVC auf der Rückseite Ihrer Karte eingeben!")} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Aktivieren<Ink duration="500" /></a>
                            )}

                        </Modal.Body>
                    </Modal>

                    {/* Card activation success */}
                    <Modal show={this.state.CardActivationSuccessModal} onHide={this.hideModalCardActivationSuccess} size="sm">
                        <Modal.Header style={{padding:0}}>
                            {/* <Modal.Title></Modal.Title> */}
                        </Modal.Header>
                        <Modal.Body style={{padding:0}}>

                            <div style={{position:"relative", background:"#F7F5F7", textAlign:"center", padding:"20px 25px"}}>
                                <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"33%"}} />
                                <FontAwesomeIcon icon={faCheckCircle} style={{position:"absolute", color:"green", bottom:"10px", left:"34%", borderRadius:"30px", background:"#fff", fontSize:"30px", padding:"1px"}} />
                            </div>

                            <div style={{padding:"25px"}}>
                                {/* <div style={{fontSize:"16px"}}>Gratuliere! Ihre Zahlungskarte ist registriert.</div>
                                <div style={{fontSize:"12px"}}>Verwenden Sie diese Karte, wenn Sie für eine Lieferservice123-Bestellung bezahlen müssen.</div> */}

                                <div style={{fontSize:"16px"}}>Ihre Karte wurde zum aktivieren vorgemerkt.</div>
                                <div style={{fontSize:"12px"}}>Sie erhalten eine SMS, sobald die Karte aktiviert ist.</div>

                                <a onClick={() => { var url = window.location.href; window.location = url; }} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Okay<Ink duration="500" /></a>

                                <div style={{fontSize:"10px", marginTop:"15px", color:"#999"}}>Die Dauer der Aktivierung variiert je nach Uhrzeit. Teilweise ist die Karte schon nach wenigen Minuten bereit, in anderen Fällen dauert es mehrere Stunden.</div>

                            </div>

                        </Modal.Body>
                    </Modal>

                    {/* We are mailing you a prepaid card  */}
                    <Modal show={this.state.PaymentCardModal} onHide={this.hideModalPaymentCard} size="sm">
                        <Modal.Header closeButton>
                            <Modal.Title style={{padding:"10px", fontSize:"22px", fontFamily:"open sans", fontWeight:600}}>Wir senden Ihnen eine Prepaid Karte</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{paddingTop:0}}>

                            <div style={{marginBottom:"30px", textAlign:"center"}}>
                                <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"50%", margin:"25px 0"}} /> <br/><br/>
                                <div>Kaufen Sie ein mit dieser Prepaid Karte.</div>
                            </div>

                            <a onClick={() => {this.hideModalPaymentCard(); this.showModalCardShipping();}} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"10px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></a>

                        </Modal.Body>
                    </Modal>

                    {/* Shipping information */}
                    <Modal show={this.state.CardShippingModal} onHide={this.hideModalCardShipping} size="sm">
                        <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                            <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Lieferadresse<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Wir senden Ihnen eine Prepaid Karte an:</div></Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 25px"}}>

                            <input 
                            style={{border:"1px solid rgba(0,0,0,0.15)", boxShadow:"0 2px 3px rgba(0,0,0,0.06)", height:"3.5rem", padding:"8px 14px", margin:"5px 0", width:"100%"}} 
                            placeholder={"Vorname des Empfängers"}
                            onChange={this.onDeliveryNameChanged}
                            value={this.state.fromName}
                            />

                            {this.state.google_script_loaded && (
                                <GooglePlacesAutocomplete
                                    initialValue={localStorage.getItem("userSetAddress")?JSON.parse(localStorage.getItem("userSetAddress")).address:null}
                                    autocompletionRequest={{
                                        componentRestrictions: {
                                            //country: ['at'],
                                            country: [localStorage.getItem("country")],
                                        }
                                    }}
                                    loader={<img src="/assets/img/various/spinner.svg" className="location-loading-spinner" alt="loading" />}
                                    renderInput={props => (
                                        <div style={{ position: "relative" }}>
                                            <label style={{fontWeight:"normal", display:"block", marginTop:"10px"}}>
                                                <input
                                                    {...props}
                                                    className="form-control search-input search-box"
                                                    placeholder={localStorage.getItem("searchAreaPlaceholder")}
                                                    // ref={input => {
                                                    //     if(input)
                                                    //     {
                                                    //         setTimeout(function(){
                                                    //             input.focus();
                                                    //         }, 300)
                                                    //     }
                                                    //     //this.searchInput = input;
                                                    // }}
                                                    // autoFocus
                                                    ref={input => {
                                                        this.searchInput = input;
                                                    }}
                                                />
                                            </label>

                                            {/* hausnummer info */}
                                            <div id="errorNum2" style={{ display:"none", color: "#fff", position: "relative", background: "orange", width: "100%", padding: "10px 15px", zIndex: 1}}><FontAwesomeIcon icon={faExclamationTriangle} style={{marginRight:"5px"}} />Gib Deine Hausnummer an</div>
                                            
                                        </div>
                                    )}
                                    renderSuggestions={(active, suggestions, onSelectSuggestion) => (
                                        <div className="location-suggestions-container">
                                            {suggestions.map((suggestion, index) => (
                                                <Flip top delay={index * 50} key={suggestion.id}>
                                                    <div
                                                        className="location-suggestion"
                                                        onClick={event => {
                                                            onSelectSuggestion(suggestion, event);
                                                            geocodeByPlaceId(suggestion.place_id)
                                                                .then(results => this.handleGeoLocationClick2(results, suggestion.structured_formatting.main_text))
                                                                .catch(error => console.error(error));
                                                        }}
                                                    >
                                                        
                                                        <span className="location-main-name">{suggestion.structured_formatting.main_text}</span>
                                                        <br />
                                                        <span className="location-secondary-name">{suggestion.structured_formatting.secondary_text}</span>
                                                    </div>
                                                </Flip>
                                            ))}
                                        </div>
                                    )}
                                />
                            )}

                            <a onClick={() => {this.hideModalCardShipping(); this.showModalShippingConfirm();}} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Speichern<Ink duration="500" /></a>

                        </Modal.Body>
                    </Modal>

                    {/* Shipping information confirm */}
                    <Modal show={this.state.ShippingConfirmModal} onHide={this.hideModalShippingConfirm} size="sm">
                        <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                            <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Lieferadresse<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Wir senden Ihnen eine Prepaid Karte an:</div></Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 25px"}}>

                            <div style={{overflow:"auto"}}>
                                <div style={{fontWeight:"bold", float:"left", width:"calc(100% - 90px)"}}>
                                    {this.state.fromName && this.state.fromName}<br/>
                                    {this.getAddress(this.state.riderAddress, "str+nr", false)}<br/>
                                    {this.getAddress(this.state.riderAddress, "plz")} {this.getAddress(this.state.riderAddress, "city")}
                                </div>
                                <div onClick={() => { this.hideModalShippingConfirm(); this.showModalCardShipping(); }} style={{color:localStorage.getItem("storeColor"), float:"right"}}>Bearbeiten</div>
                            </div>

                            <a id="sendPrepaid" onClick={() => this.soldoAddUser()} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>An diese Adresse senden<Ink duration="500" /></a>

                        </Modal.Body>
                    </Modal>

                    {/* Your card is on the way */}
                    <Modal show={this.state.CardDoneModal} onHide={this.hideModalCardDone} size="sm">
                        <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                            <Modal.Title style={{padding:"10px", fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Ihre Karte ist unterwegs!</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{paddingTop:0}}>

                            <div style={{marginBottom:"30px", textAlign:"center"}}>
                                <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"50%", margin:"25px 0"}} /> <br/><br/>
                                <div>Sobald Ihre Karte in 5-7 Tagen eintrifft, aktivieren Sie die Karte in der Shopper App.</div>
                            </div>

                            <a onClick={() => { var url = window.location.href; window.location = url; }} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"10px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Fertig<Ink duration="500" /></a>

                        </Modal.Body>
                    </Modal>

                </div></div>

                {/* shopper menu */}
                <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                    <Modal.Header closeButton style={{background:"#fff"}}>
                        <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                            <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                            <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                        <ShopperMenu />
                    
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { getSettings, saveNotificationToken, getSingleLanguageData, updateDeliveryUserInfo }
)(PaymentCard);