import * as firebase from "firebase/app";
import messaging from "../../../init-fcm";
import { saveNotificationToken } from "../../../services/notification/actions";

import React, { Component } from "react";
import axios from "axios";

import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import Nav from "../../Takeaway/Nav";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

//supermarket
import { SUPERMARKET_ORDER_ALL_URL } from "../../../configs/index";
import { SUPERMARKET_ORDER_ALL_ACCEPTED_URL } from "../../../configs/index";
import { TRANSACTION_HISTORY_URL } from "../../../configs/index";

import Itemsfromsuper from '../../Takeaway/RunningOrder/RunOrd/items.js';
import ItemsPic from '../../Takeaway/RunningOrder/RunOrd/itemsPic.js';

// copied from Location/PopularPlaces START
import Flip from "react-reveal/Flip";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import { getPopularLocations } from "../../../services/popularLocations/actions";
import Geocode from "react-geocode";
import PopularPlaces from "../../Takeaway/Location/PopularPlaces";

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

import Location from "../../Takeaway/Location/";
import ShopperMenu from "./shopperMenu.js";

import { updateDeliveryUserInfo, logoutDeliveryUser } from "../../../services/Delivery/user/actions";



declare var INSTALL_APP_POPUP;


class ShopperPwa extends Component {

    constructor() {
        super();
        
        this.state = { 
            friends:[],
            invoiceSupermarketModal: false,
            Rider2Modal: false,
            supermarketOrders: [],//batches
            supermarketOrdersAccepted: [],//current batches
            cards: [],//all cards
            no_orders: false,
            // shopper_reward: localStorage.getItem("shopperReward"), //add extra rewards to shopper (in %)
            file: null,
            google_script_loaded: false,
        };

        this.innerRef = React.createRef();
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangePP = this.onChangePP.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { settings } = this.props;


        //set country if not set
        if (!localStorage.getItem("country")) {
            this.getCountry();
        }
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }

        const { delivery_user } = this.props;
        //update delivery guy info
        if(delivery_user.success){
            this.props.updateDeliveryUserInfo(delivery_user.data.id, delivery_user.data.auth_token);
        }

        //redirect from shopperPwa to /delivery/dashboard
        if(window.location.href.split('/').pop() == "shopperPwa"){
            window.location = "/delivery/dashboard";
        }

        this.supermarketOrders();
    }

    //Standort festlegen
    showModalAddress () {
        this.setState({
            AddressModal: true
        });
    }
    hideModalAddress = () => {
        this.setState({
            AddressModal: false
        });
    }

    //inteview
    showModalInterview () {
        this.setState({
            InterviewModal: true
        });
    }
    hideModalInterview = () => {
        this.setState({
            InterviewModal: false
        });
    }
    showModalQuestionnaire () {
        this.setState({
            QuestionnaireModal: true
        });
    }
    hideModalQuestionnaire = () => {
        this.setState({
            QuestionnaireModal: false
        });
    }

    //card
    showModalPaymentCard () {
        this.setState({
            PaymentCardModal: true
        });
    }
    hideModalPaymentCard = () => {
        this.setState({
            PaymentCardModal: false
        });
    }
    showModalCardShipping () {
        this.setState({
            CardShippingModal: true
        });
    }
    hideModalCardShipping = () => {
        this.setState({
            CardShippingModal: false
        });
    }
    showModalShippingConfirm () {
        this.setState({
            ShippingConfirmModal: true
        });
    }
    hideModalShippingConfirm = () => {
        this.setState({
            ShippingConfirmModal: false
        });
    }
    showModalCardDone () {
        this.setState({
            CardDoneModal: true
        });
    }
    hideModalCardDone = () => {
        this.setState({
            CardDoneModal: false
        });
    }
    showModalCardActivation () {
        this.setState({
            CardActivationModal: true
        });
    }
    hideModalCardActivation = () => {
        this.setState({
            CardActivationModal: false
        });
    }
    showModalCardActivationSuccess () {
        this.setState({
            CardActivationSuccessModal: true
        });
    }
    hideModalCardActivationSuccess = () => {
        this.setState({
            CardActivationSuccessModal: false
        });
    }
    showModalStripeAgreement () {
        this.setState({
            StripeAgreementModal: true
        });
    }
    hideModalStripeAgreement = () => {
        this.setState({
            StripeAgreementModal: false
        });
    }

    //passport
    showModalUploadPassport () {
        this.setState({
            UploadPassportModal: true
        });
    }
    hideModalUploadPassport = () => {
        this.setState({
            UploadPassportModal: false
        });
    }

    //work model
    showModalIndependent () {
        this.setState({
            IndependentModal: true
        });
    }
    hideModalIndependent = () => {
        this.setState({
            IndependentModal: false
        });
    }
    showModalDienstnehmer () {
        this.setState({
            DienstnehmerModal: true
        });
    }
    hideModalDienstnehmer = () => {
        this.setState({
            DienstnehmerModal: false
        });
    }

    // freunde schützen
    showModalRider2 () {
        this.setState({
            Rider2Modal: true
        });
    }
    hideModalRider2 = () => {
        this.setState({
            Rider2Modal: false
        });
    }
    showModalRider3 () {
        this.setState({
            Rider3Modal: true
        });
    }
    hideModalRider3 = () => {
        this.setState({
            Rider3Modal: false
        });
    }

    //benachrichtigungen
    showModalNotification () {
        this.setState({
            NotificationModal: true
        });
    }
    hideModalNotification = () => {
        this.setState({
            NotificationModal: false
        });
    }

    //freunde schützen
    helpFriends = async (fromName, fromTel, riderAddress) => {
        try {
            // check if its supported
            const supported = ('contacts' in navigator && 'ContactsManager' in window);
            if (!supported) {
                alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 77 oder höher.");
            } else {
                // properties to get from contact.
                const props = [
                    "name",
                    "tel",
                    // "email",
                    // "addresses",
                    // "icons"
                ];
                // multiple means you can select multiple contact at a time.
                const friends = await navigator.contacts.select(props, { multiple: true });

                if(friends.length > 0){
                    this.setState({friends})
                    this.hideModalRider2();
                    this.showModalRider3();
                    this.helpFriends2(fromName, fromTel, riderAddress);
                } 
            }        
        } catch (error) {
            console.error(error);   
            alert("error: "+error)
        }
    }  
    helpFriends2 = async (fromName, fromTel, riderAddress) => {
        var div = document.getElementById('riderStatus');
        div.innerHTML = '<span style="color:green">Wird geladen...</span>';

        let friends = JSON.stringify(this.state.friends);
        var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_riderFriends.php?riderName='+fromName+'&riderTel='+btoa(fromTel)+'&riderAddress='+riderAddress+'&friends='+friends;
        axios
        .get(url)
        .then(response => {
            setTimeout(function () {
                div.innerHTML = response.data.text;
            }, 100);
        })
        .catch(function(error) {
            console.log(error);
        });
    }  

    //work model
    onSozVersChanged = (e) => {
        this.setState({
            sozVersNr: e.currentTarget.value
        });
    }
    onStNrChanged = (e) => {
        this.setState({
            stNr: e.currentTarget.value
        });
    }

    //lang and country
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("prefix", "43");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("prefix", "49");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("prefix", "41");
                } 
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    //all batches
    supermarketOrders = () => {
        
        axios
        .post(SUPERMARKET_ORDER_ALL_URL, {
            country: localStorage.getItem("country"),
            limit: 50,
            plz: (localStorage.getItem("userSetAddress")?this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "plz"):"")
        })
        .then(response => {

            // console.log("response", response);
            
            if (response.data.length) {
                if (JSON.stringify(this.state.supermarketOrders) !== JSON.stringify(response.data)) {
                    this.setState({
                        supermarketOrders: response.data,
                        no_orders: false
                    });
                }

            } else {
                this.setState({
                    no_orders: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 
    
    //current batches
    getAddress = (address, extract, long=true) => {
        // var address = address || '';
        if(address){
            if(extract == "str+nr"){
                var res = address;
                var split = res.split(",");
                if(long) var res = split.slice(0, split.length - 1).join(",");
                else var res = split[0];
                return res;
            } else if(extract == "str"){
                var res = address;
                var split = res.split(",");
                var res = split[0];
                return res;
            }  else if(extract == "plz"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only plz without city
                if(res){
                    var split = res.split(" ");
                    var res = split[1];
                    return res;
                }
            } else if(extract == "city"){
                var res = address;
                var split = res.split(",");
                var res = split[1];
    
                //only city without plz
                if(res){
                    var split = res.split(" ");
                    var res = split[2];
                    return res;
                }
            } else {
                var res = address.split(", ");
                return res[0];
            }
        }
    }
    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }
    supermarketOrdersAccepted = () => {
        const { delivery_user } = this.props;
        axios
        .post(SUPERMARKET_ORDER_ALL_ACCEPTED_URL, {
            limit: 50,
            fromTel: delivery_user.data.phone,
            orderstatus_id: 2
        })
        .then(response => {


            // console.log("response", response);
            
            if (response.data.length) {
                if (JSON.stringify(this.state.supermarketOrdersAccepted) !== JSON.stringify(response.data)) {
                    this.setState({
                        supermarketOrdersAccepted: response.data,
                        
                        no_ordersAccepted: false
                    });
                }

            } else {
                this.setState({
                    no_ordersAccepted: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 

    //time
    showTime = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return  d.getHours() + ':' + d.getMinutes();
    }

    //benachrichtigungen
    notificationPrompt = () => {
        const { delivery_user } = this.props;

        if (delivery_user.success) {//we need just to check if user has tel in table
            if (firebase.messaging.isSupported()) {
                let handler = this.props.saveNotificationToken;
                messaging
                    .requestPermission()
                    .then(async () => {
                        const push_token = await messaging.getToken();
                        handler(push_token, delivery_user.data.id, delivery_user.data.auth_token);
                    })
                    .catch(function(err) {
                        console.log("Unable to get permission to notify.", err);
                    });
                navigator.serviceWorker.addEventListener("message", message => console.log(message));
            }
        }
    }

    // hamburger menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    // upload passport
    async onSubmit(e){
        e.preventDefault() 
        let res = await this.uploadFile(this.state.file);
        console.log(res.data);

        if(res.data.error === false){
            this.hideModalUploadPassport();
            var url = window.location.href;
            window.location = url;
        } else {
            var div = document.getElementById('progress');
            div.innerHTML = '<span style="color:red;">'+res.data.message+'</span>';
        }
    }
    onChangePP(e) {
        this.setState({file:e.target.files[0]})
        document.getElementById('uploadBtn').style.display = "block";
    }
    async uploadFile(file){
        const { delivery_user } = this.props;

        const formData = new FormData();
        
        formData.append('avatar',file)
        
        return  await axios.post('//'+window.location.hostname+'/php/shopperPwa_uploadPP.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id, formData, {
            headers: {
                'content-type': 'multipart/form-data'
            },
            onUploadProgress: progressEvent => {
                var loadTxt = 'Laden: ' + Math.round(progressEvent.loaded / progressEvent.total * 100) + '%';
                var div = document.getElementById('progress');
                // div.innerHTML = '<span style="color:green">Wird geladen...</span>';
                div.innerHTML = loadTxt;
            }
        });
    }

    // insert sozVersNr and stNr
    agreement = (nr, type) => {
        console.log("nr", nr);
        if(nr == null){
            alert("Bitte deine "+type+" eingeben.");
        } else {
            const { delivery_user } = this.props;

            var url = '//'+window.location.hostname+'/php/shopperPwa_agreementVerify.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&nr='+nr+'&type='+type;
            axios
            .get(url)
            .then(res => {
                if(res.data.error === false){
                    var url = window.location.href;
                    window.location = url;
                } else {
                    alert("Ein Fehler ist aufgetreten.");
                }   
            })
            .catch(function(error) {
                console.log(error);
            });
        }
    }
    onPaymentChanged = (e) => {
        this.setState({
            zahlungsmittel: e.currentTarget.value
        });

        if(e.currentTarget.value == 'independent'){
            this.showModalIndependent();
        } else {
            this.showModalDienstnehmer();
        }
    } 

    //order card
    onDeliveryNameChanged = (e) => {
        this.setState({
            fromName: e.currentTarget.value
        });
    } 
    soldoAddUser = () =>{
        const { delivery_user } = this.props;
        
        var div = document.getElementById('sendPrepaid');
        div.innerHTML = 'Laden...';

        var url = '//'+window.location.hostname+'/php/soldo/soldo.php?fromName='+this.state.fromName+'&fromAddress='+this.state.riderAddress+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=addUser';
        axios
        .get(url)
        .then(response => {
            console.log("addUser", response);

            if(response.data.includes("error")){
                alert(response.data);
            } else {
                this.hideModalShippingConfirm(); 
                this.showModalCardDone();
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 
    soldoActivateCard = () => {
        const { delivery_user } = this.props;

        axios
        .get('//'+window.location.hostname+'/php/soldo/soldo.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&cvc='+this.state.cvc+'&act=activateCard')
        .then(response => {
            console.log("soldoActivateCard response", response);
            this.hideModalCardActivation();
            this.showModalCardActivationSuccess();
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    onCvcChanged = (e) => {
        this.setState({
            cvc: e.currentTarget.value
        });
    }  
    handleGeoLocationClick2 = (results, address) => {// copied from Location/PopularPlaces START
        //check if hausnummer in address
        if(address){
            var num = address.split(" ").pop();
            var first = num.charAt(0);
            var last = num.charAt(num.length-1);

            if(first.match(/^-{0,1}\d+$/) && isNaN(last)){
                console.log(num + " is a number");
                document.getElementById('errorNum2').style.display = "none";
            } else if(isNaN(num)) {//if no number at end of string, return false
                console.log(num + " is not a number");
                this.searchInput.focus(); //sets focus to element
                var val = this.searchInput.value; //store the value of the element
                this.searchInput.value = ''; //clear the value of the element
                this.searchInput.value = address+' '; //set that value back. 
    
                document.getElementById('errorNum2').style.display = "block";
                return false;
            } else {
                console.log(num + " is a number");
                document.getElementById('errorNum2').style.display = "none";
            }
        }

        //get this result and store in a localstorage
        // localStorage.setItem("geoLocation", JSON.stringify(results[0]));

        //for mysql
        console.log("fromLat", results[0].geometry.location.lat());
        console.log("fromLng", results[0].geometry.location.lng());

        const userSetAddress = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng(),
            address: results[0].formatted_address
        };
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));

        this.setState({
            riderAddress: results[0].formatted_address,
            fromLat: results[0].geometry.location.lat(),
            fromLng: results[0].geometry.location.lng()
        });
    }
    listAllCards = () => {
        const { delivery_user } = this.props;

        var url = '//'+window.location.hostname+'/php/shopperPwa_listCards.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id;
        axios
        .get(url)
        .then(response => {
            var cardsRes = this.strstr(response.data, "{");

            // console.log("response", cardsRes);
            
            if (response.data.length && JSON.parse(cardsRes).id) {
                if (JSON.stringify(this.state.cards) !== JSON.stringify(cardsRes)) {
                    this.setState({
                        cards: cardsRes,
                        no_cards: false
                    });
                }

            } else {
                this.setState({
                    no_cards: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    strstr = (haystack, needle, bool) => {
        // Finds first occurrence of a string within another
        //
        // version: 1103.1210
        // discuss at: http://phpjs.org/functions/strstr    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   bugfixed by: Onno Marsman
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // *     example 1: strstr(‘Kevin van Zonneveld’, ‘van’);
        // *     returns 1: ‘van Zonneveld’    // *     example 2: strstr(‘Kevin van Zonneveld’, ‘van’, true);
        // *     returns 2: ‘Kevin ‘
        // *     example 3: strstr(‘name@example.com’, ‘@’);
        // *     returns 3: ‘@example.com’
        // *     example 4: strstr(‘name@example.com’, ‘@’, true);    // *     returns 4: ‘name’
        var pos = 0;
    
        haystack += "";
        pos = haystack.indexOf(needle); if (pos == -1) {
            return false;
        } else {
            if (bool) {
                return haystack.substr(0, pos);
            } else {
                return haystack.slice(pos);
            }
        }
    }

    // interview
    onAnswerChanged = (e) => {
        this.setState({
            answer: e.currentTarget.value
        });
    } 
    nextQuestion = (correctAnswer, q) => {
        if(this.state.answer == correctAnswer){
            document.getElementById("q"+q).style.display = "none"; 
            if(q == 4){
                this.hideModalInterview();
                this.hideModalQuestionnaire();

                const { delivery_user } = this.props;

                axios
                .get('//'+window.location.hostname+'/php/shopperPwa_interviewVerify.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id)
                .then(response => {
                    console.log("response", response);
                    if(response.data.error === false){
                        var url = window.location.href;
                        window.location = url;
                    } else {
                        alert("Ein Fehler ist aufgetreten.");
                    }  
                })
                .catch(function(error) {
                    console.log(error);
                });
            } else {
                document.getElementById("q"+parseFloat(q+1)).style.display = "block";
            }
        } else {
            alert("Antwort war leider falsch! Versuche es nochmals.");
        }
    }

    //install app
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    render() {
        const { user, delivery_user, logoutDeliveryUser } = this.props;
        const { supermarketOrders, supermarketOrdersAccepted, cards } = this.state;

        if (!delivery_user.success) {
            return (
                //redirect to account page
                window.location = "/delivery/login"
            );
        }

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }        
        
        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = false
        } else if(isMobileSafari){
            // var install = <span onClick={() => { this.showPrompt(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
            var install = false;
        } else {
            var install = true;
        }

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <Nav redirectUrl={"shopper"} logo={true} active_nearme={true} disable_back_button={true} loggedin={delivery_user.success} /> */}

                {/* header */}
                <div style={{width:"100%", padding: "10px 0", float:"left"}}><div style={{maxWidth: "468px", margin: "auto"}}>

                    {/* hamburger icon */}
                    <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                    <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>Dashboard</span>
                </div></div>

                <div style={{float:"left", width:"100%", background:"#F7F7F7"}}>
                    
                    {/* username */}
                    <div style={{background:"#fff", borderTop:"1px solid #F0F0F0", borderBottom:"1px solid rgba(0, 0, 0, 0.15)"}}>
                        <div style={{maxWidth: "468px", margin:"auto", padding:"12px"}}>
                            <div style={{fontWeight: 600, fontFamily:"open sans"}}>Hallo {delivery_user.data.name}</div>
                        </div>
                    </div>
                    
                    <div style={{maxWidth: "468px", margin: "auto", background:"#F7F7F7"}}>

                        <div style={{textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                            <div style={{float:"left", width:"100%", background:"#F8F6F8", padding: "12px"}}>

                                {/* view batches */}
                                {(delivery_user.data.passport_verified && delivery_user.data.agreement_verified && delivery_user.data.card_activated && delivery_user.data.interview_verified) && (
                                    <div style={{margin:"auto", maxWidth: "468px", background:"#fff", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", border: "1px solid rgba(0, 0, 0, 0.15)"}}>

                                        {supermarketOrders.length > 0 ? (

                                            localStorage.getItem("userSetAddress") ? (
                                                <div style={{borderBottom: "1px solid rgba(0, 0, 0, 0.15)", padding:"10px", cursor:"pointer", position:"relative"}} onClick={() =>  window.location = '/delivery/batches'}>
                                                    <span style={{marginRight:"10px", float:"left", width:"28px", height:"28px", background:"rgb(252, 128, 25)", color:"#fff", textAlign:"center", padding:"5px 10px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d"}}>€</span>
                                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>{supermarketOrders.length == 1 ? "Ein Auftrag verfügbar!":supermarketOrders.length+' Aufträge verfügbar!'}</div>
                                                    <div style={{color:"rgb(252, 128, 25)", fontSize:"12px", fontFamily:"open sans", fontWeight:400}}>Aufträge anzeigen</div>
                                                    <Ink duration="500" />
                                                </div>
                                            ) : (
                                                <div style={{borderBottom: "1px solid rgba(0, 0, 0, 0.15)", padding:"10px", cursor:"pointer", position:"relative"}} onClick={() => this.showModalAddress()}>
                                                    <span style={{marginRight:"10px", float:"left", color:"rgb(252, 128, 25)", fontSize:"20px", height:"40px"}}><FontAwesomeIcon icon={faExclamationTriangle} /></span>
                                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>Kann keine Aufträge empfangen</div>
                                                    <div style={{color:"rgb(252, 128, 25)", fontSize:"12px", fontFamily:"open sans", fontWeight:400}}>Überprüfen Sie die Standortberechtigungen</div>
                                                    <Ink duration="500" />
                                                </div>
                                            )
                                        ) : null}

                                        <a onClick={() => { this.showModalNotification(); }} style={{display:"block", position:"relative", cursor:"pointer", padding:"10px", overflow:"auto"}}><i className="si si-bubble" style={{marginRight:"10px"}} /> Benachrichtigungen zulassen <Ink duration="500" /></a>

                                        {/* Benachrichtigungen Zulassen */}
                                        <Modal show={this.state.NotificationModal} onHide={this.hideModalNotification} size="sm">
                                            <Modal.Header closeButton>
                                                <Modal.Title>Benachrichtigung</Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                                <div style={{fontFamily:"open sans", fontWeight:"300", marginBottom:"15px"}}>
                                                {/* Ich möchte neue Aufträge via Push Nachricht erhalten. */}
                                                Wenn es neue Aufträge in deiner Umgebung gibt, informieren wir dich per Push Nachricht.</div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button variant="secondary" onClick={this.hideModalNotification}> 
                                                    Schließen
                                                </Button> 
                                                <Button id="confirm" variant="primary" onClick={() => this.notificationPrompt()}> 
                                                    Zulassen
                                                </Button> 
                                            </Modal.Footer>
                                        </Modal>

                                    </div>
                                )}

                                {/* passport verification */}
                                {!delivery_user.data.passport_verified && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        <div style={{position: "absolute", right: "10px", fontFamily:"open sans", fontWeight:300}}><small>Schritt 1 von 4</small></div>
                                        <FontAwesomeIcon icon={faIdCard} style={{fontSize:"60px", marginBottom:"10px"}} />
                                        <div style={{fontWeight:"bold"}}>Ausweiskopie</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Bitte einen amtlichen Lichtbildausweis hochladen.</div>
                                        <div style={{color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}} onClick={() => this.showModalUploadPassport()}>Ausweis hochladen</div>
                                    </div>
                                )}

                                {/* upload passport */}
                                <Modal show={this.state.UploadPassportModal} onHide={this.hideModalUploadPassport} size="sm">
                                    <Modal.Header closeButton>
                                        <Modal.Title>Ausweiskopie</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>

                                    <form onSubmit={this.onSubmit}>
                                        <div style={{background:"rgb(248, 246, 248)", position:"relative", cursor:"pointer", width:"100%", marginBottom:"20px", padding:"20px 10px", border:"1px dashed #000", textAlign:"center"}} onClick={() => this.fileInput.click()}>
                                            <input 
                                            type="file" 
                                            accept="image/*" 
                                            onChange={ this.onChangePP } 
                                            ref={fileInput => this.fileInput = fileInput}
                                            style={{display:"none"}}
                                            />

                                            <FontAwesomeIcon icon={faUpload} style={{fontSize:"60px", marginBottom:"10px"}} />
                                            <div style={{fontWeight:600, marginBottom:"10px"}}>Ausweis auswählen</div>   

                                            <div style={{fontFamily:"open sans", fontWeight:300, margin:"10px"}}>Max Dateigröße: 5MB (jpeg, gif, png)</div>
                                            <div id="progress"></div>  
                                            <Ink duration="500" />
                                        </div>


                                        <div style={{display:"none"}} id="uploadBtn">
                                            <Button type="submit" style={{float:"right"}}>Weiter</Button>

                                            <Button variant="secondary" onClick={this.hideModalUploadPassport} style={{float:"right", margin:"0 10px"}}> 
                                                Schließen
                                            </Button> 
                                        </div>
                                    </form>

                                    </Modal.Body>
                                </Modal>

                                {/* agreement */}
                                {(!delivery_user.data.agreement_verified && delivery_user.data.passport_verified) && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", padding:"10px 0", position:"relative"}}>
                                        
                                        <div style={{position: "absolute", right: "10px", fontFamily:"open sans", fontWeight:300}}><small>Schritt 2 von 4</small></div>
                                        <div style={{padding:"0 15px", fontWeight:"bold", color:"#666"}}>Was passt zu dir?</div>
                                        <div style={{marginTop:"12px", fontSize:"16px"}}>
                                            <label className="payment_func">
                                                Selbstständig (max. {15*10*30} EUR / Monat)
                                                <input type="radio" name="workAs" value="independent" onChange={this.onPaymentChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                Geringfügig (max. 460 EUR / Monat)
                                                <input type="radio" name="workAs" value="dienstnehmer" onChange={this.onPaymentChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                        </div>

                                        {/* accept werksvertrag */}
                                        <Modal show={this.state.IndependentModal} onHide={this.hideModalIndependent}>
                                            <Modal.Header closeButton>
                                                <Modal.Title>Selbstständig</Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>

                                                {/* Steuernummer */}
                                                <input 
                                                style={{border:"1px solid #333", padding:"5px", width:"100%"}} 
                                                placeholder={"Steuernummer *"}
                                                onChange={this.onStNrChanged} 
                                                />

                                                <div style={{marginTop:"15px", fontSize:"12px", fontFamily:"open sans", fontWeight:300}}>Durch Anklicken von Bestätigen bestätigst Du die <span style={{textDecoration:"underline", cursor:"pointer"}} onClick={() => window.open("/pages/werksvertrag", '_blank') }>Vereinbarung mit Selbstständigen</span> und Deine eingegebenen Daten und stimmst unseren Datenschutzbestimmungen sowie AGB zu.</div>

                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button variant="secondary" onClick={this.hideModalIndependent}> 
                                                    Schließen
                                                </Button> 
                                                <Button variant="primary" onClick={() => this.agreement(this.state.stNr, "steuernummer")}> 
                                                    Bestätigen
                                                </Button> 
                                            </Modal.Footer>
                                        </Modal>

                                        {/* accept freien dienstvertrag */}
                                        <Modal show={this.state.DienstnehmerModal} onHide={this.hideModalDienstnehmer}>
                                            <Modal.Header closeButton>
                                                <Modal.Title>Geringfügig</Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>

                                                {/* Sozialversicherungsnummer */}
                                                <input 
                                                type="tel" 
                                                maxLength="10" 
                                                style={{border:"1px solid #333", padding:"5px", width:"100%"}} 
                                                placeholder={"Sozialversicherungsnummer *"}
                                                onChange={this.onSozVersChanged} 
                                                />

                                                <div style={{marginTop:"15px", fontSize:"12px", fontFamily:"open sans", fontWeight:300}}>Durch Anklicken von Bestätigen bestätigst Du die <span style={{textDecoration:"underline", cursor:"pointer"}} onClick={() => window.open("/pages/freier_dienstvertrag", '_blank')}>Vereinbarung mit freien Dienstnehmern</span> und Deine eingegebenen Daten und stimmst unseren Datenschutzbestimmungen sowie AGB zu.</div>

                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button variant="secondary" onClick={this.hideModalDienstnehmer}> 
                                                    Schließen
                                                </Button> 
                                                <Button variant="primary" onClick={() => this.agreement(this.state.sozVersNr, "sozialversicherungsnummer")}> 
                                                    Bestätigen
                                                </Button> 
                                            </Modal.Footer>
                                        </Modal>

                                    </div>
                                )}

                                {/* card order */}
                                {(!delivery_user.data.card_ordered && delivery_user.data.passport_verified && delivery_user.data.agreement_verified) && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        <div style={{position: "absolute", right: "10px", fontFamily:"open sans", fontWeight:300}}><small>Schritt 3 von 4</small></div>
                                        {/* <FontAwesomeIcon icon={faCreditCard} style={{fontSize:"60px", marginBottom:"10px"}} /> */}
                                        <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"40%", margin:"25px 0 10px"}} />
                                        <div style={{fontWeight:"bold"}}>Holen Sie sich Ihre Prepaid Karte</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Um zu shoppen, müssen Sie noch diesen Schritt ausführen.</div>
                                        <div style={{color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}} onClick={() => this.showModalPaymentCard()}>Karte bestellen</div>
                                    </div>
                                )}

                                {/* card activation */}
                                {(delivery_user.data.card_ordered && !delivery_user.data.card_activated && delivery_user.data.passport_verified && delivery_user.data.agreement_verified) && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        <div style={{position: "absolute", right: "10px", fontFamily:"open sans", fontWeight:300}}><small>Schritt 3 von 4</small></div>
                                        <FontAwesomeIcon icon={faKey} style={{fontSize:"60px", marginBottom:"10px"}} />
                                        {/* <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"40%", margin:"25px 0 10px"}} /> */}
                                        <div style={{fontWeight:"bold"}}>Aktivieren Sie Ihre Preipaid Karte</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Ihre Prepaid Karte kommt in 5-7 Tagen an. Bitte aktivieren Sie Ihre Karte, sobald diese per Post eingetroffen ist.</div>
                                        <div style={{color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}} onClick={() => this.showModalCardActivation()}>Karte aktivieren</div>
                                    </div>
                                )}

                                {/* Card activation */}
                                <Modal show={this.state.CardActivationModal} onHide={this.hideModalCardActivation} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                                        <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Zahlungskarte<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Auf der Rückseite Ihrer Karte finden Sie den 3-stelligen Code (CVC).</div></Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"0 25px 25px"}}>

                                        <input 
                                        style={{border:"1px solid rgba(0,0,0,0.15)", boxShadow:"0 2px 3px rgba(0,0,0,0.06)", height:"3.5rem", padding:"8px 14px", margin:"5px 0", width:"100%"}} 
                                        placeholder={"3-stelligen Code eingeben"}
                                        onChange={this.onCvcChanged}
                                        />

                                        {this.state.cvc ? (
                                            <a onClick={() => this.soldoActivateCard()} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Aktivieren<Ink duration="500" /></a>
                                        ) : (
                                            <a onClick={() => alert("Bitte den 3-stelligen CVC auf der Rückseite Ihrer Karte eingeben!")} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Aktivieren<Ink duration="500" /></a>
                                        )}

                                    </Modal.Body>
                                </Modal>

                                {/* Card activation success */}
                                <Modal show={this.state.CardActivationSuccessModal} onHide={this.hideModalCardActivationSuccess} size="sm">
                                    <Modal.Header style={{padding:0}}>
                                        {/* <Modal.Title></Modal.Title> */}
                                    </Modal.Header>
                                    <Modal.Body style={{padding:0}}>

                                        <div style={{position:"relative", background:"#F7F5F7", textAlign:"center", padding:"20px 25px"}}>
                                            <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"33%"}} />
                                            <FontAwesomeIcon icon={faCheckCircle} style={{position:"absolute", color:"green", bottom:"10px", left:"34%", borderRadius:"30px", background:"#fff", fontSize:"30px", padding:"1px"}} />
                                        </div>

                                        <div style={{padding:"25px"}}>
                                            {/* <div style={{fontSize:"16px"}}>Gratuliere! Ihre Zahlungskarte ist registriert.</div>
                                            <div style={{fontSize:"12px"}}>Verwenden Sie diese Karte, wenn Sie für eine Lieferservice123-Bestellung bezahlen müssen.</div> */}

                                            <div style={{fontSize:"16px"}}>Ihre Karte wurde zum aktivieren vorgemerkt.</div>
                                            <div style={{fontSize:"12px"}}>Sie erhalten eine SMS, sobald die Karte aktiviert ist.</div>

                                            <a onClick={() => { var url = window.location.href; window.location = url; }} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Okay<Ink duration="500" /></a>

                                            <div style={{fontSize:"10px", marginTop:"15px", color:"#999"}}>Die Dauer der Aktivierung variiert je nach Uhrzeit. Teilweise ist die Karte schon nach wenigen Minuten bereit, in anderen Fällen dauert es mehrere Stunden.</div>

                                        </div>

                                    </Modal.Body>
                                </Modal>

                                {/* We are mailing you a prepaid card  */}
                                <Modal show={this.state.PaymentCardModal} onHide={this.hideModalPaymentCard} size="sm">
                                    <Modal.Header closeButton>
                                        <Modal.Title style={{padding:"10px", fontSize:"22px", fontFamily:"open sans", fontWeight:600}}>Wir senden Ihnen eine Prepaid Karte</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>

                                        <div style={{marginBottom:"30px", textAlign:"center"}}>
                                            <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"50%", margin:"25px 0"}} /> <br/><br/>
                                            <div>Kaufen Sie ein mit dieser Prepaid Karte.</div>
                                        </div>

                                        <a onClick={() => {this.hideModalPaymentCard(); this.showModalCardShipping();}} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"10px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></a>

                                    </Modal.Body>
                                </Modal>

                                {/* Shipping information */}
                                <Modal show={this.state.CardShippingModal} onHide={this.hideModalCardShipping} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                                        <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Lieferadresse<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Wir senden Ihnen eine Prepaid Karte an:</div></Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"0 25px 25px"}}>

                                        <input 
                                        style={{border:"1px solid rgba(0,0,0,0.15)", boxShadow:"0 2px 3px rgba(0,0,0,0.06)", height:"3.5rem", padding:"8px 14px", margin:"5px 0", width:"100%"}} 
                                        placeholder={"Vorname des Empfängers"}
                                        onChange={this.onDeliveryNameChanged}
                                        value={this.state.fromName}
                                        />

                                        {this.state.google_script_loaded && (
                                            <GooglePlacesAutocomplete
                                                initialValue={localStorage.getItem("userSetAddress")?JSON.parse(localStorage.getItem("userSetAddress")).address:null}
                                                autocompletionRequest={{
                                                    componentRestrictions: {
                                                        //country: ['at'],
                                                        country: [localStorage.getItem("country")],
                                                    }
                                                }}
                                                loader={<img src="/assets/img/various/spinner.svg" className="location-loading-spinner" alt="loading" />}
                                                renderInput={props => (
                                                    <div style={{ position: "relative" }}>
                                                        <label style={{fontWeight:"normal", display:"block", marginTop:"10px"}}>
                                                            <input
                                                                {...props}
                                                                className="form-control search-input search-box"
                                                                placeholder={localStorage.getItem("searchAreaPlaceholder")}
                                                                // ref={input => {
                                                                //     if(input)
                                                                //     {
                                                                //         setTimeout(function(){
                                                                //             input.focus();
                                                                //         }, 300)
                                                                //     }
                                                                //     //this.searchInput = input;
                                                                // }}
                                                                // autoFocus
                                                                ref={input => {
                                                                    this.searchInput = input;
                                                                }}
                                                            />
                                                        </label>

                                                        {/* hausnummer info */}
                                                        <div id="errorNum2" style={{ display:"none", color: "#fff", position: "relative", background: "orange", width: "100%", padding: "10px 15px", zIndex: 1}}><FontAwesomeIcon icon={faExclamationTriangle} style={{marginRight:"5px"}} />Gib Deine Hausnummer an</div>
                                                        
                                                    </div>
                                                )}
                                                renderSuggestions={(active, suggestions, onSelectSuggestion) => (
                                                    <div className="location-suggestions-container">
                                                        {suggestions.map((suggestion, index) => (
                                                            <Flip top delay={index * 50} key={suggestion.id}>
                                                                <div
                                                                    className="location-suggestion"
                                                                    onClick={event => {
                                                                        onSelectSuggestion(suggestion, event);
                                                                        geocodeByPlaceId(suggestion.place_id)
                                                                            .then(results => this.handleGeoLocationClick2(results, suggestion.structured_formatting.main_text))
                                                                            .catch(error => console.error(error));
                                                                    }}
                                                                >
                                                                    
                                                                    <span className="location-main-name">{suggestion.structured_formatting.main_text}</span>
                                                                    <br />
                                                                    <span className="location-secondary-name">{suggestion.structured_formatting.secondary_text}</span>
                                                                </div>
                                                            </Flip>
                                                        ))}
                                                    </div>
                                                )}
                                            />
                                        )}

                                        <a onClick={() => {this.hideModalCardShipping(); this.showModalShippingConfirm();}} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Speichern<Ink duration="500" /></a>

                                    </Modal.Body>
                                </Modal>

                                {/* Shipping information confirm */}
                                <Modal show={this.state.ShippingConfirmModal} onHide={this.hideModalShippingConfirm} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                                        <Modal.Title style={{fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Lieferadresse<div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>Wir senden Ihnen eine Prepaid Karte an:</div></Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"0 25px 25px"}}>

                                        <div style={{overflow:"auto"}}>
                                            <div style={{fontWeight:"bold", float:"left", width:"calc(100% - 90px)"}}>
                                                {this.state.fromName}<br/>
                                                {this.getAddress(this.state.riderAddress, "str+nr", false)}<br/>
                                                {this.getAddress(this.state.riderAddress, "plz")} {this.getAddress(this.state.riderAddress, "city")}
                                            </div>
                                            <div onClick={() => { this.hideModalShippingConfirm(); this.showModalCardShipping(); }} style={{color:localStorage.getItem("storeColor"), float:"right"}}>Bearbeiten</div>
                                        </div>

                                        <a id="sendPrepaid" onClick={() => this.soldoAddUser()} style={{position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"15px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>An diese Adresse senden<Ink duration="500" /></a>

                                    </Modal.Body>
                                </Modal>

                                {/* Your card is on the way */}
                                <Modal show={this.state.CardDoneModal} onHide={this.hideModalCardDone} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 10px"}}>
                                        <Modal.Title style={{padding:"10px", fontSize:"30px", fontFamily:"open sans", fontWeight:600}}>Ihre Karte ist unterwegs!</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>

                                        <div style={{marginBottom:"30px", textAlign:"center"}}>
                                            <img src={'https://lieferservice123.com/assets/img/card-soldo.jpg'} style={{maxWidth:"50%", margin:"25px 0"}} /> <br/><br/>
                                            <div>Sobald Ihre Karte in 5-7 Tagen eintrifft, aktivieren Sie die Karte in der Shopper App.</div>
                                        </div>

                                        <a onClick={() => { var url = window.location.href; window.location = url; }} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"10px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Fertig<Ink duration="500" /></a>

                                    </Modal.Body>
                                </Modal>

                                {/* questionnaire */}
                                {(!delivery_user.data.interview_verified && delivery_user.data.passport_verified && delivery_user.data.agreement_verified && delivery_user.data.card_activated) && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        <div style={{position: "absolute", right: "10px", fontFamily:"open sans", fontWeight:300}}><small>Schritt 4 von 4</small></div>
                                        <FontAwesomeIcon icon={faQuestionCircle} style={{fontSize:"60px", marginBottom:"10px"}} />
                                        <div style={{fontWeight:"bold"}}>Fragebogen</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Bitte nehmen Sie sich Zeit die Fragen zu beantworten.</div>
                                        <div style={{color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}} onClick={() => this.showModalInterview()}>Starten</div>
                                    </div>
                                )}

                                {/* questionnaire */}
                                <Modal show={this.state.InterviewModal} onHide={this.hideModalInterview} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 0"}}>
                                        <Modal.Title style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600}}>Fragebogen</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"25px"}}>

                                        <div style={{marginBottom:"30px"}}>
                                            <div>Wir würden uns freuen, Sie besser kennenzulernen. Bitte beantworten Sie die folgenden Fragen, um mit der Anmeldung fortzufahren. Es dauert nur ein paar Minuten.</div>
                                        </div>

                                        <a onClick={() => this.showModalQuestionnaire()} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"10px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Starten<Ink duration="500" /></a>

                                    </Modal.Body>
                                </Modal>

                                {/* questionnaire */}
                                <Modal show={this.state.QuestionnaireModal} onHide={this.hideModalQuestionnaire} size="sm">
                                    <Modal.Header closeButton style={{padding:"25px 25px 0"}}>
                                        <Modal.Title style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600}}>Fragebogen</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"25px"}}>

                                        <div id="q1">
                                            <div style={{marginBottom:"30px"}}>
                                                <div>Ihr Kunde hat 1 Packung Bio Milch 2% bestellt, aber der Artikel ist nicht vorrätig. Was könnte ein guter Ersatz sein?</div>
                                            </div>

                                            <label className="payment_func">
                                                1 Liter Bio Milch 2% 
                                                <input type="radio" name="answer" value="a" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                1 Packung herkömmliche Milch 2% 
                                                <input type="radio" name="answer" value="b" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                2 halbe Packung Bio Milch 2%
                                                <input type="radio" name="answer" value="c" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                1 Packung Bio Vollmilch 2%
                                                <input type="radio" name="answer" value="d" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <a onClick={() => this.nextQuestion("c", 1)} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"30px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Nächste Frage<Ink duration="500" /></a>

                                        </div>

                                        <div id="q2" style={{display:"none"}}>
                                            <div style={{marginBottom:"30px"}}>
                                                <div>Die Bestellung, die Sie einkaufen, beinhaltet Eiscreme. Welche zusätzlichen Schritte sind mit diesem Artikel zu beachten?</div>
                                            </div>

                                            <label className="payment_func">
                                                Senden Sie dem Kunden eine SMS, um ihn darüber zu informieren, dass sein Eis während der Lieferung schmelzen kann
                                                <input type="radio" name="answer" value="a" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                Transportieren Sie es in einem Kühler
                                                <input type="radio" name="answer" value="b" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                Packen Sie es zusammen mit rohem Fleisch ein, um es kühl zu halten
                                                <input type="radio" name="answer" value="c" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                Holen Sie sich diesen Artikel zuerst beim Einkaufen
                                                <input type="radio" name="answer" value="d" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <a onClick={() => this.nextQuestion("b", 2)} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"30px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Nächste Frage<Ink duration="500" /></a>

                                        </div>

                                        <div id="q3" style={{display:"none"}}>
                                            <div style={{marginBottom:"30px"}}>
                                                <div>Ein vom Kunden bestellter Artikel ist nicht vorrätig. Was sagen Sie dem Kunden?</div>
                                            </div>

                                            <label className="payment_func">
                                                "Der Artikel, den Sie bestellt haben, ist nicht vorrätig. Was möchten Sie stattdessen?"
                                                <input type="radio" name="answer" value="a" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                "Der von Ihnen bestellte Artikel ist nicht vorrätig. Ich entschuldige mich für etwaige Unannehmlichkeiten."
                                                <input type="radio" name="answer" value="b" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                "Der von Ihnen bestellte Artikel ist nicht vorrätig. Ich habe den Artikel aus der Bestellung entfernt und der Bertrag wird zurückerstattet."
                                                <input type="radio" name="answer" value="c" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>
                                            <label className="payment_func">
                                                "Der von Ihnen bestellte Artikel ist nicht vorrätig. Ich habe ihn durch einen ähnlichen Artikel einer anderen Marke ersetzt. Ist das ok?"
                                                <input type="radio" name="answer" value="d" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <a onClick={() => this.nextQuestion("d", 3)} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"30px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Nächste Frage<Ink duration="500" /></a>

                                        </div>

                                        <div id="q4" style={{display:"none"}}>
                                            <div style={{marginBottom:"30px"}}>
                                                <div>Ein Kunde fragt nach der Rechnung. Was sagen Sie dem Kunden?</div>
                                            </div>

                                            <label className="payment_func">
                                                "Sie erhalten eine Online-Rechnung. Gemäß den Richtlinien von Lieferservice123 darf ich keine Rechnung übergeben."
                                                <input type="radio" name="answer" value="a" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <label className="payment_func">
                                                "Nein, weil wir die Preise erhöhen und einige Leute nicht wissen, wie unterschiedlich die Preise sind."
                                                <input type="radio" name="answer" value="b" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <label className="payment_func">
                                                "Die Rechnung ist im Sackerl. Danke für Ihren Einkauf bei Lieferservice123."
                                                <input type="radio" name="answer" value="c" onChange={this.onAnswerChanged} /> 
                                                <span className="checkmark"></span>
                                                <Ink duration="500" />
                                            </label>

                                            <a onClick={() => this.nextQuestion("a", 4)} style={{cursor:"pointer", position:"relative", display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"5px", padding:"10px", marginTop:"30px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Fertig<Ink duration="500" /></a>

                                        </div>

                                    </Modal.Body>
                                </Modal>

                                {/* select hours */}
                                {/* {delivery_user.data.passport_verified && delivery_user.data.agreement_verified ? <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px"}}>
                                    <FontAwesomeIcon icon={faCoins} style={{fontSize:"60px", marginBottom:"10px"}} />
                                    <div style={{fontWeight:"bold"}}>Jetzt Geld verdienen</div>
                                    <div style={{color:"#999", marginTop:"10px"}}>Planen Sie Ihren Tag, indem Sie Stunden auswählen.</div>
                                    <div style={{textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Stunden Auswählen</div>
                                </div> : null} */}

                                {/* current batches */}
                                {supermarketOrdersAccepted.map((row, index) => {
                                    return (
                                        <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto"}}>

                                            <div style={{fontWeight:600, fontFamily:"open sans", padding:"10px", borderBottom: "1px solid rgba(0, 0, 0, 0.15)"}}>
                                                <img src={'https://lieferservice123.com/'+row.shopImage} style={{width: "30px", height: "30px", padding: "1px", borderRadius: "30px", border: "1px solid #ccc", marginRight:"5px"}} /> Aktueller Auftrag
                                                {row.startNavigateDate ? (
                                                    ": Lieferung"
                                                ) : row.startCheckoutDate ? (
                                                    ": Checkout"
                                                ) : row.startShoppingDate ? (
                                                    ": Shopping"
                                                ) : row.acceptedDate ? (
                                                    ": Anfahrt"
                                                ) : null}
                                            </div>

                                            <div style={{overflow:"auto", padding:"10px"}}>
                                                <div style={{float:"left", width:"calc(100% - 90px)", fontSize:"12px"}}>
                                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>Store Location</div>
                                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>{row.shopName}</div>
                                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>{this.getAddress(row.shopAddress, "str+nr")}</div>
                                                </div>
                                                <div style={{float:"right", marginTop:"10px", color:localStorage.getItem("storeColor"), border:"1px solid orange", padding:"5px 8px", borderRadius:"3px", fontSize:"10px"}} onClick={() => this.navigation(row.shopLat, row.shopLng)}>Karte anzeigen</div>
                                            </div>

                                            <div style={{overflow:"auto", padding:"10px", fontSize:"12px", padding:"0 10px"}}>
                                                <div style={{float:"left", width:"50%"}}>
                                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>Kundenname</div>
                                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>{row.fromName}</div>
                                                </div>
                                                <div style={{float:"right"}} onClick={() => this.navigation(row.shopLat, row.shopLng)}>
                                                    <div style={{fontWeight:600, fontFamily:"open sans"}}>Fällig um</div>
                                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>{this.showTime(row.added+3600)} Uhr oder früher</div> 
                                                </div>
                                            </div>

                                            <a href={row.startNavigateDate ? (
                                                    '/viewBatch/'+row.id+'?act=navigateCustomer'
                                                ) : row.startCheckoutDate ? (
                                                    '/viewBatch/'+row.id+'?act=receipt'
                                                ) : row.startShoppingDate ? (
                                                    '/viewBatch/'+row.id+'?act=startShopping'
                                                ) : row.acceptedDate ? (
                                                    '/viewBatch/'+row.id+'?act=navigateStore'
                                                ) : null} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", margin:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Zurück zum Auftrag</a>
                                        </div>
                                    )
                                })}

                                {/* Covid19 */}
                                {(!localStorage.getItem("hideCovid") && delivery_user.data.passport_verified && delivery_user.data.agreement_verified && delivery_user.data.card_activated && delivery_user.data.interview_verified) && (
                                    <div id="covid" style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        <FontAwesomeIcon icon={faTimes}  onClick={() => { document.getElementById("covid").style.display = "none"; localStorage.setItem("hideCovid", true); }} style={{position:"absolute", right:"10px", top:"10px"}} />
                                        <FontAwesomeIcon icon={faHeartbeat} style={{fontSize:"60px", marginBottom:"10px", color:"pink"}} />
                                        <div style={{fontWeight:"bold"}}>Beim Einkaufen gesund & sicher bleiben</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Untersütze die Gesundheit & Sicherheit unserer Community.</div>
                                        <a href={'/pages/covid19'} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Mehr erfahren</a>
                                    </div>
                                )}

                                {/* app install */}
                                {(install && delivery_user.data.passport_verified && delivery_user.data.agreement_verified && delivery_user.data.card_activated && delivery_user.data.interview_verified) && (
                                    <div id="covid" style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px", position:"relative"}}>
                                        {/* <FontAwesomeIcon icon={faTimes}  onClick={() => { document.getElementById("covid").style.display = "none"; localStorage.setItem("hideCovid", true); }} style={{position:"absolute", right:"10px", top:"10px"}} /> */}
                                        <FontAwesomeIcon icon={faCloudDownloadAlt} style={{fontSize:"60px", marginBottom:"10px", color:"#689F38"}} />
                                        <div style={{fontWeight:"bold"}}>Shopper App</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Bitte installiere die App, um Aufträge noch schneller anzunehmen.</div>
                                        <a onClick={() => this.showModalBerechtigungen()} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>App installieren</a>
                                    </div>
                                )}

                                {/* app-vorteile */}
                                <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen} style={{background:"rgba(51,51,51, 0.85)"}}>
                                    
                                    <Modal.Header closeButton>
                                        <Modal.Title>App-Vorteile</Modal.Title>
                                    </Modal.Header>

                                    <Modal.Body style={{padding:"0 25px 10px"}}>
                                        <div style={{marginTop:"12px"}}>
                                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Shopper App auf Desktop installieren.</div>

                                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Push-Nachrichten erhalten.</div>

                                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                                        </div>

                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                                            Schließen
                                        </Button> 
                                        <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                                            Weiter
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                                {/* app-installieren */}
                                <Modal show={this.state.installModal} onHide={this.hideModalInstall} style={{background:"rgba(51,51,51, 0.85)"}}>
                                    
                                    <Modal.Header closeButton>
                                        <Modal.Title>Installieren</Modal.Title>
                                    </Modal.Header>

                                    <Modal.Body style={{padding:"0 25px 10px"}}>
                                        <div style={{marginTop:"12px"}}>
                                            Klicke auf hinzufügen um die Shopper App auf deinen Bildschirm zu installieren.
                                        </div>

                                        <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                                            Schließen
                                        </Button> 
                                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                                            Hinzufügen
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                                {/* Freunde schützen */}
                                {(delivery_user.data.passport_verified && delivery_user.data.agreement_verified && delivery_user.data.card_activated && delivery_user.data.interview_verified) && (
                                    <div style={{background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", maxWidth: "468px", margin:"15px auto", textAlign:"center", padding:"10px"}}>
                                        <FontAwesomeIcon icon={faShoppingBag} style={{fontSize:"60px", marginBottom:"10px", color:"rgb(72, 98, 112"}} />
                                        <div style={{fontWeight:"bold"}}>Freunde schützen</div>
                                        <div style={{color:"#999", marginTop:"10px"}}>Übernehmen Sie Einkäufe von Freunden und Familie.</div>
                                        <div onClick={() => { this.showModalRider2(); }} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Mehr erfahren</div>
                                    </div>
                                )}

                                {/* freunde schützen */}
                                <Modal show={this.state.Rider2Modal} onHide={this.hideModalRider2} size="sm">
                                    <Modal.Header closeButton>
                                        <Modal.Title>Freunde schützen</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"0 25px 10px"}}>

                                        <div style={{marginBottom:"10px"}}>Möchtest Du auch Einkäufe von Freunden übernehmen, dann klicke bitte auf Kontakte auswählen. </div>

                                        <Button variant="primary" onClick={() => this.helpFriends(delivery_user.data.name, delivery_user.data.phone, this.state.riderAddress)}> 
                                            Kontakte auswählen
                                        </Button> 

                                        <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                            Je mehr Freunde du auswählst, desto weniger werden sich beim Einkaufen infizieren.
                                        </div>

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={this.hideModalRider2}> 
                                            Überspringen
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                                {/* freunde schützen */}
                                <Modal show={this.state.Rider3Modal} onHide={this.hideModalRider3} size="sm">
                                    <Modal.Header closeButton>
                                        <Modal.Title>Freunde schützen</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{padding:"0 25px 10px"}}>

                                        <div id="riderStatus" style={{marginBottom:"10px"}}></div>

                                        <Button variant="primary" onClick={() => this.helpFriends(delivery_user.data.name, delivery_user.data.phone, this.state.riderAddress)}> 
                                            Mehr Kontakte auswählen
                                        </Button> 

                                        <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                            Je mehr Freunde du auswählst, desto weniger werden sich beim Einkaufen infizieren.
                                        </div>

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={this.hideModalRider3}> 
                                            Schließen
                                        </Button>
                                    </Modal.Footer>
                                </Modal>

                            </div>
                            
                        </div>

                    </div>

                    {/* links */}
                    {window.innerWidth <= 468 && (
                        <div style={{width:"100%", float:"left"}}>
                            <a href={'/delivery/earnings'} style={{display:"block", position:"relative", cursor:"pointer", background:"#fff", borderTop: "1px solid rgba(0, 0, 0, 0.15)", padding:"10px", overflow:"auto"}}><i className="si si-credit-card" style={{marginRight:"10px"}} /> Einnahmen <Ink duration="500" /></a>

                            <a onClick={() => this.showModalAddress()} style={{display:"block", position:"relative", cursor:"pointer", background:"#fff", borderTop: "1px solid rgba(0, 0, 0, 0.15)", padding:"10px", overflow:"auto"}}><i className="si si-location-pin" style={{marginRight:"10px"}} /> Standort festlegen <Ink duration="500" /></a>

                            {/* Standort festlegen */}
                            <Modal show={this.state.AddressModal} onHide={this.hideModalAddress}>
                                <Modal.Header closeButton><Modal.Title>Standort festlegen</Modal.Title></Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Lieferoptionen und -geschwindigkeit können, abhängig von der Lieferadresse, variieren</div>
                                    <Location redirect={window.location.pathname.replace("?install=1", "")} />
                                </Modal.Body>
                            </Modal>

                            <a onClick={() => logoutDeliveryUser(delivery_user)} style={{display:"block", position:"relative", cursor:"pointer", background:"#fff", borderTop: "1px solid rgba(0, 0, 0, 0.15)", borderBottom: "1px solid rgba(0, 0, 0, 0.15)", padding:"10px", overflow:"auto"}}><i className="si si-logout" style={{marginRight:"10px"}} /> Logout <Ink duration="500" /></a>
                        </div>
                    )}

                    <div className="mb-200" style={{width: "100%", float: "left", background:"#F7F7F7"}}></div>
                
                </div> 

                {/* agb */}
                <Modal show={this.state.AGBModal} onHide={this.hideModalAGB} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>AGB</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <iframe src={'//'+window.location.hostname+'/php/footer/agb.php'} style={{width:"100%", height:(window.innerHeight / 1.5)}} frameBorder="0" />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalAGB}> 
                            Schließen
                        </Button> 
                    </Modal.Footer>
                </Modal>

                {/* shopper menu */}
                <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                    <Modal.Header closeButton style={{background:"#fff"}}>
                        <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                            <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                            <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                        <ShopperMenu />
                    
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { getSettings, saveNotificationToken, getSingleLanguageData, updateDeliveryUserInfo, logoutDeliveryUser }
)(ShopperPwa);