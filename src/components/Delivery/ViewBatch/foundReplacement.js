import React, { Component } from "react";
import { Modal, Button } from 'react-bootstrap';
import { connect } from "react-redux";
import axios from "axios";

class FoundReplacement extends Component{

    constructor() {
        super();
        this.inputRef = React.createRef();
    }

    state = {
        found_replacement: 0,
    };

    componentDidMount() {  

        setTimeout(() => {
            // console.log(this.inputRef);
            this.inputRef.current.focus();
        }, 100)
    }

    onFoundChanged = (e) => {
        this.setState({
            found_replacement: e.currentTarget.value
        });
    }

    foundReplacement = (supermarket_orders_id, id, quantity, found, replacement, found_replacement, old_replacement, old_replacement_found) => {
        console.log('replace {"id":"'+id+'","quantity":'+quantity+'} with {"id":"'+id+'","quantity":'+quantity+',"found":0,"replacement":"'+replacement+'","found_replacement":'+found_replacement+'}');

        if(found == null){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundReplacement.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&replacement='+replacement+'&found_replacement='+found_replacement;
        } else if(old_replacement) {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundReplacement.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found+'&replacement='+replacement+'&found_replacement='+found_replacement+'&old_replacement='+old_replacement+'&old_replacement_found='+old_replacement_found;
        } else {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundReplacement.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found+'&replacement='+replacement+'&found_replacement='+found_replacement;
        }
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    render(){
        let {id, row, replaceWith, currencyFormat} = this.props;
        replaceWith = replaceWith || {};
        // console.log("this.props", this.props);
        // console.log("replaceWith", replaceWith);

        const style = {
            float:"right", 
            width:"calc(100% - 50px)"
        }

        
        return(

            <div>

                
                {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"40px", float:"left"}} alt="Bild wird geladen..." />}
                <div style={row.image&&style}>
                    <div style={{color:localStorage.getItem("storeColor"), fontSize:"12px"}}>Ersetzen:</div>
                    <div>{row.marke}</div>
                    <div style={{fontSize:"13px"}}><b>{row.quantity}x</b> {row.name}</div>
                    {row.unit ? (
                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                    ) : row.grammageBadge ? (
                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                    ) : (
                        null
                    )}
                    <div><span style={{color:"#999", fontSize:"12px"}}>{currencyFormat}{row.price.toFixed(2)}</span></div>
                </div>

                
                <div style={{width:"100%", float:"left", marginTop:"15px"}}>
                    {row.image && <img src={"https://lieferservice123.com"+replaceWith.image} style={{width:"40px", float:"left"}} alt="Bild wird geladen..." />}
                    <div style={row.image&&style}>
                        <div style={{color:localStorage.getItem("storeColor"), fontSize:"12px"}}>Durch:</div>
                        <div>{replaceWith.marke}</div>
                        <div style={{fontSize:"13px"}}><b>?</b> {replaceWith.name}</div>
                        {replaceWith.unit ? (
                            <div><span style={{color:"#999", fontSize:"12px"}}>{replaceWith.unit}</span></div> 
                        ) : replaceWith.grammageBadge ? (
                            <div><span style={{color:"#999", fontSize:"12px"}}>{replaceWith.grammageBadge}</span></div>
                        ) : (
                            null
                        )}
                        <div><span style={{color:"#999", fontSize:"12px"}}>{currencyFormat}{replaceWith.price.toFixed(2)}</span></div>
                    </div>
                </div>

                <div style={{width:"100%", float:"left", color:"#666", fontSize:"13px", fontWeight:300, fontFamily:"open sans", marginTop:"40px", marginBottom:"20px"}}>Wie viel hast du gefunden?</div>

                
                <input 
                    autoFocus="true"
                    type="tel"
                    style={{width:"calc(100% - 40px)", border:"none", textAlign:"right", fontSize:"20px", color:"rgb(72, 98, 112)"}}
                    // value="inputtest"
                    onChange={this.onFoundChanged} 
                    ref={this.inputRef} 
                />
                <div style={{width:"40px", float:"right", textAlign:"center", fontSize:"20px"}}>/ {row.quantity}</div>

                <div style={{float:"left", width:"100%", marginTop:"15px"}}>
                    <button
                        onClick={() => (this.state.found_replacement > 0 ? this.foundReplacement(id, row.id, row.quantity, row.found, replaceWith.id, this.state.found_replacement, row.replacement, row.found_replacement) : alert("Bitte Menge eingeben!"))}
                        type="button"
                        style={{padding:"10px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                    >
                        Produkt gefunden
                        {/* <Ink duration="500" /> */}
                    </button>
                </div>

            </div>

        )
    }
}


const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(FoundReplacement);

