import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";

import { getSettings } from "../../../services/settings/actions";
import { connect } from "react-redux";


//bootstrap
import { Modal, Button } from 'react-bootstrap';

import { SUPERMARKET_ORDER_URL } from "../../../configs/index";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faDirections } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faReceipt } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faComment } from '@fortawesome/fontawesome-free-regular'
import { faComments } from '@fortawesome/fontawesome-free-regular';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faCamera } from '@fortawesome/free-solid-svg-icons';

import Itemsfromsuper from '../../Takeaway/RunningOrder/RunOrd/items.js';
import ItemsPic from '../../Takeaway/RunningOrder/RunOrd/itemsPic.js';
import ItemsPicBig from './itemsPic.js';

import Map from "../../Takeaway/RunningOrder/Map";

import Webcam from "react-webcam";
// import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
// import 'react-html5-camera-photo/build/css/index.css';

import style from "./style.css";
import { Redirect } from "react-router";

import { walletTransaction, createRefund } from "../../../services/Delivery/deliveryprogress/actions";

class ViewBatch extends Component {

    constructor(props) {
        super(props);

        this.webcamRef = React.createRef();
        this.leftAtDoorRef = React.createRef();

        this.state = { 
            row: [],
            google_script_loaded: false,
            PhoneOrderModal: true
        };
    }

    // hamburger menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    showModalContact () {
        this.setState({
            ContactModal: true
        });
    }
    hideModalContact = () => {
        this.setState({
            ContactModal: false
        });
    }

    showModalPhoneOrder () {
        this.setState({
            PhoneOrderModal: true
        });
    }
    hideModalPhoneOrder = () => {
        this.setState({
            PhoneOrderModal: false
        });
    }

    //delivery
    showModalDelivery (id) {
        this.setState({
            DeliveryModal: true
        });
    }
    hideModalDelivery = () => {
        this.setState({
            DeliveryModal: false
        });
    }
    showModalConfirmDeliveryMethod () {
        this.setState({
            ConfirmDeliveryMethodModal: true
        });
    }
    hideModalConfirmDeliveryMethod = () => {
        this.setState({
            ConfirmDeliveryMethodModal: false
        });
    }
    showModalCompleteDelivery () {
        this.setState({
            CompleteDeliveryModal: true
        });
    }
    hideModalCompleteDelivery = () => {
        this.setState({
            CompleteDeliveryModal: false
        });
    }
    showModalLeftAtDoorPhoto () {
        this.setState({
            LeftAtDoorPhotoModal: true
        });
    }
    hideModalLeftAtDoorPhoto = () => {
        this.setState({
            LeftAtDoorPhotoModal: false
        });
    }
    showModalUploadLeftAtDoorPhoto () {
        this.setState({
            UploadLeftAtDoorPhotoModal: true
        });
    }
    hideModalUploadLeftAtDoorPhoto = () => {
        this.setState({
            UploadLeftAtDoorPhotoModal: false
        });
    }

    showModalEarningsInfo () {
        this.setState({
            EarningsInfoModal: true
        });
    }
    hideModalEarningsInfo = () => {
        this.setState({
            EarningsInfoModal: false
        });
    }

    //receipt
    showModalReceiptPhoto () {
        this.setState({
            ReceiptPhotoModal: true
        });
    }
    hideModalReceiptPhoto = () => {
        this.setState({
            ReceiptPhotoModal: false
        });
    }
    showModalUploadReceiptPhoto () {
        this.setState({
            UploadReceiptPhotoModal: true
        });
    }
    hideModalUploadReceiptPhoto = () => {
        this.setState({
            UploadReceiptPhotoModal: false
        });
    }

    componentDidMount() {
        this.supermarketOrders();

        this.props.getSettings();


        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }
    }

    supermarketOrders = () =>{
        axios
        .post(SUPERMARKET_ORDER_URL, {
            id: window.location.href.split("/").pop()
        })
        .then (newArray => {
            // console.log("row", newArray.data);
            this.setState({
                row: newArray.data
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    itemsPic = (row, style="small") =>{
        if(style == "small"){
            if(row.items == null || row.items.length === 0) return null;        
            return <ItemsPic supermarket_orders={row} />
        } else {
            if(row.items == null || row.items.length === 0) return null;        
            return <ItemsPicBig supermarket_orders={row} />
        }
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }
    
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    sendSMS = (tel) => {
        var ua = navigator.userAgent.toLowerCase();
        var href;

        ////stackoverflow.com/questions/6480462/how-to-pre-populate-the-sms-body-text-via-an-html-link
        // if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1){
        //     href = 'sms:/' + tel;
        // } else{
        //     href = 'sms://' + tel;
        // }

        href = 'sms:' + tel;

        window.open(href);
    }

    acceptBatch = (id, cat) =>{
        const { delivery_user } = this.props;

        var div = document.getElementById('laden');
        div.innerHTML = '<span>Laden...</span>';

        var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_einkaufAccept.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&id='+id;
        axios
        .get(url)
        .then(response => {
            if(response.data.success){
                // alert("Auftrag erfolgreich übernommen. Eine SMS-Bestätigung wurde an deine Mobilnummer gesendet.");
                //redirect to running order page
                //this.context.router.history.push("/running-order-s/" + response.data.id);

                if(cat == "restaurant") {
                    window.location = '/viewBatch/'+id+'?act=phoneOrder'
                } else {
                    window.location = '/viewBatch/'+id+'?act=navigateStore'
                }
            } else {
                alert("Sorry! Auftrag wurde bereits von einem anderen User übernommen.");
            }   
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    makeTotalUnits = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            sum = sum + parseFloat(row.quantity)
        });
        return sum;
    }

    request = (id, unique_order_id, act) => {
        const { delivery_user } = this.props;

        if(act == "startShopping"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=startShopping';
            var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';
        } 
        else if(act == "startNavigate"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=startNavigate';
        }  
        else if(act == "startCheckout"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=startCheckout';
        }     
        else if(act == "markAsDelivered"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=markAsDelivered';
            var redirect = '/shopperPwa?act=dashboard';
        }   
        axios
        .get(url)
        .then(response => {
            if(act == "markAsDelivered"){
                this.__acceptToDeliver(id, unique_order_id);
            }
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            if(redirect){
                window.location = redirect;
            }
        }, 100);
    }

    __acceptToDeliver = (id, unique_order_id) => {
        const { delivery_user } = this.props;

        this.props.walletTransaction(
            delivery_user.data.auth_token,
            delivery_user.data.id,
            id,
            'deposit',
            "Payment for order: "+unique_order_id
        );
        this.setState({ loading: true });
    };


    makeTotalQuantity = (data = [], tab) => {
        let sum = 0;
        data.forEach((row) => {

            if(tab == "todo" && row.found == null){
                sum = sum + 1
            } 
            else if(tab == "review" && row.found == 0){
                sum = sum + 1
            } 
            else if(tab == "replacements" && row.found == 0 && row.replacement){
                sum = sum + 1
            } 
            else if(tab == "refunds" && row.found == 0 && row.replacement == null){
                sum = sum + 1
            } 
            else if(tab == "done" && row.found > 0){
                sum = sum + 1
            }
        });
        return sum;
    }

    showTime = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return  d.getHours() + ':' + d.getMinutes();
    }

    capture() {
        const receiptSrc = this.webcamRef.current.getScreenshot();
        // console.log("receiptSrc", receiptSrc);

        this.setState({
            receiptSrc: receiptSrc
        });

        this.showModalUploadReceiptPhoto();
    }

    uploadReceipt = (id) => {
        const { delivery_user } = this.props;

        var url = '//'+window.location.hostname+'/php/shopperPwa_uploadReceipt.php';
        var fd = new FormData();
        fd.append('user_id', delivery_user.data.id);
        fd.append('auth_token', btoa(delivery_user.data.auth_token));
        fd.append('id', id);
        fd.append('receiptSrc', this.state.receiptSrc);
        axios
        .post(url, fd)
        .then(res => {
            if(res.data.error === false){
                window.location = '/viewBatch/'+id+'?act=navigateCustomer'
            } else {
                alert("Ein Fehler ist aufgetreten.");
            }   
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    uploadLeftAtDoor = (id) => {
        const { delivery_user } = this.props;

        var url = '//'+window.location.hostname+'/php/shopperPwa_uploadLeftAtDoor.php';
        var fd = new FormData();
        fd.append('user_id', delivery_user.data.id);
        fd.append('auth_token', btoa(delivery_user.data.auth_token));
        fd.append('id', id);
        fd.append('leftAtDoorSrc', this.state.leftAtDoorSrc);
        axios
        .post(url, fd)
        .then(res => {
            if(res.data.error === false){
                //window.location = '/viewBatch/'+id+'?act=navigateCustomer'
            } else {
                alert("Ein Fehler ist aufgetreten.");
            }   
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    handedToCustomer = () => {
        this.hideModalConfirmDeliveryMethod();
        document.getElementById('confirmDeliveryMethod').style.display = "none";
        document.getElementById('markAsDelivered').style.display = "block";
    }

    captureLeftAtDoor() {
        const src = this.leftAtDoorRef.current.getScreenshot();
        // console.log("leftAtDoorSrc", src);

        this.setState({
            leftAtDoorSrc: src
        });

        this.showModalUploadLeftAtDoorPhoto();
    }

    makeTotalCheckout = (data = []) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return sum - (sum*0.3);
    }

    makeTotalRefunds = (data = [], subtotal) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return subtotal - sum;
    }

    chargeCard = (id, total) => {
        const { delivery_user } = this.props;

        var div = document.getElementById('chargePrepaid');
        div.innerHTML = 'Laden...';

        var url = '//'+window.location.hostname+'/php/soldo/soldo.php?auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&total='+total+'&id='+id+'&act=addFunds';
        axios
        .get(url)
        .then(response => {
            console.log("addFunds", response);

            if(response.data.includes("error")){
                alert(response.data);
            } else {
                document.getElementById('chargePrepaid').style.display = "none";
                document.getElementById('chargePrepaidSuccess').style.display = "block";
                document.getElementById('checkoutWeiter').style.display = "block";
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    refund = (id, unique_order_id, refunds) => {
        if(refunds){
            console.log(refunds+' will be refunded to user');

            this.props.createRefund(
                id,
                unique_order_id, 
                refunds,
                ( res ) => {
                    console.log("createRefund", res.data);
                }
            )

            setTimeout(function () {
                window.location = "/viewBatch/"+id+"?act=deliveryCanceled";
            }, 2000);
        }
    }

    checkout = (id) => {
        const { delivery_user } = this.props;

        var url = '//'+window.location.hostname+'/php/shopperPwa.php?id='+id+'&auth_token='+btoa(delivery_user.data.auth_token)+'&user_id='+delivery_user.data.id+'&act=startCheckout';  
        axios
        .get(url)
        .then(response => {
            window.location = '/viewBatch/'+id+'?act=receipt'
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    heading = (type, fromName) => {
        if(this.getUrlParameter('act') == 'phoneOrder'){
            var headingTxt = "Telefonbestellung"
            // var headingTxt = "Abholauftrag"
            var headerStyle = {
                background:"#fff", width:"100%", float:"left", paddingTop: "10px"
            };
        } else if(this.getUrlParameter('act') == 'navigateStore'){
            var headingTxt = "Fahre zum Shop"
            var headerStyle = {
                background:"#fff", width:"100%", float:"left", paddingTop: "10px"
            };
        } else if(this.getUrlParameter('act') == 'startShopping'){
            var headingTxt = <span style={{fontSize:"14px"}}>{fromName}'s Bestellung</span>
            var headerStyle = {
                background:"#fff", width:"100%", float:"left", paddingTop: "10px"
            };
        } else if(this.getUrlParameter('act') == 'reviewChanges'){
            var headingTxt = 'Änderungen Prüfen'
            var headerStyle = {
                background:"#D83584", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'checkout'){
            var headingTxt = "Kasse"
            var headerStyle = {
                background:"#3BA8D0", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'receipt'){
            var headingTxt = "Quittungsprüfung"
            var headerStyle = {
                background:"#DEA0E2", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'receiptPhoto'){
            var headingTxt = "Quittungsprüfung"
            var headerStyle = {
                background:"#DEA0E2", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        }  else if(this.getUrlParameter('act') == 'navigateCustomer'){
            var headingTxt = "Lieferung"
            var headerStyle = {
                background:"#EC837D", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'deliveryCanceled'){
            var headingTxt = "Auftrag storniert"
            var headerStyle = {
                background:localStorage.getItem("storeColor"), width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        }  

        if(type == "headingTxt") return headingTxt;
        else if(type == "headerStyle") return headerStyle;
    }

    render() {

        const { row } = this.state;

        const { delivery_user } = this.props;
        if (!delivery_user.success) {
            return (
                //redirect to account page
                <Redirect to={"/delivery/login"} />
            );
        }



        // delivery done
        if (row.markDeliveredDate && this.getUrlParameter("act") !== "deliveryDone") {
            return (
                <Redirect to={"/viewBatch/"+row.id+"?act=deliveryDone"} />
            );
        } 

        // delivery canceled
        if (row.orderstatus_id == 6 && this.getUrlParameter("act") == undefined) {
            return (
                <Redirect to={"/viewBatch/"+row.id+"?act=deliveryCanceled"} />
            );
        } 

        // navigate to customer
        if (row.startNavigateDate && this.getUrlParameter("act") == "startShopping") {
            return (
                <Redirect to={"/viewBatch/"+row.id+"?act=navigateCustomer"} />
            );
        } 

        // start shopping
        if (row.acceptedDate && this.getUrlParameter("act") == undefined) {
            return (
                <Redirect to={"/viewBatch/"+row.id+"?act=startShopping&tab=todo"} />
            );
        } 

        var tabStyle = {
            float:"left", padding:"10px 10px 5px", width:"33%"
        };

        var tabStyleHover = {
            float:"left", padding:"10px 10px 5px", width:"33%", color:localStorage.getItem("storeColor"), borderBottom:"2px solid rgb(252, 128, 25)"
        };
        const screenHeight = window.innerHeight;
        const videoConstraints = {
            facingMode: { exact: "environment" }
        };

        const confirmDeliveryMethodModal = {
            position: "absolute",
            bottom: 0,
            width: "95%"
        };


        return (
   
            <React.Fragment>

            {/* header */}
            {this.heading("headingTxt", row.fromName) && <div style={this.heading("headerStyle")}>
                <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>{this.heading("headingTxt", row.fromName)}</span>

                <span onClick={() => this.showModalContact()} style={{float:"right", padding:"10px 10px 0"}}><FontAwesomeIcon icon={faComments} style={{fontSize:"16px"}} /></span>
            </div>}

            {row.length == null ? (

                this.getUrlParameter('act') == 'phoneOrder' && row.acceptedTel == delivery_user.data.phone ? (//phone order

                    <div style={{maxWidth:"500px", marginTop:"10px", width:"100%", float:"left"}}>

                        {/* phone order */}
                        <Modal show={this.state.PhoneOrderModal} onHide={this.hideModalPhoneOrder} size="sm">
                            <Modal.Header style={{background:"#fff"}}>
                                <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                                    <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Telefonbestellung</div>
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body style={{padding:"0"}}>

                                <div style={{ maxWidth: "600px"}}>
                                   
                                    <div style={{background:"#096CD1", color:"#fff", padding:"18px", borderRadius:"5px", margin:"5px", overflow:"auto"}}>
                                        <div style={{float:"left", width:"70px"}}><FontAwesomeIcon icon={faInfoCircle} style={{fontSize:"50px"}} /></div>
                                        <div style={{float:"left", width:"calc(100% - 70px)"}}>Dies ist eine Telefonbestellung. Es gelten spezielle Anweisungen</div>
                                    </div>

                                    <div style={{padding:"0 18px 18px", fontSize:"13px"}}>

                                        <div style={{margin:"10px 0 20px", fontSize:"14px"}}>Bestellung am Telefon</div>

                                        <div style={{margin:"10px 0"}}>1. <b>{row.shopName} anrufen</b> und {row.fromName}'s Bestellung durchgeben.</div>
                                        <div style={{margin:"10px 0"}}>2. Fragen Sie am Telefon, ob <b>Kartenzahlung</b> akzeptiert ist.</div>
                                        <div style={{margin:"10px 0"}}>3. Nachdem Sie {row.shopName} über den <b>Abholauftrag telefonisch</b> informiert haben, können Sie die Bestellung abholen.</div>

                                        <div style={{fontWeight:600, fontFamily:"open sans", marginTop:"15px", color:"#fff", background:localStorage.getItem("storeColor"), textAlign:"center", border:"1px solid orange", borderRadius:"10px", cursor:"pointer", padding:"15px 10px"}} onClick={() => this.hideModalPhoneOrder()}>{/* Got it */}Verstanden</div> 

                                    </div>

                                </div>

                            
                            </Modal.Body>
                        </Modal>

                        {/* shop name, shop tel, navigate btn */}
                        <div style={{padding:"18px", overflow:"auto"}}>
                            <div style={{float:"left", marginTop:"10px"}}>
                            <div style={{fontSize:"15px", fontFamily:"open sans", fontWeight:600, color:"#333"}}>{row.shopName}</div>
                                <div style={{fontFamily:"open sans", fontWeight:300, fontSize:"13px"}}>{row.shopTel}</div>
                            </div>

                            <a href={'tel:'+row.fromTel} style={{float:"right", textAlign:"center", cursor:"pointer"}} onClick={() => this.navigation(row.shopLat, row.shopLng)}><FontAwesomeIcon icon={faPhoneAlt} style={{color:"#3DB820", fontSize:"30px", marginBottom:"5px"}} /><br/><span style={{fontFamily:"open sans", fontWeight:300}}>Anrufen</span></a>
                        </div>

                        <div style={{background:"#F9F7FB", height:"20px"}}></div>

                        {/* customer's order */}
                        <div style={{padding:"18px 18px 0", fontSize:"15px", fontFamily:"open sans", fontWeight:600, color:"#333"}}>{row.fromName}'s Bestellung</div>
                        <div style={{padding:"0 18px 18px", marginTop:"10px", overflow:"auto"}}>
                            <div style={{float:"left", widht:"100%"}}>{this.itemsPic(row)}</div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        {/* info */}
                        <div style={{padding:"18px 18px 0", fontSize:"15px", fontFamily:"open sans", fontWeight:600, color:"#333"}}>Anweisungen</div>
                        <div style={{padding:"0 18px 18px", marginTop:"10px", fontFamily:"open sans", fontWeight:300, fontSize:"13px"}}>
                            <div style={{widht:"100%"}}>1. {row.shopName} anrufen</div>
                            <div style={{widht:"100%"}}>2. {row.fromName}'s Bestellung telefonisch durchgeben</div>
                            <div style={{widht:"100%"}}>3. Bestellung abholen</div>
                        </div>



                        {/* <div style={{float:"left", width:"100%", fontSize:"12px", padding:"18px 18px 0", fontWeight:300, fontFamily:"open sans"}}>Sie können beim Restaurant unsere Lieferservice Zahlungs-Karte verwenden.</div> */}

                        {/* bestellung abholen btn */}
                        <div style={{float:"left", width:"100%", maxWidth:"500px", padding: "18px"}}>
                            <button
                                onClick={() => { window.location = '/viewBatch/'+row.id+'?act=navigateStore' }}
                                type="button"
                                className="btn-save-address"
                                style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                            >
                                Bestellung abholen
                                <Ink duration="500" />
                            </button>
                        </div>


                    </div>

                ) : this.getUrlParameter('act') == 'navigateStore' && row.acceptedTel == delivery_user.data.phone ? (//Arrive at the store

                    <div style={{maxWidth:"500px", marginTop:"10px", width:"100%", float:"left"}}>

                        {/* shop name, shop address, navigate btn */}
                        <div style={{padding:"18px", overflow:"auto"}}>
                            <div style={{float:"left", marginTop:"10px"}}>
                            <div style={{fontSize:"15px", fontFamily:"open sans", fontWeight:600, color:"#333"}}>{row.shopName}</div>
                                <div style={{fontFamily:"open sans", fontWeight:300, fontSize:"13px"}}>{this.getAddress(row.shopAddress, "str+nr")}</div>
                            </div>

                            <div style={{float:"right", textAlign:"center", cursor:"pointer"}} onClick={() => this.navigation(row.shopLat, row.shopLng)}><FontAwesomeIcon icon={faDirections} style={{color:"#3DB820", fontSize:"30px", marginBottom:"5px"}} /><br/><span style={{fontFamily:"open sans", fontWeight:300}}>Navigieren</span></div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        {/* you have x order waiting */}
                        <div style={{padding:"18px 18px 0", fontSize:"15px", fontFamily:"open sans", fontWeight:600, color:"#333"}}>Sie haben 1 Bestellung ausstehend.</div>

                        {/* customer name, products, units */}
                        <div style={{padding:"0 18px", marginTop:"15px"}}>
                            <div style={{float:"left", width:"40px", height:"40px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>A</div>
                            <div style={{float:"right", width:"calc(100% - 50px)"}}>
                                <div style={{fontFamily:"open sans", fontWeight:400}}>{this.capitalizeFirstLetter(row.fromName)}</div>
                                <div style={{fontSize:"13px", fontFamily:"open sans", fontWeight:300}}>{row.items.length} Produkte ({this.makeTotalUnits(row.items)} Einheiten)</div>
                            </div>
                        </div>

                        {/* einkaufen starten btn */}
                        <div style={{float:"left", width:"100%", maxWidth:"500px", position: "absolute", bottom: 0, padding: "18px"}}>
                            <button
                                onClick={() => { this.request(row.id, row.unique_order_id, "startShopping") }}
                                type="button"
                                className="btn-save-address"
                                style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                            >
                                Einkauf starten
                                <Ink duration="500" />
                            </button>
                        </div>

                    </div>

                ) : this.getUrlParameter('act') == 'startShopping' && row.acceptedTel == delivery_user.data.phone ? (//Customer's order

                    <div style={{padding:"0 18px 18px", width:"100%", float:"left"}}>

                        <div style={{textAlign:"center", fontSize:"13px", fontFamily:"open sans", fontWeight:400, color:"#999"}}>
                            
                            <span onClick={() =>  { window.location = '/viewBatch/'+row.id+'?act=startShopping&tab=todo' }} style={this.getUrlParameter('tab')=="todo"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "todo")} To-Do</span>
                            
                            <span onClick={() =>  { window.location = '/viewBatch/'+row.id+'?act=startShopping&tab=in_review' }} style={this.getUrlParameter('tab')=="in_review"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "review")} In Review</span>
                            
                            <span onClick={() =>  { window.location = '/viewBatch/'+row.id+'?act=startShopping&tab=done' }} style={this.getUrlParameter('tab')=="done"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "done")} Fertig</span>
                        
                        </div>

                        <div style={{marginTop:"15px", float:"left", width:"100%"}}>{this.itemsPic(row, "big")}</div>

                    </div>
                
                ) : this.getUrlParameter('act') == 'reviewChanges' && row.acceptedTel == delivery_user.data.phone ? (//Review Changes
                    
                    <div style={{padding:"0 18px 18px", width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px 0", background:"#fff", maxWidth: "600px", borderBottom:"1px solid rgb(241, 237, 242)"}}>
                            <div style={{fontWeight:"bold"}}>{row.fromName} zur Bestätigung anrufen</div>
                            <div style={{fontSize:"12px"}}>{this.makeTotalQuantity(row.items, "replacements")} Ersatz, {this.makeTotalQuantity(row.items, "refunds")} Rückerstattung</div>
                        </div>

                        <div style={{float:"left", width:"100%"}}>{this.itemsPic(row, "big")}</div>

                        <div style={{float:"left", width:"100%", margin:"10px 0"}}><a href={'tel:'+row.fromTel} style={{position:"relative", display:"block", color:"#fff", background:"#41B129", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:"5px"}} /> {row.fromName} anrufen<Ink duration="500" /></a></div>

                        {this.makeTotalCheckout(row.items).toFixed(2) == "0.00" ? (

                            <React.Fragment>

                                <button
                                onClick={() => this.refund(row.id, row.unique_order_id, this.makeTotalRefunds(row.items, row.total) > 0 && this.makeTotalRefunds(row.items, row.total).toFixed(2)) }
                                type="button"
                                style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                Rückerstattung senden
                                <Ink duration="500" />
                                </button>

                                <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Der Kunde möchte eine Rückerstattung, da keine der oben genannte Artikel verfügbar ist.</div>

                            </React.Fragment>
                        ) : (

                            <React.Fragment>

                                {row.partner == 1 ? (
                                    <button
                                        onClick={() => window.location = '/viewBatch/'+row.id+'?act=receipt' }
                                        type="button"
                                        style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Weiter
                                        <Ink duration="500" />
                                    </button>
                                ) : (
                                    <button
                                        onClick={() => window.location = '/viewBatch/'+row.id+'?act=checkout' }
                                        type="button"
                                        style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Zur Kasse gehen
                                        <Ink duration="500" />
                                    </button>
                                )}

                                {this.makeTotalRefunds(row.items, row.subtotal).toFixed(1)*(-1) !== 0.0 && (
                                    this.makeTotalRefunds(row.items, row.subtotal) < 0 ? (
                                        <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Dem Kunden werden €{this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)*(-1)} zusätzlich vom Konto abgezogen.</div>
                                    ) : (
                                        <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Dem Kunden werden €{this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)} zurückerstattet.</div>
                                    )
                                )}

                            </React.Fragment>
                        )}

                    </div>    
                    
                ) : this.getUrlParameter('act') == 'checkout' && row.acceptedTel == delivery_user.data.phone ? (//Checkout

                    <div style={{width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", maxWidth: "600px", margin:"20px auto", textAlign:"center"}}>
                            <FontAwesomeIcon icon={faCreditCard} style={{fontSize:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Zahlungskarte verwenden</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                {/* Wenn das Geschäft nur Kreditkartentransaktionen akzeptiert, verwenden Sie den PIN-Code xxxx.<br/> */}
                                Warten Sie nach dem Durchziehen Ihrer Prepaid-Karte, bis die Zahlung abgeschlossen ist.
                                <hr/>
                                {row.wallet_charged ? (
                                    <div style={{color:"green", fontWeight:"bold"}}>Die Prepaid-Karte wurde mit €{this.makeTotalCheckout(row.items).toFixed(2)} aufgeladen.</div>
                                ) : (
                                    <div>
                                        <div id="chargePrepaidSuccess" style={{display:"none", color:"green", fontWeight:"bold"}}>Die Prepaid-Karte wurde mit €{this.makeTotalCheckout(row.items).toFixed(2)} aufgeladen.</div>
                                        <Button id="chargePrepaid" onClick={() => this.chargeCard(row.id, this.makeTotalCheckout(row.items).toFixed(2))}>Karte jetzt aufladen</Button>
                                    </div>
                                )}
                            </div>

                            {row.wallet_charged ? (
                                <span onClick={() => this.checkout(row.id)} style={{display:"block", position:"relative", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></span>
                            ) : (
                                <span id="checkoutWeiter" onClick={() => this.checkout(row.id)} style={{display:"none", position:"relative", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></span>
                            )}
                        </div>


                    </div>     

                ) : this.getUrlParameter('act') == 'receipt' && row.acceptedTel == delivery_user.data.phone ? (//Receipt Check

                    <div style={{width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", maxWidth: "600px", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/receipt.png'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Rechnung behalten</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Kunden erhalten von uns eine elektronische Rechnung. Bitte nicht in die Taschen für den Kunden legen.
                            </div>
                            <a href={'/viewBatch/'+row.id+'?act=receiptPhoto'} style={{position:"relative", display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></a>
                        </div>  
                    </div>    

                )  : this.getUrlParameter('act') == 'receiptPhoto' && row.acceptedTel == delivery_user.data.phone ? (//Receipt Photo

                    <div style={{width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", maxWidth: "600px", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/receipt.png'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Rechnung fotografieren</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Bitte fotografieren Sie Ihre gesamte Rechnung auf einer ebenen Fläche.
                            </div>
                            <a onClick={() => this.showModalReceiptPhoto()} style={{position:"relative", display:"block", textTransform:"uppercase", color:localStorage.getItem("storeColor"), border:"1px solid rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faCamera} style={{marginRight:"5px"}} /> Foto aufnehmen<Ink duration="500" /></a>
                        </div>  

                        {this.state.receiptSrc || row.receipt && <div style={{textAlign:"center"}}>
                            <img src={this.state.receiptSrc ? this.state.receiptSrc : "https://lieferservice123.com/"+row.receipt.replace("/var/www/", "")} style={{height:"120px"}} />
                        </div>}

                        {this.state.receiptSrc ? (
                            <div style={{float:"left", width:"100%", maxWidth:"500px", position: "absolute", bottom: 0, padding: "18px"}}>
                                <button
                                    onClick={() => { this.uploadReceipt(row.id) }}
                                    type="button"
                                    className="btn-save-address"
                                    style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                    Weiter
                                    <Ink duration="500" />
                                </button>
                            </div>
                        ) : row.receipt ? (
                            <div style={{float:"left", width:"100%", maxWidth:"500px", position: "absolute", bottom: 0, padding: "18px"}}>
                                <button
                                    onClick={() => { window.location = '/viewBatch/'+row.id+'?act=navigateCustomer' }}
                                    type="button"
                                    className="btn-save-address"
                                    style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                    Weiter
                                    <Ink duration="500" />
                                </button>
                            </div>
                        ) : null}
                    </div>    

                )  : this.getUrlParameter('act') == 'navigateCustomer' && row.acceptedTel == delivery_user.data.phone ? (//Deliver order to customer

                    <div style={{width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", maxWidth: "600px", margin:"20px auto", textAlign:"center"}}>
                            <FontAwesomeIcon icon={faCar} style={{fontSize:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Bestellung ausliefern</div>
                            {/* <div style={{color:"#999", marginTop:"10px"}}>Fällig: so schnell wie möglich</div> */}
                            <div style={{color:"#999", marginTop:"10px"}}>Fällig um {this.showTime(row.added+3600)} Uhr oder früher</div>
                            <a onClick={() => { this.showModalDelivery(); this.request(row.id, row.unique_order_id, "startNavigate"); }} 
                            style={{position:"relative", display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Lieferung starten<Ink duration="500" /></a>
                        </div>
                    </div>   

                ) : this.getUrlParameter('act') == 'deliveryCanceled' ? (//delivery canceled

                    <div style={{width:"100%", float:"left", margin:"auto"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/order-canceled.png'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Bestellung abgebrochen</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Bestellung: {row.unique_order_id}<br/>
                                Der Kunde erhält eine Rückerstattung.
                            </div>
                        </div>  
                    </div>    

                ) : (//View batch
                    
                    <div style={{color:"#888"}}>

                        <Map 
                            show_delivery_gps={(localStorage.getItem("userSetAddress") ? true : false)}
                            delivery_gps_location={(localStorage.getItem("userSetAddress")?'{"lat":"'+JSON.parse(localStorage.getItem("userSetAddress")).lat+'","lng":"'+JSON.parse(localStorage.getItem("userSetAddress")).lng+'","address":"'+JSON.parse(localStorage.getItem("userSetAddress")).address+'","house":null,"tag":null}':"")}

                            restaurant_latitude={row.shopLat}
                            restaurant_longitude={row.shopLng}
                            
                            deliveryLocation={'{"lat":"'+row.fromLat+'","lng":"'+row.fromLng+'","address":"'+row.fromAddress+'","house":null,"tag":null}'}

                            order_id={row.id}
                            orderstatus_id={row.orderstatus_id}
                        />

                        <div style={{width:"100%", position:"absolute", top: "320px", padding:"18px"}}>

                            Gewinn max. <FontAwesomeIcon onClick={() => this.showModalEarningsInfo()} icon={faInfoCircle} style={{position:"absolute", marginLeft:"5px"}} /><br/>
                            <div style={{color:"#000", fontSize:"35px", fontWeight:"bold"}}>
                                €{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) + parseFloat(row.subtotal * row.delivery_tip/100) ).toFixed(2)}
                            </div>

                            <div style={{fontWeight:600, fontFamily:"open sans"}}>
                                €{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) ).toFixed(2)} Zustellung {row.delivery_tip > 0 && <span>+ €{(row.subtotal * (row.delivery_tip/100)).toFixed(2)} Trinkgeld</span>}
                            </div>

                            {/* Navigiere zum {row.shopName} Shop <span style={{fontFamily:"open sans", fontWeight:300}}>{localStorage.getItem("userSetAddress") ? "("+this.getDistance(row.shopLat, row.shopLng, JSON.parse(localStorage.getItem("userSetAddress")).lat, JSON.parse(localStorage.getItem("userSetAddress")).lng).toFixed(2)+ "km)":null}</span><br/>
                            <span style={{color:"rgb(26, 115, 232)", cursor:"pointer"}} onClick={() => this.navigation(row.shopLat, row.shopLng)}>{row.shopAddress}</span><br/>

                            <br/>

                            Navigiere zum Kunden {row.fromName.replace(/.(?=.{4,}$)/g, '*')} <span style={{fontFamily:"open sans", fontWeight:300}}>{localStorage.getItem("userSetAddress") ? "("+this.getDistance(row.fromLat, row.fromLng, JSON.parse(localStorage.getItem("userSetAddress")).lat, JSON.parse(localStorage.getItem("userSetAddress")).lng).toFixed(2)+ "km)":null}</span><br/>

                            <span style={{color:"rgb(26, 115, 232)", cursor:"pointer"}} onClick={() => this.navigation(row.fromLat, row.fromLng)}>{this.getAddress(row.fromAddress, "str+nr")}</span><br/> */}
                            {/* TEL <span data-tip='' data-for='telSehen'>{row.fromTel.replace(/.(?=.{4,}$)/g, '*')}</span><br/>
                            <ReactTooltip id='telSehen' place="right"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Nur für Zusteller sichtbar.</div></ReactTooltip> */}

                            <br/>
                            <br/>

                            <FontAwesomeIcon icon={faShoppingBag} style={{marginRight:"10px"}} /> {row.items.length} Produkte ({this.makeTotalUnits(row.items)} Einheiten)<br/>

                            {/* <FontAwesomeIcon icon={faCoins} style={{marginRight:"10px"}} /> Kunde bezahlt mit Bargeld<br/> */}

                            <FontAwesomeIcon icon={faCar} style={{marginRight:"10px"}} /> {this.getDistance(row.fromLat, row.fromLng, row.shopLat, row.shopLng).toFixed(2)} km<br/>

                            <br/>

                            <div style={{marginBottom:"15px", float:"left", widht:"100%"}}>{this.itemsPic(row)}</div>

                            <div style={{float:"left", width:"100%"}}>
                                <button
                                    onClick={() => this.acceptBatch(row.id, row.cat)}
                                    type="button"
                                    className="btn-save-address"
                                    style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    id="laden"
                                >
                                    Auftrag annehmen
                                    <Ink duration="500" />
                                </button>
                            </div>


                        </div>

                        
                    </div> 

                )
            ) : (
                <h2 style={{textAlign:"center", fontFamily:"open sans", fontWeight:300}}>Bestätigung wird geladen...</h2>
            )}

            {/* hamburger menu */}
            <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                <Modal.Header closeButton style={{background:"#fff"}}>
                    <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                        <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                        <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                    <div className="category-list-item" onClick={() => { window.location = '/shopperPwa?act=dashboard' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-users" />
                            </div>
                            <div className="flex-auto border-0">Dashboard</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <div className="category-list-item" onClick={() => { window.location = '/shopperPwa?act=orders' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-basket-loaded" />
                            </div>
                            <div className="flex-auto border-0">Aufträge</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    {/* <div className="category-list-item" onClick={() => { window.location = '/shopperPwa?act=hours' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-calendar" />
                            </div>
                            <div className="flex-auto border-0">Stunden</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div> */}

                    <div className="category-list-item" onClick={() =>  { window.location = '/shopperPwa?act=earnings' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-credit-card" />
                            </div>
                            <div className="flex-auto border-0">Einnahmen</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <hr/>

                    {/* <div className="category-list-item" onClick={() =>  { window.location = '/shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-user" />
                            </div>
                            <div className="flex-auto border-0">Profil</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <div className="category-list-item" onClick={() =>  { window.location = '/shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-settings" />
                            </div>
                            <div className="flex-auto border-0">Einstellungen</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <hr/> */}

                    <div className="category-list-item" onClick={() => { window.location = '/search-location?redirect=shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                    {/* <div className="category-list-item" onClick={() => { this.context.router.history.push("/search-location"+backToRes) }} style={{cursor:"pointer", position:"relative"}}> */}
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-location-pin" />
                            </div>
                            <div className="flex-auto border-0">{localStorage.getItem("firstScreenSetupLocation")}</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <div className="category-list-item" onClick={() => { this.hideModalMenu(); this.showModalLang(); }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-settings" />
                            </div>
                            <div className="flex-auto border-0">Land/Sprache ändern</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>
                
                </Modal.Body>
            </Modal>

            {/* contact customer */}
            <Modal show={this.state.ContactModal} onHide={this.hideModalContact} size="sm" style={{background:"#333"}}>
                <Modal.Header closeButton style={{background:"#fff"}}>
                    <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                        <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>{row.fromName}</div>
                        <div style={{fontSize:"12px", fontFamily:"open sans", fontWeight:500, marginTop:"5px"}}>{row.fromTel}</div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"25px"}}>

                    <div style={{background:"#fff", maxWidth: "600px", textAlign:"center"}}>
                        {/* <div style={{fontSize:"12px", color:"#999", marginTop:"10px", fontFamily:"open sans", fontWeight:300}}>
                            Bitte nur anrufen, wenn Sie die Wohnung des Kunden nicht finden.
                        </div> */}

                        {/* <div style={{fontSize:"12px", color:"#999", marginTop:"10px", fontFamily:"open sans", fontWeight:300}}>
                            Für Fragen wegen replacements, können Sie den Kunden eine Nachricht senden.
                        </div> */}

                        <div style={{marginTop:"40px"}}>
                            <a onClick={() => this.sendSMS(row.fromTel)} style={{display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faComment} style={{marginRight:"5px"}} /> Nachricht</a>
                            
                            <a href={'tel:'+row.fromTel} style={{display:"block", textTransform:"uppercase", color:"#fff", background:"#24C644", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:"5px"}} /> Anrufen</a>
                        </div>

                    </div>

                
                </Modal.Body>
            </Modal>

            {/* delivery to customer */}
            <Modal show={this.state.DeliveryModal} onHide={this.hideModalDelivery} size="sm">
                <Modal.Header closeButton>
                    <Modal.Title style={{color:"#000"}}>
                        Lieferung
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0"}}>

                    <div style={{maxWidth: "600px"}}>

                        <div style={{padding:"25px"}}>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:"bold", fontSize:"12px"}}>Adresse</div>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>{row.fromName}<br/>{row.fromAddress}</div>
                            <hr/>
                            <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:400}} onClick={() => this.navigation(row.fromLat, row.fromLng)}>Navigieren</div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        <div style={{padding:"25px"}}>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:"bold", fontSize:"12px"}}>Versuche den Kunden zu kontaktieren</div>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>Lassen Sie die Lieferung an der Tür, wenn der Kunde nicht antwortet.</div>
                            <hr/>
                            <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:400}} onClick={() => this.showModalContact()}>Kontakt</div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        <div id="confirmDeliveryMethod" style={{background:"#F9F7FB", padding:"25px"}}><a onClick={() => this.showModalConfirmDeliveryMethod()} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Zustellungsmethode bestätigen</a></div>

                        <div id="markAsDelivered" style={{display:"none", background:"#F9F7FB", padding:"25px"}}><a onClick={() => this.showModalCompleteDelivery()} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Lieferung abschließen</a></div>

                    </div>
                </Modal.Body>
            </Modal>

            {/* confirm delivery method */}
            <Modal show={this.state.ConfirmDeliveryMethodModal} onHide={this.hideModalConfirmDeliveryMethod} size="sm" dialogClassName="confirmDeliveryMethodModal" style={{background:"rgba(0, 0, 0, 0.7)"}}>
                
                <Modal.Body style={{padding:"0"}}>
                    <div style={{color:"#8D8B8F", padding:"18px", fontWeight:300, fontFamily:"open sans"}}>Zustellungsmethode bestätigen</div>
                    <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:600, fontSize:"15px"}}>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.handedToCustomer()}>Dem Kunden übergeben</div>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.showModalLeftAtDoorPhoto()}>An der Tür gelassen</div>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.hideModalConfirmDeliveryMethod()}>Schließen</div>
                    </div>
                </Modal.Body>
            </Modal>

            {/* left at door photo */}
            <Modal show={this.state.LeftAtDoorPhotoModal} onHide={this.hideModalLeftAtDoorPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto aufnehmen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalLeftAtDoorPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <Webcam
                        audio={false}
                        height={screenHeight-77} 
                        //width={"100%"}            
                        ref={this.leftAtDoorRef}
                        screenshotFormat="image/jpeg"
                        videoConstraints={videoConstraints}
                        screenshotQuality={1}
                        minScreenshotHeight={1080}
                        minScreehshotWidth={1920}
                    />

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            onClick={() => this.captureLeftAtDoor()}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto aufnehmen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>

            {/* upload left at door photo */}
            <Modal show={this.state.UploadLeftAtDoorPhotoModal} onHide={this.hideModalUploadLeftAtDoorPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto hochladen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalUploadLeftAtDoorPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <div style={{width:"100%"}}>
                        <img src={this.state.leftAtDoorSrc} style={{width:"100%", height:screenHeight-70}} />
                    </div>

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            // onClick={() => this.capture()}
                            onClick={() => {
                                document.getElementById('confirmDeliveryMethod').style.display = "none";
                                document.getElementById('markAsDelivered').style.display = "block";
                                this.hideModalConfirmDeliveryMethod();
                                this.hideModalLeftAtDoorPhoto(); 
                                this.hideModalUploadLeftAtDoorPhoto(); 
                                this.uploadLeftAtDoor(row.id);
                            }}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto hochladen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>

            {/* complete delivery */}
            <Modal show={this.state.CompleteDeliveryModal} onHide={this.hideModalCompleteDelivery} size="sm" style={{background:"#333"}}>
                <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px"}}>Möchten Sie die Bestellung wirklich als geliefert markieren?</div></Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0"}}>
                    <div style={{padding:"15px 25px 25px"}}>
                        <a onClick={() => {
                            this.request(row.id, row.unique_order_id, "markAsDelivered"); 
                            this.refund(row.id, row.unique_order_id, this.makeTotalRefunds(row.items, row.subtotal) > 0 && this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)); 
                        }} 
                        style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Als geliefert markieren</a>
                        {/* <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>You're not at the customer's location.</div> */}
                    </div>
                </Modal.Body>
            </Modal>

            {/* info for earnings estimate */}
            <Modal show={this.state.EarningsInfoModal} onHide={this.hideModalEarningsInfo} size="sm">
                <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px"}}>{/* What does the Earnings Estimate include? */}Was beinhaltet die Gewinnschätzung?</div></Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0"}}>
                    <div style={{padding:"15px 25px 25px"}}>
                        <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>
                            {/* This includes a guaranteed Batch Payment of $8 unless the order is canceled. The customer added a $2 tip.<br/>The customer has 3 days to modify the tip amount after delivery. The tip total may also change if there are order adjustments such as refunds/replacements. If the tip changes we'll always tell you why. */}
                            Dies beinhaltet eine garantierte Auftragszahlung von €{( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) ).toFixed(2)}, sofern die Bestellung nicht storniert wird. Der Kunde hat ein Trinkgeld in Höhe von {row.delivery_tip > 0 ? <span>€{(row.subtotal * (row.delivery_tip/100)).toFixed(2)} Trinkgeld</span>:"€0"} hinzugefügt.<br/>Der Kunde hat 3 Tage Zeit, um den Trinkgeldbetrag nach der Lieferung zu ändern. Die Trinkgeldsumme kann sich auch ändern, wenn Auftragsanpassungen wie Rückerstattungen / Ersatz vorgenommen werden. Wenn sich das Trinkgeld ändert, sagen wir Ihnen immer warum.
                        </div>
                        <div style={{fontWeight:600, fontFamily:"open sans", marginTop:"15px", color:localStorage.getItem("storeColor"), textAlign:"center", border:"1px solid orange", borderRadius:"2px", cursor:"pointer", padding:"10px"}} onClick={() => this.hideModalEarningsInfo()}>{/* Got it */}Verstanden</div> 
                    </div>
                </Modal.Body>
            </Modal>

            {/* receipt photo */}
            <Modal show={this.state.ReceiptPhotoModal} onHide={this.hideModalReceiptPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto aufnehmen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalReceiptPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <Webcam
                        audio={false}
                        height={screenHeight-77} 
                        //width={"100%"}            
                        ref={this.webcamRef}
                        screenshotFormat="image/jpeg"
                        videoConstraints={videoConstraints}
                        screenshotQuality={1}
                        minScreenshotHeight={1080}
                        minScreehshotWidth={1920}
                    />

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            onClick={() => this.capture()}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto aufnehmen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>

            {/* upload receipt photo */}
            <Modal show={this.state.UploadReceiptPhotoModal} onHide={this.hideModalUploadReceiptPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto hochladen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalUploadReceiptPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <div style={{width:"100%"}}>
                        <img src={this.state.receiptSrc} style={{width:"100%", height:screenHeight-70}} />
                    </div>

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            // onClick={() => this.capture()}
                            onClick={() => {this.hideModalReceiptPhoto(); this.hideModalUploadReceiptPhoto()}}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto hochladen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>


            </React.Fragment>
   
        )

    }
}


const mapStateToProps = state => ({
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});
export default connect(
    mapStateToProps,
    { getSettings, walletTransaction, createRefund }
)(ViewBatch);
