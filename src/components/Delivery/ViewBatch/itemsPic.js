import { SUPERMARKET_ITEMS_URL } from "../../../configs/index";

import React, { Component } from "react";
import { connect } from "react-redux";
import Ink from "react-ink";
import axios from "axios";

import { Modal, Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faBarcode } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import ReactTooltip from 'react-tooltip'
import ReactDOM from 'react-dom';

import FoundItem from './foundItem';
import FoundReplacement from './foundReplacement';

// import BarcodeReader from 'react-barcode-reader'
import BarcodeScannerComponent from "react-webcam-barcode-scanner";

// search shop
import Search from '../../Takeaway/Items/ItemList/Search';


class Items extends Component {

    constructor(props){
        super(props);

        this.state = {
            test: [],
            ZoomModal: false,
            FoundItemModal: false,
            RefundItemModal: false,
            ScanItemModal: false,
            ScanReplacementModal: false,
            SearchReplacementModal: false,
            FoundReplacementModal: false,
            barcode: 'No result',
        }
    }

    showModalZoom (id) {
        this.setState({
          ZoomModal: {
            [id]: true
          }
        });
    }
    hideModalZoom = () => {
        this.setState({
            ZoomModal: false
        });
    }


    showModalFoundItem (id) {
        this.setState({
            FoundItemModal: {
            [id]: true
          }
        });
    }
    hideModalFoundItem = () => {
        this.setState({
            FoundItemModal: false
        });
    }

    showModalRefundItem (id) {
        this.setState({
            RefundItemModal: {
            [id]: true
          }
        });
    }
    hideModalRefundItem = () => {
        this.setState({
            RefundItemModal: false
        });
    }

    showModalScanItem (id) {
        this.setState({
            ScanItemModal: {
            [id]: true
          }
        });
    }
    hideModalScanItem = () => {
        this.setState({
            ScanItemModal: false
        });
    }

    showModalScanReplacement (id) {
        this.setState({
            ScanReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalScanReplacement = () => {
        this.setState({
            ScanReplacementModal: false
        });
    }

    showModalSearchReplacement (id) {
        this.setState({
            SearchReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalSearchReplacement = () => {
        this.setState({
            SearchReplacementModal: false
        });
    }

    showModalFoundReplacement (id, replaceWith) {
        this.setState({
            FoundReplacementModal: {
            [id]: true
          }
        });

        this.setState({
          replaceWith: replaceWith
        });
    }
    hideModalFoundReplacement = () => {
        this.setState({
            FoundReplacementModal: false
        });
    }

    componentDidMount() {
    }

    async getName(id){
        const name = await axios
            .post(SUPERMARKET_ITEMS_URL, {
                id: id
            })
            .then(response => {
                return response.data.name;
            })
            .catch(function(error) {
                console.log(error);
                return "";
            });
        console.log('name', name);
        return name;
    }
  
    getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }


    makeTotalQuantity = (data = []) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found != null){
                sum = sum + 1
            }
        });
        return sum;
    }

    makeTotalQuantityNew = (data = [], tab) => {
       
        let sum = 0;
        data.forEach((row) => {

            if(tab == "todo" && row.found == null){
                sum = sum + 1
            } 
            else if(tab == "review" && row.found == 0){
                sum = sum + 1
            } 
            else if(tab == "replacements" && row.found == 0 && row.replacement){
                sum = sum + 1
            } 
            else if(tab == "replacementsApproved" && row.found == 0 && row.replacement && row.approved == 1){
                sum = sum + 1
            } 
            else if(tab == "refunds" && row.found == 0 && row.replacement == null){
                sum = sum + 1
            } 
            else if(tab == "done" && row.found > 0){
                sum = sum + 1
            }
        });
        return sum;
    }

    // scan product
    handleScan(data){
        this.setState({
            barcode: data,
        })
    }
    handleError(err){
        console.error(err)
    }

    country_formatted_tel = (fromTel) => {
        var fromTel = fromTel.trim();
        var first2 = fromTel.substr(0, 2);
        if(first2 == localStorage.getItem('prefix')) {
            var fromTel = "+".fromTel;
            return fromTel;
        }
    
        var firstLetter = fromTel.substr(0, 1);
        if(firstLetter == 0) {
            var fromTel = "+"+localStorage.getItem('prefix')+fromTel.substr(1);
            return fromTel;
        }
    
        return fromTel;
    }
      
    addBarcode = (id, barcode, shop) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_addBarcode.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode+'&shop='+shop;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }
      
    scanReplacement = (id, barcode) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_scanReplacement.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {   
            // console.log("id", id);
            // console.log("response.data", response.data);
            this.showModalFoundReplacement(id, response.data);
            
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }

    foundItem = (supermarket_orders_id, id, old_found, old_replacement, old_replacement_found) => {
        if(old_replacement){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=found&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity=1&found=1&old_found='+old_found+'&old_replacement='+old_replacement+'&old_replacement_found='+old_replacement_found;
        } else if(old_found || old_found == 0){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=found&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity=1&found=1&old_found='+old_found;
        } else {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=found&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity=1&found=1';
        }
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    refundItem = (supermarket_orders_id, id, quantity, found, replacement, found_replacement) => {

        if(replacement){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=refund&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found+'&replacement='+replacement+'&found_replacement='+found_replacement;
        } else if(found == 0){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=refund&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found=0';
        } else if(found) {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=refund&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found;
        } else {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?type=refund&supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found=0';
        }
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    showReplacementOptions = () =>{
        document.getElementById('replacement_options').style.display = "block";
    }

    nextLink = (supermarket_orders) => {
        
        //if(this.makeTotalQuantityNew(supermarket_orders.items, "replacements") > this.makeTotalQuantityNew(supermarket_orders.items, "replacementsApproved") || this.makeTotalCheckout(supermarket_orders.items).toFixed(2) == "0.00"){
        if(this.makeTotalQuantityNew(supermarket_orders.items, "review") > 0){

            var url = window.location.href.replace('?act=startShopping&tab='+this.getUrlParameter('tab'), '?act=reviewChanges&tab=in_review');
            return <button
                onClick={() =>  window.location = url}
                type="button"
                style={{fontFamily:"open sans", fontWeight:600, padding:"10px", border:"none", width:"100%", color:"#fff", textTransform:"uppercase", backgroundColor: localStorage.getItem("storeColor")}}
            >
                Änderungen Prüfen
            </button>
         } else {

            if(window.location.href.indexOf("viewBatch2/OD-") >= 0){//deliveryByStore
                var url = window.location.href.replace('?act=startShopping&tab='+this.getUrlParameter('tab'), '?act=receipt');
                return <button
                    onClick={() => window.location = url}
                    type="button"
                    style={{fontFamily:"open sans", fontWeight:600, padding:"10px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                >
                    Weiter
                </button>

            } else if(supermarket_orders.partner == 1){//partner
                var url = window.location.href.replace('?act=startShopping&tab='+this.getUrlParameter('tab'), '?act=receipt');
                return <button
                    onClick={() => window.location = url}
                    type="button"
                    style={{fontFamily:"open sans", fontWeight:600, padding:"10px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                >
                    Weiter
                </button>

            } else if(supermarket_orders.partner == null) {//soldo checkout
                var url = window.location.href.replace('?act=startShopping', '?act=checkout');
                return <button
                    onClick={() => window.location = url}
                    type="button"
                    style={{fontFamily:"open sans", fontWeight:600, padding:"10px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                >
                    Zur Kasse gehen
                </button>
            }
        }
    }

    makeTotalCheckout = (data = []) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return sum - (sum*0.3);
    }

    render() {
        const { supermarket_orders } = this.props;
        let sub_total = 0;

        // console.log("items", supermarket_orders.items);

        const styleReplaceWithImg = { width:"calc(100% - 50px)", color:"#ccc", float:"right" }
        const styleReplaceWithoutImg = { color:"#ccc" }
        const styleWithImg = { width:"calc(100% - 50px)", color:"#757575", float:"right" }
        const styleWithoutImg = { color:"#757575" }

        const ordersComponent = supermarket_orders.items.map((row, index) => {
                                
            sub_total += row.price*row.quantity;

            return (

                <div>
                    
                    {(this.getUrlParameter('tab') == 'todo' && row.found == null) ? (

                        row.placeholder_image ? (
                            <div key={row.id} style={{width:"100%", float:"left", margin:"5px 5px"}}>

                                <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{width:"120px", height:"120px"}} />

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={{width:"calc(100% - 130px)", color:"#757575", float:"right"}}>
                                    <b>{row.quantity}x</b> {row.name}
                                    
                                    {row.grammageBadge && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.grammageBadge})</div>}

                                    <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"12px", color:"#999"}}>{row.category} - {row.sub_category}</div>
                                </span> 

                            </div>
                        ) : (
                            <div key={row.id} style={{width:"100%", float:"left", margin:"5px 5px"}}>

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={{width:"100%", color:"#757575"}}>
                                    <b>{row.quantity}x</b> {row.name}
                                    
                                    {row.description && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.description})</div>}
                                </span> 

                            </div>
                        )
                    
                    ) : (this.getUrlParameter('tab') == 'in_review') ? (

                        row.found == 0 && row.found_replacement != null ? (//replaced
                            
                            <div key={row.id} style={{width:"100%", float:"left", padding:"20px 0", borderBottom:"1px solid #F1EDF2"}}>

                                <div style={{marginBottom:"10px", color:"#C19E3B", fontSize:"11px", fontWeight:"bold"}}>
                                    <span style={{marginLeft:row.placeholder_image?"50px":0}}><FontAwesomeIcon icon={faSync} style={{marginRight:"5px"}} /> Ersetzt</span>
                                    {row.approved && <span style={{display:"inline-block", color:"#000", background:"#ADE1F5", marginLeft:row.placeholder_image?"50px":0}}><FontAwesomeIcon icon={faCheckCircle} style={{marginRight:"5px"}} /> Kunde hat Ersatzprodukt bestätigt</span>}
                                </div>
                               
                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{width:"40px", height:"40px", opacity:0.5}} />}
                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleReplaceWithImg : styleReplaceWithoutImg}>
                                    <b>{row.quantity}x</b> {row.name}
                                    {row.grammageBadge && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.grammageBadge})</div>}
                                    {/* <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"12px", color:"#999"}}>{row.category} - {row.sub_category}</div> */}
                                </span> 

                                <div style={{width:"100%", float:"left", marginTop:"5px"}}>
                                    {row.replacement_placeholder_image && <img onClick={this.showModalZoom.bind(this, row.replacement_id)} title={row.replacement_name} src={'https://lieferservice123.com'+row.replacement_placeholder_image} style={{width:"40px", height:"40px"}} />}
                                    <span onClick={this.showModalZoom.bind(this, row.replacement_id)} style={row.replacement_placeholder_image ? styleWithImg : styleWithoutImg}>
                                        <b>{row.found_replacement}x</b> {row.replacement_name}
                                        {row.replacement_grammageBadge && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.replacement_grammageBadge})</div>}
                                        {/* <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"12px", color:"#999"}}>{row.replacement_category} - {row.replacement_sub_category}</div> */}
                                    </span> 
                                </div>

                            </div>

                        ) : row.found == 0 && row.found_replacement == null ? (//refunded

                            <div key={row.id} style={{width:"100%", float:"left", padding:"20px 0", borderBottom:"1px solid #F1EDF2"}}>

                                <div style={{marginBottom:"10px", color:"#D55214", fontSize:"11px", fontWeight:"bold"}}>
                                    <span style={{marginLeft:row.placeholder_image?"50px":0}}><FontAwesomeIcon icon={faRedo} style={{marginRight:"5px"}} /> Rückerstattet</span>
                                </div>
                                <div style={{width:"100%", float:"left"}}>
                                    {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{width:"40px", height:"40px"}} />}
                                    <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleWithImg : styleWithoutImg}>
                                        <b>{row.quantity}x</b> {row.name}
                                        {row.grammageBadge && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.grammageBadge})</div>}
                                        {/* <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"12px", color:"#999"}}>{row.category} - {row.sub_category}</div> */}
                                    </span> 
                                </div>

                            </div>

                        ) : null

                    )  : (this.getUrlParameter('tab') == 'done' && row.found > 0) ? (

                        row.placeholder_image ? (
                            
                            <div key={row.id} style={{width:"100%", float:"left", margin:"5px 5px"}}>

                                <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{width:"120px", height:"120px"}} />

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={{width:"calc(100% - 130px)", color:"#757575", float:"right"}}>
                                    <div style={{marginBottom:"10px"}}>
                                        <span style={{fontSize:"11px", color:"blue", fontWeight:"bold"}}><FontAwesomeIcon icon={faCheck} style={{marginRight:"5px"}} /> Gefunden</span>
                                    </div>
                                    
                                    <b>{row.found}x</b> {row.name}
                                    
                                    {row.grammageBadge && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.grammageBadge})</div>}

                                    <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"12px", color:"#999"}}>{row.category} - {row.sub_category}</div>
                                </span> 

                            </div>

                        ) : (

                            <div key={row.id} style={{width:"100%", float:"left", margin:"5px 5px"}}>

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={{width:"100%", color:"#757575", float:"right"}}>
                                    <div style={{marginBottom:"10px"}}>
                                        <span style={{fontSize:"11px", color:"blue", fontWeight:"bold"}}><FontAwesomeIcon icon={faCheck} style={{marginRight:"5px"}} /> Gefunden</span>
                                    </div>
                                    
                                    <b>{row.found}x</b> {row.name}
                                    
                                    {row.description && <div style={{fontFamily:"open sans", fontWeight:400, fontSize:"13px"}}>({row.description})</div>}
                                </span> 

                            </div>
                        )

                    ) : null}
                    
                    {/* show product */}
                    <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            
                            {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                            <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                            <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
            
                            {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                            <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}>

                            {(row.category=="Obst, Gemüse" || supermarket_orders.cat == "restaurant") ? (

                                row.quantity==1 ? (    
                                    <button
                                        onClick={() => this.foundItem(supermarket_orders.id, row.id, row.found, row.replacement, row.found_replacement)}
                                        type="button"
                                        style={{padding:"12px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Produkt gefunden
                                    </button>
                                ) : (
                                    <button
                                        onClick={this.showModalFoundItem.bind(this, row.id)}
                                        type="button"
                                        style={{padding:"12px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Produkt gefunden
                                    </button>
                                )

                            ) : (

                                <button
                                    onClick={this.showModalScanItem.bind(this, row.id)}
                                    type="button"
                                    style={{padding:"12px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                    Produkt gefunden
                                </button>

                            )}
                                
                            </div>

                            {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Location<span style={{float:"right"}}>{row.category}</span></div>}

                            <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                            <div onClick={this.showModalRefundItem.bind(this, row.id)} style={{width:"100%", float:"left", color:localStorage.getItem("storeColor"), cursor:"pointer", marginTop:"20px"}}>Artikel nicht gefunden</div>
            
                            {/* <button onClick={()=>this.props.addProduct(row)} type="button" class="btn-save-address" style={{backgroundColor: "rgb(252, 128, 25)"}}>In den Einkaufswagen</button> */}
                        </Modal.Body>
                    </Modal>

                    {/* found product */}
                    <Modal show={this.state.FoundItemModal[row.id]} onHide={this.hideModalFoundItem} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Menge eingeben</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <FoundItem id={supermarket_orders.id} row={row} items_length={supermarket_orders.items.length} />
                        </Modal.Body>
                    </Modal>

                    {/* scan product */}
                    <Modal show={this.state.ScanItemModal[row.id]} onHide={this.hideModalScanItem} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Barcode scannen</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:0}}>
                            <div style={{padding:"0 18px"}}>

                                <img src={"https://lieferservice123.com"+row.image} style={{width:"120px", float:"left"}} alt="Bild wird geladen..." />

                                <div style={{float:"right", width:"calc(100% - 130px)"}}>
                                    <div>{row.marke}</div>
                                    
                                    <div style={{fontSize:"13px"}}>{row.quantity}x {row.name}</div>

                                    {row.unit ? (
                                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                                    ) : row.grammageBadge ? (
                                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                                    ) : (
                                        null
                                    )}
                                </div>

                            </div>

                            {/* <p>Barcode: {this.state.barcode}</p> */}

                            <div style={{background: "#333", width: "100%", float: "left", marginTop: "15px"}}>
                                {/* <BarcodeReader 
                                onError={this.handleError} 
                                onScan={() => { this.handleScan(); this.hideModalScanItem(); this.showModalFoundItem.bind(this, row.id); }}
                                /> */}
                                <BarcodeScannerComponent
                                    width={"100%"}
                                    onUpdate={(err, result) => {
                                        if (result) {
                                            this.handleScan(result.text); 
                                            this.hideModalScanItem(); 
                                            this.addBarcode(row.id, this.state.barcode, supermarket_orders.shopName); 
                                            row.quantity==1 ? this.foundItem(supermarket_orders.id, row.id, row.found, row.replacement, row.found_replacement) : this.showModalFoundItem(row.id)
                                        }
                                    }}
                                />
                            </div>

                            {/* skip barcode scanning */}
                            {row.quantity==1 ? (   
                                <div onClick={() => this.foundItem(supermarket_orders.id, row.id, row.found, row.replacement, row.found_replacement)} style={{position:"absolute", bottom:"20px", margin:"auto", width:"100%", textAlign:"center"}}><span style={{border:"1px solid #fff", borderRadius:"3px", padding:"10px", color:"#fff"}}>Überspringen</span></div>
                            ) : (
                                <div onClick={this.showModalFoundItem.bind(this, row.id)} style={{position:"absolute", bottom:"20px", margin:"auto", width:"100%", textAlign:"center"}}><span style={{border:"1px solid #fff", borderRadius:"3px", padding:"10px", color:"#fff"}}>Überspringen</span></div>
                            )}

                        </Modal.Body>
                    </Modal>

                    {/* refund product */}
                    <Modal show={this.state.RefundItemModal[row.id]} onHide={this.hideModalRefundItem} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Artikel nicht gefunden</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {row.image ? (
                                <div>
                                    <img src={"https://lieferservice123.com"+row.image} style={{width:"120px", float:"left"}} alt="Bild wird geladen..." />

                                    <div style={{float:"right", width:"calc(100% - 130px)"}}>
                                        <div>{row.marke}</div>
                                        
                                        <div style={{fontSize:"13px"}}>{row.quantity}x {row.name}</div>

                                        {row.unit ? (
                                            <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                                        ) : row.grammageBadge ? (
                                            <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                                        ) : (
                                            null
                                        )}
                                    </div>
                                </div>
                            ) : (
                                <div style={{width:"100%"}}>
                                    <div>{row.marke}</div>
                                    
                                    <div style={{fontSize:"13px"}}>{row.quantity}x {row.name}</div>
    
                                    {row.unit ? (
                                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                                    ) : row.grammageBadge ? (
                                        <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                                    ) : (
                                        null
                                    )}
                                </div>
                            )}

                            <div style={{width:"100%", float:"left", color:"#666", fontSize:"16px", fontWeight:500, fontFamily:"open sans", marginTop:"40px"}}>Artikel zurückerstatten</div>
                            <div style={{width:"100%", float:"left", fontWeight:300, fontFamily:"open sans", fontSize:"12px"}}>Der Kunde möchte eine Rückerstattung, wenn der oben genannte Artikel nicht verfügbar ist.</div>

                            <input 
                                autoFocus
                                type="text"
                                style={{width:"calc(100% - 40px)", border:"none", textAlign:"right", fontSize:"20px", color:"rgb(72, 98, 112)"}}
                            />

                            <FontAwesomeIcon icon={faRedo} style={{fontSize:"80px", color:"#E90033", margin:"15px 0", width:"100%"}} />
            
                            <div style={{float:"left", width:"100%", marginTop:"15px"}}>
                                <button
                                    onClick={() => this.refundItem(supermarket_orders.id, row.id, row.quantity, row.found, row.replacement, row.found_replacement)}
                                    type="button"
                                    style={{padding:"10px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                    Artikel zurückerstatten
                                    {/* <Ink duration="500" /> */}
                                </button>
                            </div>

                            <div onClick={() => this.showReplacementOptions()} style={{float:"left", width:"100%", marginTop:"30px", fontWeight:600, fontFamily:"open sans"}}><FontAwesomeIcon icon={faCaretRight} style={{color:"#939193", marginRight:"5px"}} /> Ersatzprodukt (replacement)</div>

                            <div id="replacement_options" style={{display:"none", width:"100%", float:"left", marginTop:"5px", color:localStorage.getItem("storeColor")}}>
                                <div onClick={() => this.showModalScanReplacement(row.id)} style={{padding:"5px 10px"}}><FontAwesomeIcon icon={faBarcode} style={{marginRight:"5px"}} /> Scannen</div>

                                <div onClick={() => this.showModalSearchReplacement(row.id)} style={{padding:"5px 10px", border:"1px solid orange"}}><FontAwesomeIcon icon={faSearch} style={{marginRight:"5px"}} /> Suchen</div>
                            </div>

                        </Modal.Body>
                    </Modal>

                    {/* scan replacement */}
                    <Modal show={this.state.ScanReplacementModal[row.id]} onHide={this.hideModalScanReplacement} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Ersatzprodukt scannen</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:0}}>

                            {/* <p>Barcode: {this.state.barcode}</p> */}

                            <div style={{background: "#333", width: "100%", float: "left", marginTop: "15px"}}>
                                <BarcodeScannerComponent
                                    width={"100%"}
                                    onUpdate={(err, result) => {
                                        if (result) {
                                            this.handleScan(result.text); 
                                            this.hideModalScanItem(); 
                                            this.scanReplacement(row.id, this.state.barcode); 
                                        }
                                    }}
                                />
                            </div>
                        </Modal.Body>
                    </Modal>

                    {/* search replacement */}
                    <Modal show={this.state.SearchReplacementModal[row.id]} onHide={this.hideModalSearchReplacement} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Ersatzprodukt suchen</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0px", margin:"0px"}}>
                            <Search />
                        </Modal.Body>
                    </Modal>

                    {/* found replacement */}
                    <Modal show={this.state.FoundReplacementModal[row.id]} onHide={this.hideModalFoundReplacement} style={{background:"#333"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Menge eingeben</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <FoundReplacement id={supermarket_orders.id} row={row} replaceWith={this.state.replaceWith} currencyFormat={supermarket_orders.currencyFormat} />
                        </Modal.Body>
                    </Modal>
                
                </div>

            )
 
        });

        const fee = supermarket_orders.trinkgeld;

        return (
            <React.Fragment>

                {ordersComponent}


                {this.makeTotalQuantity(supermarket_orders.items) == supermarket_orders.items.length ? (

                    <div style={{float:"left", width:"100%", marginTop:"15px"}}>

                        {this.getUrlParameter('tab') == 'todo' && <div style={{fontSize:"20px", color:"#999", fontFamily:"open sans", fontWeight:300, textAlign:"center", marginTop:"50%"}}>Du bist fertig!</div>}

                        <div style={{width:"100%", position:"absolute", bottom:0, left:0}}>
                            {this.getUrlParameter('act') == 'startShopping' && this.nextLink(supermarket_orders)}
                        </div>
                    </div>

                ) : (
                    null
                )}
                
            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(Items);

