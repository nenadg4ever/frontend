import React, { Component } from "react";
import { Modal, Button } from 'react-bootstrap';
import { connect } from "react-redux";
import axios from "axios";

class FoundItem extends Component{

    constructor() {
        super();
        this.inputRef = React.createRef();
    }

    state = {
        found: 0,
    };

    componentDidMount() {  

        setTimeout(() => {
            // console.log(this.inputRef);
            this.inputRef.current.focus();
        }, 100)
    }

    onFoundChanged = (e) => {
        this.setState({
            found: e.currentTarget.value
        });
    }

    foundItem = (supermarket_orders_id, id, quantity, found, old_found, old_replacement, old_replacement_found) => {
        // console.log('replace {"id":"'+id+'","quantity":'+quantity+'} with {"id":"'+id+'","quantity":'+quantity+',"found":'+found+'}');

        if(old_replacement){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found+'&old_found='+old_found+'&old_replacement='+old_replacement+'&old_replacement_found='+old_replacement_found;
        } else if(old_found || old_found == 0){
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found+'&old_found='+old_found;
        } else {
            var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found='+found;
        }
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    render(){
        const {id, row, items_length} = this.props;
        return(

            <div>

                {row.image ? (
                    <div>
                        <img src={"https://lieferservice123.com"+row.image} style={{width:"40px", float:"left"}} alt="Bild wird geladen..." />

                        <div style={{float:"right", width:"calc(100% - 50px)"}}>
                            <div>{row.marke}</div>
                            
                            <div style={{fontSize:"13px"}}>{row.quantity}x {row.name}</div>

                            {row.unit ? (
                                <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                            ) : row.grammageBadge ? (
                                <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                            ) : (
                                null
                            )}
                        </div>
                    </div>
                ) : (
                    <div style={{width:"100%"}}>
                        <div>{row.marke}</div>
                        
                        <div style={{fontSize:"13px"}}>{row.quantity}x {row.name}</div>

                        {row.unit ? (
                            <div><span style={{color:"#999", fontSize:"12px"}}>{row.unit}</span></div> 
                        ) : row.grammageBadge ? (
                            <div><span style={{color:"#999", fontSize:"12px"}}>{row.grammageBadge}</span></div>
                        ) : (
                            null
                        )}
                    </div>
                )}

                <div style={{width:"100%", float:"left", color:"#666", fontSize:"13px", fontWeight:300, fontFamily:"open sans", marginTop:"40px", marginBottom:"20px"}}>Wie viel hast du gefunden?</div>

                
                <input 
                    autoFocus="true"
                    type="tel"
                    style={{width:"calc(100% - 40px)", border:"none", textAlign:"right", fontSize:"20px", color:"rgb(72, 98, 112)"}}
                    // value="inputtest"
                    onChange={this.onFoundChanged} 
                    ref={this.inputRef} 
                />
                <div style={{width:"40px", float:"right", textAlign:"center", fontSize:"20px"}}>/ {row.quantity}</div>

                <div style={{float:"left", width:"100%", marginTop:"15px"}}>
                    <button
                        onClick={() => (this.state.found > 0 ? this.foundItem(id, row.id, row.quantity, this.state.found, row.found, row.replacement, row.found_replacement) : alert("Bitte Menge eingeben!"))}
                        type="button"
                        style={{padding:"10px", border:"none", width:"100%", color:"#fff", borderRadius:"6px", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                    >
                        Produkt gefunden
                        {/* <Ink duration="500" /> */}
                    </button>
                </div>

            </div>

        )
    }
}


const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(FoundItem);

