import React, { Component } from "react";

import Meta from "../../helpers/meta";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { loginDeliveryUser } from "../../../services/Delivery/user/actions";
import { getAllLanguages } from "../../../services/translations/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

import BackButton from "../../Takeaway/Elements/BackButton";
import Nav from "../../Takeaway/Nav";

class Login extends Component {
    state = {
        loading: false,
        email: "",
        password: "",
        error: false
    };
    static contextTypes = {
        router: () => null
    };
    componentDidMount() {
        this.props.getSettings();

        setTimeout(() => {
            this.setState({ error: false });
        }, 380);

        this.props.getAllLanguages();
    }

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    handleInputEmail = event => {
        this.setState({ email: event.target.value });
    };
    handleInputPassword = event => {
        this.setState({ password: event.target.value });
    };

    handleLogin = event => {
        event.preventDefault();
        this.setState({ loading: true });
        this.props.loginDeliveryUser(this.state.email, this.state.password);
    };

    componentWillReceiveProps(nextProps) {
        const { delivery_user } = this.props;
        if (delivery_user !== nextProps.delivery_user) {
            this.setState({ loading: false });
        }
        if (nextProps.delivery_user.success) {
            // this.context.router.push("/delivery");
        }
        if (nextProps.delivery_user.success === "false") {
            this.setState({ error: true });
        }

        if (this.props.languages !== nextProps.languages) {
            if (localStorage.getItem("multiLanguageSelection") === "true") {
                if (localStorage.getItem("userPreferedLanguage")) {
                    this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                    // console.log("Called 1");
                } else {
                    if (nextProps.languages.length) {
                        console.log("NEXT", nextProps.languages);

                        const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            } else {
                // console.log("Called 2");
                if (nextProps.languages.length) {
                    const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                    this.props.getSingleLanguageData(id);
                }
            }
        }
    }

    render() {
        console.log(this.props.languages);

        const { delivery_user } = this.props;
        if (delivery_user.success) {
            return (
                //redirect to account page
                window.location = "/delivery/dashboard"
            );
        }

        return (
            <React.Fragment>
                <Meta
                    seotitle="Login"
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />
                {/* PreLoading the loading gif */}
                <img src="/assets/img/loading-food.gif" className="hidden" alt="prefetching" />
                {this.state.error && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("loginErrorMessage")}</div>
                    </div>
                )}
                {this.state.loading && (
                    <div className="height-100 overlay-loading">
                        <div>
                            <img src="/assets/img/loading-food.gif" alt={localStorage.getItem("pleaseWaitText")} />
                        </div>
                    </div>
                )}
                <div style={{ backgroundColor: "#f2f4f9" }}>

                    {/* <div className="input-group">
                        <div className="input-group-prepend">
                            <div style={{ height: "3.5rem" }} />
                        </div>
                    </div> */}

                    {(window.innerWidth < 768 ? <div className="input-group">
                        <div className="input-group-prepend">
                            <BackButton history={this.props.history} />
                        </div>
                    </div> : <Nav logo={true} active_nearme={true} disable_back_button={true} />)}

                    {/* <img src="/assets/img/login-header.png" className="login-image pull-right mr-15" alt="login-header" /> */}

                    <div className="login-texts px-15 mt-50 pb-20" style={{maxWidth:"468px", margin:"auto"}}>
                        <span className="login-title">{localStorage.getItem("loginLoginTitle")} für Zusteller/Shopper</span>
                        <br />
                        <span className="login-subtitle">{localStorage.getItem("loginLoginSubTitle")}</span>
                    </div>

                </div>

                <div className="height-70 bg-white mb-100" style={{maxWidth:"468px", margin:"auto"}}>
                    <form onSubmit={this.handleLogin}>
                        <div className="form-group px-15 pt-30">

                            <label className="col-12 edit-address-input-label" style={{paddingLeft:0}}>
                                {localStorage.getItem("loginLoginEmailLabel")}
                            </label>
                            <div className="col-md-9 pb-5" style={{paddingLeft:0, maxWidth:"100%"}}>
                                <input type="text" name="email" onChange={this.handleInputEmail} className="form-control edit-address-input" />
                            </div>

                            <label className="col-12 edit-address-input-label" style={{paddingLeft:0}}>
                                {localStorage.getItem("loginLoginPasswordLabel")}
                            </label>
                            <div className="col-md-9" style={{paddingLeft:0, maxWidth:"100%"}}>
                                <input
                                    type="password"
                                    name="password"
                                    onChange={this.handleInputPassword}
                                    className="form-control edit-address-input"
                                />
                            </div>

                        </div>
                        <div className="mt-20 px-15 pt-15 button-block">
                            <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                {localStorage.getItem("loginLoginTitle")}
                            </button>
                        </div>
                    </form>
                    {localStorage.getItem("multiLanguageSelection") === "true" &&
                        (this.props.languages && this.props.languages.length > 0 && (
                            <div className="mt-100 d-flex align-items-center justify-content-center">
                                <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                                <select
                                    onChange={this.handleOnChange}
                                    defaultValue={
                                        localStorage.getItem("userPreferedLanguage")
                                            ? localStorage.getItem("userPreferedLanguage")
                                            : this.props.languages.filter(lang => lang.is_default === 1)[0].id
                                    }
                                    className="form-control language-select"
                                >
                                    {this.props.languages.map(language => (
                                        <option value={language.id} key={language.id}>
                                            {language.language_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        ))}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    delivery_user: state.delivery_user.delivery_user,
    languages: state.languages.languages,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { loginDeliveryUser, getSettings, getAllLanguages, getSingleLanguageData }
)(Login);
