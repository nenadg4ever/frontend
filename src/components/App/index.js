import React, { Component } from "react";

import Desktop from "../../components/Desktop";
import Takeaway from "../../components/Takeaway";
import Home from "../../components/Home/FirstScreen";
import { connect } from "react-redux";
import { getSettings } from "../../services/settings/actions";

import { getAllLanguages } from "../../services/translations/actions";

class App extends Component {
    componentDidMount() {
        //force update settings everytime
        this.props.getSettings();
        this.props.getAllLanguages();
    }

    render() {

        if(window.location.host == "lieferservice123.com" || window.location.host == "lieferservice.me" || window.location.host == "lieferservice.ag" || window.location.host == "lieferservice.vip"){
            return (
                <Takeaway languages={this.props.languages} />
            );
        } else {
            return (
                window.innerWidth <= 768 ? <Home languages={this.props.languages} /> : <Desktop languages={this.props.languages} />
            );
        }

    }
}

const mapStateToProps = state => ({
    settings: state.settings.settings,
    user: state.user.user,
    notification_token: state.notification_token.notification_token,
    languages: state.languages.languages
});

export default connect(
    mapStateToProps,
    { getSettings, getAllLanguages }
)(App);
