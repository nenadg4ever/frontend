import { GET_DELIVERY_RESTAURANTS_URL, GET_SELFPICKUP_RESTAURANTS_URL } from "../../../../configs/index";
import React, { Component } from "react";

import ContentLoader from "react-content-loader";
import DelayLink from "../../../helpers/delayLink";
import Ink from "react-ink";
import LazyLoad from "react-lazyload";
import ProgressiveImage from "react-progressive-image";

import axios from "axios";
import PromoSlider from "../PromoSlider";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMotorcycle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal } from 'react-bootstrap';

import Location from "../../Location";


class RestaurantList extends Component {
    
    constructor() {
        super();
        this.state = { 
            total: null,
            store: [],
            loading: false,
            no_store: false,
            bgWidth: window.innerWidth,
            bgHeight: (window.innerHeight / 4.2),
            cat: 'shops',
            subcat: ''
        };
    }
    
    componentDidMount() {
        localStorage.setItem("userPreferredSelection", "DELIVERY");
        localStorage.setItem("userSelected", "DELIVERY");
        this.getDeliveryStore();
      
        this.timeout = setInterval(() => {
            this.setState({ 
                bgWidth: window.innerWidth,
                bgHeight: (window.innerHeight / 4.2)
            });
            
        }, 1000);
    }

    //setup location
    showModalAddress (modal) {
        this.setState({
            AddressModal: true
        });
        if(modal !== undefined){
            this.setState({
                modal: modal
            });
        }
    }
    hideModalAddress = () => {
        this.setState({
            AddressModal: false
        });
    }

    //opening hours
    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }
    openingHours = (openFrom="14:00", openTo="03:00") => {
        //now date
        var now = new Date(); 

        //given date
        var h = now.getHours();
        var day = now.getDate();
        var month = ('0' + (now.getMonth() + 1)).slice(-2)
        var year = now.getUTCFullYear();

        //function slice() removes last 3 char from openTo to only get h without min
        //check if after midnight because date comparison should not be like 2020/01/09 14:00 - 2020/01/09 03:00
        
        if (h < 8 && openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var from = this.addDays2(from, -1);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
        }else if (openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is NOT after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
            var to = this.addDays2(to, 1);
        }else{
            var from = new Date(year+"/"+month+"/"+day+" "+openFrom); //set current date and time from table of restaurant 
            var to   = new Date(year+"/"+month+"/"+day+" "+openTo); //set current date and time from table of restaurant
        }

        var today = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+' '+now.getHours()+":"+now.getMinutes(); 
        var check = new Date(today); //this must always be current date, never change

        //return from+" - "+to;
        if (check > from && check < to)
            return ""; 
        else 
            return "geschlossen"; 
            
    }
    addDays2 = (date, amount) => {
        var tzOff = date.getTimezoneOffset() * 60 * 1000,
            t = date.getTime(),
            d = new Date(),
            tzOff2;
      
        t += (1000 * 60 * 60 * 24) * amount;
        d.setTime(t);
      
        tzOff2 = d.getTimezoneOffset() * 60 * 1000;
        if (tzOff != tzOff2) {
          var diff = tzOff2 - tzOff;
          t += diff;
          d.setTime(t);
        }
      
        return d;
    }

    multiSort = (array, sortObject = {}) => {
        const sortKeys = Object.keys(sortObject);

        // Return array if no sort object is supplied.
        if (!sortKeys.length) {
            return array;
        }

        // Change the values of the sortObject keys to -1, 0, or 1.
        for (let key in sortObject) {
            sortObject[key] = sortObject[key] === 'desc' || sortObject[key] === -1 ? -1 : (sortObject[key] === 'skip' || sortObject[key] === 0 ? 0 : 1);
        }

        const keySort = (a, b, direction) => {
            direction = direction !== null ? direction : 1;

            if (a === b) { // If the values are the same, do not switch positions.
                return 0;
            }

            // If b > a, multiply by -1 to get the reverse direction.
            return a > b ? direction : -1 * direction;
        };

        return array.sort((a, b) => {
            let sorted = 0;
            let index = 0;

            // Loop until sorted (-1 or 1) or until the sort keys have been processed.
            while (sorted === 0 && index < sortKeys.length) {
                const key = sortKeys[index];

                if (key) {
                    const direction = sortObject[key];

                    sorted = keySort(a[key], b[key], direction);
                    index++;
                }
            }

            return sorted;
        });
    }

    removeDuplicatedResturant = (arrayRes) => {
        arrayRes = arrayRes || [];
        const allResturantsName = [];
        return arrayRes.filter((ele) => {
            if (allResturantsName.includes(ele.name)) {
                return false;
            } else {
                allResturantsName.push(ele.name);
                return true;
            }
        });
    }

    getDeliveryStore = () => {
        if (localStorage.getItem("userSetAddress")) {
            if (!this.state.loading) {
                // Set loading state to true to
                // avoid multiple requests on scroll
                this.setState({
                    loading: true
                });

                // var cat = window.location.href.split("/").pop();
                // console.log("cat", this.state.cat);

                const userSetAddress = JSON.parse(localStorage.getItem("userSetAddress"));
                // make XHR request
                axios
                    .post(GET_DELIVERY_RESTAURANTS_URL, {
                        latitude: userSetAddress.lat,
                        longitude: userSetAddress.lng,
                        cat: this.state.cat,
                        subcat: this.state.subcat
                    })
                    .then(response => {
                        const store = response.data;
                        if (store.length) {

                            var sortStore = [];

                            for (var i = 0; i < store.length; i++) {

                                var item = store[i];

                                sortStore.push({
                                    "id" : item.id,
                                    "name" : item.name,
                                    "orders_accepted_online" : item.orders_accepted_online,
                                    "orders_accepted_tel" : item.orders_accepted_tel,
                                    "slug" : item.slug,
                                    "description" : item.description,
                                    "address" : item.address,
                                    "currencyFormat" : item.currencyFormat,

                                    "montag" : item.montag,
                                    "dienstag" : item.dienstag,
                                    "mittwoch" : item.mittwoch,
                                    "donnerstag" : item.donnerstag,
                                    "freitag" : item.freitag,
                                    "samstag" : item.samstag,
                                    "sonntag" : item.sonntag,

                                    "delivery_charges" : item.delivery_charges,
                                    "delivery_type" : item.delivery_type,
                                    "price_range" : item.price_range,

                                    "image" : item.image,
                                    "placeholder_image" : item.placeholder_image,
                                    
                                    "is_featured" : item.is_featured,
                                    "distance" : item.distance,
                                    "status" : ((item[this.getDayName()] == "geschlossen" || this.openingHours(item[this.getDayName()].slice(0, 5), item[this.getDayName()].slice(-5)) == "geschlossen" || item[this.getDayName()] == "") ? 0 : 1) 
                                });
                            }

                            var sortStore = this.removeDuplicatedResturant(this.multiSort(sortStore, {status: 'desc', distance: 'asc', orders_accepted_online: 'desc', is_featured: 'desc'}));

                            this.setState({
                                total: sortStore.length,
                                //store: [...this.state.store, ...store],
                                store: [...this.state.store, ...sortStore],
                                loading: false,
                                no_store: false
                            });
                        } else {
                            this.setState({
                                total: null,
                                loading: false,
                                no_store: true
                            });
                        }
                    });
            }
        }
    }

    filter = (cat, subcat) => {
        if(cat == "Restaurant") {
            cat = "restaurant"
        } else if(cat == "Lebensmittel") {
            cat = "grocery"
        } else if(cat == "Tankstelle") {
            cat = "gasStation"
        } else if(cat == "Drogerie") {
            cat = "drugStore"
        } else if(cat == "Tierbedarf") {
            cat = "petSupplies"
        } else if(cat == "Alkohol") {
            cat = "alcohol"
        } else if(cat == "Cannabisladen") {
            cat = "cannabisStore"
        } else if(cat == "Elektrofachgeschäft") {
            cat = "electricalSuppliesStore"
        } else if(cat == "Handygeschäft") {
            cat = "cellPhoneStore"
        } else if(cat == "Computergeschäft") {
            cat = "computerStore"
        } else if(cat == "Baumarkt") {
            cat = "hardwareStore"
        } else if(cat == "Möbelgeschäft") {
            cat = "furnitureStore"
        } else if(cat == "Buchhandlung") {
            cat = "bookStore"
        } else if(cat == "Sportgeschäft") {
            cat = "sportingGoodsStore"
        } else if(cat == "Modegeschäft") {
            cat = "clothingStore"
        } else if(cat == "Schuhgeschäft") {
            cat = "shoeStore"
        }
        this.setState(
            () => ({
                cat: cat,
                subcat: subcat,
                store: [],
            }),
            () => {
                this.getDeliveryStore();
            }
        );
    }
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz+city"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    render() {
        const {bgWidth, bgHeight} = this.state;

        // var cat = window.location.href.split("/").pop();
        // if(cat == 'Grocery'){
        //     var bgID = "1452948491233-ad8a1ed01085";
        // } else if(cat == 'restaurant'){
        //     var bgID = "1571939634097-f1829662cdfc";
        // } else if(cat == 'cellPhoneStore'){
        //     var bgID = "1556656793-08538906a9f8";
        // }else if(cat == 'pharmacy'){
        //     var bgID = "1564202572318-0bda12d302cc";
        // }else if(cat == 'bookStore'){
        //     var bgID = "1507842217343-583bb7270b66";
        // }else{
        //     var bgID = "1452948491233-ad8a1ed01085";
        // }

        var categories = ["Restaurant", "Lebensmittel", "Tankstelle", "Drogerie", "Tierbedarf", "Alkohol", "Cannabisladen", 
        "Elektrofachgeschäft", "Handygeschäft", "Computergeschäft", "Baumarkt", "Möbelgeschäft", "Buchhandlung", 
        "Sportgeschäft", "Modegeschäft", "Schuhgeschäft"];
        var categoriesRestaurant = [
            "Pizza", "Burger", "Sushi", 
            "Österreichisch", "Asiatisch", "Indisch", 
            "Italienisch", "Griechisch", "Spanisch", "Mexikanisch", "Balkan", "Türkisch", "Vietnamesisch", "Chinesisch", "Thailändisch", "Orientalisch", 
            "Döner", "Spareribs", "Schnitzel"
        ];

        console.log("this.state.cat", this.state.cat);

        return (
            <React.Fragment>

                {/* <div className="backgroundImg" style={{backgroundImage: "linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(https://images.unsplash.com/photo-"+bgID+"?ixlib=rb-1.2.1&auto=format&fit=crop&w="+bgWidth+"&h="+bgHeight+"&q=80)"}}> */}
                <h2 style={{background:"rgb(232, 233, 238)", color:"#393939", padding:"0 18px 10px", margin:0, textAlign:"center"}}>
                    {this.state.store.length === 0 ? (
                        <div>
                            Shops
                            <div style={{fontSize:"14px", marginTop:"5px"}}>Laden...</div>
                        </div>
                    ) : this.state.no_store ? (
                        <div>Keine Anbieter gefunden</div>
                    ) : (
                        <div>
                            Shops
                            <div style={{fontSize:"14px", marginTop:"5px"}}>
                                {this.state.total} Ergebnisse in <span onClick={() => this.showModalAddress("?showModalShops=1") } style={{color:"#1a0dab", textDecoration:"underline", cursor:"pointer"}}>{this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "plz+city")}</span>
                            </div>
                        </div>
                    )}
                </h2>

                {/* categories */}
                <div className={"sticky-top pt-10 pb-10 text-center"} style={{width:"100%", background:"#fff", borderBottom:"1px solid #eee", overflow:"overlay"}}>
                    {categories.map((category, index) => (
                        // <button onClick={() => this.filter(category)} className={ "btn btn-preference mr-2 " + (this.state.cat == category ? "user-preferred" : "") }>
                        //     {category}
                        // </button>
                        <span onClick={() => this.filter(category)} style={{cursor:"pointer"}} className={ "ml-2 mr-2 " + (this.state.cat == category ? "user-preferred" : "") }>{category}</span>
                    ))}
                </div>

                {/* restaurant categories */}
                {this.state.cat == "restaurant" && (
                    <div className={"sticky-top pt-10 pb-10 text-center fs-10"} style={{top:"41.77px", background:"#fff", borderBottom:"1px solid #eee", overflow:"overlay", width:"100%", fontWeight:300, fontFamily:"open sans"}}>
                        {categoriesRestaurant.map((category, index) => (
                            <span onClick={() => this.filter("restaurant", category)} style={{cursor:"pointer"}} className={ "ml-2 mr-2 " + (this.state.subcat == category ? "user-preferred" : "") }>{category}</span>
                        ))}
                    </div>
                )}

                {this.state.store.length === 0 ? (

                    <ContentLoader height={378} width={400} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb" style={{background:"#fff"}}>
                        <rect x="20" y="20" rx="4" ry="4" width="80" height="78" />
                        <rect x="144" y="30" rx="0" ry="0" width="115" height="18" />
                        <rect x="144" y="60" rx="0" ry="0" width="165" height="16" />

                        <rect x="20" y="145" rx="4" ry="4" width="80" height="78" />
                        <rect x="144" y="155" rx="0" ry="0" width="115" height="18" />
                        <rect x="144" y="185" rx="0" ry="0" width="165" height="16" />

                        <rect x="20" y="270" rx="4" ry="4" width="80" height="78" />
                        <rect x="144" y="280" rx="0" ry="0" width="115" height="18" />
                        <rect x="144" y="310" rx="0" ry="0" width="165" height="16" />
                    </ContentLoader>

                ) : (

                    this.state.store.map((restaurant, index) => (
                        <div key={restaurant.id} className="shopsList_v2" style={{borderBottom: "1px solid #eee"}}>
                            <div className="col-xs-12 col-sm-12 restaurant-block" style={{position:"relative"}}> 

                                {/* deliveries */}
                                {/* {restaurant.orders_accepted_online ? <span style={{position: "absolute", bottom: "15px", left: "5px", color:"#999", fontSize:"10px", fontWeight:"bold"}}>
                                    <span style={{margin:"0 8px", color:localStorage.getItem("storeColor")}}>
                                        {restaurant.orders_accepted_online} {localStorage.getItem("homePageDeliveries")}
                                    </span>
                                </span> : ""} */}
                                            
                                <a href={"/"+restaurant.slug} className="text-center" style={{marginBottom:0}}>
                                    <div
                                        className={`block-content block-content-full ${
                                            restaurant.is_featured ? "ribbon ribbon-bookmark ribbon-warning pt-2" : "pt-2"
                                        } `}
                                    >
                                        {restaurant.is_featured ? (
                                            <div className="ribbon-box">{localStorage.getItem("restaurantFeaturedText")}</div>
                                        ) : null}

                                        {/* <LazyLoad> */}
                                            <ProgressiveImage delay={100} src={restaurant.image} placeholder={restaurant.placeholder_image}>
                                                {(src, loading) => (
                                                    <img
                                                        src={src}
                                                        alt={restaurant.name}
                                                        className="restaurant-image"
                                                        style={{
                                                            // filter: loading ? "blur(1.2px) brightness(0.9)" : "none"
                                                        }}
                                                    />
                                                )}
                                            </ProgressiveImage>
                                        {/* </LazyLoad> */}
                                    </div>

                                    <div className="block-content block-content-full restaurant-info">

                                        <div className="font-w600 mb-5 text-dark" style={{fontSize:"18px"}}>{restaurant.name}</div>

                                        <div className="font-size-sm text-muted truncate-text">{restaurant.description}</div>
                                        {/* <div className="font-size-sm text-muted truncate-text">{restaurant.distance.toFixed(2) < 1 ? restaurant.distance.toFixed(2)*1000+" m":restaurant.distance.toFixed(2)+" km"} · {restaurant.address}</div> */}

                                        {/* <hr className="my-10" /> */}
                                        
                                        <div className="restaurant-meta mt-5 align-items-center justify-content-between text-muted" style={{ fontSize:"12px"}}>

                                            {/* rating */}
                                            {/* <div className="p-0 text-left">
                                                <i className="fa fa-star pr-1" style={{ color: localStorage.getItem("storeColor") }} />{" "} {restaurant.rating}
                                            </div> */}

                                            {(restaurant.orders_accepted_online+restaurant.orders_accepted_tel) >= 5 ? 
                                                <span style={{paddingRight:"8px"}}>
                                                    <i className="fa fa-star pr-1" style={{color: localStorage.getItem("storeColor")}} />{restaurant.orders_accepted_online+restaurant.orders_accepted_tel}
                                                </span>
                                            : null}

                                            {/* delivery time */}
                                            {(restaurant[this.getDayName()]) ? 
                                                ((restaurant[this.getDayName()] == "geschlossen" || this.openingHours(
                                                    restaurant[this.getDayName()].slice(0, 5),//from ex: 10:00
                                                    restaurant[this.getDayName()].slice(-5)//to ex: 23:00
                                                ) == "geschlossen") ? 
                                                <span style={{color:"#ff4246", paddingRight:"8px"}}>
                                                    <i className="si si-clock pr-1" />
                                                    {localStorage.getItem("homePageClosed")}
                                                </span> : <span style={{color:"green", paddingRight:"8px"}}>
                                                    <i className="si si-clock pr-1" />
                                                    {localStorage.getItem("homePageOpen")}
                                                    {/* {restaurant[this.getDayName()]} */}
                                                </span>) 
                                            : null}
                                            
                                            {/* delivery charges */}
                                            <FontAwesomeIcon icon={faMotorcycle} style={{fontSize:"12px", marginRight:"3px"}} /><span style={{paddingRight:"8px"}} >{restaurant.delivery_charges=='0.00'?localStorage.getItem("homePageFree"):restaurant.delivery_charges+" "+(restaurant.country == 'ch' ? "CHF":localStorage.getItem("currencyFormat"))}</span>

                                            {/* min order */}
                                            <span style={{display:"inline-block"}}>
                                                <FontAwesomeIcon icon={faShoppingCart} style={{fontSize:"12px", marginRight:"3px"}} />
                                                {localStorage.getItem("homePageForTwoText")}{" "}
                                                {restaurant.price_range}{" "}
                                                {restaurant.currencyFormat}
                                            </span>
                                        </div>
                                    </div>
                                    <Ink duration="500" hasTouch={false} />
                                </a>
                            </div>

                            {localStorage.getItem("showPromoSlider") === "true" && (
                                <React.Fragment>
                                    {this.props.slides && this.props.slides.length > 0 && (
                                        <React.Fragment>
                                            {index === this.props.slides[0]["promo_slider"]["position_id"] - 1 && (
                                                <PromoSlider
                                                    slides={this.props.slides}
                                                    size={this.props.slides[0]["promo_slider"]["size"]}
                                                    secondarySlider={true}
                                                />
                                            )}
                                        </React.Fragment>
                                    )}
                                </React.Fragment>
                            )}
                        </div>
                    ))

                )}

                {/* setup location */}
                <Modal show={this.state.AddressModal} onHide={this.hideModalAddress} size="sm" style={{background:"rgba(51,51,51,0.85)"}}>
                    <Modal.Header closeButton className="bg-grey"><Modal.Title>Standort festlegen</Modal.Title></Modal.Header>
                    <Modal.Body style={{padding:"0 25px 25px"}} className="bg-grey">
                        <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Lieferoptionen und -geschwindigkeit können, abhängig von der Lieferadresse, variieren</div>
                        <Location 
                            modal={this.state.modal || ''} 
                            redirect={window.location.href.replace("?install=1", "").split("/").pop()}
                        />
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

export default RestaurantList;
