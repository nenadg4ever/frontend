// import * as firebase from "firebase/app";

import React, { Component } from "react";
import axios from "axios";

import Footer from "../Footer";
import Meta from "../../helpers/meta";
import Nav from "../Nav";
import PromoSlider from "./PromoSlider";
import { Redirect } from "react-router";
import RestaurantList from "./RestaurantList";
import { connect } from "react-redux";
import { getPromoSlides } from "../../../services/promoSlider/actions";
import { getSettings } from "../../../services/settings/actions";
// import messaging from "../../../init-fcm";
// import { saveNotificationToken } from "../../../services/notification/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";
import Slide from "react-reveal/Slide";

class Home extends Component {
    static contextTypes = {
        router: () => null
    };

    async componentDidMount() {
        this.props.getSettings();
        //if currentLocation doesnt exists in localstorage then redirect the user to firstscreen
        //else make API calls
        if (localStorage.getItem("userSetAddress") !== null) {
            // this.context.router.history.push("/search-location");
            // console.log("Redirect to search location");
            // return <Redirect to="/search-location" />;
            this.props.getPromoSlides();
        } else {
            //call to promoSlider API to fetch the slides
        }

        //push notification, save token in push_tokens tbl
        // const { user } = this.props;
        // if (user.success) {
        //     if (firebase.messaging.isSupported()) {
        //         let handler = this.props.saveNotificationToken;
        //         messaging
        //             .requestPermission()
        //             .then(async function() {
        //                 const push_token = await messaging.getToken();
        //                 handler(push_token, user.data.id, user.data.auth_token);
        //             })
        //             .catch(function(err) {
        //                 console.log("Unable to get permission to notify.", err);
        //             });
        //         navigator.serviceWorker.addEventListener("message", message => console.log(message));
        //     }
        // }

        if (localStorage.getItem("state") !== null) {
            const languages = JSON.parse(localStorage.getItem("state")).languages;
            if (languages && languages.length > 0) {
                if (localStorage.getItem("multiLanguageSelection") === "true") {
                    if (localStorage.getItem("userPreferedLanguage")) {
                        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                        // console.log("Called 1");
                    } else {
                        if (languages.length) {
                            const id = languages.filter(lang => lang.is_default === 1)[0].id;
                            this.props.getSingleLanguageData(id);
                        }
                    }
                } else {
                    // console.log("Called 2");
                    if (languages.length) {
                        const id = languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.languages !== nextProps.languages) {
        }
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    render() {

        var cat = window.location.href.split("/").pop();
        localStorage.setItem("branche", cat);

        if (localStorage.getItem("userSetAddress") === null) {
            // this.context.router.history.push("/search-location");
            console.log("Redirect to search location");
            if(document.URL.indexOf("?cat=") >= 0) {
                this.context.router.history.push("/search-location?cat="+this.getUrlParameter("cat"));
            } else {
                return <Redirect to={"/search-location?redirect="+cat} />;
            }
        }

        const { history, user, promo_slides } = this.props;

        // console.log(promo_slides.mainSlides.length);

        return (
            <React.Fragment>

                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <div className="height-100-percent" style={{background:"rgb(232, 233, 238)"}}>

                    <Nav logo={true} active_nearme={true} disable_back_button={true} history={history} loggedin={user.success} />
                    
                    {/* Passing slides as props to PromoSlider */}

                    {localStorage.getItem("showPromoSlider") === "true" && (
                        <React.Fragment>
                            {promo_slides && promo_slides.mainSlides && promo_slides.mainSlides.length > 0 && (
                                <PromoSlider slides={promo_slides.mainSlides} size={promo_slides.mainSlides[0]["promo_slider"]["size"]} />
                            )}
                        </React.Fragment>
                    )}

                    <Slide bottom duration={500}>
                        <RestaurantList slides={promo_slides.otherSlides} />
                    </Slide>

                    <Footer active_nearme={true} />

                </div>
                
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    promo_slides: state.promo_slides.promo_slides,
    user: state.user.user,
    locations: state.locations.locations,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getPromoSlides, getSettings, getSingleLanguageData }
)(Home);
