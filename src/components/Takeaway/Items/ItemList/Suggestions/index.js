import React, { Component } from 'react'
import Ink from "react-ink";
import '../../../FirstScreen/suggestion2.css';
import ReactDOM from 'react-dom';

import ItemBadge from "../ItemBadge";

//bootstrap
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import Customization from "../../Customization";


const style = {
	float:"left", 
	width:"calc(100% - 110px)"
}

class Suggestions extends Component {

	state = {
		ZoomModal: false,
		customizationModal: false,
	};

    //customization
    showModalCustomization (id) {
        this.setState({
            customizationModal: {
                [id]: true
            }
        });
    }
    hideModalCustomization = () => {
        this.setState({
            customizationModal: false
        })
    }

	showModalZoom (id) {
		this.setState({
			ZoomModal: {
				[id]: true
			}
		});
	}
	hideModalZoom = () => {
		this.setState({
			ZoomModal: false
		});
	}

	componentDidMount() {
		// document.addEventListener('click', this.handleClickOutside, true);
	}

	componentWillUnmount() {
		// document.removeEventListener('click', this.handleClickOutside, true);
	}

	categoryHtml  = (r, props) => (
		<div 
			key={r.id} 
			style={{display:"inline-block", textDecoration:"underline", fontSize:"12px", margin:"2px 5px", position:"relative", cursor:"pointer", color: (props.selectedCategory === r.category) ? "blue" : "rgb(79, 101, 135)"}} 
			onClick={()=> props.searchReplacements ? props.addReplacement(r) : props.suggestion2OptionClicked(r, 'category')}
		>
			{r.category} 
			<Ink duration="500" />
		</div>
	);
	subCategoryHtml = (r, props, currentSearchType) => (
		<div 
			key={r.id} 
			style={{display:"inline-block", textDecoration:"underline", fontSize:"12px", margin:"2px 5px", position:"relative", cursor:"pointer", color: (props.selectedSubCategory === r[currentSearchType]) ? "blue" : "rgb(79, 101, 135)"}} 
			onClick={()=>props.suggestion2OptionClicked(r, 'subCategory', currentSearchType)}
		>

			{r[currentSearchType]} 
			<Ink duration="500" />

		</div>
	);
	errorMessageHtml = () => {
		console.log("in error message");

		return (
			this.props.no_sub_cat_results ? (
				<h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Keine Ergebnisse gefunden</h2>
			) : (
				<h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Laden...</h2>
			)
		)
	};
	handleClickOutside = event => {
		const domNode = ReactDOM.findDOMNode(this);

		if (!domNode || !domNode.contains(event.target) && !this.state.ZoomModal) {
			console.log("handle click");
			// document.getElementById('suggestionsBox').style.display = "none";
			// document.getElementById('closeSuggestionsIcon').style.display = "none";
			// this.props.resetSearchInputStates();
		}
	}
	breadcrumbHtml  = (currentSearchType) => {
		if (this.props.selectedCategory || this.props.subCategoryList.length) {

			return (

				<React.Fragment>
			
				{/* <div style={{background:"rgb(231, 238, 248)", borderBottom:"1px solid #d7d7d7", borderTopLeftRadius:"3px", borderTopRightRadius:"3px"}}> */}
					
					<div style={{width:"100%", display:"inline-block", fontSize:"12px", margin:"2px 5px", position:"relative", fontWeight:"bold"}}>

						<span onClick={()=>this.props.getSearchInfoCat(false, true)} style={{color:"rgb(79, 101, 135)", padding:"5px 10px", cursor:"pointer"}}>Home</span>

						{(localStorage.getItem("selectedCategory")) && (
							<span>
								<span>&#187;</span>
								<span onClick={()=>this.props.suggestion2OptionClicked({category: localStorage.getItem("selectedCategory")}, 'category')} style={{color:"rgb(79, 101, 135)", padding:"5px 10px", cursor:"pointer"}}>
									{localStorage.getItem("selectedCategory")}
								</span>
							</span>
						)}

						{(localStorage.getItem("sub_category")) && (
							<span>
								<span>&#187;</span>
								<span style={{padding:"5px 10px"}}>{localStorage.getItem("sub_category")}</span>
							</span>
						)} 
						{/* { (localStorage.getItem("sub_category2")) ? ` -> ${localStorage.getItem("sub_category2")}` : '' }  */}

					</div>
					<br />

				{/* </div> */}
				</React.Fragment>

			)
		} else {
			return '';
		}
	}

	productItemHtml = (r, props) => (
		<li className={"suggestion-word-style"+((r.quantity >= 0) ? ' borderAdd' : '')} key={r.id} style={{textAlign:"left", background:"#fff", borderTop: "1px solid rgb(238, 238, 238)"}}>

			{r.placeholder_image && <img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{maxWidth:"100px", marginRight:"10px", float:"left"}} onClick={this.showModalZoom.bind(this, r.id)} />} 

			{r.quantity >= 0 && <div className="react-reveal quantity-badge-list moreCss">{r.quantity}</div>}

			<div style={r.placeholder_image&&style} onClick={()=> props.searchReplacements ? props.addReplacement(r, props.id) : this.addButtonClicked(r)}>

				<div style={{color:"#333", fontSize:"12px"}}>{r.name}</div>

				<div style={{color:"#888", fontSize:"10px"}}>{r.category} {r.sub_category ? " » "+r.sub_category : null} {r.sub_category2 ? " » "+r.sub_category2 : null}</div>

				<div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px", marginTop:"15px"}}>
					{parseFloat(r.price).toFixed(2)} € 
					<span style={{color:"#888", fontSize:"10px", fontWeight:"normal", marginLeft:"15px"}}>{r.grammageBadge}</span>
				</div>

			</div>

			{/* show product details */}
			<Modal show={this.state.ZoomModal[r.id]} onHide={this.hideModalZoom} size="lg" style={{background:"rgba(51,51,51,0.85)"}}>
				<Modal.Header closeButton>
				</Modal.Header>
				<Modal.Body style={{padding:"0 25px 25px"}}>

					<div className="productImage">
						
						{/* recommended, new */}
						<ItemBadge item={r} />

						<img src={"https://lieferservice123.com"+r.image} alt="Bild wird geladen..." />
					</div>

					<div className="productDetails">

						<h1>{r.name}</h1>

						<div style={{margin:"20px 0"}}>
							<div className="price">
								{(r.price_old && r.price_old !== r.price) ? (
									<span>
										<del style={{color:"#999", fontSize:"14px", fontWeight:400, fontFamily:"open sans"}}>€{r.price_old.toFixed(2)}</del> €{parseFloat(r.price).toFixed(2)}
									</span>
								) : (
									<span>€{parseFloat(r.price).toFixed(2)}</span>
								)}
							</div>
							{r.grammageBadge && <div style={{color:"rgb(142, 142, 142)", fontSize:"14px"}}>{r.grammageBadge}</div>}
						</div>

						{r.description && <div className="description" dangerouslySetInnerHTML={{ __html: r.description }}></div>}

						{props.searchReplacements ? (
							<button onClick={()=> props.addReplacement(r, props.id)} type="button" className="btn btn-lg" style={{background:"#60b246", color:"#fff"}}><FontAwesomeIcon icon={faPlus} style={{marginRight:"5px"}} /> Ersatzprodukt auswählen</button>
						) : (
							<button onClick={()=> { this.addButtonClicked(r); }} type="button" className="btn btn-lg" style={{background:"#60b246", color:"#fff"}}><FontAwesomeIcon icon={faPlus} style={{marginRight:"5px"}} /> In den Einkaufswagen</button>
						)}

						{r.marke && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Marke:</b> <span style={{fontWeight:300}}>{r.marke}</span></div>}
						{r.category && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Kategorie:</b> <span style={{fontWeight:300}}>{r.category}</span></div>}
						{r.sub_category && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Unterkategorie:</b> <span style={{fontWeight:300}}>{r.sub_category}</span></div>}
						{r.sub_category2 && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Sub-Unterkategorie:</b> <span style={{fontWeight:300}}>{r.sub_category2}</span></div>}

					</div>
				</Modal.Body>
			</Modal>

			<Modal show={this.state.customizationModal[r.id]} onHide={this.hideModalCustomization} size="sm" style={{background:"rgba(51,51,51,0.85)"}}>
				<Modal.Header closeButton>
				</Modal.Header>
				<Modal.Body style={{padding:"32px"}}>
					{(r.addon_categories && r.addon_categories.length) ? (
						<Customization
							product={r}
							forceUpdate={this.props.forceUpdate}
							hideModalCustomization={this.hideModalCustomization}
						/>
					) : (null)}
				</Modal.Body>
			</Modal>

			{/* <Ink duration="500" /> */}

		</li>
	);
    addButtonClicked = (item)=>{
		console.log("item", item);
        if (item.addon_categories && item.addon_categories.length) {
            this.showModalCustomization(item.id);
        } else {
            this.props.suggestion2OptionClicked(item, 'product')
        }
    };

	render() {

		let { results, catSearchResults, modal_input_results, selectedCategory, subCategoryList, currentSearchType } = this.props;

		results = results || [];
		modal_input_results = modal_input_results || [];

		results = results.map((productItem) => {
			const itemFoundIndex =  modal_input_results.findIndex(ele => (ele && productItem && ele.id === productItem.id));
			
			if (itemFoundIndex >= 0) {
				// console.log(modal_input_results[itemFoundIndex]);
				productItem.quantity = modal_input_results[itemFoundIndex].quantity;
			}

			return productItem;
		});

		switch (currentSearchType) {
			case 'cat':
				currentSearchType = 'sub_category';
			break;

			case 'sub_category':
				currentSearchType = 'sub_category2';
			break;

			default:

			break;
		}

		const firstSelectInput = localStorage.getItem("firstSelectInput");
		catSearchResults = catSearchResults || [];
		let options = '';
		selectedCategory = localStorage.getItem("selectedCategory");

		if (selectedCategory || (subCategoryList.length)) {
			if (subCategoryList.length || results.length) {
				options = [this.breadcrumbHtml(currentSearchType)]
				.concat(
					subCategoryList.map(r => this.subCategoryHtml(r, this.props, currentSearchType))
					.concat(
						results.map(r => this.productItemHtml(r, this.props))
					)
				);
			} else if (firstSelectInput) {
				options = this.errorMessageHtml();
			}
		} else if (catSearchResults.length || results.length) {
			options = [this.breadcrumbHtml(currentSearchType)]
			.concat(
				catSearchResults.map(r => this.categoryHtml(r, this.props))
				.concat(
					results.map(r => this.productItemHtml(r, this.props))
				)
			);
		} else if (firstSelectInput) {
			options = this.errorMessageHtml();
		}

		// options = `${JSON.stringify(this.breadcrumbHtml(currentSearchType))} ${JSON.stringify(options)}`

		return (

			<div>

				<ul id="suggestionsBox" style={{textAlign:"center", zIndex:2, borderRadius:"3px", overflow:"auto", listStyle:"none", padding:0, margin:0, background:"#fff", maxWidth:"100%"}}> {options}</ul>

			</div>

		)

	}
}

export default Suggestions