import React, { Component } from "react";
import { addProduct, removeProduct } from "../../../../../services/cart/actions";

import Customization from "../../Customization";
import Fade from "react-reveal/Fade";
import Ink from "react-ink";
import LazyLoad from "react-lazyload";
import ProgressiveImage from "react-progressive-image";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class RecommendedItems extends Component {
    static contextTypes = {
        router: () => null
    };

    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            this.props.update();
        }, 100);
    };

    render() {
        const { addProduct, removeProduct, product, cartProducts, restaurant } = this.props;
        product.quantity = 1;
        return (
            <React.Fragment>
                <div key={product.id} className="col-6 p-0 d-flex justify-content-center">
                    <div className="block block-link-shadow mb-50">
                        <div className="block-content recommended-item-content pt-0">
                            <LazyLoad>
                                <ProgressiveImage src={product.image} placeholder={product.placeholder_image}>
                                    {src => (
                                        <Link to={restaurant.slug + "/" + product.id}>
                                            <img src={src} alt={product.name} className="img-fluid recommended-item-image" />
                                        </Link>
                                    )}
                                </ProgressiveImage>
                                <React.Fragment>
                                    {cartProducts.find(cp => cp.id === product.id) !== undefined && (
                                        <Fade duration={150}>
                                            <div
                                                className="quantity-badge-recommended"
                                                style={{
                                                    backgroundColor: localStorage.getItem("storeColor")
                                                }}
                                            >
                                                <span>
                                                    {product.addon_categories.length ? (
                                                        <React.Fragment>
                                                            <i className="si si-check" style={{ lineHeight: "1.3rem" }}></i>
                                                        </React.Fragment>
                                                    ) : (
                                                        <React.Fragment>{cartProducts.find(cp => cp.id === product.id).quantity}</React.Fragment>
                                                    )}
                                                </span>
                                            </div>
                                        </Fade>
                                    )}
                                </React.Fragment>
                            </LazyLoad>
                            <div className="mt-2 mb-2 recommended-item-meta">
                                <div className="px-5 text-left">
                                    <span className="meta-name">{product.name}</span>
                                    <br />
                                    <span className="meta-price">
                                        {localStorage.getItem("hidePriceWhenZero") === "true" && product.price === "0.00"
                                            ? null
                                            : localStorage.getItem("currencyFormat") + " " + product.price}
                                    </span>
                                    {product.addon_categories.length > 0 && (
                                        <span className="ml-2 customizable-item-text" style={{ color: localStorage.getItem("storeColor") }}>
                                            {localStorage.getItem("customizableItemText")}
                                        </span>
                                    )}
                                </div>
                                <div className="btn-group btn-group-sm mt-5 btn-full" role="group" aria-label="btnGroupIcons1">
                                    {product.addon_categories.length ? (
                                        <button
                                            disabled
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                        >
                                            <span className="btn-dec">-</span>
                                            <Ink duration="500" />
                                        </button>
                                    ) : (
                                        <button
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                            onClick={() => {
                                                removeProduct(product);
                                                this.forceStateUpdate();
                                            }}
                                        >
                                            <span className="btn-dec">-</span>
                                            <Ink duration="500" />
                                        </button>
                                    )}
                                    {product.addon_categories.length ? (
                                        <Customization
                                            product={product}
                                            addProduct={addProduct}
                                            update={this.props.forceStateUpdate}
                                            forceUpdate={this.forceStateUpdate}
                                        />
                                    ) : (
                                        <button
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                            onClick={() => {
                                                addProduct(product);
                                                this.forceStateUpdate();
                                            }}
                                        >
                                            <span className="btn-inc">+</span>
                                            <Ink duration="500" />
                                        </button>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cartProducts: state.cart.products
});

export default connect(
    mapStateToProps,
    { addProduct, removeProduct }
)(RecommendedItems);
