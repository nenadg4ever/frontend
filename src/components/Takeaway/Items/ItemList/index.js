import React, { Component } from "react";
import { addProduct, removeProduct } from "../../../../services/cart/actions";

import Collapsible from "react-collapsible";
import ContentLoader from "react-content-loader";
import Customization from "../Customization";
import Fade from "react-reveal/Fade";
import Ink from "react-ink";
import ItemBadge from "./ItemBadge";
import LazyLoad from "react-lazyload";
import { Link } from "react-router-dom";
import ProgressiveImage from "react-progressive-image";
import RecommendedItems from "./RecommendedItems";
import ShowMore from "react-show-more";
import Slide from "react-reveal/Slide";
import { connect } from "react-redux";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown'

import axios from "axios";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { faChevronCircleDown } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';


import MainPageCategories from './ItemCat/mainPageCategories';
import MainPageSubCategories from './ItemCat/mainPageSubCategories';
import MainPageSub2Categories from './ItemCat/mainPageSub2Categories';
import MainPageSub2Items from './ItemCat/mainPageSub2Items';


class ItemList extends Component {
    state = {
        update: true,
        customizationModal: false,
        ZoomModal: false,

    };

    showModalZoom (id) {
        this.setState({
			ZoomModal: {
				[id]: true
			}
        });
    }
	hideModalZoom = () => {
		this.setState({
			ZoomModal: false
		});
	}

    //customization
    showModalCustomization (id) {
        this.setState({
            customizationModal: {
                [id]: true
            }
        });
    }
    hideModalCustomization = () => {
        this.setState({
            customizationModal: false
        })
    }
    
    removeButtonClicked = (item)=>{
        item.quantity = 1;
        this.props.removeProduct(item);
        this.forceStateUpdate();
    };
    addButtonClicked = (item)=>{
        if (item.addon_categories && item.addon_categories.length) {
            this.showModalCustomization(item.id);
        } else {
            this.props.addProduct(item);
            this.forceStateUpdate();
        }
    };
    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            if (this.state.update) {
                this.setState({ update: false });
            } else {
                this.setState({ update: true });
            }
        }, 100);
    };

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    catImg = (category, description) => {
        if(category ){

            if(category.toLowerCase().indexOf("pizzastangerl") > -1) {
                var id = '1599553132172-8089d8f65b5c';
            } else if(category.toLowerCase().indexOf("pizza") > -1 || category.toLowerCase().indexOf("pizzen") > -1) {
                var myArr = [
                    '1534308983496-4fabb1a015ee','1585778718569-2efde6c55c80', '1590947132387-155cc02f3212', '1600346019001-8d56d1b51d59', '1558701228-22a3ddd21a66'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("pasta") > -1) {
                var myArr = [
                    '1574969903809-3f7a1668ceb0', '1555949258-eb67b1ef0ceb','1551183053-bf91a1d81141', '1579684947550-22e945225d9a', '1551183053-bf91a1d81141'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("kebap") > -1 || category.toLowerCase().indexOf("kebab") > -1) {
                var id = "1529006557810-274b9b2fc783";
            } else if(category.toLowerCase().indexOf("burger") > -1) {
                if(category.toLowerCase().indexOf("burger-menü") > -1 || category.toLowerCase().indexOf("burger menü") > -1) {
                    var myArr = [
                        '1600891964751-dc0cc8611e47', '1583086987435-49e189663659'
                    ];
                } else {
                    var myArr = [
                        '1571091718767-18b5b1457add', '1586190848861-99aa4a171e90', '1583086987435-49e189663659', '1600891964751-dc0cc8611e47', 
                    ];
                }
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("salat") > -1 || category.toLowerCase().indexOf("salad") > -1) {
                var myArr = [
                    '1529059997568-3d847b1154f0','1549745708-fa4a8423a0b4', '1561043433-aaf687c4cf04', '1544982590-068dabc956dc'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("dessert") > -1) {
                var myArr = [
                    '1585164743050-e27677e3819d', '1560143160-ad7fcd3a67ca'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("alkoholfrei") > -1) {
                var id = "1551538827-9c037cb4f32a";
            } else if(category.toLowerCase().indexOf("alkohol") > -1) {
                var id = "1567579518027-7a113279f8ce";
            } else if(category.toLowerCase().indexOf("beilagen") > -1) {
                var myArr = [
                    '1526230427044-d092040d48dc','1536304993881-ff6e9eefa2a6'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            }else if(category.toLowerCase().indexOf("sauce") > -1) {
                var id = "1600607700607-bf154beb191f";
            } else if(category.toLowerCase().indexOf("suppe") > -1) {
                var myArr = [
                    '1578861256505-d3be7cb037d3','1547592166-23ac45744acd', '1582724790987-313797eb6119'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("nudel") > -1) {
                var id = "1600490036275-35f5f1656861";
            } else if(category.toLowerCase().indexOf("reis") > -1) {
                if(description){
                    if(description.toLowerCase().indexOf("asiat") > -1 || description.toLowerCase().indexOf("sushi") > -1)
                        var id = "1584269600464-37b1b58a9fe7";
                } else {
                    var id = "1536304993881-ff6e9eefa2a6";
                }
            } else if(category.toLowerCase().indexOf("risotto") > -1) {
                var id = "1581073746562-e7fd2422f0eb";
            } else if(category.toLowerCase().indexOf("tintenfisch") > -1) {
                var id = "1485827329522-c625acce0067";
            } else if(category.toLowerCase().indexOf("sushi") > -1) {
                var myArr = [
                    '1563612116828-a62f45c706e4', '1583623025817-d180a2221d0a', '1570780775848-bc1897788ce0', 
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("maki") > -1) {
                var id = "1593614201641-6f16f8e41a4e";
            }  else if(category.toLowerCase().indexOf("fisch") > -1) {
                var myArr = [
                    '1548179015-b23bec2b1a2a', '1499125562588-29fb8a56b5d5', '1580476262798-bddd9f4b7369', '1579887829114-282b4fa31072'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("garnelen") > -1) {
                var id = "1547004786-891dfa05c829";
            } else if(category.toLowerCase().indexOf("tortilla") > -1) {
                var id = "1552332386-f8dd00dc2f85";
            } else if(category.toLowerCase().indexOf("vorspeise") > -1) {
                if(description){
                    if(description.toLowerCase().indexOf("asiat") > -1){
                        var myArr = [
                            '1515022376298-7333f33e704b', '1548029960-695d127f4543'
                        ];
                        var id = myArr[Math.floor(Math.random() * myArr.length)];
                    } 
                } else {
                    var myArr = [
                        '1577969181928-69c4e557c99a', '1541529086526-db283c563270', '1558679582-4d81ce75993a'
                    ];
                    var id = myArr[Math.floor(Math.random() * myArr.length)];
                }
            } else if(category.toLowerCase().indexOf("döner") > -1) {
                var id = "1530469912745-a215c6b256ea";
            } else if(category.toLowerCase().indexOf("vegetarisch") > -1) {
                var id = "1593001872095-7d5b3868fb1d";
            } else if(category.toLowerCase().indexOf("hähnchen") > -1 || category.toLowerCase().indexOf("chicken") > -1) {
                var id = "1600555379765-f82335a7b1b0";
            } else if(category.toLowerCase().indexOf("rind") > -1) {
                var myArr = [
                    '1504973960431-1c467e159aa4','1556269923-e4ef51d69638'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("schwein") > -1) {
                var id = "1592501163544-adeb63c1855f";
            } else if(category.toLowerCase().indexOf("lamm") > -1) {
                var myArr = [
                    '1502038683567-85dc225345c1','1507150370052-1e798df49f29'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("grill") > -1) {
                var myArr = [
                    '1555939594-58d7cb561ad1','1560380185-9644d262854c'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("eintöpfe") > -1) {
                var id = "1544068192-4c4af19868c6";
            } else if(category.toLowerCase().indexOf("ribs & wings") > -1) {
                var id = "1544025162-d76694265947";
            } else if(category.toLowerCase().indexOf("eiscreme") > -1) {
                var id = "1561845730-208ad5910553";
            } else if(category.toLowerCase().indexOf("nachspeisen") > -1) {
                var id = "1587314168485-3236d6710814";
            } else if(category.toLowerCase().indexOf("schnitzel") > -1 || category.toLowerCase().indexOf("fleisch") > -1) {
                var id = "1599921841143-819065a55cc6";
            } else if(category.toLowerCase().indexOf("cordon bleu") > -1) {
                var id = "1585325701956-60dd9c8553bc";
            } else if(category.toLowerCase().indexOf("fladenbrot") > -1) {
                var id = "1574448857443-dc1d7e9c4dad";
            } else if(category.toLowerCase().indexOf("huhn") > -1) {
                var id = "1532550907401-a500c9a57435";
            } else if(category.toLowerCase().indexOf("hot dog") > -1 || category.toLowerCase().indexOf("bosner") > -1) {
                var myArr = [
                    '1580217593601-ab363b00687b', '1575496118038-3689d62e5235'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("würstel") > -1) {
                var myArr = [
                    '1547424450-75ec164925ad', '1540287082551-b6f969623813'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("lasagne") > -1) {
                var myArr = [
                    '1560750133-c5d4ef4de911', '1602541770236-d2b65f51202c', '1595666548990-788e19dc3885'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("wraps") > -1) {
                var id = "1562059390-a761a084768e";
            } else if(category.toLowerCase().indexOf("burritos") > -1) {
                var myArr = [
                    '1600315958029-b922dee42ac2', '1563282397-cdc218eccfda'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("tacos") > -1) {
                var myArr = [
                    '1552332386-f8dd00dc2f85', '1518830686998-b8847466b372'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("quesadillas") > -1) {
                var id = '1570461226513-e08b58a52c53';
            } else if(category.toLowerCase().indexOf("sashimi") > -1) {
                var id = "1579398937948-598009379315";
            } else if(category.toLowerCase().indexOf("milchshakes") > -1) {
                var myArr = [
                    '1528740096961-3798add19cb7', '1541658016709-82535e94bc69', '1583024012457-b6de05b7003a'
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            } else if(category.toLowerCase().indexOf("smoothies") > -1) {
                var myArr = [
                    '1514994960127-ed3ef9239d11',
                ];
                var id = myArr[Math.floor(Math.random() * myArr.length)];
            }   
            
            if(id){
                return <div style={{margin:"auto", maxWidth:"468px"}}><img src={"https://images.unsplash.com/photo-"+id+"?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=468&h=132&q=80"} /></div>
            }
        }
    }

    heading = (category, slug) => {
        if(this.getUrlParameter('act') === 'category'){
            var act = "sub_category";
            var heading = <a href={slug+"?category="+this.getUrlParameter('catType')+"&act="+act+"&catType="+btoa(category)}>{category}</a>
        } 
        else if(this.getUrlParameter('act') === 'sub_category'){
            var act = "sub_category2";
            var heading = <a href={slug+"?category="+this.getUrlParameter('category')+"&sub_category="+this.getUrlParameter('catType')+"&act="+act+"&catType="+btoa(category)}>{category}</a>
        }
        else if(this.getUrlParameter('act') === 'sub_category2'){
            var heading = category
        } 
        else {
            var act = "category";
            var heading = <a href={slug+"?act="+act+"&catType="+btoa(category)}>{category}</a>
        }

        return heading;
    }

    breadcrump = slug => {
        if(this.getUrlParameter('act') === 'category'){
            var breadcrump = <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:300, margin:"10px 15px"}}>
                <span><a href={slug}>Startseite</a></span>
                <span> » <a href={"?act=category&catType="+this.getUrlParameter('catType')}>{atob(this.getUrlParameter('catType'))}</a></span>
            </div>
        } 
        else if(this.getUrlParameter('act') === 'sub_category'){
            var breadcrump = <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:300, margin:"10px 15px"}}>
                <span><a href={slug}>Startseite</a></span>
                <span> » <a href={"?act=category&catType="+this.getUrlParameter('category')}>{atob(this.getUrlParameter('category'))}</a></span>
                <span> » {atob(this.getUrlParameter('catType'))}</span>
            </div>
        }
        else if(this.getUrlParameter('act') === 'sub_category2'){
            var breadcrump = <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:300, margin:"10px 15px"}}>
                <span><a href={slug}>Startseite</a></span>
                <span> » <a href={"?act=category&catType="+this.getUrlParameter('category')}>{atob(this.getUrlParameter('category'))}</a></span>
                <span> » <a href={"?category="+this.getUrlParameter('category')+"&sub_category="+this.getUrlParameter('sub_category')+"&act=sub_category&catType="+this.getUrlParameter('sub_category')}>{atob(this.getUrlParameter('sub_category'))}</a></span>
                <span> » {atob(this.getUrlParameter('catType'))}</span>
            </div>
        } 
        else {
            var breadcrump = "";
        }

        return breadcrump;
    }

    render() {
        const { data, addProduct, removeProduct, cartProducts, restaurant } = this.props;

        var itemCount = data.items || "";

        return (
            <React.Fragment>
                <div id="allItems" style={{background:"rgb(232, 233, 238)", overflow:"auto"}}>

                    {this.breadcrump(restaurant.slug)}
                    
                    {!data.items ? (
                        <h2 style={{padding:"20px 10px", textAlign:"center", fontFamily:"open sans", fontWeight:300}}>Laden...</h2>
                    ) : itemCount == '' && (
                        <h2 style={{textAlign:"center", fontFamily:"open sans", fontWeight:300}}>Dieser Anbieter hat noch keine Produkte hinzugefügt.</h2>
                    )}

                    {/* Restaurant, CBD Shop, Handy Shop, etc */}
                    {(data.items && restaurant.cat == "restaurant") ? (

                        Object.keys(data.items).map((category, index) => (
                            <div key={category} id={category}>
                                
                                {/* <Collapsible trigger={category} open={index === 0 ? true : false}> */}
                                <Collapsible trigger={category} open={true}>

                                    {this.catImg(category, restaurant.description)}
                                    
                                    {data.items[category].map(item => (
                                        <Slide bottom duration={250} key={item.id}>

                                            <span className="hidden">{(item.quantity = 1)}</span>

                                            <div className="category-list-item" style={{display:"flex", justifyContent: "space-between"}} >

                                                {item.image !== "" && (
                                                    <React.Fragment>
                                                        {cartProducts.find(cp => cp.id === item.id) !== undefined && (
                                                            <Fade duration={150}>
                                                                <div className="quantity-badge-list" style={{ backgroundColor: localStorage.getItem("storeColor"), zIndex:3 }}>
                                                                    <span>
                                                                        {(item.addon_categories && item.addon_categories.length) ? (
                                                                            <React.Fragment>
                                                                                <i className="si si-check" style={{ lineHeight: "1.3rem" }} ></i>
                                                                            </React.Fragment>
                                                                        ) : (
                                                                            <React.Fragment>
                                                                                {cartProducts.find(cp => cp.id === item.id).quantity}
                                                                            </React.Fragment>
                                                                        )}
                                                                    </span>
                                                                </div>
                                                            </Fade>
                                                        )}
                                                    </React.Fragment>
                                                )}

                                                <div style={{cursor:"pointer", padding: "10px 12px", paddingRight:0}} onClick={() => { this.addButtonClicked(item) }} className={item.image !== "" ? "flex-item-name" : "flex-item-name ml-0"}>

                                                    <Ink duration="300" />
                                                    
                                                    <div style={{width:"100%", float:"left"}}>

                                                        {/* name */}
                                                        <span className="item-name" style={{width:"calc(100% - 40px)", float:"left"}}>{item.name}</span> 

                                                        {/* add */}
                                                        <span style={{position:"absolute", right:0, top:"5px"}} title="In den Einkaufswagen">
                                                            <span onClick={() => {this.addButtonClicked(item)}} style={{borderLeft: "1px solid #ebebeb", borderBottom: "1px solid #ebebeb", color: localStorage.getItem("cartColorBg"), background:"#fff", padding: "5px 10px"}}><FontAwesomeIcon icon={faPlus} /></span>
                                                        </span>

                                                    </div>

                                                    {/* recommended, new */}
                                                    <ItemBadge item={item} />

                                                    <div style={{float:"left", width:(item.image !== null?"calc(100% - 80px)":"calc(100% - 40px)")}}>
                                                        {/* description */}
                                                        {item.description !== null ? (
                                                            <React.Fragment>
                                                                <div className="block-with-text" dangerouslySetInnerHTML={{
                                                                            __html: item.description.replace(/<(.|\n)*?>/g, '')
                                                                        }}
                                                                    ></div>
                                                            </React.Fragment>
                                                        ) : (
                                                            <br />
                                                        )}

                                                        {/* price */}
                                                        <span className="item-price" onClick={() => {this.addButtonClicked(item)}} style={{cursor:"pointer"}}>
                                                            {localStorage.getItem("hidePriceWhenZero") === "true" && item.price === "0.00"
                                                                ? null
                                                                : (
                                                                    restaurant.currencyFormat + " " + parseFloat(item.price).toFixed(2)
                                                                )}
                                                        </span>

                                                    </div>

                                                    {/* image */}
                                                    {item.image !== null && (
                                                        <span style={{float:"right", marginTop:"10px"}}><img src={"https://lieferservice123.com"+item.image} alt={item.name} className="flex-item-image" /></span>
                                                    )}

                                                </div>

                                                {/* Customization */}
                                                <div className="item-actions pull-right pb-0 mt-10">
                                                    <div className="btn-group btn-group-sm" role="group" aria-label="btnGroupIcons1">

                                                        <Modal show={this.state.customizationModal[item.id]} onHide={this.hideModalCustomization} size="sm">
                                                            <Modal.Header closeButton>
                                                            </Modal.Header>
                                                            <Modal.Body style={{padding:"32px"}}>
                                                                {(item.addon_categories && item.addon_categories.length) ? (
                                                                    <Customization
                                                                        product={item}
                                                                        forceUpdate={this.forceStateUpdate}
                                                                        hideModalCustomization={this.hideModalCustomization}
                                                                    />
                                                                ) : (null)}
                                                            </Modal.Body>
                                                        </Modal>
                                                    </div>
                                                </div>

                                            </div>

                                        </Slide>
                                    ))}
                                </Collapsible>
                                
                            </div>
                        ))
                        
                    ) : data.items && (

                        Object.keys(data.items).map((category, index) => (

                            <div id={category} key={index} style={{background:"#fff", marginBottom:"10px", width:"100%", float:"left"}}>

                                <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:400, margin:"10px 15px"}}>{this.heading(category, restaurant.slug)}</div>

                                <div className="mainPageScroll" style={{width:"100%", overflow:"scroll"}}>
                                    <div style={{width:this.getUrlParameter('act') === 'sub_category2'?"100%":"max-content"}}>
                                        
                                        {data.items[category].map((r) => (
                                            
                                            <li className={"suggestion-word-style mainPageItem"+((r.quantity >= 0) ? ' borderAdd' : '')} key={r.id}>

                                                {/* {cartProducts.find(cp => cp.id === r.id) !== undefined && ( */}
                                                    <div className="addToOrderQuickAdd" id={"addToOrderQuickAdd"+r.id} onMouseLeave={() => document.getElementById("addToOrderQuickAdd"+r.id).style.display = "none"} style={{display:"none", position:"absolute", height:"42px", background:"#fff", background:"#fff", zIndex:1, overflow:"auto", lineHeight:3.5, borderRadius:"4px", boxShadow:"rgba(0, 0, 0, 0.25) 0px 3px 10px"}}>

                                                        {/* minus icon */}
                                                        <span style={{height:"42px", float:"left", textAlign:"center", cursor:"pointer"}}><FontAwesomeIcon onClick={()=>{ r.quantity = 1; this.removeButtonClicked(r); }} icon={faMinus} style={{fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px"}} /></span>

                                                        {/* quantity */}
                                                        {cartProducts.find(cp => cp.id === r.id) !== undefined && ( 
                                                            <span className="quantityQuick" style={{height:"42px", float:"left", textAlign:"center", fontSize:"16px", lineHeight:2.6}}>
                                                                {cartProducts.find(cp => cp.id === r.id).quantity}
                                                            </span>
                                                        )}

                                                        {/* plus icon */}
                                                        <span style={{height:"42px", float:"left", textAlign:"center", cursor:"pointer"}}><FontAwesomeIcon onClick={()=> this.addButtonClicked(r)} icon={faPlus} style={{fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px"}} /></span>
                                                    </div>
                                                {/* )} */}

                                                {cartProducts.find(cp => cp.id === r.id) !== undefined ? (
                                                    <span onClick={() => document.getElementById("addToOrderQuickAdd"+r.id).style.display = "block"} icon={faPlus} style={{float:"right", fontSize:"12px", background:localStorage.getItem("cartColorBg"), color:"#fff", borderRadius: "20px", width: "24px", height: "24px", padding: "3px", position:"absolute", right:"10px", textAlign:"center"}}>
                                                        {(r.addon_categories && r.addon_categories.length) ? (
                                                            <React.Fragment>
                                                                <i className="si si-check" style={{ lineHeight: "1.3rem" }} ></i>
                                                            </React.Fragment>
                                                        ) : (
                                                            <React.Fragment>
                                                                {cartProducts.find(cp => cp.id === r.id).quantity}
                                                            </React.Fragment>
                                                        )}
                                                    </span>
                                                ) : (
                                                    <FontAwesomeIcon className="addProd" onClick={()=>{ this.addButtonClicked(r); setTimeout(() => { document.getElementById("addToOrderQuickAdd"+r.id).style.display = "block"; }, 100); }} icon={faPlus} style={{float:"right", fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px", position:"absolute", right:"10px", cursor:"pointer"}} />
                                                )}

                                                <img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{marginRight:"10px", marginBottom:"5px", cursor:"pointer"}} onClick={this.showModalZoom.bind(this, r.id)} /> 

                                                <div style={{width:"100%"}} onClick={this.showModalZoom.bind(this, r.id)}>

                                                <div style={{color:"#333", fontSize:"12px", cursor:"pointer"}}>{r.name}</div>

                                                <div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px", cursor:"pointer"}}>
                                                    {(r.price_old && r.price_old !== r.price) ? (
                                                        <span>
                                                            €{parseFloat(r.price).toFixed(2)} <del style={{color:"#999", fontWeight:300, fontFamily:"open sans", fontSize:"13px"}}>€{r.price_old.toFixed(2)}</del>
                                                        </span>
                                                    ) : (
                                                        <span>€{r.price.toFixed(2)}</span>
                                                    )}

                                                    <div style={{color:"#888", fontSize:"10px", fontWeight:"normal"}}>{r.grammageBadge}</div>
                                                </div>

                                                </div>

                                                {/* show product details */}
                                                <Modal show={this.state.ZoomModal[r.id]} onHide={this.hideModalZoom} size="lg">
                                                    <Modal.Header closeButton>
                                                    </Modal.Header>
                                                    <Modal.Body style={{padding:"0 25px 25px"}}>

                                                        <div className="productImage">
                                                            <div style={{position:"absolute"}}><ItemBadge item={r} /></div>
                                                            <img src={"https://lieferservice123.com"+r.image} alt="Bild wird geladen..." />
                                                        </div>

                                                        <div className="productDetails">

                                                            <h1>{r.name}</h1>
                                                            
                                                            <div style={{margin:"20px 0"}}>
                                                                <div className="price">
                                                                    {(r.price_old && r.price_old !== r.price) ? (
                                                                        <span>
                                                                            <del style={{color:"#999", fontSize:"14px", fontWeight:400, fontFamily:"open sans"}}>€{r.price_old.toFixed(2)}</del> €{parseFloat(r.price).toFixed(2)}
                                                                        </span>
                                                                    ) : (
                                                                        <span>€{r.price.toFixed(2)}</span>
                                                                    )}
                                                                </div>
                                                                {r.grammageBadge && <div style={{color:"rgb(142, 142, 142)", fontSize:"14px"}}>{r.grammageBadge}</div>}
                                                            </div>

                                                            {r.description && <div className="description" dangerouslySetInnerHTML={{ __html: r.description }}></div>}

                                                            <button onClick={()=> { this.addButtonClicked(r); this.hideModalZoom(); }} type="button" class="btn btn-lg" style={{background:"#60b246", color:"#fff"}}><FontAwesomeIcon icon={faPlus} style={{marginRight:"5px"}} /> In den Einkaufswagen</button>

                                                            {r.marke && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Marke:</b> <span style={{fontWeight:300}}>{r.marke}</span></div>}
                                                            {r.category && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Kategorie:</b> <span style={{fontWeight:300}}>{r.category}</span></div>}
                                                            {r.sub_category && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Unterkategorie:</b> <span style={{fontWeight:300}}>{r.sub_category}</span></div>}
                                                            {r.sub_category2 && <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}><b>Sub-Unterkategorie:</b> <span style={{fontWeight:300}}>{r.sub_category2}</span></div>}

                                                        </div>
                                                    </Modal.Body>
                                                </Modal>

                                                {/* Customization */}
                                                <Modal show={this.state.customizationModal[r.id]} onHide={this.hideModalCustomization} size="sm" style={{background:"rgba(51,51,51, 0.85)"}}>
                                                    <Modal.Header closeButton>
                                                    </Modal.Header>
                                                    <Modal.Body style={{padding:"32px"}}>
                                                        {(r.addon_categories && r.addon_categories.length) ? (
                                                            <Customization
                                                                product={r}
                                                                forceUpdate={this.forceStateUpdate}
                                                                hideModalCustomization={this.hideModalCustomization}
                                                            />
                                                        ) : (null)}
                                                    </Modal.Body>
                                                </Modal>

                                            </li>

                                        ))}

                                    </div>
                                </div>

                            </div>
                        ))

                    )}

                    <div  className="mb-20"></div>


                    {/* Lebensmittel, Drogerie, Tannkstelle, Baumärkte */}
                    {/* <div id="prod">
                    
                        {(restaurant.cat !== "restaurant" && !this.getUrlParameter('act')) ? <MainPageCategories 
                        modal_input_results={cartProducts} 
                        selectedShop={restaurant.name}
                        slug={restaurant.slug}

                        shouldUpdate={this.state.update}
                        update={this.forceStateUpdate}
                        addProduct={addProduct}
                        removeProduct={removeProduct}
                        ></MainPageCategories> : null}

                        {(restaurant.cat !== "restaurant" && this.getUrlParameter('act') === 'category') ? <MainPageSubCategories 
                        category={this.getUrlParameter('catType')} 
                        modal_input_results={cartProducts} 
                        selectedShop={restaurant.name} 
                        slug={restaurant.slug}

                        shouldUpdate={this.state.update}
                        update={this.forceStateUpdate}
                        addProduct={addProduct}
                        removeProduct={removeProduct}
                        ></MainPageSubCategories> : null}

                        {(restaurant.cat !== "restaurant" && this.getUrlParameter('act') === 'sub_category') ? <MainPageSub2Categories 
                        category={this.getUrlParameter('catType')} 
                        subCat={this.getUrlParameter('subCat')} 
                        modal_input_results={cartProducts} 
                        selectedShop={restaurant.name} 
                        slug={restaurant.slug}

                        shouldUpdate={this.state.update}
                        update={this.forceStateUpdate}
                        addProduct={addProduct}
                        removeProduct={removeProduct}
                        ></MainPageSub2Categories> : null}

                        {(restaurant.cat !== "restaurant" && this.getUrlParameter('act') === 'sub_category2') ? <MainPageSub2Items 
                        category={this.getUrlParameter('catType')} 
                        subCat={this.getUrlParameter('subCat')} 
                        subCat2={this.getUrlParameter('subCat2')} 
                        modal_input_results={cartProducts} 
                        selectedShop={restaurant.name}
                        slug={restaurant.slug}

                        shouldUpdate={this.state.update}
                        update={this.forceStateUpdate}
                        addProduct={addProduct}
                        removeProduct={removeProduct}
                        ></MainPageSub2Items> : null}

                    </div> */}

                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cartProducts: state.cart.products
});

export default connect(
    mapStateToProps,
    { addProduct, removeProduct }
)(ItemList);