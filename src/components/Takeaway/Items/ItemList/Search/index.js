import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";
import { connect } from "react-redux";

import Suggestions from '../Suggestions'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

import { addProduct, removeProduct } from "../../../../../services/cart/actions";
import { searchProduct, clearSearchProduct } from "../../../../../services/items/actions";

class Search extends Component {
    
    constructor() {
        
        super();
        
        this.state = { 
            searchQuery: '',
            serchResults: [],
            selectedCategory: '',
            subCategoryList: [],
        };

        this.searchAjax = React.createRef();

    }

    componentDidMount() {
        this.props.clearSearchProduct();

        setTimeout(() => {
            this.searchAjax.focus(); //sets focus to element
            this.getSearchInfoCat(localStorage.getItem("firstSelectInput") ? true : false)
        }, 100)
    }

    onSearchChange = () => {
        document.getElementById('suggestionsBox').style.display = "block";
        document.getElementById('closeSuggestionsIcon').style.display = "block";
       
        this.setState({
            searchQuery: this.searchAjax.value,
            // remove this to not reset the category on search product.
            selectedCategory: '',
        }, () => {
          if (this.state.searchQuery && this.state.searchQuery.length > 1) {
                // if (this.state.searchQuery.length % 2 === 0) {
                    console.log(this.state.currentSearchType);
                    const selectedCategory = localStorage.getItem("selectedCategory");
                    const sub_category = localStorage.getItem("sub_category");
                    const sub_category2 = localStorage.getItem("sub_category2");

                    if (sub_category2) {
                        this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(sub_category2) || ''}&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (sub_category) {
                        this.getSubCategoryInfo('sub_category2', `&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (selectedCategory) {
                        this.getSubCategoryInfo('subcat', '');
                    } else {
                        this.getSearchInfoCatRequestToServer(false);
                    }
                    this.getSearchInfo();
                // }
          } else if (!this.state.searchQuery) {
              console.log("resetSearchInputStates");
              this.setState({
                selectedCategory: '',
                searchResults: [],
                subCategoryList: [],
                searchQuery: '',
                currentSearchType: 'cat'
            });
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
          }
        })
    }

    getSearchInfo = () => {
        this.props.searchProduct(
            this.state.searchQuery,
            '', 
            localStorage.getItem("selectedCategory")?localStorage.getItem("selectedCategory"):"",
            localStorage.getItem("sub_category")?localStorage.getItem("sub_category"):"", 
            localStorage.getItem("sub_category2")?localStorage.getItem("sub_category2"):"", 
            this.props.restaurant.name,
            this.props.restaurant.id,
            localStorage.getItem("country"),
            this.props.restaurant.uploader,
        );

        // var url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?act=&q=${encodeURIComponent(this.state.searchQuery)}&country=${localStorage.getItem("country")}&shop=${this.props.restaurant.name}&restaurant_id=${this.props.restaurant.id}`;
        // const selectedCat = localStorage.getItem("selectedCategory");
        // if (selectedCat) {

        //     if (this.state.currentSearchType === 'cat') {
        //         // act=&cat=${encodeURIComponent(selectedCat)}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory)}&
        //         url = `${url}&cat=${encodeURIComponent(selectedCat)}`;
        //     } else {
        //         url = `${url}&cat=${encodeURIComponent(selectedCat)}&${this.state.currentSearchType}=${encodeURIComponent(this.state.selectedSubCategory)}`;
        //     }
        // }
        // axios.get(url)
        //   .then(( res ) => {
        //     this.setState({
        //         searchResults: res.data
        //     })
        //   })
    }

    getSearchInfoCatRequestToServer = (showProductsOnInit) => {
        //home cat
        // var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_einkaufSearch.php?q='+encodeURIComponent(this.state.searchQuery || "")+'&act=cat&country='+localStorage.getItem("country")+'&shop='+this.props.restaurant.name;
        // axios.get(url)
        // .then(( res ) => {
        //     this.setState({
        //         catSearchResults: res.data,
        //         no_sub_cat_results: false
        //     });
        //     if (showProductsOnInit) {
        //         this.getSearchInfo();
        //     }

        //     if (!res.data.length) {
        //         this.setState({
        //             no_sub_cat_results: true
        //         });
        //     }
        // })
          
        this.props.searchProduct(
            this.state.searchQuery,
            'cat', 
            localStorage.getItem("selectedCategory")?localStorage.getItem("selectedCategory"):"",
            localStorage.getItem("sub_category")?localStorage.getItem("sub_category"):"", 
            localStorage.getItem("sub_category2")?localStorage.getItem("sub_category2"):"", 
            this.props.restaurant.name,
            this.props.restaurant.id,
            localStorage.getItem("country"),
            this.props.restaurant.uploader,
            ( res ) => {

                this.setState({
                    catSearchResults: res.data,
                    no_sub_cat_results: false
                });
                if (showProductsOnInit) {
                    this.getSearchInfo();
                }

                if (!res.data.length) {
                    this.setState({
                        no_sub_cat_results: true
                    });
                }

            }
        )

    }

    getSubCategoryInfo = (actType, extraParams = '') => {
        // get sub scteogry..
        // const selectedCategory = localStorage.getItem("selectedCategory");
        // var url = url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?q=${encodeURIComponent(this.state.searchQuery)}&act=${actType}${extraParams}&cat=${encodeURIComponent(selectedCategory)}&country=${localStorage.getItem("country")}&shop=${this.props.restaurant.name}`;;
        
        // axios.get(url)
        //   .then(( res ) => {
        //     if (res.data.length) {
        //         this.setState({
        //             subCategoryList: res.data,
        //             no_sub_cat_results: false
        //         });
        //     } else {
        //         this.setState({
        //             no_sub_cat_results: true,
        //             subCategoryList: []
        //         });
        //     }
        //   }).catch(err => {
        //       console.error(err);
        //   });

        this.props.searchProduct(
            this.state.searchQuery,
            `${actType}`,
            localStorage.getItem("selectedCategory")?localStorage.getItem("selectedCategory"):"",
            localStorage.getItem("sub_category")?localStorage.getItem("sub_category"):"", 
            localStorage.getItem("sub_category2")?localStorage.getItem("sub_category2"):"", 
            this.props.restaurant.name,
            this.props.restaurant.id,
            localStorage.getItem("country"),
            this.props.restaurant.uploader,
            ( res ) => {
                
                if (res.data.length) {
                    this.setState({
                        subCategoryList: res.data,
                        no_sub_cat_results: false
                    });
                } else {
                    this.setState({
                        no_sub_cat_results: true,
                        subCategoryList: []
                    });
                }
            }
        )

    }

    getSearchInfoCat = (firstClicked, showProductsOnInit) => {
        console.log("firstSelectInput", firstClicked);

        if(firstClicked === false){

            if (document.getElementById('suggestionsBox')) {
                document.getElementById('suggestionsBox').style.display = "block";
                document.getElementById('closeSuggestionsIcon').style.display = "block";
            }
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
            localStorage.setItem("firstSelectInput", true);
    
            this.setState({
                subCategoryList: [],
                selectedCategory: '',
                selectedSubCategory: '',
                currentSearchType: 'cat'
            });
            this.getSearchInfoCatRequestToServer(showProductsOnInit);
        } else {
            //nothing
            console.log("dont hide suggestions box");
        }
    }

    suggestion2OptionClicked = (item, type, keyType) => {
        // console.log(type);
        if (type === 'category') {// when category item is clicked
            localStorage.removeItem('sub_category');
            this.setState({
                currentSearchType: 'cat',
                selectedSubCategory: '',
                selectedCategory: item.category
            });
            setTimeout(() => {
                this.getSearchInfo();
                this.getSubCategoryInfo('subcat');
            }, 200);
            localStorage.setItem("selectedCategory", item.category);
            localStorage.setItem("firstSelectInput", true);
            
        } else if (type === 'subCategory') {
            
            localStorage.setItem(keyType, item[keyType]);

            // subCategoryList
            this.setState({
                selectedSubCategory: item[keyType],
                currentSearchType: keyType
            });
            setTimeout(() => {
                this.getSearchInfo();
                if (keyType === 'sub_category') {
                    this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(item.sub_category) || ''}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory) || ''}`);
                }
            }, 200)
        } else {// when product item is clicked
            this.props.addProduct(item);
            this.forceStateUpdate();
        }
    };

    resetSearchInputStates = () => {
        this.setState({
            selectedCategory: '',
            catSearchResults: [],
            searchResults: [],
            subCategoryList: [],
            searchQuery: '',
            currentSearchType: 'cat',
            no_sub_cat_results: false

        });

        localStorage.removeItem("selectedCategory");
        localStorage.removeItem("sub_category");
        localStorage.removeItem("sub_category2");
    };

    closeSuggestionsBox = () =>{
        console.log("we resettning closeSuggestionsBox");

        document.getElementById('suggestionsBox').style.display = "none";
        document.getElementById('closeSuggestionsIcon').style.display = "none";
        this.searchAjax.value = ''; //clear the value of the element
        this.searchAjax.focus(); //sets focus to element

        this.resetSearchInputStates();

        localStorage.removeItem("firstSelectInput");
    }

    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            if (this.state.update) {
                this.setState({ update: false });
            } else {
                this.setState({ update: true });
            }
        }, 100);
    };

    render() {

        const { catList } = this.state;
        const { addProduct, removeProduct } = this.props;


        return (

            <React.Fragment>

                {/* searchbox */}
                <div className="searchBox2" id="searchBox2">

                <div className="input-location-icon-field" style={{ position: "relative" }}>
                    <label style={{display:"inherit"}}>
                        <input
                            onChange={this.onSearchChange}
                            onClick={() => this.getSearchInfoCat(localStorage.getItem("firstSelectInput") ? true : false)}
                            ref={input => this.searchAjax = input}
                            className="form-control search-input search-box"
                            placeholder={this.props.restaurant.name+' Suche...'}
                            style={{height:"40px", background:"#EFEFEF", border:"1px solid rgba(34, 34, 34, 0.15)", borderRadius:"5px"}}
                        />
                    </label>

                    <span id="closeSuggestionsIcon" title={localStorage.getItem("firstScreenLocate")} className="closeMe" style={{display:"none"}} onClick={() => this.closeSuggestionsBox()}><FontAwesomeIcon style={{marginRight:"5px", color:"#adb5bd"}} icon={faTimesCircle} /><Ink duration="500" /></span>

                </div>

                <Suggestions 
                    getSearchInfoCat={this.getSearchInfoCat} 
                    currentSearchType={this.state.currentSearchType} 
                    // resetSearchInputStates={this.resetSearchInputStates} 
                    subCategoryList={this.state.subCategoryList} 
                    selectedCategory={this.state.selectedCategory} 
                    suggestion2OptionClicked={this.suggestion2OptionClicked} 
                    selectedSubCategory={this.state.selectedSubCategory} 
                    modal_input_results={this.props.cartProducts} 
                    catSearchResults={this.state.catSearchResults} 
                    results={this.props.searchResults.searchResults} 
                    no_sub_cat_results={this.state.no_sub_cat_results}
                    forceUpdate={this.forceStateUpdate}
                />

                </div>

            </React.Fragment>

        )
    }
}


const mapStateToProps = state => ({
    user: state.user.user,
    searchResults: state.items
});

export default connect(
    mapStateToProps,
    { addProduct, removeProduct, searchProduct, clearSearchProduct}
)(Search);