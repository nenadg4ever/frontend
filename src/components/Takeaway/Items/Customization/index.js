import React, { Component } from "react";

import Ink from "react-ink";
import Popup from "reactjs-popup";
import { connect } from "react-redux";

import { addProduct, removeProduct } from "../../../../services/cart/actions";


class Customization extends Component {
    _processAddons = product => {
        let addons = [];
        addons["selectedaddons"] = [];

        let radio = document.querySelectorAll("input[type=radio]:checked");
        for (let i = 0; i < radio.length; i++) {
            addons["selectedaddons"].push({
                addon_category_name: radio[i].name,
                addon_id: radio[i].getAttribute("data-addon-id"),
                addon_name: radio[i].getAttribute("data-addon-name"),
                price: radio[i].value
            });
        }

        let checkboxes = document.querySelectorAll("input[type=checkbox]:checked");

        for (let i = 0; i < checkboxes.length; i++) {
            addons["selectedaddons"].push({
                addon_category_name: checkboxes[i].name,
                addon_id: checkboxes[i].getAttribute("data-addon-id"),
                addon_name: checkboxes[i].getAttribute("data-addon-name"),
                price: checkboxes[i].value
            });
        }

        this.props.addProduct(Object.assign(addons, product));

        console.log("logthis", Object.assign(addons, product));
    };
    render() {
        const { product, forceUpdate, addProduct, removeProduct } = this.props;


        return (
            <React.Fragment>

                {/* <h3>{localStorage.getItem("customizationHeading")}</h3> */}

                {product.addon_categories.map(addon_category => (

                    // <div key={addon_category.id} className="addon-category-block">
                    <div key={addon_category.id}>

                        <React.Fragment>

                            <p className="addon-category-name">{addon_category.name}</p>

                            {addon_category.addons.length && (

                                <React.Fragment>

                                    {addon_category.addons.map((addon, index) => (

                                        <React.Fragment key={addon.id}>

                                            <div className="form-group addon-list">
                                                <input
                                                    type={addon_category.type === "SINGLE" ? "radio" : "checkbox"}
                                                    className={addon_category.type === "SINGLE" ? "magic-radio" : "magic-checkbox"}
                                                    name={addon_category.name}
                                                    data-addon-id={addon.id}
                                                    data-addon-name={addon.name}
                                                    value={addon.price}
                                                    defaultChecked={addon_category.type === "SINGLE" && index === 0 && true}
                                                />
                                                {addon_category.type === "SINGLE" && <label htmlFor={addon.name}></label>}

                                                <label className="text addon-label" htmlFor={addon.name}>
                                                    {addon.name} {localStorage.getItem("currencyFormat")}
                                                    {addon.price}{" "}
                                                </label>
                                            </div>

                                        </React.Fragment>
                                    ))}

                                </React.Fragment>
                            )}

                            <hr />

                        </React.Fragment>
                    </div>
                ))}

                <button 
                    className="btn btn-lg" 
                    onClick={() => { 
                        this._processAddons(product); 
                        forceUpdate(); 
                        this.props.hideModalCustomization();
                    }} 
                    style={{ 
                        backgroundColor:localStorage.getItem("cartColorBg"), 
                        color:localStorage.getItem("cartColorText") 
                    }}
                >
                    {localStorage.getItem("customizationDoneBtnText")}
                </button>
                
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    { addProduct, removeProduct }
)(Customization);