import React, { Component } from "react";
import BackWithSearch from "../../Elements/BackWithSearch";
import ContentLoader from "react-content-loader";
import ProgressiveImage from "react-progressive-image";
import { Link } from "react-router-dom";
import Ink from "react-ink";
import axios from "axios";
import { connect } from "react-redux";
import Nav from "../../Nav";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';

import { getSettings } from "../../../../services/settings/actions";
import { getSingleLanguageData } from "../../../../services/translations/actions";

class RestaurantInfo extends Component {

    constructor() {
        super();
        this.state = { 
            textIdx: 0,
            bgWidth: window.innerWidth,
            bgHeight: (window.innerHeight / 4.2),
            withLinkToRestaurant: false,
         };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {

        //if one config is missing then call the api to fetch settings
        if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        }

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);


        this.setState({ withLinkToRestaurant: this.props.withLinkToRestaurant });

        if (this.props.history.location.state && this.props.history.location.state.fromExplorePage) {
            this.setState({ withLinkToRestaurant: this.props.history.location.state.fromExplorePage });
        }

        this.registerScrollEvent();

        this.timeout = setInterval(() => {
            this.setState({ 
                bgWidth: window.innerWidth,
                bgHeight: (window.innerHeight / 4.2)
            });
            
        }, 1000);

        // this.setCurrencyFormat();
    }

    setCurrencyFormat = ()=>{
        const { restaurant } = this.props;
        localStorage.setItem("currencyFormat2", restaurant.currencyFormat);
    }

    componentWillReceiveProps=(props)=>{
        this.setCurrencyFormat();
    }

    titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        // Directly return the joined string
        return splitStr.join(' '); 
    }

    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }

    openingHours = (openFrom="14:00", openTo="03:00") => {
        //now date
        var now = new Date(); 

        //given date
        var h = now.getHours();
        var day = now.getDate();
        var month = ('0' + (now.getMonth() + 1)).slice(-2)
        var year = now.getUTCFullYear();

        //function slice() removes last 3 char from openTo to only get h without min
        //check if after midnight because date comparison should not be like 2020/01/09 14:00 - 2020/01/09 03:00
        
        if (h < 8 && openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is after midgnight
            var from = new Date(year + "/" + month + "/" + (day - 1) + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
        }else if (openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is NOT after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + (day + 1) + " " + openTo); 
        }else{
            var from = new Date(year+"/"+month+"/"+day+" "+openFrom); //set current date and time from table of restaurant 
            var to   = new Date(year+"/"+month+"/"+day+" "+openTo); //set current date and time from table of restaurant
        }

        var today = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+' '+now.getHours()+":"+now.getMinutes(); 
        var check = new Date(today); //this must always be current date, never change

        //return from+" - "+to;
        if (check > from && check < to)
            return ""; 
        else 
            return "geschlossen"; 
            
    }

    getDynamicPic = (description) => {
        if (description.indexOf("sterreich") > 0)
            return "schnitzel";
        else if (description.indexOf("talienisch") > 0)
            return "pizza";
        else if (description.indexOf("exikanisch") > 0)
            return "burrito";
        else if (description.indexOf("siatisch") > 0 || description.indexOf("hinesisch") > 0 || description.indexOf("ushi") > 0)
            return "sushi";
        else if (description.indexOf("urger") > 0)
            return "burgrestaurant.imager";
        else
            return "pizza";
    }

    getAddress = (address, country=false) => {
        if(country === false){
            var res = address;
            var split = res.split(",");
            var res = split.slice(0, split.length - 1).join(",");
            return res;
        }else{
            var res = address.split(", ");
            return res[0];
        }
    }
    
    componentWillUnmount() {
        this.removeScrollEvent();
    }

    fixedRestaurantInfo = hidden => {
        if (this.child) {
            if (hidden) {
                this.child.heading.classList.add("hidden");
            } else {
                this.child.heading.classList.remove("hidden");
            }
        }
    };

    registerScrollEvent() {
        window.addEventListener("scroll", this.scrollFunc);
    }
    removeScrollEvent() {
        window.removeEventListener("scroll", this.scrollFunc);
    }
    scrollFunc = () => {
        if (document.documentElement.scrollTop > 55) {
            let hidden = false;
            this.fixedRestaurantInfo(hidden);
        }
        if (document.documentElement.scrollTop < 55) {
            let hidden = true;
            this.fixedRestaurantInfo(hidden);
        }
    };

    openChrome = () =>{
        if(document.URL.indexOf("//app.") >= 0){
            var url = "https://"+window.location.host+window.location.pathname+"?install=1";
        } else {
            var url = "https://app."+window.location.host+window.location.pathname+"?install=1";
        }

        window.open(url);
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    uppercase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    host(){
        // var d= document.domain.split('.');
        // return d[1] || d[0];
        var h = window.location.host.replace("."+this.domain(), "");
        var h = this.uppercase(h);
        return h;
    }
    domain(){
        var d = window.location.host;
        var d = d.split(".").pop();
        return d;
    }

    render() {
        const { history, restaurant } = this.props;
        const {bgWidth, bgHeight} = this.state;

        return (
            <React.Fragment>
                <div className="bg-white">

                    {/* <BackWithSearch
                        ref={node => {
                            this.child = node;
                        }}
                        history={history}
                        boxshadow={false}
                        has_restaurant_info={true}
                        restaurant={restaurant}
                    /> */}

                    {restaurant.length === 0 ? (null
                        // <ContentLoader height={170} width={200} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                        //     <rect x="20" y="70" rx="4" ry="4" width="80" height="78" />
                        //     <rect x="144" y="85" rx="0" ry="0" width="115" height="18" />
                        //     <rect x="144" y="115" rx="0" ry="0" width="165" height="16" />
                        // </ContentLoader>
                    ) : (
                            <React.Fragment>

                                <Nav shop={true} restaurant={restaurant} bgWidth={bgWidth} bgHeight={bgHeight} page={'RestaurantInfo'} />

                                <div style={{background:"rgb(232, 233, 238)", overflow:"auto", padding:"5px 0"}}></div>

                            </React.Fragment>
                        )}
                </div>

            </React.Fragment>
        );
    }
}

//export default RestaurantInfo;


const mapStateToProps = state => ({
    settings: state.settings.settings,
    language: state.languages.language,
});


export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData }
)(RestaurantInfo);