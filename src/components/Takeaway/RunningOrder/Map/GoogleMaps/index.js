import { GoogleApiWrapper, Map, Marker, InfoWindow } from "google-maps-react";
import mapStyle from "../../../GeoLocationPage/mapStyle.json";
import ContentLoader from "react-content-loader";
import React, { Component } from "react";

class GoogleMaps extends Component {
    state = {
        zoom: 12,
        // isOpenCustomer: false,
        // isOpenMe: false,
        // isOpenShop: false,
    };

    componentDidMount() {
        const deliveryMarker = this.refs.deliveryRefIcon;

        if (deliveryMarker) {
            deliveryMarker.style.transform = "rotate(" + 50 + "deg)";
            deliveryMarker.style.transition = "transform 1s linear";
        }
        setTimeout(() => {
            this.setState({ zoom: 15 });
        }, 1 * 1000);
    }

    componentWillReceiveProps(nextProps) {
        setTimeout(() => {
            if (document.querySelector('[src*="/assets/img/various/marker-delivery.png"]') && nextProps.delivery_gps_location.heading) {
                console.log("Inside update delivery icon");
                document.querySelector('[src*="/assets/img/various/marker-delivery.png"]').style.transform =
                    "rotate(" + nextProps.delivery_gps_location.heading + "deg)";
                document.querySelector('[src*="/assets/img/various/marker-delivery.png"]').style.transition = "transform 0.25s linear";
            }
        }, 500);
    }


    handleToggleCustomer = () => {
        this.setState({
            isOpenCustomer: true,
            isOpenMe: false,
            isOpenShop: false
        });
    }

    handleToggleMe = () => {
        this.setState({
            isOpenCustomer: false,
            isOpenMe: true,
            isOpenShop: false
        });
    }

    handleToggleShop = () => {
        this.setState({
            isOpenCustomer: false,
            isOpenMe: false,
            isOpenShop: true
        });
    }

    render() {
        // console.log("this.props", this.props);
        
        return (
            <Map
                ref={ref => {
                    this.map = ref;
                }}
                google={this.props.google}
                style={{
                    width: "100%",
                    // height: "55vh"
                    height: "320px"
                }}
                initialCenter={{
                    lat: JSON.parse(this.props.deliveryLocation).lat,
                    lng: JSON.parse(this.props.deliveryLocation).lng
                }}
                zoom={this.state.zoom}
                styles={mapStyle}
                zoomControl={false}
                mapTypeControl={false}
                scaleControl={true}
                streetViewControl={false}
                fullscreenControl={false}
            >

                {this.state.isOpenCustomer && (
                    <InfoWindow
                        // onCloseClick={() => {
                        //     setSelectedCenter(null);
                        // }}
                        position={{
                            lat: JSON.parse(this.props.deliveryLocation).lat,
                            lng: JSON.parse(this.props.deliveryLocation).lng
                        }}
                        visible={true}
                    >
                        <div style={{fontWeight:"bold"}}>Kunde</div>{JSON.parse(this.props.deliveryLocation).address}
                    </InfoWindow>
                )}

                {this.state.isOpenMe && (
                    <InfoWindow
                        // onCloseClick={() => {
                        //     setSelectedCenter(null);
                        // }}
                        position={{
                            lat: JSON.parse(this.props.delivery_gps_location).lat,
                            lng: JSON.parse(this.props.delivery_gps_location).lng
                        }}
                        visible={true}
                    >
                        <div style={{fontWeight:"bold"}}>Ich</div>{JSON.parse(this.props.delivery_gps_location).address}
                    </InfoWindow>
                )}

                {this.state.isOpenShop && (
                    <InfoWindow
                        // onCloseClick={() => {
                        //     setSelectedCenter(null);
                        // }}
                        position={{
                            lat: parseFloat(this.props.restaurant_latitude),
                            lng: parseFloat(this.props.restaurant_longitude)
                        }}
                        visible={true}
                    >
                        <div style={{fontWeight:"bold"}}>Shop</div>
                    </InfoWindow>
                )}




                <Marker
                    position={{
                        lat: JSON.parse(this.props.deliveryLocation).lat,
                        lng: JSON.parse(this.props.deliveryLocation).lng
                    }}
                    icon={{
                        url: "https://lieferservice123.com/assets/img/various/marker-home.png",
                        // anchor: new this.props.google.maps.Point(32, 32),
                        scaledSize: new this.props.google.maps.Size(34, 54)
                    }}
                    onClick={this.handleToggleCustomer}
                    
                >
                </Marker>

                <Marker
                    position={{
                        lat: parseFloat(this.props.restaurant_latitude),
                        lng: parseFloat(this.props.restaurant_longitude)
                    }}
                    icon={{
                        url: "https://lieferservice123.com/assets/img/various/marker-restaurant.png",
                        // anchor: new this.props.google.maps.Point(32, 32),
                        scaledSize: new this.props.google.maps.Size(34, 54)
                    }}
                    onClick={this.handleToggleShop}
                ></Marker>

                {this.props.show_delivery_gps && (
                    <Marker
                        position={{
                            lat: JSON.parse(this.props.delivery_gps_location).lat,
                            lng: JSON.parse(this.props.delivery_gps_location).lng
                        }}
                        icon={{
                            url: "https://lieferservice123.com/assets/img/various/marker-delivery.png",
                            anchor: new this.props.google.maps.Point(32, 32),
                            scaledSize: new this.props.google.maps.Size(54, 54)
                        }}
                        onClick={this.handleToggleMe}
                    ></Marker>
                )} 
            </Map>
        );
    }
}

const MapLoadingContainer = () => (
    <ContentLoader height={320} width={window.innerWidth} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
        <rect x="0" y="0" rx="0" ry="0" width={window.innerWidth} height="320" />
    </ContentLoader>
);

export default GoogleApiWrapper({
    apiKey: localStorage.getItem("googleApiKey"),
    LoadingContainer: MapLoadingContainer
})(GoogleMaps);
