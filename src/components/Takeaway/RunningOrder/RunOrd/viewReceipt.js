import { SUPERMARKET_ITEMS_URL } from "../../../../configs/index";

import React, { Component } from "react";
import { connect } from "react-redux";
import Ink from "react-ink";
import axios from "axios";

import { Modal, Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faBarcode } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faNotEqual } from '@fortawesome/free-solid-svg-icons';

import ReactTooltip from 'react-tooltip'
import ReactDOM from 'react-dom';

// import FoundItem from './foundItem';
// import FoundReplacement from './foundReplacement';

// import BarcodeReader from 'react-barcode-reader'
import BarcodeScannerComponent from "react-webcam-barcode-scanner";


// search shop
import Suggestions from '../../Items/ItemList/Suggestions'
import Items from './items.js';


class ViewReceipt extends Component {

    constructor(props){
        super(props);

        this.state = {
            test: [],
            ZoomModal: false,
            FoundItemModal: false,
            AnotherOptionsModal: false,
            RefundItemModal: false,
            ScanItemModal: false,
            ScanReplacementModal: false,
            SearchReplacementModal: false,
            FoundReplacementModal: false,
            barcode: 'No result',

            
            // search shop
            searchQuery: '',
            serchResults: [],
            selectedCategory: '',
            subCategoryList: [],
            // shopName: 'Billa',
        }
    }

    showModalZoom (id) {
        this.setState({
          ZoomModal: {
            [id]: true
          }
        });
    }
    hideModalZoom = () => {
        this.setState({
            ZoomModal: false
        });
    }


    showModalFoundItem (id) {
        this.setState({
            FoundItemModal: {
            [id]: true
          }
        });
    }
    hideModalFoundItem = () => {
        this.setState({
            FoundItemModal: false
        });
    }

    showModalAnotherOptions (id) {
        this.setState({
            AnotherOptionsModal: {
            [id]: true
          }
        });
    }
    hideModalAnotherOptions = () => {
        this.setState({
            AnotherOptionsModal: false
        });
    }

    showModalRefundItem (id) {
        this.setState({
            RefundItemModal: {
            [id]: true
          }
        });
    }
    hideModalRefundItem = () => {
        this.setState({
            RefundItemModal: false
        });
    }

    showModalScanItem (id) {
        this.setState({
            ScanItemModal: {
            [id]: true
          }
        });
    }
    hideModalScanItem = () => {
        this.setState({
            ScanItemModal: false
        });
    }

    showModalScanReplacement (id) {
        this.setState({
            ScanReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalScanReplacement = () => {
        this.setState({
            ScanReplacementModal: false
        });
    }

    showModalSearchReplacement (id) {
        this.setState({
            SearchReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalSearchReplacement = () => {
        this.setState({
            SearchReplacementModal: false
        });
    }

    showModalFoundReplacement (id, replaceWith) {
        this.setState({
            FoundReplacementModal: {
            [id]: true
          }
        });

        this.setState({
          replaceWith: replaceWith
        });
    }
    hideModalFoundReplacement = () => {
        this.setState({
            FoundReplacementModal: false
        });
    }


    async getName(id){
        const name = await axios
            .post(SUPERMARKET_ITEMS_URL, {
                id: id
            })
            .then(response => {
                return response.data.name;
            })
            .catch(function(error) {
                console.log(error);
                return "";
            });
        console.log('name', name);
        return name;
    }

    refundItem = (supermarket_orders_id, id, quantity) => {

        var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found=0';
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    
    }
  
    getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }


    makeTotalQuantity = (data = [], tab) => {
        let sum = 0;
        data.forEach((row) => {

            if(tab == "todo" && row.found == null){
                sum = sum + 1
            } 
            else if(tab == "replacements" && row.found == 0 && row.replacement){
                sum = sum + 1
            } 
            else if(tab == "refunds" && row.found == 0 && row.replacement == null){
                sum = sum + 1
            } 
            else if(tab == 'adjustments' && row.found > 0 && row.found !== row.quantity){
                sum = sum + 1
            }
            else if(tab == "done" && row.found > 0 && row.quantity == row.found){
                sum = sum + 1
            }
        });
        return sum;
    }

    // scan product
    handleScan(data){
        this.setState({
            barcode: data,
        })
    }
    handleError(err){
        console.error(err)
    }

    country_formatted_tel = (fromTel) => {
        var fromTel = fromTel.trim();
        var first2 = fromTel.substr(0, 2);
        if(first2 == localStorage.getItem('prefix')) {
            var fromTel = "+".fromTel;
            return fromTel;
        }
    
        var firstLetter = fromTel.substr(0, 1);
        if(firstLetter == 0) {
            var fromTel = "+"+localStorage.getItem('prefix')+fromTel.substr(1);
            return fromTel;
        }
    
        return fromTel;
    }
      
    addBarcode = (id, barcode, shop) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_addBarcode.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode+'&shop='+shop;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }
      
    scanReplacement = (id, barcode) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_scanReplacement.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {   
            // console.log("id", id);
            // console.log("response.data", response.data);
            this.showModalFoundReplacement(id, response.data);
            
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }

    foundItem = (supermarket_orders_id, id) => {

        var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity=1&found=1';
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    
    }

    showReplacementOptions = () =>{
        document.getElementById('replacement_options').style.display = "block";
    }

    //search shop
    onSearchChange = () => {
        document.getElementById('suggestionsBox').style.display = "block";
        document.getElementById('closeSuggestionsIcon').style.display = "block";
       
        this.setState({
            searchQuery: this.searchAjax.value,
            // remove this to not reset the category on search product.
            selectedCategory: '',
        }, () => {
          if (this.state.searchQuery && this.state.searchQuery.length > 1) {
                // if (this.state.searchQuery.length % 2 === 0) {
                    console.log(this.state.currentSearchType);
                    const selectedCategory = localStorage.getItem("selectedCategory");
                    const sub_category = localStorage.getItem("sub_category");
                    const sub_category2 = localStorage.getItem("sub_category2");

                    if (sub_category2) {
                        this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(sub_category2) || ''}&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (sub_category) {
                        this.getSubCategoryInfo('sub_category2', `&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (selectedCategory) {
                        this.getSubCategoryInfo('subcat', '');
                    } else {
                        this.getSearchInfoCatRequestToServer(false);
                    }
                    this.getSearchInfo();
                // }
          } else if (!this.state.searchQuery) {
              console.log("resetSearchInputStates");
              this.setState({
                selectedCategory: '',
                searchResults: [],
                subCategoryList: [],
                searchQuery: '',
                currentSearchType: 'cat'
            });
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
          }
        })
    }
    getSearchInfo = () => {
        var url = `//${window.location.hostname}/php/twilio/${this.state.firstScreen_einkaufSearch}.php?act=&q=${encodeURIComponent(this.state.searchQuery)}&country=${localStorage.getItem("country")}&shop=${this.state.shopName}&restaurant_id=${this.state.restaurant_id}`;
        const selectedCat = localStorage.getItem("selectedCategory");
        if (selectedCat) {

            if (this.state.currentSearchType === 'cat') {

                // act=&cat=${encodeURIComponent(selectedCat)}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory)}&
                url = `${url}&cat=${encodeURIComponent(selectedCat)}`;
            } else {

                url = `${url}&cat=${encodeURIComponent(selectedCat)}&${this.state.currentSearchType}=${encodeURIComponent(this.state.selectedSubCategory)}`;
            }
        }
        axios.get(url)
          .then(( res ) => {
            this.setState({
                searchResults: res.data
            })
          })
    }
    getSearchInfoCatRequestToServer = (showProductsOnInit) => {
        //home cat
        var url = '//'+window.location.hostname+'/php/twilio/'+this.state.firstScreen_einkaufSearch+'.php?q='+encodeURIComponent(this.state.searchQuery || "")+'&act=cat&country='+localStorage.getItem("country")+'&shop='+this.state.shopName+'&restaurant_id='+this.state.restaurant_id;
        axios.get(url)
          .then(( res ) => {
            this.setState({
                catSearchResults: res.data,
                no_sub_cat_results: false
            });
            if (showProductsOnInit) {
                this.getSearchInfo();
            }

            if (!res.data.length) {
                this.setState({
                    no_sub_cat_results: true
                });
            }
          })
    }
    getSearchInfoCat = (firstClicked, showProductsOnInit) => {
        console.log("firstSelectInput", firstClicked);

        if(firstClicked === false){

            if (document.getElementById('suggestionsBox')) {
                document.getElementById('suggestionsBox').style.display = "block";
                document.getElementById('closeSuggestionsIcon').style.display = "block";
            }
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
            localStorage.setItem("firstSelectInput", true);
    
            this.setState({
                subCategoryList: [],
                selectedCategory: '',
                selectedSubCategory: '',
                currentSearchType: 'cat'
            });
            this.getSearchInfoCatRequestToServer(showProductsOnInit);
        } else {
            //nothing
            console.log("dont hide suggestions box");
        }
    }
    getSubCategoryInfo = (actType, extraParams = '') => {
        // get sub scteogry..
        const selectedCategory = localStorage.getItem("selectedCategory");
        var url = url = `//${window.location.hostname}/php/twilio/${this.state.firstScreen_einkaufSearch}.php?q=${encodeURIComponent(this.state.searchQuery)}&act=${actType}${extraParams}&cat=${encodeURIComponent(selectedCategory)}&country=${localStorage.getItem("country")}&shop=${this.state.shopName}&restaurant_id=${this.state.restaurant_id}`;
        
        axios.get(url)
          .then(( res ) => {
            if (res.data.length) {
                this.setState({
                    subCategoryList: res.data,
                    no_sub_cat_results: false
                });
            } else {
                this.setState({
                    no_sub_cat_results: true,
                    subCategoryList: []
                });
            }
          }).catch(err => {
              console.error(err);
          });
    }
    suggestion2OptionClicked = (item, type, keyType) => {
        // console.log(type);
        if (type === 'category') {// when category item is clicked
            localStorage.removeItem('sub_category');
            this.setState({
                currentSearchType: 'cat',
                selectedSubCategory: '',
                selectedCategory: item.category
            });
            setTimeout(() => {
                this.getSearchInfo();
                this.getSubCategoryInfo('subcat');
            }, 200);
            localStorage.setItem("selectedCategory", item.category);
            localStorage.setItem("firstSelectInput", true);
            
        } else if (type === 'subCategory') {
            
            localStorage.setItem(keyType, item[keyType]);

            // subCategoryList
            this.setState({
                selectedSubCategory: item[keyType],
                currentSearchType: keyType
            });
            setTimeout(() => {
                this.getSearchInfo();
                if (keyType === 'sub_category') {
                    this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(item.sub_category) || ''}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory) || ''}`);
                }
            }, 200)
        } else {// when product item is clicked
            this.addButtonClicked(item);
        }
    };
    resetSearchInputStates = () => {
        this.setState({
            selectedCategory: '',
            catSearchResults: [],
            searchResults: [],
            subCategoryList: [],
            searchQuery: '',
            currentSearchType: 'cat',
            no_sub_cat_results: false

        });

        localStorage.removeItem("selectedCategory");
        localStorage.removeItem("sub_category");
        localStorage.removeItem("sub_category2");
    
    };
    closeSuggestionsBox = () =>{
        console.log("we resettning closeSuggestionsBox");

        document.getElementById('suggestionsBox').style.display = "none";
        document.getElementById('closeSuggestionsIcon').style.display = "none";
        this.searchAjax.value = ''; //clear the value of the element
        this.searchAjax.focus(); //sets focus to element

        this.resetSearchInputStates();

        localStorage.removeItem("firstSelectInput");
    }
    addButtonClicked = (item)=>{
        if (item.addon_categories && item.addon_categories.length) {
            this.setState({ popupOpenItem: item.id });
        } else {
            this.props.addProduct(item);
            this.forceStateUpdate();
        }
        
    };
    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            if (this.state.update) {
                this.setState({ update: false });
            } else {
                this.setState({ update: true });
            }
        }, 100);
    };
    addReplacement = (replaceWith, id) => {
        // console.log("replaceWith", replaceWith);
        this.showModalFoundReplacement(id, replaceWith);
    };
    setShopName = (shopName, restaurant_id, cat) => {
        this.setState({
            shopName: shopName
        });
        this.setState({
            restaurant_id: restaurant_id
        });
        if(cat == "restaurant"){
            this.setState({
                firstScreen_einkaufSearch: 'FirstScreen_einkaufSearchRestaurant'
            });
        } else {
            this.setState({
                firstScreen_einkaufSearch: 'FirstScreen_einkaufSearch'
            });
        }
    }

    request = (supermarket_orders_id, id, quantity, replacement, found_replacement, act) => {
        document.getElementById('approve'+id).style.display = "none";
        document.getElementById('approved'+id).style.display = "block";

        console.log('replace {"id":"'+id+'","quantity":'+quantity+',"found":0,"replacement":'+replacement+',"found_replacement":'+found_replacement+'} with {"id":"'+id+'","quantity":'+quantity+',"found":0,"replacement":'+replacement+',"found_replacement":'+found_replacement+',"approved":1}');

        var url = '//'+window.location.hostname+'/php/shopperPwa_confirmReplacement.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&replacement='+replacement+'&found_replacement='+found_replacement+'&act='+act;
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    items = (supermarket_orders, url) =>{
        if(supermarket_orders.items == null || supermarket_orders.items.length === 0) return null;        
        return <Items supermarket_orders={supermarket_orders} url={url} />;
    }

    render() {
        const { supermarket_orders } = this.props;
        let sub_total = 0;

        console.log("items", supermarket_orders.items);

        const styleReplaceWithImg = {
            width:"calc(100% - 115px)", float:"left", marginLeft:"15px"
        }
        const styleReplaceWithoutImg = {
            width:"calc(100% - 50px)", float:"left"
        }

        const styleWithImg = {
            width:"calc(100% - 115px)", float:"left", marginLeft:"15px"
        }
        const styleWithoutImg = {
            width:"calc(100% - 50px)", float:"left"
        }

        var newOrdersDataElements = (tab) => {

            var isAvailable = false;

            const ordersComponent = supermarket_orders.items.map((row, index) => {
                                
                sub_total += row.price*row.quantity;

                if(tab == null || tab == undefined)
                return;
                
                if(tab == 'replacements' && row.found == 0 && row.found_replacement != null){
                    isAvailable = true;
                    return (
                        <div>   
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #959296", background:"#fff"}}>

                                <div style={{width:"100%", float:"left", opacity:0.5}}>
                                    {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                    <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleReplaceWithImg : styleReplaceWithoutImg}>
                                        <div style={{color:"#A04362"}}>Ausverkauft</div>
                                        <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                        {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                    </span> 
                                    <s style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</s>
                                </div>

                                <div style={{width:"100%", float:"left"}}>
                                    {row.replacement_placeholder_image && <img onClick={this.showModalZoom.bind(this, row.replacement_id)} title={row.replacement_name} src={'https://lieferservice123.com'+row.replacement_placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                    <span onClick={this.showModalZoom.bind(this, row.replacement_id)} style={row.replacement_placeholder_image ? styleWithImg : styleWithoutImg}>
                                        <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.found_replacement}x</b> {row.replacement_name}</div>
                                        {row.replacement_grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.replacement_grammageBadge}</div>}
                                    </span> 
                                    <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.replacement_price)*parseFloat(row.found_replacement)).toFixed(2)}</span>
                                </div>

                            </div>

                            {/* show product */}
                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>

                            {/* show replacement */}
                            <Modal show={this.state.ZoomModal[row.replacement_id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.replacement_image && <img src={"https://lieferservice123.com"+row.replacement_image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.replacement_marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.replacement_quantity}x</b> {row.replacement_name}</div>
                    
                                    {row.replacement_grammageBadge && <div><span style={{fontSize:"12px"}}>({row.replacement_grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.replacement_category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.replacement_category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.replacement_price && row.replacement_price.toFixed(2)}</span></div>

                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }

                if(tab == 'refunds' && row.found == 0 && row.found_replacement == null){
                    isAvailable = true;
                    return (
                        <div>
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #959296", background:"#fff"}}>

                                <div style={{width:"100%", float:"left"}}>
                                    {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                    <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleWithImg : styleWithoutImg}>
                                        <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                        {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                    </span> 
                                    <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</span>
                                </div>

                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }

                if(tab == 'adjustments' && row.found > 0 && row.found !== row.quantity){
                    isAvailable = true;
                    return (
                        <div>   
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #959296", background:"#fff"}}>

                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image&&styleWithImg}>
                                    <div style={{color:"#A04362"}}>
                                        {row.found < row.quantity ? <span style={{marginRight:"5px"}}>⇣</span>:<span style={{marginRight:"5px"}}>⇡</span>} 
                                        {row.found}x (ursprünglich {row.quantity}x)
                                    </div>
                                    
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.found}x</b> {row.name}</div>
                                
                                    {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}

                                </span> 

                                <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>
                                    <s style={{fontFamily:"open sans", fontWeight:600, fontSize:"13px"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</s><br/>
                                    €{(parseFloat(row.price)*parseFloat(row.found)).toFixed(2)}
                                </span>
                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }  

                if(tab == 'done' && row.found > 0 && row.quantity == row.found){
                    isAvailable = true;
                    return (
                        <div> 
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #959296", background:"#fff"}}>

                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image&&styleWithImg}>
                                    
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                    
                                    {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}

                                </span> 

                                <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.found)).toFixed(2)}</span>

                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }  

            });

            return isAvailable?ordersComponent:false; 

        }

        const fee = supermarket_orders.trinkgeld;

        const styleHeader = {
            textTransform:"uppercase", padding:"10px", fontSize:"16px", letterSpacing:2, overflow:"auto", borderTop:"1px solid #959296"
        }

        return (
            <React.Fragment>

                <div style={{padding:"10px"}}>

                    <div style={{width:"100%", float:"left", padding:"15px 0", marginTop:"15px"}}>
                        <div style={{float:"left", width:"50%", textAlign:"center"}}><span style={{marginRight:"5px", background:"#4194B5", color:"#fff", padding:"5px 10px", fontWeight:"bold", borderRadius:"3px"}}>{this.makeTotalQuantity(supermarket_orders.items, "done")}</span> Artikel Geliefert </div>
                        <div style={{float:"left", width:"50%", textAlign:"center"}}><span style={{margin:"0 5px", background:"#FEC134", color:"#fff", padding:"5px 10px", fontWeight:"bold", borderRadius:"3px"}}>{this.makeTotalQuantity(supermarket_orders.items, "replacements")+this.makeTotalQuantity(supermarket_orders.items, "refunds")+this.makeTotalQuantity(supermarket_orders.items, "adjustments")}</span> Anpassungen</div>
                    </div>

                    {(newOrdersDataElements("replacements")!==false || newOrdersDataElements("refunds")!==false || newOrdersDataElements("adjustments")!==false) && (
                        <div style={{width:"100%", float:"left", margin:"20px 0", overflow:"auto", border:"1px solid #959296", borderRadius:"5px"}}>

                            <div style={{background:"#FEC134", padding:"10px", width:"100%", float:"left", fontSize:"18px", fontWeight:500, fontFamily:"open sans", color:"#fff", textTransform:"uppercase"}}>
                                Anpassungen ({supermarket_orders.shopName}) 
                                <span style={{textAlign:"right", float:"right", background:"orange", padding:"0 10px", borderRadius:"5px"}}>{this.makeTotalQuantity(supermarket_orders.items, "replacements")+this.makeTotalQuantity(supermarket_orders.items, "refunds")+this.makeTotalQuantity(supermarket_orders.items, "adjustments")}</span>
                            </div>
                        
                            {newOrdersDataElements("replacements")!==false ? (
                                <div>
                                    <div style={styleHeader}>Ersatz</div>
                                    {newOrdersDataElements("replacements")}
                                </div>
                            ):null} 
                            {newOrdersDataElements("refunds")!==false ? (
                                <div>
                                    <div style={styleHeader}>Rückerstattet</div>
                                    {newOrdersDataElements("refunds")}
                                </div>
                            ):null} 
                            {newOrdersDataElements("adjustments")!==false ? (
                                <div>
                                    <div style={styleHeader}>Quantitäts-Anpassungen</div>
                                    {newOrdersDataElements("adjustments")}
                                </div>
                            ):null} 
                        </div>
                    )}

                    {newOrdersDataElements("done")!==false && (
                        <div style={{width:"100%", float:"left", margin:"20px 0", overflow:"auto", border:"1px solid #959296", borderRadius:"5px"}}>
                            <div style={{background:"#4194B5", padding:"10px", width:"100%", float:"left", fontSize:"18px", fontWeight:500, fontFamily:"open sans", color:"#fff", textTransform:"uppercase"}}>
                                Gefunden 
                                <span style={{textAlign:"right", float:"right", background:"#394C5D", padding:"0 10px", borderRadius:"5px"}}>{this.makeTotalQuantity(supermarket_orders.items, "done")}</span>
                            </div>

                            {newOrdersDataElements("done")}
                        </div>
                    )} 

                </div>


                <div style={{width:"100%", float:"left", margin:"20px 0 10px", padding:"10px", borderBottom:"1px solid #959296", fontWeight:"bold", fontSize:"16px", textTransform:"uppercase"}}>Gesamtbetrag</div>
                <ul style={{listStyle:"none", padding:0, margin:0, marginBottom:"20px", width:"100%", float:"left"}}>
                    {this.items(supermarket_orders, "viewReceipt")}
                </ul>

            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(ViewReceipt);

