import { SUPERMARKET_ITEMS_URL } from "../../../../configs/index";

import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

class Items extends Component {

    state = {
        test: [],
    };

    componentDidMount() {
    }
    
    async getName(id){
        const name = await axios
            .post(SUPERMARKET_ITEMS_URL, {
                id: id
            })
            .then(response => {
                return response.data.name;
            })
            .catch(function(error) {
                console.log(error);
                return "";
            });
        console.log('name', name);
        return name;
    }

    makeTotalRefunds = (data = [], subtotal) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return subtotal - sum;
    }

    render() {
        const { supermarket_orders, url } = this.props;

        // let sub_total = 0;
        // const ordersComponent = supermarket_orders.items.map((row, index) => {       
        //     sub_total += row.price*row.quantity;
        // });

        var sub_total = supermarket_orders.subtotal;

        const trinkgeld = sub_total * (supermarket_orders.delivery_tip/100);
        const service_fee = sub_total * (localStorage.getItem("serviceFee")/100);

        var credit = this.makeTotalRefunds(supermarket_orders.items, supermarket_orders.subtotal);

        const viewReceipt = {
            fontWeight:"normal", fontFamily:"open sans", fontWeight:400, width:"100%", float:"left", padding:"5px 10px"
        }

        return (
            <React.Fragment>
                
                {/* {url == "" && ordersComponent} */}

                {url == "viewReceipt" ? (
                    <div>

                    <li style={viewReceipt}>
                        <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{parseFloat(sub_total).toFixed(2)}</span> 
                        <span style={{float:"right"}}>Zwischensumme <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                    </li>

                    <li style={viewReceipt}>
                        <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{parseFloat(supermarket_orders.delivery_charge).toFixed(2)}</span> 
                        <span style={{float:"right"}}>Lieferkosten <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                    </li>

                    <li style={viewReceipt}>
                        <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{parseFloat(service_fee).toFixed(2)}</span> 
                        <span style={{float:"right"}}>Servicegebühr <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                    </li>

                    {supermarket_orders.delivery_tip > 0 && <li style={viewReceipt}>
                        <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{parseFloat(trinkgeld).toFixed(2)}</span> 
                        <span style={{float:"right"}}>Trinkgeld <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                    </li>}

                    {this.makeTotalRefunds(supermarket_orders.items, supermarket_orders.subtotal) < 0 ? (//subtotal after checkout is higher
                        <li style={viewReceipt}>
                            <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{parseFloat(credit*(-1)).toFixed(2)}</span> 
                            <span style={{float:"right"}}>Anpassungen <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                        </li>
                    ) : this.makeTotalRefunds(supermarket_orders.items, supermarket_orders.subtotal) > 0 ? (//subtotal after checkout is lower
                        <li style={viewReceipt}>
                            <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>-{parseFloat(credit).toFixed(2)}</span> 
                            <span style={{float:"right"}}>Anpassungen <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                        </li>
                    ) : null}

                    <li style={{width:"100%", float:"left", fontWeight:"bold", borderTop:"1px solid #959296", padding:"10px", marginTop:"5px"}}>
                        <span style={{padding:"0 4px", width:"50px", float:"right", textAlign:"right"}}>{(parseFloat(sub_total) + parseFloat(supermarket_orders.delivery_charge) + parseFloat(trinkgeld) + parseFloat(service_fee) - parseFloat(credit)).toFixed(2)}</span> 
                        <span style={{float:"right"}}>Gesamt <span style={{marginLeft:"20px"}}>{supermarket_orders.currencyFormat}</span></span>
                    </li>

                    </div>
                ) : (
                    <div>

                    <li style={{fontWeight:"normal", fontSize:"80%", fontFamily:"open sans", fontWeight:400}}>
                        Zwischensumme
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{supermarket_orders.currencyFormat} {parseFloat(sub_total).toFixed(2)}</span> 
                    </li>

                    <li style={{fontWeight:"normal", fontSize:"80%", fontFamily:"open sans", fontWeight:400}}>
                        Lieferkosten
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{supermarket_orders.currencyFormat} {parseFloat(supermarket_orders.delivery_charge).toFixed(2)}</span> 
                    </li>

                    <li style={{fontWeight:"normal", fontSize:"80%", fontFamily:"open sans", fontWeight:400}}>
                        Servicegebühr
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{supermarket_orders.currencyFormat} {parseFloat(service_fee).toFixed(2)}</span> 
                    </li>

                    {supermarket_orders.delivery_tip > 0 && <li style={{fontWeight:"normal", fontSize:"80%", fontFamily:"open sans", fontWeight:400}}>
                        Trinkgeld
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{supermarket_orders.currencyFormat} {parseFloat(trinkgeld).toFixed(2)}</span> 
                    </li>}

                    {supermarket_orders.coupon_name && <li style={{fontWeight:"normal", fontSize:"80%", fontFamily:"open sans", fontWeight:400}}>
                        Gutschein '{supermarket_orders.coupon_name}'
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>- {supermarket_orders.currencyFormat} {parseFloat(supermarket_orders.coupon_discount).toFixed(2)}</span> 
                    </li>}

                    {url == "RunOrd" && <hr/>}

                    <li style={{fontWeight:"bold"}}>
                        Gesamt
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{supermarket_orders.currencyFormat} {(parseFloat(sub_total) + parseFloat(supermarket_orders.delivery_charge) + parseFloat(trinkgeld) + parseFloat(service_fee) - parseFloat(supermarket_orders.coupon_discount)).toFixed(2)}</span> 
                    </li>

                    </div>
                )}
                
            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(Items);

