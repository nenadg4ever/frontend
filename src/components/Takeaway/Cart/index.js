import React, { Component } from "react";
import { addProduct, removeProduct } from "../../../services/cart/actions";
import { checkUserRunningOrder, updateUserInfo } from "../../../services/user/actions";

import BackWithSearch from "../../Takeaway/Elements/BackWithSearch";
import BillDetails from "./BillDetails";
import CartCheckoutBlock from "./CartCheckoutBlock";
import CartItems from "./CartItems";
import Coupon from "./Coupon";
import DelayLink from "../../helpers/delayLink";
import Footer from "../Footer";
import Ink from "react-ink";
import Meta from "../../helpers/meta";
import Nav from "../Nav";
import OrderComment from "./OrderComment";
import { Redirect } from "react-router";
import RestaurantInfoCart from "./RestaurantInfoCart";
import { calculateDistance } from "../../helpers/calculateDistance";
import { connect } from "react-redux";
import { getRestaurantInfoById } from "../../../services/items/actions";
import { updateCart } from "../../../services/total/actions";

class Cart extends Component {
    static contextTypes = {
        router: () => null
    };

    state = {
        loading: false,
        alreadyRunningOrders: false,
        is_operational_loading: true,
        is_operational: true,
        distance: 0
    };

    componentDidMount() {
        if (this.props.cartProducts.length) {
            document.getElementsByTagName("body")[0].classList.add("bg-grey");
        }

        this.props.getRestaurantInfoById(localStorage.getItem("activeRestaurant"));

        const { user } = this.props;
        if (user.success) {
            this.props.checkUserRunningOrder(user.data.id, user.data.auth_token);
            this.props.updateUserInfo(user.data.id, user.data.auth_token);
        } else {
            this.setState({ alreadyRunningOrders: false });
        }
    }

    checkMinOrder(minOrder, gesamt) {
        if (parseFloat(gesamt) < parseFloat(minOrder)) {
            return "none";
        }else{
            return "block";
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.restaurant_info.id) {
            this.__doesRestaurantOperatesAtThisLocation(nextProps.restaurant_info);
        }

        if (nextProps.running_order) {
            this.setState({ alreadyRunningOrders: true });
        }
    }
    addProduct = product => {
        const { cartProducts, updateCart } = this.props;
        let productAlreadyInCart = false;

        cartProducts.forEach(cp => {
            if (cp.id === product.id) {
                if (JSON.stringify(cp.selectedaddons) === JSON.stringify(product.selectedaddons)) {
                    cp.quantity += 1;
                    productAlreadyInCart = true;
                }
            }
        });

        if (!productAlreadyInCart) {
            cartProducts.push(product);
        }

        updateCart(cartProducts);
    };

    removeProduct = product => {
        const { cartProducts, updateCart } = this.props;

        const index = cartProducts.findIndex(p => p.id === product.id && JSON.stringify(p) === JSON.stringify(product));
        //if product is in the cart then index will be greater than 0
        if (index >= 0) {
            cartProducts.forEach(cp => {
                if (cp.id === product.id) {
                    if (JSON.stringify(cp) === JSON.stringify(product)) {
                        if (cp.quantity === 1) {
                            //if quantity is 1 then remove product from cart
                            cartProducts.splice(index, 1);
                        } else {
                            //else decrement the quantity by 1
                            cp.quantity -= 1;
                        }
                    }
                }
            });

            updateCart(cartProducts);
        }
    };

    __doesRestaurantOperatesAtThisLocation = restaurant_info => {
        //send user lat long to helper, check with the current restaurant lat long and setstate accordingly
        const { user } = this.props;
        if(!user.success){
            var distance = calculateDistance(
                restaurant_info.longitude,
                restaurant_info.latitude,
                JSON.parse(localStorage.getItem("userSetAddress")).lng,
                JSON.parse(localStorage.getItem("userSetAddress")).lat
            );
        }
        else if (user.success) {
            var distance = calculateDistance(
                restaurant_info.longitude,
                restaurant_info.latitude,
                user.data.default_address.longitude,
                user.data.default_address.latitude
            );
        } 
        
        console.log(distance);
        this.setState({ distance: distance });
        if (distance <= restaurant_info.delivery_radius) {
            this.setState({ is_operational: true, is_operational_loading: false });
        } else {
            this.setState({ is_operational: false, is_operational_loading: false });
        }
    };

    componentWillUnmount() {
        document.getElementsByTagName("body")[0].classList.remove("bg-grey");
    }
    
    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        if (localStorage.getItem("userSetAddress") === null) {
            const props = this.props;
            if(document.URL.indexOf("?pwa=") >= 0){
                var backToRes = "?pwa="+this.getUrlParameter("pwa");
            } else if(props.restaurant){
                var slug = window.location.href.replace("?install=1", "").split("/").pop();
                var backToRes = "?redirect="+slug;
            } else {
                var backToRes = "";
            }
            return <Redirect to={"/search-location"+backToRes} />;
        }
        if (!this.props.cartProducts.length) {
            document.getElementsByTagName("body")[0].classList.remove("bg-grey");
        }
        const { user, cartTotal, cartProducts, restaurant_info } = this.props;
        return (
            <React.Fragment>
                {/* <Meta
                    seotitle={localStorage.getItem("cartPageTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <Nav logo={true} disable_back_button={true} /> */}

                {cartProducts.length > 0 && (
                    <h2 style={{color:"#393939", padding:"20px 18px", margin:0, textAlign:"center"}}>
                        {localStorage.getItem("cartPageTitle")}
                        <div style={{fontSize:"14px", marginTop:"5px"}}>{cartProducts.length} Artikel im Warenkorb</div>
                    </h2>
                )}


                <div style={{maxWidth:"768px", margin:"auto", marginTop:"15px"}}>
                    {this.state.loading ? (
                        <div className="height-100 overlay-loading">
                            <div>
                                <img src="/assets/img/loading-food.gif" alt={localStorage.getItem("pleaseWaitText")} />
                            </div>
                        </div>
                    ) : (
                        <React.Fragment>

                            {/* <BackWithSearch boxshadow={true} has_title={true} title={localStorage.getItem("cartPageTitle")} disbale_search={true} /> */}
                            
                            {cartProducts.length ? (
                                <React.Fragment>
                                    <div>

                                        {/* <RestaurantInfoCart restaurant={restaurant_info} /> */}
                                        
                                        <div className="block-content block-content-full bg-white pt-10 pb-5">
                                            {/* <h2 className="item-text mb-10">{localStorage.getItem("cartItemsInCartText")}</h2> */}
                                            {cartProducts.map((item, index) => (
                                                <CartItems
                                                    item={item}
                                                    addProduct={this.addProduct}
                                                    removeProduct={this.removeProduct}
                                                    key={item.name + item.id + index}
                                                />
                                            ))}
                                        </div>

                                        {/* <OrderComment /> */}

                                    </div>

                                    <div>

                                        {/* <Coupon /> */}

                                        {(this.state.alreadyRunningOrders) ? (
                                            <div className="auth-error ongoing-order-notify">
                                                <DelayLink to="/my-orders" delay={250} className="ml-2">
                                                    {localStorage.getItem("ongoingOrderMsg")}{" "}
                                                    <i className="si si-arrow-right ml-1" style={{ fontSize: "0.7rem" }}></i>
                                                    <Ink duration="500" />
                                                </DelayLink>
                                            </div>
                                        ):null}

                                    </div>

                                    <div>
                                        <BillDetails total={cartTotal.totalPrice} distance={this.state.distance} />
                                    </div>

                                    {this.state.is_operational_loading ? (
                                        <img
                                            src="/assets/img/various/spinner.svg"
                                            className="location-loading-spinner"
                                            alt="loading"
                                            style={{ marginTop: "-1rem" }}
                                        />
                                    ) : (
                                        <div style={{display:this.checkMinOrder(restaurant_info.price_range, cartTotal.totalPrice)}}>
                                            <CartCheckoutBlock
                                                restaurant_id={this.props.restaurant_info.id}
                                                cart_page={this.context.router.route.location.pathname}
                                                is_operational_loading={this.state.is_operational_loading}
                                                is_operational={this.state.is_operational}
                                            />
                                        </div>
                                    )}
                                </React.Fragment>
                            ) : (
                                <div style={{textAlign:"center"}}>
                                    {/* <img className="cart-empty-img" src="/assets/img/cart-empty.png" alt={localStorage.getItem("cartEmptyText")} /> */}
                                    <h2 className="cart-empty-text text-center">{localStorage.getItem("cartEmptyText")}</h2>
                                    {(this.state.alreadyRunningOrders) ? (
                                        <div
                                            className="auth-error ongoing-order-notify"
                                            style={{
                                                position: "fixed",
                                                bottom: "4rem"
                                            }}
                                        >
                                            <DelayLink to="/my-orders" delay={250} className="ml-2">
                                                {localStorage.getItem("ongoingOrderMsg")}{" "}
                                                <i className="si si-arrow-right ml-1" style={{ fontSize: "0.7rem" }}></i>
                                                <Ink duration="500" />
                                            </DelayLink>
                                        </div>
                                    ):null}
                                    
                                    {/* <Footer active_cart={true} /> */}
                                </div>
                            )}
                        </React.Fragment>
                    )}

                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
    cartProducts: state.cart.products,
    cartTotal: state.total.data,
    user: state.user.user,
    running_order: state.user.running_order,
    restaurant: state.restaurant
});

export default connect(
    mapStateToProps,
    { checkUserRunningOrder, updateCart, addProduct, removeProduct, getRestaurantInfoById, updateUserInfo }
)(Cart);
