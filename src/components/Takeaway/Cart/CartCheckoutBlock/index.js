import React, { Component } from "react";

import DelayLink from "../../../helpers/delayLink";
import Ink from "react-ink";
import { Link } from "react-router-dom";
import { checkConfirmCart } from "../../../../services/confirmCart/actions";
import { connect } from "react-redux";
import { placeOrder } from "../../../../services/checkout/actions";

class CartCheckoutBlock extends Component {
    static contextTypes = {
        router: () => null
    };
    // state = {
    //     loading: true,
    //     is_operational: true
    // };

    componentWillReceiveProps(nextProps) {
        // const { checkout} = this.props;
        if (nextProps.checkout !== this.props.checkout) {
            //redirect to running order page
            this.context.router.history.push("/running-order");
        }
        // console.log("NEXT PROPS - " + nextProps.is_operational);
        // if (nextProps.is_operational !== this.props.is_operational) {
        //     console.log("Came here -> FROM CHILD");
        //     this.setState({ is_operational: false, loading: false });
        // }
    }

    render() {
        // console.log("LOADING - " + this.state.loading);

        const { user } = this.props;
        return (
            <React.Fragment>
                <div className="bg-white" style={{ height: user.success && localStorage.getItem("userSelected") === "SELFPICKUP" ? "auto" : "" }}>

                {this.props.is_operational ? (

                    user.success ? (

                        user.data.default_address == null ? (

                            // show /my-addresses link if default_address is null
                            <div className="p-15">
                                <h2 className="almost-there-text m-0 pb-5">{localStorage.getItem("cartSetYourAddress")}</h2>
                                <DelayLink
                                    to="/my-addresses"
                                    delay={200}
                                    className="btn btn-lg btn-continue"
                                    style={{
                                        position: "relative",
                                        backgroundColor: localStorage.getItem("storeColor")
                                    }}
                                >
                                    {localStorage.getItem("buttonNewAddress")}
                                    <Ink duration={500} />
                                </DelayLink>
                            </div>

                        ) : (

                            <React.Fragment>

                                {/* change delviery address */}
                                {localStorage.getItem("userSelected") === "DELIVERY" && (
                                    <div className="px-15 py-10">
                                        <Link
                                            to={{
                                                pathname: "/my-addresses",
                                                state: {
                                                    restaurant_id: this.props.restaurant_id
                                                }
                                            }}
                                            className="change-address-text m-0 p-5 pull-right"
                                            style={{position:"relative", color: localStorage.getItem("storeColor") }}
                                        >
                                            {localStorage.getItem("cartChangeLocation")}
                                            <Ink duration={400} />
                                        </Link>
                                        <h2 className="deliver-to-text m-0 pl-0 pb-5">{localStorage.getItem("cartDeliverTo")}</h2>
                                        <div className="user-address truncate-text m-0">
                                            {user.data.default_address.address !== null && (
                                                user.data.default_address.address
                                            )}
                                        </div>
                                    </div>
                                )}

                                {/* checkout with account */}
                                <div style={{ marginTop: "1.6rem" }}>
                                    <a href={"/checkout"} onClick={() => this.props.checkConfirmCart()} className="btn btn-lg btn-make-payment" style={{ backgroundColor: localStorage.getItem("cartColorBg"), color: localStorage.getItem("cartColorText"), position: "relative" }}>
                                        {localStorage.getItem("checkoutSelectPayment")}
                                        <Ink duration={400} />
                                    </a>
                                </div>

                            </React.Fragment>

                        )
                    ) : (

                        <React.Fragment>

                            {/* change delviery address */}
                            {localStorage.getItem("userSelected") === "DELIVERY" && (
                                <div className="px-15 py-10">
                                    <a href={"/search-location?redirect=cart"} className="change-address-text m-0 p-5 pull-right" style={{color: localStorage.getItem("storeColor"), position:"relative"}}>
                                        {localStorage.getItem("cartChangeLocation")}
                                        <Ink duration={400} />
                                    </a>
                                    <h2 className="deliver-to-text m-0 pl-0 pb-5">{localStorage.getItem("cartDeliverTo")}</h2>
                                    <div className="user-address truncate-text m-0">
                                        {localStorage.getItem("userSetAddress") && (
                                            JSON.parse(localStorage.getItem("userSetAddress")).address
                                        )}
                                    </div>
                                </div>
                            )}

                            {/* checkout without account */}
                            <div style={{ marginTop: "1.6rem" }}>
                                <a href="/checkout" delay={200} className="btn btn-lg btn-make-payment" style={{ backgroundColor: localStorage.getItem("cartColorBg"), color: localStorage.getItem("cartColorText"), position: "relative" }}>
                                    {localStorage.getItem("checkoutSelectPayment")}
                                    <Ink duration={500} />
                                </a>
                            </div>

                        </React.Fragment>

                    )
                ) : (

                    // not operational at this address
                    <div className="auth-error bg-danger">
                        <div className="error-shake">{localStorage.getItem("cartRestaurantNotOperational")}</div>
                    </div>

                )}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    addresses: state.addresses.addresses,
    cartProducts: state.cart.products,
    cartTotal: state.total.data,
    coupon: state.coupon.coupon,
    checkout: state.checkout.checkout,
    restaurant: state.restaurant
});

export default connect(
    mapStateToProps,
    { placeOrder, checkConfirmCart }
)(CartCheckoutBlock);
