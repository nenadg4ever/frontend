import React, { Component } from "react";
import axios from "axios";

import DelayLink from "../../helpers/delayLink";
import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";


import Flip from "react-reveal/Flip";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import { getPopularLocations } from "../../../services/popularLocations/actions";
import Geocode from "react-geocode";
import PopularPlaces from "../../Takeaway/Location/PopularPlaces";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faMoneyCheck } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faCertificate } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faPaintRoller } from '@fortawesome/free-solid-svg-icons';
import { faCarrot } from '@fortawesome/free-solid-svg-icons';
import { faTaxi } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faClinicMedical } from '@fortawesome/free-solid-svg-icons';
import { faSmoking } from '@fortawesome/free-solid-svg-icons';
import { faFlask } from '@fortawesome/free-solid-svg-icons';
import { faDog } from '@fortawesome/free-solid-svg-icons';
import { faBook } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faStoreAlt } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown, Accordion, Card } from 'react-bootstrap';

import ReactTooltip from 'react-tooltip';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

//cookie
import CookieConsent from "react-cookie-consent";

import UserMenu from "../Account/UserMenu";
import Shops from "../Shops/RestaurantList";
import Location from "../Location";

declare var INSTALL_APP_POPUP;

class FirstScreen extends Component {

    constructor(props) {
        super(props);
        
        this.state = { 
            google_script_loaded: false,
            berechtigungenModal: false,
            house: "",
            tag: "",
            closedShopsModal: false,
        };
    }

    static contextTypes = {
        router: () => null
    };

    //Standort festlegen
    showModalAddress (modal) {
        this.setState({
            AddressModal: true
        });
        if(modal !== undefined){
            this.setState({
                modal: modal
            });
        }
    }
    hideModalAddress = () => {
        this.setState({
            AddressModal: false
        });
    }

    //shops
    showModalShops () {
        this.setState({
            shopsModal: true
        });
    }
    hideModalShops = () => {
        this.setState({
            shopsModal: false,
            closedShopsModal: true
        });
    }

    // Mein Konto
    showModalAccount () {
        this.setState({
            accountModal: true
        });
    }
    hideModalAccount = () => {
        this.setState({
            accountModal: false
        });
    }
    showModalLang () {
        this.setState({
            LangModal: true
        });
    }
    hideModalLang = () => {
        this.setState({
            LangModal: false
        });
    }

    //install app
    showModalInstall () {
        this.setState({
           installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }

    componentDidMount() {
        // remove firstSelectInput from localstorage in order to show suggestion box if user not using x icon
        localStorage.removeItem("firstSelectInput");

        //set country if not set
        if (!localStorage.getItem("country")) {
            this.getCountry();
        }
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        this.props.getPopularLocations();

        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }

        //workaround for desktop view - search not working on first request
        //if(!isMobile){
            if(localStorage.getItem("refreshed_desktop")){
                console.log("workaround search box: already refreshed")
            } else {
                localStorage.setItem("refreshed_desktop", true)
                console.log("workaround search box: refresh in 1.5 s")
                
                setTimeout(function () {
                    var url = window.location.href;
                    window.location = url;
                }, 1500);
            }
        //}
    }

    componentWillUnmount() {
        //remove script when component unmount
        const existingScript = document.getElementById("googleMaps");
        if (existingScript) {
            existingScript.parentNode.removeChild(existingScript);
        }
    }

    //install app
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    //google maps
    handleGeoLocationClick = (results, address) => {
        //check if hausnummer in address
        if(address){
            var num = address.split(" ").pop();
            var first = num.charAt(0);
            var last = num.charAt(num.length-1);

            if(first.match(/^-{0,1}\d+$/) && isNaN(last)){
                console.log(num + " is a number");
                document.getElementById('errorNum').style.display = "none";
            } else if(isNaN(num)) {//if no number at end of string, return false
                console.log(num + " is not a number");
                this.searchInput.focus(); //sets focus to element
                var val = this.searchInput.value; //store the value of the element
                this.searchInput.value = ''; //clear the value of the element
                this.searchInput.value = address+' '; //set that value back. 
    
                document.getElementById('errorNum').style.display = "block";
                return false;
            } else {
                console.log(num + " is a number");
                document.getElementById('errorNum').style.display = "none";
            }
        }
        
        // set localstorage for userSetAddress
        // console.log(results);
        const userSetAddress = {
            lat: (typeof(results[0].geometry.location.lat) === "function") ? results[0].geometry.location.lat() : results[0].geometry.location.lat,
            lng: (typeof(results[0].geometry.location.lng) === "function") ? results[0].geometry.location.lng() : results[0].geometry.location.lng,
            address: results[0].formatted_address,
            house: this.state.house,
            tag: this.state.tag
        };
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
        
        //get this result and store in a localstorage
        localStorage.setItem("geoLocation", JSON.stringify(results[0]));

        //redirect the user to map marker page
        setTimeout(() => {
            // this.context.router.history.push("/shops");
            this.showModalShops()
        }, 100);
    };
    getMyLocation = () => {
        const location = navigator && navigator.geolocation;

        if (location) {
            location.getCurrentPosition(
                position => {
                    this.reverseLookup(position.coords.latitude, position.coords.longitude);
                },
                error => {
                    alert(localStorage.getItem("gpsAccessNotGrantedMsg"));
                }
            );
        }
    };
    reverseLookup = (lat, lng) => {
        Geocode.setApiKey(localStorage.getItem("googleApiKey"));

        Geocode.fromLatLng(lat, lng).then(
            response => {
                // const address = response.results[0].formatted_address;
                // console.log(address);
                const myLocation = [
                    {
                        formatted_address: response.results[0].formatted_address,
                        geometry: {
                            location: {
                                lat: lat,
                                lng: lng
                            }
                        }
                    }
                ];
                this.handleGeoLocationClick(myLocation);
            },
            error => {
                console.error(error);
            }
        );
    };
    hideErrorNum = () =>{
        document.getElementById('errorNum').style.display = "none";
        document.getElementById('errorNum').style.display = "none";
    } 

    //logo
    uppercase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    host(){
        // var d= document.domain.split('.');
        // return d[1] || d[0];
        var h = window.location.host.replace("."+this.domain(), "");
        var h = this.uppercase(h);
        return h;
    }
    domain(){
        var d = window.location.host;
        var d = d.split(".").pop();
        return d;
    }

    //lang and country
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("prefix", "43");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("prefix", "49");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("prefix", "41");
                    localStorage.setItem("userPreferedLanguage", "1");
                } else {
                    localStorage.setItem("userPreferedLanguage", "2");
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    changeCountry = (country) => {
        if(country !== localStorage.getItem("country")){
            localStorage.removeItem("userSetAddress");
        }

        localStorage.setItem("country", country);
        if(country == "AT"){
            localStorage.setItem("prefix", "43");
            localStorage.setItem("shop", "Spar");
        } 
        else if(country == "DE"){
            localStorage.setItem("prefix", "49");
            localStorage.setItem("shop", "REWE");
        } 
        else if(country == "CH"){
            localStorage.setItem("prefix", "41");
        } 
        var url = window.location.href;
        window.location = url;
    }
    changeLang = (lang) => {
        // console.log("lang", lang);
        this.props.getSingleLanguageData(lang);
        localStorage.setItem("userPreferedLanguage", lang);
        var url = window.location.href;
        window.location = url;
    };    
    convertCodetoLang = (code)=>{
        if(code == "DE"){
            var country = "Deutschland"
        } else if(code == "AT"){
            var country = "Österreich"
        } else if(code == "CH"){
            var country = "Schweiz"
        } 

        return country;
    }

    capitalizeFLetter = (name) => { 
        var name = name.charAt(0).toUpperCase() + name.slice(1)
        return name;
    } 
    
    render() {
        const { user } = this.props;

        if (localStorage.getItem("activeShop") !== null) {
            return <Redirect to={"/"+localStorage.getItem("activeShop")} />;
        } else if (localStorage.getItem("userSetAddress") !== null) {
            setTimeout(() => {
                // return <Redirect to="/shops" />;
                if(this.state.closedShopsModal == false){
                    this.showModalShops();
                }
            }, 1500);
        }

        //required chrome version for alert
        if(osName == "Windows"){
            var versionRequired = 70;
        } else if(osName == "Android"){
            var versionRequired = 31;
        } else if(osName == "Ubuntu"){
            var versionRequired = 72;
        } 

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = <span onClick={this.getMyLocation} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenSetupLocation")}<Ink duration="500" /></span>
        } else if(isMobileSafari){
            var install = <span onClick={() => { this.showPrompt(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
        } else {
            var install = <span onClick={() => { this.showModalBerechtigungen(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
        }
        
        if(localStorage.getItem("userPreferedLanguage") == 1 && localStorage.getItem("country") == "AT"){
            var langCss = "locale-flag-de-at";
        } else if(localStorage.getItem("userPreferedLanguage") == 1 && localStorage.getItem("country") == "DE"){
            var langCss = "locale-flag-de";
        } else if(localStorage.getItem("userPreferedLanguage") == 2){
            var langCss = "locale-flag-en";
        }

        var langStyle = {
            marginBottom:"15px", cursor:"pointer", color:"#666"
        };

        var langStyleBold = {
            marginBottom:"15px", cursor:"pointer", fontWeight:"bold", color:"#666"
        };
        

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <CookieConsent
                    location="bottom"
                    buttonText="Okay"
                    cookieName="myAwesomeCookieName2"
                    style={{ background: "#2B373B" }}
                    buttonStyle={{ color: "#4e503b", fontSize: "13px" }}
                    expires={150}
                >
                Wir verwenden Cookies zur Verbesserung unserer Website.
                </CookieConsent>

                <div className="imgbg">
                    {/* header */}
                    <div style={{zIndex: 3}}>
                        {/* lang */}
                        <span onClick={() => { this.showModalLang(); }} style={{float:"left", margin:"12px", fontSize: "26px"}}><span className={"locale-flag "+langCss}></span></span>
                        
                        {/* login */}
                        <span className="loginBtn" style={{float:"right", margin:"12px", position:"relative", fontWeight:600, fontFamily:"open sans", color:"#fff", border:"2px solid #fff", boxShadow:"rgba(0, 0, 0, 0.5) 0px 2px 6px 0px", padding:"10px 16px", display:"inline-block", borderRadius:"4px", cursor:"pointer"}} onClick={() => this.showModalAccount()}>
                            {this.props.user.success ? this.capitalizeFLetter(this.props.user.data.name) : "Anmelden"}
                            <Ink duration="500" />
                        </span>
                    </div>

                    {/* set address */}
                    <div style={{float:"left", width:"100%", padding:"40px 18px 40px"}}>

                        <div className="searchArea">

                            <div className="hourInfo"><b>Geliefert in 60 Min.</b></div>

                            <div className="sloganInfo" style={{textAlign:"center", marginTop:"5px", marginBottom:"10px", fontFamily:"open sans", fontWeight:300}}>Verabschieden Sie sich von schweren Taschen, Zeitverschwendung und Warteschlangen.</div>

                            <div style={{maxWidth:"540px", margin:"auto"}}>
                                {this.state.google_script_loaded && (
                                    <GooglePlacesAutocomplete
                                        autocompletionRequest={{
                                            componentRestrictions: {
                                                //country: ['at'],
                                                country: [localStorage.getItem("country")],
                                            }
                                        }}
                                        loader={<img src="/assets/img/various/spinner.svg" className="location-loading-spinner" alt="loading" />}
                                        renderInput={props => (
                                            <div className="input-location-icon-field" style={{ position: "relative" }}>
                                                <label style={{display:"inherit"}}>
                                                    <input
                                                        {...props}
                                                        // onFocus={ this.hideErrorNum } 
                                                        className="form-control search-input search-box"
                                                        placeholder={localStorage.getItem("searchAreaPlaceholder")}
                                                        ref={input => {
                                                            this.searchInput = input;
                                                        }}
                                                        // onClick={window.innerWidth < 768 ? () => this.showModalAddress():""}
                                                        onClick={() => this.showModalAddress()}
                                                        style={{border:"1px solid rgb(189, 189, 189)", height:"56px", fontSize:"16px", fontStyle:"italic", paddingLeft: "40px"}}
                                                    />
                                                </label>

                                                {/* locate me */}
                                                <span title={localStorage.getItem("firstScreenLocate")} className="locateMe" onClick={this.getMyLocation}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faMapMarkerAlt} /><Ink duration="500" /></span>

                                                {/* missing nr */}
                                                <div id="errorNum" style={{ display:"none", color: "#fff", position: "relative", background: "orange", width: "100%", padding: "10px 15px", zIndex: 1}}><FontAwesomeIcon icon={faExclamationTriangle} style={{marginRight:"5px"}} />Gib Deine Hausnummer an</div>
                                                
                                            </div>
                                        )}
                                        renderSuggestions={(active, suggestions, onSelectSuggestion) => (
                                            <div className="location-suggestions-container">
                                                {suggestions.map((suggestion, index) => (
                                                    <Flip top delay={index * 50} key={suggestion.id}>
                                                        <div
                                                            className="location-suggestion"
                                                            onClick={event => {
                                                                onSelectSuggestion(suggestion, event);
                                                                geocodeByPlaceId(suggestion.place_id)
                                                                    .then(results => this.handleGeoLocationClick(results, suggestion.structured_formatting.main_text))
                                                                    .catch(error => console.error(error));
                                                            }}
                                                        >
                                                            
                                                            <span className="location-main-name">{suggestion.structured_formatting.main_text}</span>
                                                            <br />
                                                            <span className="location-secondary-name">{suggestion.structured_formatting.secondary_text}</span>
                                                        </div>
                                                    </Flip>
                                                ))}
                                            </div>
                                        )}
                                    />
                                )}
                            </div>

                            <div className="cities" style={{maxWidth:"540px", margin:"auto"}}>                                  
                                <div style={{marginTop:"15px"}}><a href={"javascript:void()"} onClick={() => { document.getElementById("cities").style.display = "block"; }}> Alle Städte ansehen ›</a></div>
                                <div id="cities" style={{display:"none"}}><PopularPlaces newClass={true} handleGeoLocationClick={this.handleGeoLocationClick} locations={this.props.popular_locations} /></div>
                            </div>

                            {/* categories */}
                            {/* <div className="scrollbar">
                                <div style={{width:"max-content"}}>

                                    {localStorage.getItem("country") !== "CH" ? 
                                    <a className="einkaufenLink" href={'/grocery'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faCarrot} /></span><br/>
                                        Supermarkt
                                        <Ink duration="500" />
                                    </a> : null}

                                    <a className="einkaufenLink" href={'/restaurants'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faUtensils} /></span><br/>
                                        Restaurant
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/electronic'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faMobileAlt} /></span><br/>
                                        Elektronik<br/>& Zubehör
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/drugstore'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faFlask} /></span><br/>
                                        Drogerie
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/pharmacy'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faClinicMedical} /></span><br/>
                                        Apotheke
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/tobacco'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faSmoking} /></span><br/>
                                        Tabak & Zubehör
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/pet'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faDog} /></span><br/>
                                        Tiernahrung & Zubehör
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/libraries'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faBook} /></span><br/>
                                        Bibliothek & Bücherei
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/restaurants?cat=restaurants'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faTaxi} /></span><br/>
                                        Taxis
                                        <Ink duration="500" />
                                    </a>

                                    <a className="einkaufenLink" href={'/restaurants?cat=restaurants'}>
                                        <span style={{textAlign:"left"}}><FontAwesomeIcon style={{fontSize:"28px", marginBottom:"15px"}} icon={faPaintRoller} /></span><br/>
                                        Dienstleist<br/>ungen
                                        <Ink duration="500" />
                                    </a>

                                </div>
                            </div> */}

                        </div>

                    </div>
                </div>

                {/* advantages */}
                <div style={{overflow:"auto", padding: "60px 16px 60px", display:"block", lineHeight: "24px"}}>
                    
                    <div className="advantagesIndexList" style={{fontSize: "15px"}}>

                        <div className="advantagesIndex" onClick={() => window.location = "/shopper"}>
                            {/* <FontAwesomeIcon icon={faShoppingCart} className="store" style={{color:"#010101"}} /> */}
                            <div className="img"><img src={"https://lieferservice.me/assets/img/shopper.jpg"} /></div>
                            <div>
                                <h1>Werde Shopper</h1>
                                <p>Als Shopper & Zusteller verdienen Sie bis zu 30€ pro Stunde.<a>Verdiene Geld <FontAwesomeIcon icon={faArrowRight} style={{marginLeft:"5px", fontSize:"10px"}} /></a></p>
                            </div>
                        </div>

                        <div className="advantagesIndex" onClick={() => window.location = "/store"}>
                            {/* <FontAwesomeIcon icon={faStoreAlt} className="store" style={{color:"#010101"}} /> */}
                            <div className="img"><img src={"https://lieferservice.me/assets/img/store.jpg"} /></div>
                            <div>
                                <h1>Werde Partner</h1>
                                <p>Umsatz um bis zu 60% steigern und neue Kunden erreichen.<a>Geschäft anmelden <FontAwesomeIcon icon={faArrowRight} style={{marginLeft:"5px", fontSize:"10px"}} /></a></p>
                            </div>
                        </div>

                        <div className="advantagesIndex" onClick={() => this.showModalBerechtigungen()}>
                            {/* <FontAwesomeIcon icon={faMobile} className="store" style={{color:"#010101"}} > */}
                            <div className="img"><img src={"https://lieferservice.me/assets/img/phone.jpg"} /></div>
                            <div>
                                <h1>{localStorage.getItem("firstScreenDownloadApp")}</h1>
                                <p>{localStorage.getItem("firstScreenDownloadAppSub")}<a>App installieren <FontAwesomeIcon icon={faArrowRight} style={{marginLeft:"5px", fontSize:"10px"}} /></a></p>
                            </div>
                        </div>
                    </div>
                </div>

                {/* FAQ */}
                <div className="faq">
                    <div className="faqHead">Fragen & Antworten</div>
                    <Accordion>

                        {/* Was ist Lieferservice123? */}
                        <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="2" className="accordionHead">
                            Was ist Lieferservice123?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2" className="accordionBody">
                                <Card.Body>Unsere Mission ist es, alle Produkte von Restaurants, Supermärkte und Einzelhandelsgeschäften, der Welt ultraschnell und bequem im 2-Stunden-Fenster zugänglich zu machen.</Card.Body>
                            </Accordion.Collapse>
                        </Card>

                        {/* Wie funktioniert Lieferservice123.com? */}
                        <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="6" className="accordionHead">
                            Wie funktioniert Lieferservice123.com?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="6" className="accordionBody">
                                <Card.Body>

                                <div style={{textAlign:"center", fontSize: "15px", overflow:"auto", margin:"auto"}}>

                                    <div className="howto">
                                        <FontAwesomeIcon icon={faMapMarkerAlt} style={{width:"50px", height:"50px"}} />
                                        <div>1. {localStorage.getItem("firstScreenEnterLocation")}</div>
                                        <p>
                                            {localStorage.getItem("firstScreenEnterLocationSub")}<br/>
                                        </p>
                                    </div>

                                    <div className="howto">
                                        <FontAwesomeIcon icon={faStoreAlt} style={{width:"50px", height:"50px"}} />
                                        <div>2. {localStorage.getItem("firstScreenChooseRestaurant")}</div>
                                        <p>{localStorage.getItem("firstScreenChooseRestaurantSub")}</p>
                                    </div>

                                    <div className="howto">
                                        <FontAwesomeIcon icon={faShoppingCart} style={{width:"50px", height:"50px"}} />
                                        <div>3. {localStorage.getItem("firstScreenPaymentMethod")}</div>
                                        <p>
                                            {localStorage.getItem("firstScreenPaymentMethodSub")}<br/>
                                        </p>
                                    </div>
                                </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>

                        {/* Wo wird der Lieferservice angeboten? */}
                        <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="10" className="accordionHead">
                            Wo wird der Lieferservice angeboten?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="10" className="accordionBody">
                                <Card.Body>Um dies herauszufinden, geben Sie Ihre Adresse in der Suche ein. Wenn Ihr Gebiet noch nicht vom Dienst abgedeckt ist, wird eine Meldung angezeigt. Wir sind bemüht den Service auf alle Großstädte auszuweiten.</Card.Body>
                            </Accordion.Collapse>
                        </Card>

                        {/* Wie viel kostet die Lieferung? */}
                        <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="11" className="accordionHead">
                            Wie viel kostet die Lieferung?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="11" className="accordionBody">
                                <Card.Body>Die Lieferung kostet max. 4,90 € und für eine schnelle Lieferung können Sie sogar online Trinkgeld festlegen. Die Gesamtkosten werden Ihnen vor Bestätigung der Bestellung immer klar angegeben.</Card.Body>
                            </Accordion.Collapse>
                        </Card>

                        {/* <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="4" className="accordionHead">
                            Wie kann ich Premium Vorteile gratis freischalten?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="4" className="accordionBody">
                                <Card.Body>Entweder bei mindestens 10 Restaurants bestellen oder <span onClick={() => { this.openContactPopup(); }} style={{cursor:"pointer", color:"blue", textDecoration:"underline"}}>10 Freunde einladen</span>. 
                                <br/><br/>
                                Danach wird deine Telefonnummer automatisch mit Premium Vorteilen freigeschaltet. (32% Rabatt, Gratisgetränk und kostenlose Zustellung bei allen Restaurants)</Card.Body>
                            </Accordion.Collapse>
                        </Card> */}

                        {/* <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="5" className="accordionHead">
                            Welche Vorteile haben Restaurant Besitzer?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="5" className="accordionBody"><Card.Body>
                                <div style={{}}>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> 0,00% Provision = mehr Gewinn <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant1' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Mehr Umsatz dank Rabattsystem <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant2' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Kundentreue durch Dauerauftrag <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant3' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Gratis App nur für Dein Restaurant <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant4' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Automatische Anmeldung <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant5' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Zahlungskräftige VIP Kunden <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant6' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Kunden Verifizierung über Tel Nr. <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='restaurant7' /><br/>
                                    <FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /> Kostenloser Service<br/>
                                </div>

                                <ReactTooltip id='restaurant1' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir verlangen keine Provision<br/>und finanzieren uns über Investoren,<br/>Spenden, Werbung, etc.</div></ReactTooltip>

                                <ReactTooltip id='restaurant2' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Dank inteligentem Rabattsystem<br/>kann Ihr Restaurant mehr<br/>Umsatz erreichen.</div></ReactTooltip>

                                <ReactTooltip id='restaurant3' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Mehr Kundentreue durch<br/>automatische Bestellungen<br/>via Dauerauftrag.</div></ReactTooltip>

                                <ReactTooltip id='restaurant4' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Eine gratis App mit deinem Logo<br/>u. Restaurantnamen. Die App wird<br/>auf den Kunden Bildschirm installiert,<br/>sodass der User schneller über<br/> dein Restaurant bestellen kann.<br/>Mit deiner eigenen App können<br/>Kunden nur bei deinem Restaurant<br/>bestellen. </div></ReactTooltip>

                                <ReactTooltip id='restaurant5' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir zeigen alle Restaurants, die wir im<br/>Internet finden können. Sie brauchen<br/>daher Ihr Restaurant nicht anzumelden. </div></ReactTooltip>

                                <ReactTooltip id='restaurant6' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>VIP Kunden erhalten mehr<br/>Rabatt, bestellen dafür aber<br/>auch viel und regelmäßig.</div></ReactTooltip>

                                <ReactTooltip id='restaurant7' place="left"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Nicht angemeldete User müssen sich<br/>via Telefonnummer verifizieren, um bei<br/>deinem Restaurant zu bestellen.</div></ReactTooltip>

                                <div style={{fontSize:"10px", margin:"10px 0", fontWeight:"400", fontFamily:"open sans"}}>
                                    Gerne möchten wir Sie beraten - TEL +43 800 070855
                                </div>    
                                    
                            </Card.Body></Accordion.Collapse>
                        </Card> */}

                        {/* Was ist eine 1-Klick Bestellung? */}
                        {/* <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="0" className="accordionHead">
                            Was ist eine 1-Klick Bestellung?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0" className="accordionBody">
                                <Card.Body>
                                <div style={{marginBottom:"10px", fontWeight:"bold"}}>Derzeit nur für Restaurants verfügbar!</div>
                                Mit 1-Klick wird das Bestellen zum Kinderspiel, denn es ist keine User Interaktion erforderlich um eine 1-Klick Bestellung aufzugeben.
                                <br/><br/>
                                Du kannst eine 1-Klick Bestellung auf deinen Homescreen speichern, und mit nur einem Klick, den Countdown (10 s) der Bestellung einleiten.
                                <br/><br/>
                                Dank der automatischen Standortermittlung kann auch im Freien (z.B. Parks) bestellt werden. Längen- und Breitengrad werden (beim Öffnen der 1-Klick Bestellung) automatisch ermittelt, um deine Bestellung ohne Einschränkung, von überall aufzunehmen.</Card.Body>
                            </Accordion.Collapse>
                        </Card> */}

                        {/* Was ist eine Dauerauftrags Bestellung? */}
                        {/* <Card className="accordionCard">
                            <Accordion.Toggle as={Card.Header} eventKey="1" className="accordionHead">
                            Was ist eine Dauerauftrags Bestellung?
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1" className="accordionBody">
                                <Card.Body><div style={{marginBottom:"10px", fontWeight:"bold"}}>Derzeit nur für Restaurants verfügbar!</div>Dauerauftrags Bestellungen helfen Dir den Bestellvorgang zu überspringen, um Zeit zu sparen. Du kannst zeitgesteuerte Bestellungen auf unserer App aktivieren, um wiederkehrende Bestellungen zu wiederholen (automatisieren).<br/><br/>Einmal einstellen und Dein Essen wird regelmäßig zur vereinbarten Zeit zugestellt. (Geeignet für Unternehmen ohne Kantine).</Card.Body>
                            </Accordion.Collapse>
                        </Card> */}

                    </Accordion>
                </div>

                {/* install app */}
                {matchMedia('(display-mode: standalone)').matches == false ? <div style={{width:"100%", float:"left", textAlign:"center", padding: "0 18px 50px", margin:"30px 0"}}>
                    <div style={{maxWidth:"768px", margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                        {/* <div className='faqHead'>{localStorage.getItem("firstScreenDownloadApp")}</div> */}

                        {/* <div> */}
                            {/* <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppSub")}</div>  */}
                            {/* <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppWin")}</div> */}
                        {/* </div> */}
 
                        <a onClick={() => { this.showModalBerechtigungen(); }} style={{cursor:"pointer", margin:"4px", display: "inline-block"}}><img width="156px" alt="Google Play" src="https://lieferservice123.com/assets/img/playstore.png" /></a>
                    
                        <a onClick={() => { this.showPrompt() }} style={{cursor:"pointer", margin:"4px", display: "inline-block"}}><img width="156px" alt="App Store" src="https://lieferservice123.com/assets/img/appstore.png" /></a>

                        <div id="showPwaprompt" style={{display:"none"}}>
                            <PWAPrompt debug={1} 
                            copyTitle={"Add to Home Screen"} 
                            copyBody={"This website has app functionality. Add it to your home screen to use it in fullscreen and while offline."}
                            copyShareButtonLabel={"1) Press the 'Share' button"}
                            copyAddHomeButtonLabel={"2) Press 'Add to Home Screen'"}
                            copyClosePrompt={"Cancel"} 
                            />
                        </div>
                    </div>
                </div>
                : null}
                
                {/* language */}
                <Modal show={this.state.LangModal} onHide={this.hideModalLang}>
                    <Modal.Header closeButton>
                        {/* <Modal.Title>Einstellungen</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>

                        <div style={{float:"left", width:"188px", borderRight:"1px solid #f8f5f2", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>Land</div>
                            <div style={localStorage.getItem("country")=="DE"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("DE"); }}>Deutschland</a></div>
                            <div style={localStorage.getItem("country")=="AT"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("AT"); }}>Österreich</a></div>
                            <div style={localStorage.getItem("country")=="CH"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("CH"); }}>Schweiz</a></div>
                        </div>

                        <div style={{float:"left", width:"188px", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>{localStorage.getItem("footerLanguage")}</div>

                            <div style={localStorage.getItem("userPreferedLanguage")==1?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-de"></span></div> <a onClick={() => { this.changeLang("1"); }} style={{marginLeft:"10px"}}>Deutsch</a></div>

                            <div style={localStorage.getItem("userPreferedLanguage")==2?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-en"></span></div> <a onClick={() => { this.changeLang("2"); }} style={{marginLeft:"10px"}}>English</a></div>
                        </div>

                    </Modal.Body>
                </Modal>

                {/* footer links */}
                <div className="footerLinks" style={{padding: "12px", textAlign:"center", width:"100%", margin:"auto"}}>

                    {/* <a onClick={() => { this.showModalContact(); }}>Kontakt</a> */}
                    <a href={'/pages/cookies'}>Cookies</a>
                    <a href={'/pages/agb'}>{localStorage.getItem("footerTerms")}</a>
                    <a href={'/pages/impressum'}>{localStorage.getItem("footerCompanyDetails")}</a>
                    {/* <a onClick={() => { this.showModalDatenschutz(); }}>Datenschutz</a> */}
                    
                </div>

                {/* app-vorteile */}
                <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>App-Vorteile</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferservice123 auf Desktop installieren.</div>

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Ohne Internetverbindung bei Lieferservice123 bestellen.</div> */}

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bis zu 32% Mengenrabatt.</div> */}

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bessere Kommunikation zwischen Shopper und Kunde durch Push-Nachrichten.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                            Weiter
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* app-installieren */}
                <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>Installieren</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            Klicke auf hinzufügen um Lieferservice123 auf deinen Bildschirm zu installieren.
                        </div>

                        <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                            Hinzufügen
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* account */}
                <Modal show={this.state.accountModal} onHide={this.hideModalAccount} size="sm">
                    <Modal.Header closeButton style={{padding:"16px 20px 0"}}>
                        <Modal.Title>Hi, {this.props.user.success ? this.capitalizeFLetter(this.props.user.data.name) : "Gast"}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 0 10px"}}>

                        <hr style={{borderColor:"#e5e5e5"}} />
                        
                        <UserMenu />

                    </Modal.Body>
                </Modal>

                {/* shops */}
                <Modal show={this.state.shopsModal} onHide={this.hideModalShops}>
                    <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}>
                    </Modal.Header>
                    <Modal.Body style={{padding:0, background:"rgb(232, 233, 238)"}}>
                        <Shops />
                    </Modal.Body>
                </Modal>

                {/* deliver to */}
                <Modal show={this.state.AddressModal} onHide={this.hideModalAddress} size="sm">
                    <Modal.Header closeButton className="bg-grey"><Modal.Title>Standort festlegen</Modal.Title></Modal.Header>
                    <Modal.Body style={{padding:"0 25px 25px"}} className="bg-grey">
                        <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Lieferoptionen und -geschwindigkeit können, abhängig von der Lieferadresse, variieren</div>
                        <Location 
                            modal={this.state.modal || ''} 
                            redirect={window.location.href.replace("?install=1", "").split("/").pop()}
                        />
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    settings: state.settings.settings,
    user: state.user.user,
    language: state.languages.language,
    popular_locations: state.popular_locations.popular_locations
});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData, getPopularLocations }
)(FirstScreen);
