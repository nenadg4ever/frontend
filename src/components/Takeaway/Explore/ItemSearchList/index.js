import React, { Component } from "react";

import { Link } from "react-router-dom";
import Ink from "react-ink";
import LazyLoad from "react-lazyload";
import ProgressiveImage from "react-progressive-image";

class ItemSearchList extends Component {
    render() {
        const { items } = this.props;
        console.log(items);

        return (
            <React.Fragment>
                <div className="bg-white mb-50 mt-10">
                    <h5 className="px-15 mb-1 text-muted">{localStorage.getItem("exploreItemsText")}</h5>
                    {items.map(item => (
                        <div key={item.id} className="col-xs-12 col-sm-12 restaurant-block">
                            <Link
                                to={{
                                    pathname: "../restaurants/" + item.restaurant.slug + "/" + item.id,
                                    state: {
                                        fromExplorePage: true
                                    }
                                }}
                                className="block block-link-shadow text-center light-bottom-border"
                            >
                                <div className="block-content block-content-full">
                                    <LazyLoad>
                                        <ProgressiveImage delay={100} src={item.image} placeholder={item.placeholder_image}>
                                            {(src, loading) => (
                                                <img
                                                    src={src}
                                                    alt={item.name}
                                                    className="restaurant-image"
                                                    style={{
                                                        filter: loading ? "blur(1.2px) brightness(0.9)" : "none"
                                                    }}
                                                />
                                            )}
                                        </ProgressiveImage>
                                    </LazyLoad>
                                </div>
                                <div className="block-content block-content-full restaurant-info">
                                    <div className="font-w600 mb-5">{item.name}</div>
                                    <div className="font-size-sm font-w600 truncate-text">
                                        <span className="text-muted">By</span>{" "}
                                        <span style={{ color: localStorage.getItem("storeColor") }}>{item.restaurant.name}</span>
                                    </div>

                                    <div className="font-size-sm font-w600 text-muted">
                                        {localStorage.getItem("currencyFormat")} {item.price}
                                    </div>
                                    <br />
                                </div>

                                <Ink duration="500" />
                            </Link>
                        </div>
                    ))}
                </div>
            </React.Fragment>
        );
    }
}

export default ItemSearchList;
