import React, { Component } from "react";

import Ink from "react-ink";

class AddressList extends Component {
    render() {
        const { user, address } = this.props;
        return (
            <React.Fragment>
                <div className={!address.is_operational && this.props.fromCartPage ? "address-not-usable" : ""} style={{background:"#fff", padding:"10px", paddingRight:0, marginBottom:"10px", borderRadius:"0.275rem"}}>

                    {user.data.default_address_id === address.id ? (
                        // current address (selected btn)
                        <button className="btn btn-lg pull-right btn-address-default">
                            <i className="si si-check" style={{ color: localStorage.getItem("storeColor") }} />
                            <Ink duration={200} />
                        </button>
                    ) : (
                        <React.Fragment>
                            {this.props.fromCartPage ? (
                                <React.Fragment>
                                    {address.is_operational ? (
                                        <button
                                            className="btn btn-lg pull-right btn-address-default"
                                            onClick={() => this.props.handleSetDefaultAddress(address.id)}
                                        >
                                            <i className="si si-check" />
                                            <Ink duration={200} />
                                        </button>
                                    ) : (
                                        <span className="text-danger text-uppercase font-w600 text-sm08">
                                            {" "}
                                            <i className="si si-close"></i> {localStorage.getItem("addressDoesnotDeliverToText")}
                                        </span>
                                    )}
                                </React.Fragment>
                            ) : (
                                // select address
                                <button className="btn btn-lg pull-right btn-address-default" onClick={() => this.props.handleSetDefaultAddress(address.id)}>
                                    <i className="si si-check" />
                                    <Ink duration={200} />
                                </button>
                            )}
                        </React.Fragment>
                    )}

                    {/* full address */}
                    <p className="m-0 text-capitalize text-sm08">{address.address}</p>

                    {!this.props.fromCartPage && (
                        <React.Fragment>
                            {user.data.default_address_id !== address.id ? (
                                // delete address text
                                <div className="btn-edit-address mt-5" style={{ color: localStorage.getItem("storeColor"), cursor:"pointer", display:"inline-block", border:"1px solid orange", padding:"1px 5px", borderRadius:"3px", fontSize:"12px" }} onClick={() => this.props.handleDeleteAddress(address.id)}>{localStorage.getItem("deleteAddressText")}</div>
                            ) : (
                                // current address text
                                <div className="btn-edit-address" style={{color:localStorage.getItem("storeColor"), fontWeight:300, fontFamily:"open sans"}}>Current Address</div>
                            )}
                        </React.Fragment>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default AddressList;
