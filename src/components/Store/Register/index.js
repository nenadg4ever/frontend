import React, { Component } from "react";
import { loginUser, registerUser, sendOtp, verifyOtp } from "../../../services/user/actions";

import BackButton from "../../Takeaway/Elements/BackButton";
import ContentLoader from "react-content-loader";
import { NavLink } from "react-router-dom";
import { Redirect } from "react-router";
import SimpleReactValidator from "simple-react-validator";
import SocialButton from "../../Takeaway/Auth/SocialButton";
import { connect } from "react-redux";
import Nav from "../../Takeaway/Nav";
import Ink from "react-ink";
import axios from "axios";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

declare var INSTALL_APP_POPUP;
class Register extends Component {
    constructor() {
        super();
        this.validator = new SimpleReactValidator({
            autoForceUpdate: this,
            messages: {
                required: localStorage.getItem("fieldValidationMsg"),
                string: localStorage.getItem("nameValidationMsg"),
                email: localStorage.getItem("emailValidationMsg"),
                regex: localStorage.getItem("phoneValidationMsg"),
                min: localStorage.getItem("minimumLengthValidationMessage")
            }
        });
    }

    state = {
        loading: false,
        name: "",
        email: "",
        phone: "",
        password: "",
        otp: "",
        accessToken: "",
        provider: "",
        error: false,
        email_phone_already_used: false,
        invalid_otp: false,
        showResendOtp: false,
        countdownStart: false,
        countDownSeconds: 15
    };

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        if (localStorage.getItem("enableFacebookLogin") === "true" || localStorage.getItem("enableGoogleLogin") === "true") {
            setTimeout(() => {
                if (this.refs.socialLogin) {
                    this.refs.socialLogin.classList.remove("hidden");
                }
                if (this.refs.socialLoginLoader) {
                    this.refs.socialLoginLoader.classList.add("hidden");
                }
            }, 0.5 * 1000);
        }

        // const { user } = this.props;
        // if (user.success) {
        //     this.setStoreOwnerRole(user.data.id, user.data.auth_token);
        // }
    }

    handleInputChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleRegister = event => {
        event.preventDefault();

        if (
            this.validator.fieldValid("name") &&
            this.validator.fieldValid("email") &&
            this.validator.fieldValid("phone") &&
            this.validator.fieldValid("password")
        ) {
            this.setState({ loading: true });
            if (localStorage.getItem("enSOV") === "true") {
                //sending email and phone, first verify if not exists, then send OTP from the server
                this.props.sendOtp(this.state.email, this.state.phone, null);
            } else {
                this.props.registerUser(
                    this.state.name,
                    this.state.email,
                    this.state.phone,
                    this.state.password,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }
        } else {
            console.log("Validation Failed");
            this.validator.showMessages();
        }
    };

    handleRegisterAfterSocialLogin = event => {
        event.preventDefault();
        this.setState({ loading: true });
        if (this.validator.fieldValid("phone")) {
            if (localStorage.getItem("enSOV") === "true") {
                //sending email and phone, first verify if not exists, then send OTP from the server
                this.props.sendOtp(this.state.email, this.state.phone, null);
            } else {
                this.props.loginUser(
                    this.state.name,
                    this.state.email,
                    null,
                    this.state.accessToken,
                    this.state.phone,
                    this.state.provider,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }
        } else {
            this.setState({ loading: false });
            console.log("Validation Failed");
            this.validator.showMessages();
        }
    };

    resendOtp = () => {
        if (this.validator.fieldValid("phone")) {
            this.setState({ countDownSeconds: 15, showResendOtp: false });
            this.props.sendOtp(this.state.email, this.state.phone, null);
        }
    };

    handleVerifyOtp = event => {
        event.preventDefault();
        console.log("verify otp clicked");
        if (this.validator.fieldValid("otp")) {
            this.setState({ loading: true });
            this.props.verifyOtp(this.state.phone, this.state.otp);
        }
    };

    componentWillReceiveProps(newProps) {
        const { user } = this.props;

        if (user !== newProps.user) {
            this.setState({ loading: false });
        }

        if (newProps.user.success) {
            if (newProps.user.data.default_address !== null) {
                const userSetAddress = {
                    lat: newProps.user.data.default_address.latitude,
                    lng: newProps.user.data.default_address.longitude,
                    address: newProps.user.data.default_address.address,
                    house: newProps.user.data.default_address.house,
                    tag: newProps.user.data.default_address.tag
                };
                localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
            }
            // this.context.router.history.goBack();
            //after registration set store owner role instead of going back to last page
            this.setStoreOwnerRole(newProps.user.data.id, newProps.user.data.auth_token);
        }

        if (newProps.user.email_phone_already_used) {
            this.setState({ email_phone_already_used: true });
        }
        if (newProps.user.otp) {
            this.setState({ email_phone_already_used: false, error: false });
            //otp sent, hide reg form and show otp form
            document.getElementById("registerForm").classList.add("hidden");
            document.getElementById("socialLoginDiv").classList.add("hidden");
            document.getElementById("phoneFormAfterSocialLogin").classList.add("hidden");
            document.getElementById("otpForm").classList.remove("hidden");

            //start countdown
            this.setState({ countdownStart: true });
            this.handleCountDown();
            this.validator.hideMessages();
        }

        if (newProps.user.valid_otp) {
            this.setState({ invalid_otp: false, error: false, loading: true });
            // register user
            if (this.state.social_login) {
                this.props.loginUser(
                    this.state.name,
                    this.state.email,
                    null,
                    this.state.accessToken,
                    this.state.phone,
                    this.state.provider,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            } else {
                this.props.registerUser(
                    this.state.name,
                    this.state.email,
                    this.state.phone,
                    this.state.password,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }

            console.log("VALID OTP, REG USER NOW");
            this.setState({ loading: false });
        }

        if (newProps.user.valid_otp === false) {
            console.log("Invalid OTP");
            this.setState({ invalid_otp: true });
        }

        if (!newProps.user) {
            this.setState({ error: true });
        }

        //old user, proceed to login after social login
        if (newProps.user.proceed_login) {
            console.log("From Social : user already exists");
            this.props.loginUser(
                this.state.name,
                this.state.email,
                null,
                this.state.accessToken,
                null,
                this.state.provider,
                JSON.parse(localStorage.getItem("userSetAddress"))
            );
        }

        if (newProps.user.enter_phone_after_social_login) {
            this.validator.hideMessages();
            document.getElementById("registerForm").classList.add("hidden");
            document.getElementById("socialLoginDiv").classList.add("hidden");
            document.getElementById("phoneFormAfterSocialLogin").classList.remove("hidden");
            // populate name & email
            console.log("ask to fill the phone number and send otp process...");
        }
    }

    handleSocialLogin = user => {
        //if otp verification is enabled
        if (localStorage.getItem("enSOV") === "true") {
            //save user data in state
            this.setState({
                name: user._profile.name,
                email: user._profile.email,
                accessToken: user._token.accessToken,
                provider: user._provider,
                social_login: true
            });
            //request for OTP, send accessToken, if email exists in db, user will login
            this.props.sendOtp(user._profile.email, null, user._token.accessToken, user._provider);
        } else {
            //call to new api to check if phone number present

            //if record phone number present, then login,

            //else show enter phone number
            this.setState({
                name: user._profile.name,
                email: user._profile.email,
                accessToken: user._token.accessToken,
                provider: user._provider,
                social_login: true
            });
            this.props.loginUser(
                user._profile.name,
                user._profile.email,
                null,
                user._token.accessToken,
                null,
                user._provider,
                JSON.parse(localStorage.getItem("userSetAddress"))
            );
        }
    };

    handleSocialLoginFailure = err => {
        this.setState({ error: true });
    };

    handleCountDown = () => {
        setTimeout(() => {
            this.setState({ showResendOtp: true });
            clearInterval(this.intervalID);
        }, 15000 + 1000);
        this.intervalID = setInterval(() => {
            console.log("interval going on");
            this.setState({ countDownSeconds: this.state.countDownSeconds - 1 });
        }, 1000);
    };

    componentWillUnmount() {
        //clear countdown
        console.log("Countdown cleared");
        clearInterval(this.intervalID);
    }

    //footer
    showModalAGB () {
        this.setState({
            AGBModal: true
        });
    }
    hideModalAGB = () => {
        this.setState({
            AGBModal: false
        });
    }

    //install app
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    //work model
    showModalSVS () {
        this.setState({
            SVSModal: true
        });
    }
    hideModalSVS = () => {
        this.setState({
            SVSModal: false
        });
    }
    showModalASVG () {
        this.setState({
            ASVGModal: true
        });
    }
    hideModalASVG = () => {
        this.setState({
            ASVGModal: false
        });
    }

    // When the user clicks on the button, scroll to the top of the document
    topFunction = () => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    //add store owner role after registration
    setStoreOwnerRole = (user_id, auth_token) => {
        axios
        .get('//'+window.location.hostname+'/php/shopperPwa_isStoreOwner.php?user_id='+user_id+'&auth_token='+btoa(auth_token))
        .then(response => {
            console.log("setStoreOwnerRole", response.data);
            window.location = "https://store.lieferservice123.com";
        })
        .catch(function(error) {
            console.log(error);
        });
    } 

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }

        const { user, delivery_user, history } = this.props;
        if(delivery_user.success || window.location.href.indexOf("/store?redirect=1") >= 0) {
            return (
                window.location = "https://store.lieferservice123.com"
            );
        }

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        return (
            <React.Fragment>

                {/* PreLoading the loading gif */}
                <img src="/assets/img/loading-food.gif" className="hidden" alt="prefetching" />

                {this.state.error && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("registerErrorMessage")}</div>
                    </div>
                )}
                {this.state.email_phone_already_used && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("emailPhoneAlreadyRegistered")}</div>
                    </div>
                )}
                {this.state.invalid_otp && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("invalidOtpMsg")}</div>
                    </div>
                )}
                {this.state.loading && (
                    <div className="height-100 overlay-loading">
                        <div>
                            <img src="/assets/img/loading-food.gif" alt={localStorage.getItem("pleaseWaitText")} />
                        </div>
                    </div>
                )}

                <div style={{ backgroundColor: "#f2f4f9" }}>
                    <div style={{ backgroundColor: "#f2f4f9" }}>
                        {/* {window.innerWidth < 768 ? (
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <BackButton history={this.props.history} />
                                </div>
                            </div>
                        ) : (
                            <Nav redirectUrl="shopper" logo={true} active_nearme={true} disable_back_button={true} history={history} loggedin={user.success} />
                        )} */}
                        <Nav redirectUrl="store" logo={true} active_nearme={true} disable_back_button={true} history={history} loggedin={user.success} />
                    </div>
                </div>

                <div className="bg-white">

                    <div style={{background:"linear-gradient(318.68deg, rgb(15, 73, 92) 0%, rgb(13, 54, 64) 49.72%, rgb(8, 20, 31) 100%)"}}>

                        <div  style={{maxWidth:"400px", margin:"auto", padding:"30px 18px 20px"}}>

                            <div style={{fontSize:"34px", fontFamily:"open sans", fontWeight:600, color:"#fff"}}>Lassen Sie uns zusammenarbeiten
</div>

                            <form onSubmit={this.handleRegister} id="registerForm">
                                <div className="form-group pt-30">

                                    {/* name */}
                                    {this.validator.message("name", this.state.name, "required|string")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input placeholder={localStorage.getItem("loginLoginNameLabel")} type="text" name="name" onChange={this.handleInputChange} style={{padding: "5px", width:"100%"}} />
                                    </div>

                                    {/* email */}
                                    {this.validator.message("email", this.state.email, "required|email")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input placeholder={localStorage.getItem("loginLoginEmailLabel")} type="text" name="email" onChange={this.handleInputChange} style={{padding: "5px", width:"100%"}} />
                                    </div>

                                    {/* phone */}
                                    {this.validator.message("phone", this.state.phone, ["required", { regex: ["^\\+[1-9]\\d{1,14}$"] }, { min: ["8"] }])}
                                    <div style={{marginBottom:"10px"}}>
                                        <input
                                            placeholder={localStorage.getItem("loginLoginPhoneLabel")+" "+(localStorage.getItem("phoneCountryCode") === null ? "" : "("+localStorage.getItem("phoneCountryCode")+")")}
                                            // defaultValue={localStorage.getItem("phoneCountryCode") === null ? "" : localStorage.getItem("phoneCountryCode")}
                                            name="phone"
                                            type="tel"
                                            onChange={this.handleInputChange}
                                            style={{padding: "5px", width:"100%"}}
                                        />
                                    </div>

                                    {/* password */}
                                    {this.validator.message("password", this.state.password, "required|min:8")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input
                                            placeholder={localStorage.getItem("loginLoginPasswordLabel")}
                                            type="password"
                                            name="password"
                                            onChange={this.handleInputChange}
                                            style={{padding: "5px", width:"100%"}}
                                        />
                                    </div>

                                </div>

                                <div className="mt-20 pt-15 button-block">
                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("registerRegisterTitle")}
                                    </button>
                                </div>
                                
                            </form>

                            <form onSubmit={this.handleVerifyOtp} id="otpForm" className="hidden">
                                <div className="form-group px-15 pt-30">
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("otpSentMsg")} {this.state.phone}
                                        {this.validator.message("otp", this.state.otp, "required|numeric|min:4|max:5")}
                                    </label>
                                    <div className="col-md-9">
                                        <input name="otp" type="tel" onChange={this.handleInputChange} className="form-control edit-address-input" required />
                                    </div>

                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("verifyOtpBtnText")}
                                    </button>

                                    <div className="mt-30 mb-10">
                                        {this.state.showResendOtp && (
                                            <div className="resend-otp" onClick={this.resendOtp}>
                                                {localStorage.getItem("resendOtpMsg")} {this.state.phone}
                                            </div>
                                        )}

                                        {this.state.countDownSeconds > 0 && (
                                            <div className="resend-otp countdown">
                                                {localStorage.getItem("resendOtpCountdownMsg")} {this.state.countDownSeconds}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </form>

                            {/* <form onSubmit={this.handleRegisterAfterSocialLogin} id="phoneFormAfterSocialLogin" className="hidden">
                                <div className="form-group px-15 pt-30">
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("socialWelcomeText")} {this.state.name},
                                    </label>
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("enterPhoneToVerify")}{" "}
                                        {this.validator.message("phone", this.state.phone, ["required", { regex: ["^\\+[1-9]\\d{1,14}$"] }, { min: ["8"] }])}
                                    </label>
                                    <div className="col-md-9 pb-5">
                                        <input
                                            defaultValue={localStorage.getItem("phoneCountryCode") === null ? "" : localStorage.getItem("phoneCountryCode")}
                                            name="phone"
                                            type="tel"
                                            onChange={this.handleInputChange}
                                            className="form-control edit-address-input"
                                        />
                                    </div>
                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("registerRegisterTitle")}
                                    </button>
                                </div>
                            </form>

                            <div className="text-center mt-3 mb-20" id="socialLoginDiv">
                                <p className="login-or mt-2">OR</p>
                                <div ref="socialLoginLoader">
                                    <ContentLoader height={60} width={400} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                                        <rect x="28" y="0" rx="0" ry="0" width="165" height="45" />
                                        <rect x="210" y="0" rx="0" ry="0" width="165" height="45" />
                                    </ContentLoader>
                                </div>
                                <div ref="socialLogin" className="hidden">
                                    {localStorage.getItem("enableFacebookLogin") === "true" && (
                                        <SocialButton
                                            provider="facebook"
                                            appId={localStorage.getItem("facebookAppId")}
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={() => console.log("Failed didn't get time to init or login failed")}
                                            className="facebook-login-button mr-2"
                                        >
                                            {localStorage.getItem("facebookLoginButtonText")}
                                        </SocialButton>
                                    )}
                                    {localStorage.getItem("enableGoogleLogin") === "true" && (
                                        <SocialButton
                                            provider="google"
                                            appId={localStorage.getItem("googleAppId")}
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={() => console.log("Failed didn't get time to init or login failed")}
                                            className="google-login-button"
                                        >
                                            {localStorage.getItem("googleLoginButtonText")}
                                        </SocialButton>
                                    )}
                                </div>
                            </div> */}

                            <div id="riderFormular">

                                <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                    <p style={{marginTop:"5px", fontSize:"11px", fontFamily:"open sans", fontWeight:400}}>
                                    Sie müssen mindestens 18 Jahre alt sein, um sich anzumelden. Mit Ihrer Anmeldung stimmen Sie den <span style={{textDecoration: "underline", cursor: "pointer"}} onClick={() => { this.showModalAGB(); }}>AGB</span> von Lieferservice123 zu und bestätigen, dass Sie die Datenschutzbestimmungen gelesen haben. Durch die Angabe Ihrer Telefonnummer stimmen Sie dem Empfang von Textnachrichten von Lieferservice123 zu.
                                    </p>
                                </div>

                                <div style={{marginTop:"20px", fontSize:"12px", color:"#fff"}}>
                                    <p style={{marginTop:"5px"}}>
                                        {/* Sie haben bereits ein Konto? <a href={'/shopperPwa?act=dashboard'} style={{textDecoration:"underline"}}>Jetzt anmelden</a> */}
                                        Sie haben bereits ein Konto? <a href={'https://store.lieferservice123.com'} style={{textDecoration:"underline"}}>Jetzt anmelden</a>
                                    </p>
                                </div>

                                {/* <div style={{padding: "20px 16px 16px", fontSize: "15px", maxWidth:"1020px", margin:"auto"}}>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Online Trinkgeld erhalten<br/>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Sofortauszahlung nach Auftrag<br/>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Shoppe via Prepaid Card<br/>
                                </div> */}

                            </div>  

                        </div>

                    </div>  

                    {/* add to homescreen */}
                    {matchMedia('(display-mode: standalone)').matches == false ? <div id="download_app" style={{width:"100%", float:"left", textAlign:"center", padding: "60px 18px 50px"}}>
                        <div style={{maxWidth:"768px", margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                            <div style={{fontSize: "24px", fontWeight: 400, fontFamily: "open sans", marginBottom: "15px"}}>{localStorage.getItem("firstScreenDownloadApp")}</div>

                            <div>
                                {/* {localStorage.getItem("firstScreenDownloadAppSub")}  */}
                                <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppWin")}</div>
                            </div>
    
                            <div style={{marginBottom:"5px"}}><a onClick={() => { this.showModalBerechtigungen(); }} style={{cursor:"pointer"}}><img width="156px" alt="Google Play" src="//lieferservice123.com/assets/img/playstore.png" /></a></div>
                        
                            <div><a onClick={() => { this.showPrompt() }} style={{cursor:"pointer"}}><img width="156px" alt="App Store" src="//lieferservice123.com/assets/img/appstore.png" /></a></div>

                            <div id="showPwaprompt" style={{display:"none"}}>
                                <PWAPrompt debug={1} 
                                copyTitle={"Add to Home Screen"} 
                                copyBody={"This website has app functionality. Add it to your home screen to use it in fullscreen and while offline."}
                                copyShareButtonLabel={"1) Press the 'Share' button"}
                                copyAddHomeButtonLabel={"2) Press 'Add to Home Screen'"}
                                copyClosePrompt={"Cancel"} 
                                />
                            </div>
                        </div>
                    </div>
                    : null}

                    {/* advantages */}
                    <div style={{background:"linear-gradient(318.68deg, rgb(15, 73, 92) 0%, rgb(13, 54, 64) 49.72%, rgb(8, 20, 31) 100%)", overflow:"auto", padding: "60px 18px 16px", textAlign:"center", lineHeight: "24px"}}>
                        
                        <div style={{fontSize: "15px", maxWidth:"1184px", margin:"auto"}}>

                            <div className="advantagesShopper">
                                <div>Wir übernehmen die Lieferung</div>
                                <p>Wir nutzen unser starkes Shopper-Netzwerk, um Ihre Lieferaufträge innerhalb von durchschnittlich 60 Minuten zu erfüllen.</p>
                            </div>

                            <div className="advantagesShopper">
                                <div>Neue Kunden erreichen</div>
                                <p>Wir bieten Ihr Produkt auf unserer App und Website an, damit Kunden Ihr Geschäft entdecken und Produkte bestellen können. All dies geschieht, ohne dass Sie einen Finger rühren müssen!</p>
                            </div>

                            <div className="advantagesShopper">
                                <div>Gratis App für dein Geschäft</div>
                                <p>Heutzutage wollen immer mehr Menschen die Bequemlichkeit der Apps. Wir bitten eine kostenlose App mit deinem Geschäftsnamen und Logo.</p>
                            </div>

                        </div>
                    </div>

                    {/* tel verification */}
                    <Modal show={this.state.VerifyRegistrationModal} onHide={this.hideModalVerifyRegistration} size="sm">
                        <Modal.Header closeButton>
                            <Modal.Title>Bestätigen</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>

                            <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Aktion</div>
                            <div style={{marginBottom:"15px"}}>Telefonnummer verifizieren</div>

                            {/* <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Restaurant</div>
                            <div style={{marginBottom:"15px"}}>{this.props.restaurant_info.name}<br/>{this.props.restaurant_info.address}</div> */}

                            <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300", marginBottom:"15px"}}>Klicke auf <span onClick={() => this.sendCode(this.state.fromTel, "", "", true)} style={{color:"#0872BA", cursor:"pointer"}}>Code senden</span> um den Bestätigungscode erneut an {this.state.fromTel} zu senden.</div>

                            <input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", marginBottom:"5px", border:"1px solid #ccc"}} ref={this.innerRef} />

                            <div id="result"></div>

                            <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>
                                Bitte überprüfen Sie die SMS-Daten und geben Sie mit dem Code Ihr Go.
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalVerifyRegistration}> 
                                Schließen
                            </Button> 
                            <Button id="confirm" variant="primary" onClick={() => this.acceptRegistration(this.state.fromName, this.state.fromTel, this.state.fromAddress, this.state.verificationCode, this.state.total, "", "", this.state.trinkgeld, this.state.fromLat, this.state.fromLng)}> 
                                Bestätigen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* agb */}
                    <Modal show={this.state.AGBModal} onHide={this.hideModalAGB} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>AGB</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <iframe src={'//'+window.location.hostname+'/php/footer/agb.php'} style={{width:"100%", height:(window.innerHeight / 1.5)}} frameBorder="0" />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalAGB}> 
                                Schließen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* work as */}
                    <div style={{margin: "60px 16px 16px", float:"left", width:"100%", fontSize:"30px", fontFamily:"open sans", fontWeight:400, textAlign:"center"}}>Vorteile Vergleichen</div>

                    <div style={{margin:"18px 18px 40px", float:"left", width:"100%"}}><div style={{maxWidth:"700px", margin:"auto"}}>

                        {/* store advantages */}
                        <div className="workModel">
                            <div style={{fontWeight:"bold", fontSize:"20px"}}>Vorteile für dein Geschäft</div>

                            <div style={{maxWidth:"300px", margin:"auto"}}>
                                <li>Keine Provision</li>
                                <li>Mehr Umsatz</li>
                                <li>Sie inserieren, wir liefern</li>
                                <li>Gratis Store App</li>
                                <li>Sofort Auszahlung von Einnahmen</li>
                                <li>Mehr Kunden</li>
                                <li>Gutscheine u. Rabatte erstellen</li>
                            </div>
                        </div>

                        {/* customer advantages */}
                        <div className="workModel">
                            <div style={{fontWeight:"bold", fontSize:"20px"}}>Vorteile für deine Kunden</div>

                            <div style={{maxWidth:"300px", margin:"auto"}}>
                                <li>Online Zahlung</li>
                                <li>Lieferung innerhalb 60 Min.</li>
                                <li>App gratis herunterladen</li>
                                <li>Ersatzprodukte erhalten</li>
                                <li>Zeitaufträge einstellen</li>
                                <li>Lieferung auf der Karte verfolgen</li>
                                <li>Bestellen ohne Anmeldung</li>
                            </div>
                        </div>

                    </div></div>

                    {/* become partner */}
                    <div className="shopper2" style={{padding:"20px 0"}} onClick={() => this.topFunction()}>
                        <div style={{color:"#fff", fontFamily:"open sans", fontWeight:"bold"}}>Werden Sie jetzt unser Partner - es ist ganz einfach!</div>
                        <div style={{margin:"20px 0 15px"}}><span style={{border:"2px solid #fff", borderRadius:"4px", padding:"10px 20px"}}>Jetzt anmelden<Ink duration="500" /></span></div>
                    </div>

                    {/* app-vorteile */}
                    <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Store App</Modal.Title>
                        </Modal.Header>

                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <div style={{marginTop:"12px"}}>
                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Store App auf Desktop installieren.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Geschäftsinformationen in Echzeit aktualisieren.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Gutscheine und Rabatte erstellen.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Sofort Auszahlung von Einnahmen.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                            </div>

                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                                Schließen
                            </Button> 
                            <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                                Weiter
                            </Button>
                        </Modal.Footer>
                    </Modal>

                    {/* app-installieren */}
                    <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Installieren</Modal.Title>
                        </Modal.Header>

                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <div style={{marginTop:"12px"}}>
                                Klicke auf hinzufügen um die Store App auf deinen Bildschirm zu installieren.
                            </div>

                            <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die App.</div>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalInstall}> 
                                Schließen
                            </Button> 
                            <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                                Hinzufügen
                            </Button>
                        </Modal.Footer>
                    </Modal>

                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { registerUser, loginUser, sendOtp, verifyOtp }
)(Register);
